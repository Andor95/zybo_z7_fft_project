library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity test_design_1 is 
end test_design_1;


architecture TB of test_design_1 is

component design_1 is
port (
  S : out STD_LOGIC_VECTOR (39 downto 0)
);
end component design_1;

signal S : STD_LOGIC_VECTOR (39 downto 0);
begin

DUT: component design_1 port map (
  S => S
);


end TB;

