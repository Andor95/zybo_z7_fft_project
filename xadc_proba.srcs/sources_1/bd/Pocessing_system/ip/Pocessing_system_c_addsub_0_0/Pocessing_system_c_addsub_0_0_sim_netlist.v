// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Fri Feb 14 00:11:18 2020
// Host        : DESKTOP-8BPORRM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               D:/DOKUK/Mesteri/FPGA/xadc_proba/xadc_proba.srcs/sources_1/bd/Pocessing_system/ip/Pocessing_system_c_addsub_0_0/Pocessing_system_c_addsub_0_0_sim_netlist.v
// Design      : Pocessing_system_c_addsub_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "Pocessing_system_c_addsub_0_0,c_addsub_v12_0_12,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_12,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module Pocessing_system_c_addsub_0_0
   (A,
    B,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [31:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Pocessing_system_processing_system7_0_0_FCLK_CLK0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 32}" *) output [31:0]S;

  wire [31:0]A;
  wire [31:0]B;
  wire CLK;
  wire [31:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000000" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "32" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  Pocessing_system_c_addsub_0_0_c_addsub_v12_0_12 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000000" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "32" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) (* ORIG_REF_NAME = "c_addsub_v12_0_12" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module Pocessing_system_c_addsub_0_0_c_addsub_v12_0_12
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [31:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]B;
  wire CLK;
  wire [31:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000000" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "32" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  Pocessing_system_c_addsub_0_0_c_addsub_v12_0_12_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
RhedCTftnR/lFLJouqVu00X8CC4TkDlMiW/WeWJSYDyQHVO1xW1z9+hmgAziXI4s3uGElBs9cXRS
4yVMV7QH0w==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
qX90FYlZfOA3sZpcv0rrvWRKCSlr3skWpeAe5OSxHcZPsVQFyY0hhWVDtP/vB+dV9hIUwAN29Fn9
+L9OEXYMlIw5gH6s9NmquAs3lmPRLTrrpKJWdvf6+b+LeG9CPwLz87bXAk4qQW80zUHBaDKDLV3q
pd/gEf8Y3st+mLOGjNU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
chpH3Rj5RAirYZHkpJvTu4EJpydPPSy15v646y2lN/1w3bwHI+M8EpLMBjimx8uIWRJZ6dDWPR8E
zkwK1TMroEKFaL8IkFMSHPyzqbrH9l1jxYFs0ee8Itp8Rg8qenv5RXM+h4JRTtRmzHk1A8s8zeKY
MgXzIBCAzBa/vSgzDuDaUnO8r8f/5KtRjmE28JLNXXAxyijBrMTCiIHMRJZL5/+LgUyVq7/Zr5yx
7kuNGWv7Q0wESEqhsQbK6UNel5ak1cor7647dYJgIxnNZ6jGVJPkqi8ydAIZ0z0Dy4txBvVaMyP6
2kYYnbVN6S6q6yr/QcJHEOgefF59B/8JuWzdoQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z2XauOF3/9FUIv1kEFfEtdy93+zHY5q9dH1pJCNLytoWWhhJBfCI5Uc2w/fQLrvVw2Ivy0/rs9qH
9fomTNECWeCphNDVpWGNcFDGE51jt6SDWt7FmgfZq4iXN7XWrfmkQa4DB7QbtSBHCMcBT53TKbDH
suKNhAWMV0htWeNEgN4Y0biiz8U4RLT1stdsMMtEzfYVhtrTmFWLihJ/9gJ01pm/kv4OB2cJEslg
sjbxCE2B4Y1uSj93tnBAw4/f2RYGfPcSrkaXkNgOYK2dc5NQgd82XvP8siAK/ETcZQ1lBK367Rxv
nlr6QUMwKALmjLVe/IThpCRGbOSy3QykkwnpHw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
F/mF2RV90mf5PFUZZUjMej6jQS+Qx16uCeiDQxUX+H+O2yjP6UECegDbtLmGk9algbDuGBCE0KgZ
HzSxX5pMwDq1J7nFyBsu1dGyu8NeJxfu0fFA/qS/SYJSTtwnZ/eIj09mNLJ3w8wGR87ke1ETTRpx
4Ni9DzGJ/aaWFaUenL/x4UWS9yqlaNi5Utcpa4kcUHC6fW0asZsThZJBqpArO54TF40nxZAD+V82
6mBGFOKUzgmHqXNsbVif4JqUd63LG8YWxjrOeesq61xzyjw9caQMuA9v/5sQslCFaeSt2atgqyaA
y4mcm0kU60s6mYqJr4KZt8DWG7LPGoIjhjpnMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
YNrNxIZl8p2OCPz+4Y4awQ09ZZ41X8EdKu2SX1gXfB3qoV3EOXf4eFHtYJ+bFIccfEIqPfZ+dnr7
udcSDAJMcxqZe+YNk5hTq+uX4PlnQH/IVlkgyYiDhQ7aRIS5pwtIRC3biXn80933ov4zlWLI+ZBq
bG8lodZjxKh6HZPWi7s=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eWToRZf4bzAQhHTj44yEkOqolJ3BOvlBPKnKoDCpSvCHSrnRcJKgOd47PnboABTqLVstQenz8AFe
rWHBbaad7KOIh3LsEXBwDHKDdby7iDAB5nd3j2Wp7qiHOv7WpIJ5RG1GMQa8+QXHMVaV4jSvsdmt
d0D9H0WEVaqrSFW2ucpsMYNE0LnKcSJ1SejKOcgISzaGJgXcmEAOVAOnCDPc4slwc3dt7Jne1KvY
i66An36Mfhhd1b0RikMf4yqpM+uVrL5XWIP+TBtw3iQRE0ZkUSn3K0My73/2vKKBfwyV0c+M/Il+
aMxNaD44Gq+/f6zGjkelgMEI4BjB5rs1KRvSCg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ZqKe0Pr2A0f5ii4gQYFI0/zgcVeqd73UEzqehF3WabD1l12yPApKD57LZimGX9I/qq74i59bGAj0
l9ppYzKxkokeNpUOhiZuxZgz7H5dheMzN0KNDiIhiOu7yX2EOPN2bZPqjD+h/YBrRjK/B3YmCEHC
UpFOj+GdSoAnDeBymWOsxluRSAyF9ij5UNIe9jJzHOtMJCtAMFTVthP6PjE5sgZkqX/y4+eerUCW
B3IfKucjqsPGOnaa/l3p4/3w5/rD/iGjNZo+IojcorBkM04mIEF2TlWmIoXXhAEn3rBfTK9NR4qv
uuGEt32cwAhp45xxS9gX2SiyxT31GgXkD3x9pQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
J7x8HUcARa3ePsnvOf0pxxLhmM5Z5ZbCIocqVB3L3plWdFNkxyIAgsMOai6wNgk1pqzT6Isf7gvx
eGt4W73rD2z1ukx6iN0+mizheqDEfadxnagPf7PMFUjwnj530RxSGBvl5AHFK+kTempeyRNIz58q
JNxeY+AqxWCJ3X7bNNucfeuxMag1SxUsJPIfC4DZJYNC5KjQQodOd7/nZg4ZzwjRx5HaH2V0N6ev
uy5eKPvpd3xFw/uqUwXB83ghZTsBQ83kBIl8xii9p3c1S8/P9d9WCa5H/aHeIVN6WyuN4P8inIR5
ST0rkVnj4cMa9hGlrnR6kyCvfDiO4V+S4RGhIQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 23136)
`pragma protect data_block
dpRxO5azf8wzq4TLfwUJgJp4PkzXkMWHrLapBQ1zC/ww27p8JuN9JxQKDTeer3PSszUhWhgApWBs
TVDkdd7dLuUyCD9mDu2D3BjqpHrx4Y0gCmM6aupgTLcutJl+y7qJXaUAs4Gqk9vbrE3/K3GFCr4O
fsBb1/IRkpDzHWjPZDfMcSixHk45xCHDd140I8N+b3zg0u6sNscZEO7cx11ihVQyxfUuhFi2MiPq
77nx1ABIpAMiYHY8vwzZyIwWQQHRmW9Bt9lKcL6PzVUcd4Tg/qEBXZBf/wImizL6z2yF7SHI3ZZ9
BjV5YzO+H95jfss+xaEuZd2+1n3sruTp0b5Z6l1Igi4nPm9/h4aTRLCQf9/R9b2xIuCSE1mos4Bv
ETsPyNcwN8rrj5DKut/wpSKKYsH5G489IX7v49HKJE0YfKexy1BovoXnSvcFMxK7KrgIQfeXoNjk
lYg9qI70ouADhcgpFXhPWWXUTgXCMzxWvNVry4YfOMFGiobXsDhgy/n+eQ/KcO6sdeZdV8tKEniO
vFsQ71pg1VFXv9CEhLWX6S7zKvoFoK4jcwaymosV4MTBoEVktwrMfuQRTIU7M7Xnt4T/qJPXnl9E
FjJke3lpxbWP9qDyhnc/mtakhBTNno+zuVd1zV1pFbFdmxKbn8oMCrkMlGZeST7do9v4ta0B3CTo
80GnjQ2MDMiPZDAn6t0Lz8zw3DKC6DZ4peMzEF69yLL9kU0wlDdR/eGk4dFHx1o138IuwhXOiUkE
59V4OHgBtyh6oGL6PqtAi9+1KibGtfjTBBqANKGAs2zQWD0lk+qNbExyDtQ/enpkFhpLQExpWGFx
8IzTk87+eLYHz2+WGi7GRmtwRehTv0yeztqZn8fmjpKdbhz+m04Lj8Ehp/WqSuQYX2MT4otgSia/
zq22Bz3L0GwaQjwTGicjbYWyhBAY06glP8sR02tCwmZp6SbUNUe75p+Z0oY+KaqqOrNzPA9kky9L
Tjda3xhzWq+5LgWfUjz/Y7DYJnmyPa1hXOrvhoWsb7OqELcDrPOfwa/CA8T8zcvVQj0PdcVTZ4Ae
la9kXf6DbxiuqT9LmxWqlpYlKYIdn3l8awzzXrqLDfwBKY4lXEMZq4WRae0YXybjXeca6AmDV+4E
2aIcLSQrgWcgq0U9Oe/6l72PrUTuLYVnCxEWOmdQFO1/l404447SWu7ddb2VCEoBadOQ36noJlSD
FQT6MK8RD7xWkvKW8QtLUiVWdGGvCt84K+GLhOmve5x0U3BfqpcGwcNEaolSALpjFCsjquP1V+si
K1MEGhgpbGLlbkkva9VtbCKJPBGxcp9YrUVKYHxOm+TSbBDECFGU5+j18PpyOPZ6FyT7D9DvCpR5
SdsiEQBHLi0t15PNkVsKEk0IxgSPWdz/eatiMmidjBoMl/rnC1Wllef591ZrtODclK33YkwL35l2
SS3hgOmvBVmh+bxva4QVoAyD2/Os+qbdztAhh4oDC1j9nQnfTB5MrsW3+A3XrZIy3tKuwAiJOmBG
2HhQjN8/m5vFPFnAvQY8qSc6hpE0zKjvmtqfDVnhNTkbZgkfgvCu6PUCYYGRqpEsHGFHkPiRw0tX
a0QbgxoX3p60b0GpkMPC9kzG8oX8NskGWKz6sWDZtLk0y+uHMdcmsv0sdZgb4cLOLdM/DhZxuYTe
JZJJjojdj2PbcpmMBYHNc7wBbQq5g4du5kvTzBJBEdkGqSWjsu4ApPKr6fM6nMq27grr20i2SEWV
wa28R+XPxv+wN54pUZVWbiSYKNz86lupH905+axhWHFoCr0wUhoJ5Fwyjxh6WBVcZzuvhcImHhb1
tSzSrjGxt63bVkdRaADJY9vy3EV28xhurkoMFvxMnl6+2qss5AFu49DLUwTmdpLusmhsHGVz9wr8
GCIyn18IcEEf/YTFIPgYPDi+D1oBZD2iKGQu8UHWzoV2pSsxjeD2CATNSxpK/8qDkw60KzBWayJZ
11UMmJRVO/Y7mjIH8PFJQS8n56HZMXm+rkQyApTGqalKmSJ6p8uFZqb6HCrumQaSfmfGRSnJEOwV
zFMpAzEvL4qNYOE4FKOf8Zc+Bq/qVUAUGrPp7YPU+cFMGVdgxgGy4fC3ZFDCIEslwi6hmS172Khp
ophVcY3VFdjA2t1gsiei4eWH95wXe3JqkITgLb8mjWSrQCr8At587VCRoVyDc9gfvfiCir0zD+7X
PveXJotIPeUmBJO/69B1H7J51y4ir4Quk6JTUslCjfIE2WFqjbGWr65gtmxoiCLlipdc9ALRJYpd
e/9mE4ncY4Br0/eTKdKr7P4mi9vsE8bEPfgkxTdU2ISPQ64ppTz1CFcYGl9zt4+LDc3J9R6wXhWw
HdbgTvS3LA9rHUT3bIExs6bJvgRm8sKuP8i4Ic1QhgkxdMo+SvyrlJGbL5eB9SX8cSWexUHCYdQJ
y2PXzCtnr5F2XSTXdMzuQeNEiFCLzH7clWU9j/nJuU52u0eyq0QWdkpWa6SOlbwSTh7Bvp7gVxlG
wEdE1aBJSMc4cQkVM992I9zYzBuzgfqnTnZV9aop9x1vZ0ocSpyyHkxSFw3IoORwleJ81a/XZLg0
1nRk9QPvN5ALjHVgiphKM/Z4W22+DBNZA+4J1UURXqCbh4Htftss9kpUObeWjbTopP4lQyAncoWw
GzmOpI3Ur5nD7O+zxfdiP0xT51sDomPH6t0A97eRoumbrkHdbio3S4v5E5szysKSKjp8iKcKXbcR
pvsQmhZuI1S/Ddu7NiAANSzTFW+07X3uJPo5AFkNcvZQDCUuXDyKp9JiUXd74SF5d67tJD8h4OlV
KjUcPOdWAhBpDTl70vNEId8ie1u+TEtVoI3EuRP/tqKZQiIOA1WEXE67yblMAyAgHu/b7bhDeloq
U0DX3r8lmvuesuTAIBLY5Ozs8mEUraROD0xQdCCrRRz0wNcbe3PwnPLik4j3cWFW4vemif1/gBsP
+C7lCx4+ZiuPnD0BzPzsQdDS5O8vStWZO6SCBK27HhI61Wxpa4FKyWM5Z606cU3M1MwTXULByGVS
hgl7D+XdOPnQ17ikXKZi/8RLi89AkH8xEoWKMbTjWB1XxDQOV3gf6bDvO1IIYxccRctY7nGVTtyq
2OQ/e/+RFgjpDqEhFMgKfRrWMnBcSw0JSUKMiouRm6mCN6bgkW8uj8h4VFJXpEWPhtyoH/fuXKMn
A3KUZoiocM6DEv4a2OCqYaTUkenVVlIIYXOug4cBTxxdc+BeHtok6OSAy8TyyzoJ8A3bPrX2CCu8
xxEUA1V33+SAeffFfuv2viSno1wfjbh0q83yD9eHNi+lnbzw2ybRBHRPIdzsbAtlBuFFGQfuzPub
es1jksaqS8lmfV03L36ZyENZXFkXAbbYldXUG7UGc0XuW2oyGVDR+XOYK5++HMmnLul+NsGk6H/O
sdGlos5TfB1ML5GmEkOqjRn0at/cWY7ji22JXoml4rwaC5tIL26diADEW7TP4zhJnygme8O+9W1b
B2QivjmJRwkg+NbaCkEADuRZNEf92az6U4Vu9Zt1C0vjSCWkUQO/EqDKEIjok/fQF84IROGUH152
noMPrUOVnBR2dBa0xKyaLS/uMHKPe5H4ctXujfE4IgPYLZgNRiugTMipBkwiQmLBxV3my2McyO7h
n7RdpmDnuWp1SDHqusHwIMLzDh4GucbM9Ye9jcVE5S3qcadewD9kAJiD1xB+EYDDe8ujHbl0UJEm
TvB6d2ZG/eMTnVFXHPVkEbkqConVaxPDWFfYvDsgJvDZQbI+emymrlssfA9+q9UFp9TDaoI3JNfc
8XKaFBwMX+HfU474PqqVTLJb2scR3DPjlIr2FyGcCC0NFeQ+4udtBzZAVX1Y1vq6ZYhf1FlaNvOw
xsAYwtAHraYsZhYSn04Tc2V2GkFS8QeW/0dKZhb+kWG232dQ3WMEUtheUokqP9J8ZbJNniBXMqio
6+Io1jLyXN92pdgx2fmRYpXKX+NCxYzpVj54puCSv0fhyqVfSH203a+leU3Q9qwleUmZsmT2+ezb
jLiqsDxEKhN4Iom4jSSTAjw4ZRd1daEihy+w/XrjpS4LfdQ57j1gtgfyaxC99bzvWvYUf9xYr3mw
s/KVk+BxfkyTRVWA6iAmzgTQovpFCQEnbGDHKxhDzzbFhu92da1S+yaa/FNjfJ28+uF1oGuCIj7o
hpSUWF+MpuqlFlw0dZ0+TSY696wWspf9bi07NQFS2dbew6ZHqhs4pJbzatL3jeuzdY8zB7BHwrOb
Z3bpfIVtlFcW8zFOtcw0edU3v2dcR6Dbafpzh9TSFd/SRZEJZXLBJjd8aqjkJUsoF3pAtKjo8vT/
c9OKNywGc9Cfi/GBO6JyRtQ12WOF5yQrknDJ1XEX4DCqKpVlbBgRgVzd1pQTUC4PHW8Swn0nRtD1
Bvd7cyD7bPoF+IlUN40Jc7QUVOTwia+y6Mw4NbiB+VD0aVtFjZ5P+LNTZ9t24/LubJ+MWQU9ZWBL
breZZou+u5hKi9uKGUJteyCSipxXqyCwxp8CNSAvIz7J8dXEDDSieQmC55reKl/nmBZcuJYP3Q2d
RT07ICMb3fqyhxe/SBfB9PciRxRPH7rKngBoBVkKlxZm8YiDIrb0vhtmXvIb3cCn+BYMF9MMun3i
Iji7+sABK/kiuzl8D8GaXT3hBGa2BzA9H34rpqRs8eMCrWY3t6V2Gc/UkXxYKiTDqGGizac8ZH4v
hV6Y1AqZ7CEwN8Q/IhvCCb3xefoS5FKiCZJ5lp3OlOWHx0jZoKDALGwwR1As+DEnLnwm8AYw8x3e
RhY1xkzBEO8zdAwUT0x5PPchmWpegozwdNYN96GSmr29XzsfmBThpoKIZG8vwyKta78C6Pp5317z
6S6FrrKtW2P14KHTJcG4QHo82oPC9qHTc2UotovrN2ob72aPWK+pY4+Q9lO4eXUt0Yc0qKP9/hfP
/7OOPl85+nS/LKLBpz80WLOJrK9J9D0mL6TV2bFuhujhw0VPhL05L00SSO8UM3LCDZe6NbUqZzQ/
Kl2EP/wtQ1etEa6a4Ro5jDhg3BhQVuzpDP8/MNPwu8IA5KT0LBYXP4imN3ntCiZRRzji6M5AAQri
/6rJz6x02z6iQg0gUmXcf15sz8FtTsEojbiWqxYnn3XF5Zi6yxVaLXmD0rdKzdJQF2sxLR9s/009
4tNOkU8DyhMUN/EnxSGHs7eNkg/M/NWFBHqEkkZ29ioPfvruoBwDwGHgjGVK5BdFEjJNr2NDR5re
7uhMi6D/jJV/U2fJdAGV8+DqRIo+Ye/D/ZQqbQHtTUCKFHuIZ1KcD6KVQF6XQmxChObVQffQPEIB
CLkbypuGl1zJVK+V/me0Cxen3BcxP6E3T4wuLY3Oyi8zAPYTbqDbQ+OXNj9P8/aqrO65D7zN1JU/
7zNZj0FiiyGu5pScNfKk2em+eoEu7nPmWyukpo/Ahk5ItCz0Ng9PwBf5a7kd2TpsvZR+Uocil3A0
pCYSL+H05wzhphoMMF9AP4Rt6MEO9IguMWjtaWFCsMOlp8nhNXS/HPGmURQsE2BoR+d7FuXoH223
tmQWLs9F4tRih83Vf2HBobZ8/oyDUPjAKe3qYj0WZAMuWkTi8eblunZIcNmP0ELu62ckLo+pCdax
8MJZeCZk9INZckpwY+/rTf/441kMXmM3Pi4QHcdIbs1cose64exHbw7oBwrDfCgEQOu7FUOEzYIH
i1YVEGDGnV0xTBtGQcRSPxRMZpCUeNAKrH2R73A4XT+l5nmdwo8gMmiuRv3YEGQqEs1XfstfrWNC
LzHFM+R6itZ3B43UUAkMDVOFc6lCRme6HpyJSBkD0a8R0Ch0KgH7b4IkHNwP3mhCmcqhcxJoW4AC
Wnc8yLa9sj6zjWrV3sYAhqqjb91PT3l/O6ijueskaF0fx91ATY4TB0HZqOfKnArLsyXen8pYlEDA
EBZlmUq94B+v91vLImQwEPzRv3kzYFzBhA6ookr/FmrQ3gRDxJ9zuVOFmrBAZ2Ul3VlEwMsaE/nT
WUM33mia39xD1/PvywuVdy2TTUwSZMlYzd6oVb3GtMVlAxo18BR+w+DQS/HkELhyT2r9t8M68WHB
IxS6vBlo3KEswGzKefdJnbWGx8cAZJEfNQHRLT2FlsQ7TViCQ8cKW0b+s1wjk3/AYZzukkLNPCdf
SBvZxxsDm36P4Kw94l9pt8RTw1ZwAVajFLieWE8d1GEFnuVjmpaCnkZ7+rvtD6yOxZV2Se+ehXQt
pmcSpvsom4BinR+ERTQ3Vc3zsUExQzNlTiJKWPJGbRgLewqcMCF93fNJ0X3OarUUAfXAJxuNRnyQ
ADhityNSUChlBhHolJwY6Xw5MdF7V/v33Gdf46rWb+Nv8NjJZT+HhuDc1TR+A7k6c7nVGWE88uoW
pvMc70Kufs6k4+wxEsrpBHPtbrf7/RlxwYF0VI8COaLBOkZIvN4is9bnzSTOdhNSoyu9JaxzGX3i
mdjYSYI7EXTNWugMin8tQRdOhbgp/rAi0aJhaXleFk89fsHQ1ALcAS6tNpZPNeIUesmcgH5xI9G4
/Tfa15HjFs+SE9O2sjgWzOnc8SedMoFyqrrboU9GXYw+r3+xw/yNlTImN8xglcpHsP7mV8Oz8N1l
XFQS/5uqggesNwiWsEwQFzZnKRm9LLIqmVqEeu31iTOwIV11L1bIzwMLPSunY0fowekcoWwj6G2L
1TqQVQOjIEIEAP3ZQQySpSzdGmGPZ0kCATvYi6HI2bJECuNSbDfGHfjllKRys3jKv5InIm8/5N3k
6x8mXmPojqXjshEKdckpoEAsFVFeyjrjvQBsHpZm3kH6DttTr+fIqIZxPLUSLKOsmGaUZMoOnvnA
DVyJflY7Zz0AJe35Q+DN2QJAupc9XFASi0js/V8/PxVIzgWTPNbVTc/DvenRhDRH1nLVpuWYoKjn
3yIw0CS2lPkcsTDMAhbPDt3HJuWxRRfU5OlGFdqcqJTtB/NLNRiG2UKaMlheS8kybkIAfrqKMREo
vv5hwRBdk2JlkzkjRDsaWF21x92jBEQLFqvneZ1SfcMlh+YxXDw5QlB89xYw3l8jqna6imEe6laG
Q8NEbMA1L6a5NU7ZzesGtKPabzFt9K6CwK/wYjbFRkx/ltlAs1nokeQGeH3BT/yPyaZkQh+gYkJb
5fjr0YcV5oBcIc/WK02cXVm+9KyhOxJcztOOzy6Kf0916PrKH8grPUjlSHIhfJIpVYEFoD4/w0t+
O/HgYpX9CL1ZwO5AdgvART0gDnocLMgM8pYRQ6PIyz+4pWWxaWEWdy9odhtkh90lZdwoWYO6Wx/b
+u6RUiZqav2afXVwvyM+Qo7u1R9jBUsFYjd9pbzppEGfOAh4vXxQTmJ2DSp8eCNDSwXSQwf8xBLl
WsF7z6SeH385p/j+jtkvKdx5Kwq7sycRVBz3xn7GqwCRcIG99auBMsUcc6JnhVCf4EmZjIablAV7
Vtgs1qpuS7By7EcgqiwpvjRuV0BGjJabs0PdOFEJkbIOL7WZhb5majOEMtnugY5we4oViaqI9Aj4
z/VjUnps0DaL1V5wECM7G21haSA10nJyZiX6Zyl0msIZ6Kd92/IKlUWnrClbONntrf+ZkiLQf/Gn
Ltui07kYexhEs91kdVADaOcuFlF5db8lxJoslwms9EdxXTwTp7dNforFqy3hIOc0cTxbj6fjzfpb
dPwRTF0vCjK0jWGwt7YYMSPftqxu+bfaKAXn96DzETSW8qfsrisgSGAF/tPmbaWhJFFf4t5XUC3C
19qHOK+w30BN+QL0LHf6lsBLPn7KsB5z6HMlSQvQiHE0HTeFijEq51ofDQWmlxS2sba+X6nnOI+n
7G5ufIzZ/sgy+DJDtq5uBltBYT/2zuNsnDKA810FQHnZJLfBmqair3T9zebnI4whWHeNbccuQLdc
XUYwkeoOF6Lt6fohRO6Ogw7Web4BimjgD7RCqfSuSHnwudBla9dRLI7/3NyzRdFOkSMeg+mPNzDu
aLUELOvruBiEg6seYtpJrwk01unKHSYRDDVWQNkcCKGvpB7PptmJzB8TpRUXSNUH+48gRBrzW9j8
1T67C1jOfwtpYN5qyxUGNQJJ4hG/Wwd/LxlSBJCs2BiDmbMwjQOMOJQhwE8taqjiIfkXlKV+n2D5
qeusTMM0Mec8FTWmmXJ3KAClXrtYX1XZrszTrpWhgX8LTggvwTFX9qJkIzEtZNQUI00aTrH+LNrR
ZQmYC+FvemkEWgKOww7FJpjmjSbBceekDvCuiaP+6qFcT8kb2RV10EEBFDKtoDyaawuiz9z3tcJn
M4E8ND9eLuqoPUvQtqYHUa00W5kPc4w15028KIhCYGkG334RbqqndjDyXTlq6/4XqEH2/QWV7HkU
2RxFUzCcu8mq7tIXJuA288x9jJBA0PrG3hNlBuStmZvEq6CuX6aGnR3Nrl8hndyFmNR0dVHan8Xj
Jad9rraj6Tx9LSMaeKR+j0zJJ0Dd6tSasmz+2ChbF4oy7tCn51FG4v4iUNDtUPXGMjHilMdmcCps
iJpinpVSiwuxAUqdkqhNbBs6f3xgZgNfU5hO4a/qZyJvJhL7wgHvC5xQYCH935GDVMAUe+ypPnIF
e+aVVszEOJozQ+3xJlQEnTXUV/l6hrIvgozfgNIxTKiHDz0uJ5WCU7Oh8cYTnQ/I3u24w6+HuhGN
dKkU8ULMUKkQ9PqGRtMq5joA7a+kaqFEtCaQYLH7/70yIsFDQR0d8gIcdMfmmuTDhsVBIQUHFbB7
AAnmv3cqLtcVezIrjHgZGfWONlpQU/TTBZSChyiSobwxkmyi+66ee1M4mqKC6I8plI7LoUezTgY7
59d/1Etfase+TtcrzdmZpg1mgj5YDoXLKoZUdfFFHYiQ/jDcRGcQYhBTggEnOF1VITkm3Fz5M4QX
jwBg7Tq27gLA9b0AQwbNlZIuHL8P9GIyFrcnhj5eErx2BAMhUnyaJIqkieomn3E5NpDo36ddZBYV
pBCMS78nxff3UuRi38n/ucEL1auLGAMTmbLMozpNEWyeGSdHyekjTdGVNRYNEdGVvhZ1ILaUw1pF
ySkehTUHz8N9wSD0yUmogY1hsjfDAX+i96odEE/Um063J7c9Ef/4D5WCYu97xg9INxyQliFUHqd3
+LN3wg/9sq2RwTjfNjwyEyQ+tnaPIjY+365wYpTRh/Xv+yYuHekYida8LT4wUhmqVdmqVnGIfTli
JNHcfjK5wuv/NjwpewXqZFSVbnsVLpTTDBsoxGy7lej9YrbOeNMeTle4CgNrqEABZlOVyN8PEgoh
eiXKZJ7YKGnDwHPzy1+oI9pMPHdBCNSJYovFeVkggH2KQ0K3o0KL7ZFgqEB8SY1OjPOEAiS2o9ty
gKT5jMIVmtw8fhBC2RZGChzc2USiunP6/Ie+GsTymnL8qAwHGGYbBrf/Y8jaWdxgTNflGQrOQOWk
6Yhk323cUyuH8JqAqHaRQoB4YMNT/gDOyX+J1ir3/kqNu343lRUjtTSPXJR3riLRwtLLc1ZjOVk+
ZMMUU/XHe1MkoJ9RQBRNFs5Mq0T/eI/MdGKicIiY0nasVEKTT02dSauvfa6j68psoEfDm3p/769y
ev4ZvhkiVMxkBeg6WWVe1dOdiaUwscHbAeVFcsX0Iu6xID887Lu0hHnIlpbKgSzKEcoUKmjup4Er
RjL1RgefAm4sJqOYfwRr1k3srnkmuDzxl3Doefdtitv+b8j2GIO5wAa993WNQuJoStGOwAsjdn6v
+C/rxmgdV2Mg507207GkUrefMw/E9brCDROsAEi8NErtkzVMRGSB2AYjLWYaFUrAVgeqhvY+TOI1
GbsTnNDYY9LmxC85QQBQR2mauQbB00eMHUL3FQO78b9Fnmd1Mjlgp2otFmKIzLTq538d6O7og9HR
lvaE1jdYRH6QBP/hSvl89AFISG06g7T407mILzI/nS/ABl969DKH6GdpXXTMd8Wg+OENjxH5pDaF
5+8PzQVl0G66e7asboCr9B9CZSdLxZM0wjx07wbiyDqblKbqxt5LqW0laG+NJHFMNK56vjyMQXNJ
38qXAqFOQrplylpiW18mJW7j/nFHo3XIiIprYYFfeYCQHFOU7BhMJ54JePYhMZz55EkrJqqus5JY
V+6+CLrbjX7yu/8ADCD11BM36knpIat2UG6GiNXUWLMjDkigS7zwMZPjRTaGdxFrSwX87ysKTHdc
96G+FFFy9UTvqkZFTsjI82v6mjIbFfDX+WHoD85uWn+ArEuDjW8OyOrGQMr+DCEezINTjndgRKVU
Ebc+7x7C12KPXxEKU26iv0qPBvSZy4LyN+gvWcwA9Dlz3iD7gKGcmmrL/CysSkMmBYvfzf8ZwLQI
nTmMeeAaeQmAIi3Ph3imHp469c3dnw5jUAl3Csv+GnYJ3uI8Yy02RrOQOLgoazNXe/QSg/OW12Lt
pMiqk9jyZpNZQEJtFL/29P8Rfe1WttQhKoRw6Xh78aiAtOIShGwlYu1wfKJkvT0K6WVnksgm9Nht
V11xy45K+9IjcSPP5L5cqYulhjSff6Qve5tHeaKibpMaLkjzjWiPn9TgA3WpV0Y+ciHT/xV0n+PL
i7mfkgsf9MVInlSelJXbAepqcO9NOhHzrL/vQZdSCxwOOoICKeA+2Hyr5++Zsg4yiwHxOlNljjMV
p7N9A3AgxLMT3i7DSQuu/aC+81EwEQrbgWztPJfaYBdh5NDGOWEKahw0uYadkL2E45N3gGZ8OgaM
IElNvcn3XFoM0NQLCo5NyFzxiAUGN1r6qtg1RDwXcvd7g+h25LOqdOpq6EEds17cTKZJj4Ns2a3+
YFJlGbySaLRWJXDSrNOxHy3OtneLM3+3nT0sV+gotlnlCB+YMf/OXvAQ9iSlgYqJe4D86lgW17OC
LMtqJkaRYauiVqe1OqOuuVaQBczgyTf846JrxrCE3n3qq31yi65kvOqFy8bIE965N0whCQWTLh4Q
vH9Tn3SDwnwJa0h+sMwX8duolQWgtyVfFM/jPc8qC5qyWfWSyna8Mj8pVoTaDBZNJDa1aAO4K0CN
fekItMUSJApNtPHme4lsNuBCpgqrlH0KePXTB3cq7tsh4Fqz98x87lioWg57Ujq9dln0Q5DHIgYs
Duraf/5OU8jBVq5k+22dmPHuR9ZVAdj/XnO9cpDYx9gf4Ih8qMWJxggdyclHujW/HZqBDxTzO1mY
FSFvrh/CzmlaDOmFCjeTUz4nUvZpAuRDeMIua5hk+91oAMt2ALO4+4AbZvf+Z3tCUvJFJFeWZ3nA
IjmK/Ib+O1UivzUfao0JSqcQcrbsz+K+/GxA07ODi1OMlamiU6duWV8de2PWbmZXMejfoz6ya11+
/gWYnRF5WyIFE1E55Hj25cNdix0XDg3BruS3UpVQoYlEseWAEbXFBa4+Jta+8Nb29JKe0guXE/jB
fYeHamxeaJnxriRfYFE4b61hBhmGeqCY1U28ixdmj806iCJ10sFcECPbSjWIPHil/a8h3QAJpAsW
LLx1sPCxSEY9KFy7IHtGmi3Lb+iCtehzRZEt7KWf2ZkZ/0zzaoiLAqMdAg0ApmOybHMCwz1ygckd
AQLFXFjDOHda9VnGxkuGsCFOzCmUQx+0eFGKdStvHT3w0107uc6aE/ROWi98ggZkiPBJmmriF1AB
Tgsdn7XpoNWtszI4GKdd02qyZfYLM748EbJWkO9IP8T/yTXWJhzWhFV04j0wfk07bOJ5JGpf6CWr
434BoW+ChehpxWyuXQKe8fpSeYMcUJp3OZYUsZu9BvzVSVPt8rnydAUBPoymavyEtLOI1Eg6DEQB
cvQJUBXz4FGFI9UsDlzHik0LYoxpoqfi5B4bV0kPjWmOlJz1g9KueB+IfJ5b1zg7gPP2OBQjGXbR
rqVJHTZHm488w0PE3Gnou9MlA/g1hUZ8135Dazlj7ojHYr37M4JAAv2isY8dv3EWD8zACo+blEsp
bw6G5Q2Wu4qFvhlx2TECoImbPJqKeI/e5T0fUk/MkkPE3RCJ4Me1QzQLHLi/9AA+80B3I5gTYmF1
D1uc1RM/VwBxJwmdbpWvAsJ2jf+zs1VjXdgkSR5v7UivkpXIWVWGK1zPmCw2fyN5G2mMR413tIXl
/iTVQ9MXAsXXhOrPVW93QmLPjUuduoSz3dOc7PfsrpFGE5f0CmZVyVYGz0ya7t7MN/jXet2Ph7cP
OLfiTCOLmPEyBUF/URnGqZ8n+B48sT2VSh1xm+n1Uo2Tt0JkHNvqwCTVPD6x8YpMYj68HKMRX/+1
wl2DSdfj0j/uwZzphLXj4rwyz5VhODCn1xi+czrqfrMLaRP4S5TcZ1fV2zW51Ao2LScL6TUIppJr
3gBcRLj3XJcz7mi6EtlyoiZYPdDS2VmvVpVINk3S1WNX1rCEkPriSq5fBq/gp+n6ieCRnp/502FE
zZYBtqcJqtNfeifOOBpv6vfiIofQksbcxB5nTfCpfRn0SXo0Im+CinwjIj5rje4IDGQQm1/kK9FY
wEF5mvBQi+NqX+zRXkJQNkV/PgT7aMEpJSguNHN4QWtqPPB7kSaa2dQ6LMx1z1xoULxWf1GFwTjR
7mpMYhHaEVvma3pOZ0mWeywd8R8jV0AvQj+R2zjL7I9YQPGpDe4fvw7B+ejXMKW4J12j4FEEvgvU
YGD1sMoa3IzaRegFOZAoRws6sVrBQZSC6x9roKyjlr0jI5USNWea4hYJV8t7Go+RRXjCp8WbyTZC
qpRS42H9N1Zivw+g/Sd73Fd5LWTFLcAxvhU8usFVoINgh/DVU5EVVyLj9+lno33lZKk7gPsYswZQ
XBXGpffWpi27TGh0wOSUUds0T5FIbbQBC64X1mg1qSNzsnV+lhdXSeXZc36N8XlYrAqSojBd7cWz
wM4BQYkkh5UA6cszoUK+AwtZOiE4UCjBG0C31KYaBKta+18kCEIdkV1CLBEFle+ShhIwe6sjWhAz
lVlylD/aaSWWGkUew41eWFP6QL3PMT9JiYFPQvBr7gwvPxijboN76hm5p6o/nsjeYf1Q5xkcQD6U
uZubnkK25dgE5MHHlLY/MuOqOJXfbvJE7sMKNn5rjO3gxdffVYL9F5wbSI/RzWKDhrJSxYHqLeZR
g/RX+sc27vTdCgSomzCXT4fmFJXIWhItWgh9FGB3d/lUcTR8DzDEnb3Qo2mjuCHUyzELeJKc+ErU
HThap/paCslkeAAyVb13ODdOStWFCUiSvrueKStoqJl5e2ykPBCAxNzyOsp6+nmV9DXV4zBcEUDB
EuVpQs6BCFvjfvROA+Y9czmIbapVtfDTQoe+OauDR7YEnQTCs5hY8EiMP7LD1d7EVkM3rTsesO/s
LrNAxYtpij1/Y9DL8hw+zlQ7Jp0w4j1JCDnVJSMmgDLcFOBUhZYBwbzGrSnC8NMRAYjewpjVjmpm
+ORq3xs4oZvkPb53VPP9iaqrFZFZAXaSr3dIUAhpDJ/TT8Uey2QsOsRNDXghTg2hPVjMXp0KHbkW
8dZc9vsHe4BVRH/Nx7biZr1GbLTjCIxcpK+r86hk2e0/KfpPNLiODbxZdmuLT0q+WrbDE8VuzFxh
Ug+XSXQ4o7rVDIQRkeobpgXkRPZ8r7UxjEEXfP7k9Ca1LBIvk8yB5b+CU8BqPeryv7PhGfUoomsk
oa8xS2v17Xtr29nN1XIOiY8jByZEx0O09K2GORrLHMwxkW2PbPP5VvcD+IZgJZDQB3EYmhDRDzaN
2UQEpfl4TLrbHeIFIJFT50pUl/tQP2WXjUYTmqj1pQO9cGXJjBeg/2mFernOb51JDF2hVOIBlIy/
EOYPMbsk6txI0bvIV/T0WsSsr32zoE0F6bWlCGQS2mFs0iFK0Z0OLkp7GcZbEN7WfBTbcAew7dZK
Bkey4ZlitnCJGp2p64oiBv51t4lPdaJ+7X+i+laQIihYACWV6Qa8WsY6bo9O5Jn59O2UZeohtHbf
tqtIMv9WwUyH9HBaXieF1L+vElkn4UgSi7W9dOn61wcYhJeH7XLLlH0W8gRZkphnM3jCiU62qbz0
3mQGH+dU0G9ezMZkkZmfhrCQbXybiISiuxUiuVoE8fmTfWsEpZHi7pEF+XPDaupQNtnjtE6biWc6
S4agIHo3xyt0i1Pk5M/OjcHWCux0HENupGC9IOew1dmPDuqi52PgYUQinzB7q83b5TeXPq166/SR
nXtw53B8EsvuO4vt/wBDOnzzyXobt1/LKkek7vkTBQVkq9iKCGndzAxxMUGA+uafDeYs3v0XciL8
o2T6O2UB10+Q0l8tQrZeHu1Jb37JcLkUeP73hz2KS/EYZpPCm52Z6AYsrViSRtWuxS9rlUx1diPk
PwwksEBwumPF1gKbm+amqwXiQ7EWhaPuEz7iThOtl4hSJbop73pfSVmxIWtaq+ve8cJuS+etL+Vk
YGqVfyw6yx3OhJjP0Mvc1SLpxoX9wfSg52GWYmm35TxTd4s/ksO/2OlftqboAXAgXN83cJ+mFVRr
ObGi6CZpl1dkQe6xwRWI3jDgTfKtbcGLnZEaJcEb92P8jmcXR9mDVgWgC624vBj3RMXrF2S+GCXw
4vGHWepAapwdYhjBPuvzM8ElAwYuZW0sBa4wWLpzRvb4o5JfzQ5B4Majx1hda8eZ+2x1pe07BFK1
E6cTZfDGkw3hkd7qobbSdG8YDfgn0RQdVshfepPvNzpL9RN0BB7pE6I5GAIAvKNXuXIl9++WpheQ
BAuH6GUMBg0qkcHS/7r/poT9VFhxfl13YBVy+vMsbT9jDOcNrJqudDa1LUCtrI+uptOQXpEDbjkj
KEmc6Lt+ymoi7IYFBdYMvUkXJVNzqq6vlz6UrEf8tHW6qug/+c7f4GngFuUZHTzmZiiCpN0alkFd
C5upugE9DAWn18HTovisltaVVWwY2Hk86DuaJlnrgha5saXhq5lbmKyM65mVVk+nsr6bOTSIZ0bj
HWj4kkeHKx6V9ryMErdLdxTB3eqSLB6BFWMfKRAmPbvYfgFjPirvO1P/2nLLGn7qsGspVXQb96xz
ik1SCjI7YzR8AH3c4DTnUY1KTat0CBne+y7zv2hXvHFa0kBuaqACvTRSuDRpivp+tFxY5GI5dtEa
DQ6vHUSMp0gYE/YqojQfabj0FiYRBDRsO/0Y+4SmbnaoYeNBJDHKOQM8A0ixVuv0zRXazHLCr7ZV
KLY+lGIxUYf0CADQFMepJoY0PIP/NIUXdAGoZjfiCqdCiA6dJBNRGgIAmWqEMR7FJXwyJWTqV0yC
aowFuHif3Dc7uSW2IavMPJFMYXMdIH9xLwhkOt0OnyZjjROJDCHlRoAvbBd/kHwpNqwOQ6DK2mzD
2+reToAQ1PQ/Xh5R1DR7XzPb7fzbDr5R7TfRl6qlI3Qd7yCvmurYmjk1Zw6Tw3zZRbByOuScLGtf
Fx8xdvaWKtpTkz80fqcGObt32VgOFoGPQp7EtFwxxt7Vz/0h07a1fAGr9cpWsNcsNtdofktv6x/G
IcnawawhSLquv4zdm7hs0Ttx7Il5iYhb6xL6aP2jLa9yIUH7fDZYwRmt7TaJzQk8dN+ZJKGXAOd7
VBA0LvRbmC1D32kVrV9NMh1Wy2hq1XI0W/Zg2olHFcIXvbcE5jmQcYmkbGZgR7ZVgIQ2Qw1Za5vt
zdyXqTyOsbUycPTAAhu9V30pOUo7s6ii06JxZTwnEJVxx2ojC83GfeZK+JDm115eQ9ctaz/OAida
KPj/AyLOoZnicZXkx7HEdeZGpAOZ2US7t0geoRxh7iBPe9Httx0+a4enEvLoIJiaYLLmIurd+vS8
JnmT6uDiywuX4CqCE7z7c/8fkU5LhyVp/Q6vsVACRy8FWFt+vDyEx5Y7mLpD3RHdNbYgIk4di76f
NkkkWTkQgKrTekoSSPeIfOQfwsB2WpYjLUPMRVWs5BFxDaYkhvHI/MUjyRY9sTTSfGCNriX1dILS
3yKZpMeMa4BY6H3Bo85bQDW7lQAqSqvAX3vFfiihB/pcKcA9q/D4XkYT2BY/KX2DCq+96dZXGKrC
uP4A5wgRhT6ibUa9yXuciSUwP94y4/31FxoOLOPIcO2l3/3Ib2VJb/3uKBHv9gwgwOfluWTPQmHY
IJsTE0mdI/Fs+plH05Aiu2VChAhSEwlgUNYtJs5ICXhRwFH5Aqm9El9uKemNBxj9Pk+f3nIRrfaB
a3kL3MHw6vDvMBv/gL6zbk/9KYebmvWDz2HDTU+b5W6m+S7Rj+eb04z1BLRrHFmqqT9ULuEjC8+C
ZXCT28KWkM0b8h9yBeEy4H7DYzlzBO5TFJ1MpUMJeHA1/d3uOAZeFgS4nN//DBP94pbAUPppvDi4
vnCfbM9oMAbkuU8ZZdnB/BvedHR6PQAvamIhWCfsi9IiaJB/UOsbkiZFjrOjDx17Ynkm2QJSijUS
QE9llaqE3+q/A+BHQ5oBax+RgiDtB+89hHD5aHDvLOyCC8WepSTgkfHMZ/zXH/+yqe5RVth8fQDe
3HlHWkJJ+MDL97vwPXf1zZxR3toWRMMJwwHC09fG4ahvhXyommgpwEK568jpUxU/L17Ef4vD8DAX
o2rTUAj59JGvm2rAPhjDdkjiNqv9SNINXeBjVmjey4cCE/QJC0XYL3JzgBF7tBnhk6DPNIZV8MMs
TpewTnTcHKuGZO2t1o8QcRF/cORIK/5cFydxkDuAHy0/d4M8nRvIKtR1J01I4C16AEOOQiaWhxen
pAZ1KKMGdzl4qDQOXSsP+qbjb+/OxaKjwUwpuoaVH6cfcHgdf/XQbGUuAUDpk41Ulax3dlDlU7vD
hWvBeY7leUnI9pGjuhDUciEKxRywiIUYdmmPXvyYvzFGNnVTdnz7ZtUI658suaanc1VBX6pvn/3o
7ZDIlXEbGjqpigyaPcQ1epFCn0XsvdORv9ytyYpGEwybUs1pvIHqH5pI0tEL5kEZ8QnJ1tu3V7SC
IyQGbGfh3JSZBLjeucBKLOkaAvX53g3HFgYtxmjyWue3KQWv0V9+p7H0ZahTYQwstNDaHWzAhxau
tr61IYQR5DZpMlabXnoJELZdl9/ErfYbQSv0AIjXXeDFwGJv0TBvVFcgRnbRDxkUUMFcqQKIvX6e
Avyt/VxnuijWp12Y5g6no2LAx+QVq8bASjB+8Il7Vsho9zbrKQ8cx2+1CXHbqsrUV9lb+7QVjeLH
KOZy5MT2tmm0hsFsD+Pg8MUoAp/R/oUkOqPssqXlK184e4EH6GJrPFq9A6Ces0OvRQvxaUi0BKnQ
/jbW3/KtE8wLTs7bdHtF0fiCvnVw1q6m/gvYd92OFlsWCBKkSNNe5c4hU/0PV+voOikRdmGVHawB
faiPq5s+oGG0+B7SlNs23cemDCcny0uN2GsFZnMQbzqBLglAr1N64aF8pXv03oLck+tfpuat2Y+m
L37M8n8z0o4TZ3jMHW2Heq9ybz8WHPxGFqiCKiFNPKQk2282Fy6OOQoHq9Wm1JurRZZellnFjEKC
zTDlUC5jS/uvtf3I0sq8sYRb/45ESBxdKOCC0PmYTb5lNp7UcO7pO65I/NxEOogsOYQcpojJMBfG
fbWWdfsSdLGvb+IghhU871FV2boDKqNaOjmr14haNyVZZVHwlau1ZtC8UqeHnoc2KDsOPQeNkUrx
J1UEP5O6BBR6w4gkyNrhM7drbsDEDwO1vftiJS7MJJjDpW9RAHcW16VyNXEUdLbUmUK4xuyqpsQJ
jHO2HNTLqmexIRt0EvXQC1slReJA4JThZWcVt8Kz18GLMJGugPbnWCetg6K6smqs8ZPxreRaNzqf
ykUG9kBjgiyFkZkUHG2IQV3Teuy0l1cTji1WzMcFTsR7bfQXX0WdNh89C/iVi2kAEuIVjBQnECDu
6DT16M6eqO6k27w4azS0wIU51bxuIb3VnZd+2m11jXEdElIZ4G65QdYtWwZ+kMo/4lfUy5+ofY9x
xQ2/xRbRCW+zc8eoddZ2C/asbXc+MWcVenYNmQxZkedkOJ7za09Ei42WWJgVTFdGw2YQcoL3L9xC
qw6ZfzdTo/nz9cZJhAhYA/xqqTGSdppz63Lu5aBJRuK3tcmFD3xkCL3U4gppouovionC6EBt+ffk
VA5/0dRGcoaan7WsEI87daPQzF3+0bhPfFREZT9VDlgIQVJ4iXX1Omk5WRMVpDV8Ss6u1t65d7Ov
THNQBevIjfnzDwqJ7peccyrcJ85QeGh7q2TEyrL/TRvjBVA8lqhC+ZFt3FxRc6sHlVTH5pC1qR/M
m4D4lG3imf/gqNM6pSxVGoLDehVXiTplUYSRaqZYrD+aB0d+g12wg9HMdVrYOn6kf/wQWRVAVvwP
WVwthljFgPJFStAIWvdSqsT1+eAloDbHY2YE75TzzssRnvinDVI8Z3YYrfwmds5PFMOMdmpG1PpO
josnVwNiXVEeIn9FMC4iXWDbq9VNwFLcmsxjVvj0CRWqQBfZi5OQGCw9OOescOnKtJpczMv7eAtD
2ynmztYLicgJfWEYcJyYTN0HvhIJ58OaDwkk5JkNrYEWoPv9f+AtzAVakM27lOWwxJRIm0eGo3bH
0exPkQlOdsIb+9epBEpU6wVP5OqYeeeW6cDjwM0r/fDycfaektCq4IGAMkDgRPYFvhl0mCfUKM/O
iGdCCT+4djpPGKr3Jd99Cx23MvvNn5fF1CcK5T5XC76Nto7tyUEMxQfLxpfh657mHx+otYUT+0ec
32La8TM+xPeSTQquJOHSqMJrHu9qRgpAfhghV2M7UK70yfIXQ2dUQWewKzKXHVy3gyrOeLdTJBBF
FB0PAXxdTmMmAIHBG+rEiAkc6I8RVGgx2YuzP0BkbnBIVbIDY9N4p4il++pSbB5vTiyoXNlYqSOA
wdV6IcAVv0kacPW6VAPJrV4u9VOituwT/LRc/aAN627Y9RgV0G85HfQeVWc5fk4wiPGuAUSpnwPu
dlwD8M3oYg1yw7u2FUBqsIfNW8DTEqZngNlJQnREexGTiIFLQeVT25C1eqVr4x54XHTSMr6EsK+2
UFwzuCS1LFum44k4QNUcfoA2uyyGZln6UPD+tGdsXndIOJH3e98aJRNNkUHoa+AGrudv4j1mRQ0h
uUx8XkPkDNBOit+i9vpV0c7qHsD+7gFQ+jmNJUxzTMsQaZpuYVYxR8zMWnAXsoYaTtgA1cHLUdcu
KNaDuSey2KrzHjrA3qOTpA84IHH0oYBCnvmLWGZdWPUsSA/r4Pb6knySXW09ZHR04uc3rMAoX7RR
GsyM4OPZfGie9yYH4NS61uO0YEzPLg9Xy2oSSV51w6pZnm520uXXIgldgoOPZBmQHKb/rc/V3M/h
muotXMC6d7Fl+RNR0NwWnl9o7LX4EUVB+pXhyFPOyhSFa+sLlK5p/fVmq9OlxGTwDUpKkGXh6Dly
Mohx9GhjaM6qMeWQ7q1+FnlIOJQUlscbW1UmcPdZUv2PnlLCZw+zHblZroO/mYsDyLc/iUnWFqxK
iGtHYHFNxaOv+xcj/BZA+O1R5PghBSKQFgStPRjvYjIP6CQWxQrf5pfkbWfp7Ndm1pjbXQySDbT7
HT73bo3Mn0gUS1Zyuab2dizAnpPoRBtpwteNNP8wpEJJYauWnji2Gtq8UNcVda0VoQFxj6DewXBm
59dVQ7eRElwamgHjMxQdeWvPQz4eP5FEIWT2jBVp/2GPlVOdZBkc0ZVLkDNMBAD6qeo3aBRi5UPV
t4SLr/ZbXhDSoNoGaEm3l9lGnGMLD57379sn3ZM196euq+KxKyNLyJ11dNL+jR9muwS8UrbQcBil
/IQ1XQRA6to3zZjAwqUtnu/gFobD3FntiEMhbd9+mm5j+D4RNzDMp3LRy8kk0fUT5IAnLijacceb
N1nafrnavqcn3ROyzEvI17xTBUsS/ZVeI8zT2XHS3P+CoqjDjnIe5rkqGGeZhwEsqMNCfskVUF6T
xyN4rkdAfk/Gb9fo12mECe1AspGvrxxuTHyt9/AygD+RfRN/FTItBVA/oBMVDUV6qRNYrEt6KU7A
EqpJMiL8HL/qbWlpmYdLvHrSh6fbWFEdww8k7JiSBMbtsSyL3r7nI5eajzLnDmF1XeEipPNhKXVA
GQDqSpboNtjTiW/ruSF28oqdc57KM1LZk3N7Kwfmm6P7ByYbTxo9zJZmkEkaBqYIh8jc3vVB/pS0
GmzMeiaFa1UmIhVzS1+YsOd5cfMRrOzmuwkEIICxSl+1s7Ghi+daHbNUoZ0su2cWb9BUTuucjMLY
MLbgL1TPnW2Tyl3twDim28ydljvVRf4UNiWa2NLjVZoalcuel1NPBFwtbdBQuayK3TJiiPMc1a1g
/MFtfqIw8/58ypXaLVUfRPz88CRirfToopcvHStitZc+Sq7JS2Pzejde2haPKsgXW0O0CUQAsFMM
cTKq9ICdqx1jA5eeOpwymjvDdRaXpvhPQqNEMZySQ9TKiB/GtfVE8wpf89fTjbUQY3DGaTY/U71w
CnEub01croN3mKirKAMzIUFX9JOJTaT/RRLrGrCh/MpEGq1BMpCTJOMqmiavokCE9o3Igf4qB7oH
KfvnNvNvjrs0GkpMcL5QTxipJaoqrI0c+1e/RjH3u2dnQRC6vZCxjxDXXWPSgdV66hCeuG6PhK7Z
A1wSYJay+NdCj82cOfQaLh/opPiwzxHUCkolcPvHmDczZUoRMdeSP7rCXnuvu+J7HZlSMzOY9kS+
ty9F4T+Cg8LHAQh1Z2r40TSDnkTxW4FIIxD0sI6WPS/6FQJwcWuCYXrXSbtS/y3xLGSg5cNL0EZc
YUV+M+jnRNa5pPZUEzzMrbTvQHAGvOE7auUozUATEKeklwTMwlD/sTbl019BoazEJlRtUQBNl2Ay
qm2SXHXNgbNFacBf39SuDJt3QxNMhijMP+3RUSXQanb103Gdg3XjR/w+eCYkocQGEApzr9o7o8Ll
E74HGp0VeDzN285Svc5uTDiOsrFriVF3fwZNuOoYil9oaaq4FXFunrhfAw1PUeSxn6qZjAx9Dx0n
9Ckjnf3snq0nXe9ZiqqRakAxKhLi0PGqyvnaZGcnP1gM01LqeVBBdgaMFGCptFuqHpcfj09J2Tt5
ro+Qd1Gehdz362AK2Y6ZytMMwLRBa5acA9zN52XImTqA0p9/zEYS2dOtmGOFjl936jODeGmg9J6B
u/ez7ZFrWjE8NI/RA1CyyNOcbUK+Dz9OtRngDn/+ApFntqJca28zqXipdR8NmB2ST/5ocqs5emAp
9zXU9MVntG7J5RIc63uVL16C/wSXPPr9XgZcPqScZtFAv9hd8SdZEfahPRak/fpUYpbqEVTsiG7b
LZlrZDRHfcULw0PlGsjHiuAbF1Di2rmTJ4bEire1AWeJMYDLbg7kA3ojei5ReSjZbI/+6KQ0wOMI
G8V1H1Uv546SG+o5sqrFqCelKgcnV12AvZyoQ/6mpCVpXd+GorY1djEgcEMZ5nUgq+eh1bm03N2E
aYTbTeFAV+efqE0knf5AtDWt2LxM6CM/PW5BHcBDXA+yG7IWPcBzyCcwDiR4CKm8GPwVV+k3n7JG
pEMNuNH5mUfKd3VOf090/OOYr+ge5f4d9jgVczQxUcCOEzvDwKOMikjnY7dlq0Yg41l8AfQhmjk9
d25HoMLgOtbpnHGSwjLtnemKyQz5JhGt05uy24NMeJPhClbJftc1FmDcMeguZyMzD9J5imv94PGv
fEq8v2OBDHUW2DS3Xbwf2M67ylP4CQjgDiyfpjgj/NwWk8plzDFDWUIjfeZ9Bfz75hHh4/ZqvgD9
kuJD8iqBDJVjWWcQgQytE0pjmwkXz0u5QOhwjVZTOx4wvhDqF2ULg3vqVXSB7QvjZ8GFounSBjgH
LeFt5T3AmBi2SLbHGMT6DwWvRJSkJ3TnJdzIUT23e3icZBNmrR6vMrAHXA4OWfaEuElyRLuXu7w/
FzJHx9YB6nq71aYko4+9kJwBNymFo1EkXtT1/c2CVrqQEVdEuKbtAdjzGVhkOrX7uHTgeU4O3Zbk
N/aIJVYzPWBTe1O/5HTcW9wSR7JLTOivY3hYDiRdOF5Ek+CMVQhBCWANfGlK9blRDE3acThRLh4W
u27DkM5V8cvt3vHV42H/aK3HuJ7OSkyqjBt/VZamArnqkmbtmVMweBT+CzTo7KbwuaqLYDcC7O5R
K65Cn/fmmwEOllZDpCJ45tEPPkekqhyYmBIYS/0ZmWhhReMoZGFkE3dQT5WZWFQjLwhqe3BxmyMC
jUKair/VuuZEe4RWCAhKfVu0FvlLcSaWFs60QVcvszY79MnZR3z2zxJvdmjqgdeuP0mjEzdqADIF
BXsDERNVkh4o8/hmyiyx58wCc1/Y0UbsLq8o1BYjDOPWzavYeY8v8pVDxUD7yrKAHU+0Ip9uFV44
rhuS6x3skMRLSAvBJoUMavPmO8jF9nPpMX5W+/hTGUj42NqgggZg4V7AN48YEdVrlIk0fs8scA7n
oDwM2B3wHoiC7gBAxAO8wqJr6kAO3/V96m2EjXmjF5veFuLjB7+iOy2lZxkCIMJvMeQzj/ch8XTG
/nTjn5Ahl2P57UuNMXZgo7a8Yg9l4hLG7IumiBSz4jF3gav0z5oZwC//CZpdovfkg91gAIi7EpAp
rnXtUbEuVG6yzNh0JYbfq5R+bPfhAQe7mP6VSDGFfxKQ17z7h3jRJLAPnpg1SWGzD7AMuWS8Hi7r
2f1kWLr2S6gCQe4vSkqUK11bdq2rrtzjcPjlMWu3yp16pzseEONdpL4ihyGtnBsSWFBNOII14KLt
aXif9GlCFol5qTE2xFe0pg4qQXYSNUQYYofihx2uu4u3F/95fJF1zlGLzHR1HqFqFfg0nojy805B
AFOeY6YKy9j1yMNt1IA8nT8oR5rJKa4kai0Fp+gURlvYm0JOgbJWD1eU/cZrBzeROUncAWhgdYmq
fMD6UZKwKixgQ+QNcdTKgzfMJ4sjQDuulFoS5Qil6JgULH6sL+5+qJmbFEwkmzsxDSsGwykhURF6
46gl8V5zG8Re0cD1Jb6Aq0wHeg/hvW4AI/R5BO624lqYxOUaH8DWKY+XhuA/JCdgJVj04U2MAas1
tDZekMh66wFRcZHRtNtOBShhiN3mFcyp8hsk1hWc3ZpxygMkPlJ+Se5oo3pzfWpPoRiIS/scX4Ei
TCVq6dyOcrPc87vO3iqO8S+qqh4qR8hA88wJtxJINb4WTbc9rvCWI2kY6mUowe64NppPpfgFCpFP
sS7wQRNqlaCZrJ0MASsLPmvdlJswX07DZsxNrvPTqQfC1pi70sRR6hMhbOLsQ3uaHeMDJbo272/p
SzCEpQmdqnKnTcVwN+H9kEEzbOZXakPEwhdpsHA2aGMvXp+jVKxn6+gJWhP6FqQqwgGP8C6QEmd+
3pjMCxG2ZzGcvPFNKLliB0ynqCPWXlp8UN4/8PE3hk53nrwNvXqVhrjrWFYdAt9ASzfBSQ7PI1ZG
CEiLuybS65LcZaS2B2tlt42CFdrL7IivyvK024m63+g8e9KV4mKbMks9iejlLj+misXMqHjzPtwW
5sX8GuwZp1nx6NQnHeRWYhTifM18Lf+u6SftbWj6Z5ypV7oFvVvf/WBGEr9PPpDrO4lq9oFkeQK4
ulTBkeUhUSguKO9Ng8qXmWSUMKQGLEWYvJDEr7fJMxOMWINoJanD2brM3hm57RzbNNKfUS4/4WK0
KREhSYIeFEik7GHFFAJficqWv7fL/qPm1dGOCIknJX5nuBJ40MShEEaUqC7GFwl/ZVnC7JIe+GtX
wrVFJp5zeiROoLoGi9/qFRKs+Wh6huM7+doexhmipbj6nwTpwSR52PK49/EWvlfYMpP8Xae+ucqm
kL08KIcbYKsuhpWDd3icmrUWz6eozbclA2Cc8L8/yE0ekeSSd3NTJjAFAwklx/ZiBSWnE+3ctoX5
tMahFlnqzWH4S7nDsI4TQ2E7YKu60GfGEc2blh5+G+j/l3C5UpGFNSbwaKxLWf34+AY/mdY0K59g
+lT2dshA5hT8YRAO7kjefnzBeKLig6Xoxon6JxINsdGDdTjho8HsnOo75nfPNzG4pTztKkdJc0mI
PwUXFB3GpAFkz1mTfcOG1u676aPanwQtEmUnrYpJy2FpsMvDiP4OG6gUbQ6I28iU1RoEr1/8GnUF
CyqMJc8O3kJ1XHSW6Z//RsbLq0HKNQlkDlLHXcssUq0T/JyQIjg4MIVyNXg5z0ghhFMCOzDy0iKA
e34eRejgXxCh5irbuiiFGuStXq3xDosAxGMeJ49QfkdFZ6JnSU+GpntPfUQbDqf4UZq8FdDBFZiT
Tk8GrjWrN+e8DnLzeIvMpI6VrfSAzW+1CdR79HVuCJ2Ht++xpXiO2WRldmusqDqM71N0Lq46s9t5
7NnkKZ0vxGYd60JWvfHU01BlpItUzg/Zc3+f4g4tuHQM0SLQmqaVuq5jd0ePU9I138zGJymrwNwb
nEdqQaPlaVAWrGyYxKMB2fbeI9N25cyZ9I0ebiz9tpiPLmJxMO9zLFoYdRhlBd37D16iC0/uwm2D
09pgXXge+Qxg9fAKaGTblbYjUCqnFYBkt5TXinGWHbXr9K93BvkT6Pm9DEWIK7nKUasK4FM7kKl5
bHTctV4xnl4+u6XEO5P9a5642nO52HBqbO6rMxbEscMH3ZNeFiuYsULvVXCo63agnpKuV5/PRNTa
5J/uW7nU6WOKy/GTDZOHkESLdg+h2XarnaZvgifHirmv7ZN1S+sEFiii5bt/gJvvgQrvpgwjzI5x
xFfXCqkrJA/+/t8QdWOjc2TeIvmgySXDTLgEu971GomyugMShzEYyWHEojm4yrCm3sFcB1qF5DMp
C6Mt1u/DAr8KhEn6EMLaKkAmCzEeeGewuIWDPInHFpFot7tNWBTxIDgbo9Jwq3ssc+ETwyX0lVlt
cEOlX8giIhWlZql6i3aOnYO1XUixi0/Pi+O94hDKGwoJ2+jvwuBCNtoHnJVJcSvge2j0oVPo/dsx
DLzN1BkwJ4Y5TPLW7ZxZiT1hDEfYtDjId8ABzcLNPGLjbjNnsOnF7M5EfwDwpI3eud7jH7S8U935
WhQ3I+GgHc90Uk3dt5FfDR7kuEBc455QAcYvEqNRiLmvYEJlNAbnMxXjXV00v7TiryGZHPHK155L
D/DpatH7oQOck9Z3vIAQc8OggbGSS95KXsX/+qaJKFvsa0iVt1kyrhjxEM8qtxqhjCB8JU0+Bcps
BCQDUHLW5h/j71ntdvF1m+WjkL45AEbvWtNacZWxW0T2SiI6lxq7v8T8ioePBZKcvyGpkaVL7K/b
dSU2aBTmCyfHPlXhlOs6t0+sYkl+zU529NOj/ocHRqTc1DqWApBRdYrY9bhycp4h5oniOOuv8f65
5rrdsU1u51vZ4tticnUv4OBC9yD9Onh3ex+AWXSgQLX3wMsDSwRhXe3TiaKwi0gvmqiVlKNhqPXK
b8+KIO0m8IsKpT2Jti09lSDP7LR0fx3e2WeBvI0KFgePrSc3o1N6yEiXQRbv7p8SM+S9LocDZ9DL
W5JC2OlmY0Y8Uw0ZcAZ4Kxbungw4NXlQ6EkTZ64clyDRNJmprbn8fjFtIWwARSU/mTx2zBUOYdjy
zggPtXy4cV+c1VJGYtXKy7Xs8OQyYvACbOVx6mq2BB662yvnpA3BIoO85OP7bc8sEnySPWxicT9o
A/m8x30dQNk63m8aJzRHjvcYFDxQJyYCaCy7NDm6UQI0hogT+GlBbUXfyXuduCj/NYEigCZSMD/T
HrH3J6DGCR3u+6gxbYxn0UAbBtlciJufhHLQpWxauhzMUQpiVO/B32m9hzk2plH04a73Dm2hb4pO
1KC1c0Z78z+gG1kXNmZcmOyYYi2IetXdTFIxqIN2gMIs0bXgauqQA4QhgyEMqL5G38HU+hE9pSCd
eqNRYkMMJe9cdJSkA3BIEAI5lnvWioZrS5JAVbe3EQP1t/Jlld/J7lZl5IbItuJNqLy/S3jC/vsK
9JfKNQSY9Hg/+2n3xS6QhqcaxFn0rtFmsPfFrsGChdxG46y0L60bkZpFys76kjJoDa7fJORLKcIR
9mQoH0u0IPJ7wbe4g/YOXZgWrYKovXDdW/LRB4dufVUq3z04gjwXnzcqQAGaJ1S2F5+Xrd+A0up8
CnPyglbgPy3KZxcTT7iOQLrfXqC1FPezKKEXmpf2msoUNSTxf5gFKCFeoYPdkdfmFsG2YQl1uRkE
HuNtJJRGo/9RD+sdSeTvJsfylUKXPpzBw0IiWeuI72eUV/uKT2T4azhkfivOqVGuYu6BJnX1r6Fs
ahQFza7qqThIGsbSRMtkuCIRI1s6rcwdaS5jmQBi35sB27E0d3OCURo7Q3yasgcbw7qgxGAy9/PQ
k5yMizNIkWW8QcXqjNdPX/39wXEm3y9JmLbbszM+BzTkIIzi+xpeHmgHd+eFIToBdFCZmLRUXDP5
udcT9V4LBl0Zd/d69O+YpL1DTppdEnrXkqctwmXq2BxXIXNLUNn4y2UBtEIEhneBInIyUljLBosU
YaWgWOIWni0bxiGE+SpSn6YjaF7xUfNhHXExZ2Mqg+Nk5qc6MvChqGeQxG4QQJcz+v6U98uq1hug
zyumrKDjTt8obODcE2g/avnC7/8yeJpcQy+zTCpK/pmVnNqKIRVvuV65Q/fTiZpfQbIpwGTTiX57
aJ4kfBYCyOvANxpt/cVuCrE+mIzvPL1BTDyf+rsL5rDaqSD5u9sSvoTEpD/0ZgE10KPa5iaeUZel
92cyA+yBBuhgM07gLyovgi1odMX1x9g0/QuQRY5FOcuRmfu/N8UR8zfuTNql114AWrYBu6yj5sB9
hRGBozwfSJXNyIi8+mX6ZpiZJhmzZ9Dctq5PXnpT+HkgJ4YmJOcgqohRL6KEBUNsOZ5KukJAeO9I
wydFkBtSLTTe7g3K+ossAcaR/w52NBSqJRO/rEvIZynMz2Z+kzwJEWqvyPQ7FHvj6VVMQC2lE3My
N6dZmxt8dtG269m284CI7GjiUAMdJdWXh2acziShwdiMG81E+7mVJO1010xMj3RWyTD3cB7s68TF
YE5uH7fJOuabG03xBgMPkcDZX0/B5Jy5IpUWnDfsMhpgRjeOzPKIScv7P++EdZL5oWx9OigEV33R
+E0j3+6KLkl2/QIGWWZXCvdstDA4qZ2iSJ9xMn/qj4b3I4a+empdHFsy2XuUIC6SYAFQE7a8qf0d
7NMTyz0ZfG9H/CEvo34gVk0vZAqwfoguZ8XZbqjWnqB95IhBeb1rVXkeDOfOWt4cNNMIEXErhqNs
XbU+MXYRHgyR8sEmLsHPIU7sAsqBahiXwEjuBtSCwVMwJo6sZOjpimLnnaP6MqZ/5khyVlb2udeW
ZPSZh0EHO2OpkDX40zyyHret2Wq4Ns+1boUq7gwq6ICEisLJ+WuLaqYfXjZ8dYxv8OXiE+wRbme2
qtwyKRZs0OqdNPbavI1ezj1/y8ijEs4BLXClHg8WDArCsDdTfKpFt9FPNy5NHjNQKlB59UusOrDu
p7H2hnRMOGvIopOzmd8deS5x9ZNsxYmD3Dui9CFyV1TFEs6/Y4W/EJBPpXJPV8YAMu9d1IB7ln7P
36zRKn765vpUgxUMVcKieIvSiw8/DLEJXsAnjruoql/OtTK2orjyCHXisGDB2C9B1NErPOvTSbcI
tJ4lB88bOFcZr2DktsDPxtJtTqFS03l0BG2Lj0caV7VvvEH6z0cHBAblS6hf7PzsQjucpLf0axSR
KrtmqNYkRN9HZXhoTEtbNJbsdGbA37ZCnxiVnXYmsDgkm1rtnNL3a84CtfZ9bSvb7sNbEBhV8wW7
g8q17qoRbHYz+2vl15dDCSL91K8fpv4WnkPEdenGjSiNtRPG61ixksr02PDazn4thF/DBHURl906
2VBzEHxO+gb0SOXvX28ofN7s2o7KIeTWEG5uV/ymAJ96k8aiKru8ykd8HQZhgOtmTzoWztGxvdK3
25OpYMN7aKVlouANuNttiwNyY38FhcL8Hhrw7xFHEZoYqWrZKKNNPgxko4TJAD6JnslKUpX4m0Mc
gaedaxpaSaRdLhsxPejRayoFy7ywpJ4mCMNdz4sZYDzTU8RQnhCaUJ6rrEe4uyEoRMu3a6Hi7xBg
Bi1eI47hueXAZBQf7k9z0ZcLqwDIjqri8fkihoRiY67LTcr2f6RqU3Od4SqEVYxQdHCVp9IaJcON
2VBil9bweZVs1sGSLw04CyiQborL48GKX0sR6+xpgQeNYSpiC9pZH0DNpMVPix9fzxjoLdBIBpp8
kIleBNZKMVsbiDFS/+TQOQk6BHCYSOnzlzKRGytEbP/2yNSXDwb72az7DllN6+bJe0+UoaRd0cqJ
78IcAh67xnWlG+WeHs9ba6Gy6nXCjNYe8MzNPjq644A2BMEcz0AwvSPDYQmW80Ssg10GthsnZR80
5kAJAmvpplSTXquKXDLENbJyutXpU5hxQjFCZvx4AQQfSItofsH4Alzr301NHTnTYFJOKmyvo6Z9
vMqUjNe5vY7I5tBiwsKCKXn66rBP6cXokqMWbQur45S5rLRH/s4Xk3Ihu5WMzhlSswOpSkZ+Udtb
8oh+M/9jnuL5QNO+9pVv2MeKv4PxVHg+ks5qg5qbgm4J7Z+lKBUDJ09PfOmlG1Otj9Hh3Umis6Q9
GP9e56HOh3eSH4jAmLRkIWCAeAR0NBJjrnUP9MZ+AlyDi+WUCoITFdcepw87CPlBVyqE9S44OKFh
ymqlh8DW5zRPpdGN6KNPpQk3l1bTDO0hClFNXwXvrgubgEprObxOMJ+PwW8ow4i0+X3Beeehn8y/
7S5/GSlZjLuOdP58m8cX6lY6+OEEgnsP/FRTPnajpB8wtE1eB+7uxhNaA4ooAhraPWByrlYDVTQg
wloCUdZM28ThremZjimQ0Hn8vsp8p+NzJ5w/OT5YX4iRCGssrOuEdbpg/Ds/nRiREEeb6c/SpyzQ
yHlIDZoqPooO4qZ9k260+T2/JWHWQlwifQAX+39YepYWA73cecC5rdMGVgq+ctFX8/MkfYOIlKed
rglJ6HGudU0P7EXHCYUxeqjWNzxAXhKvLceB+3ElwtNPfVPsXlddiTGwkGDXDgaPAiwsIeC49sVk
1DZYOrxv1ayPC+8BLZHfw3ZJ5nNDwNTtucAQK4t2QCBNNnpaQpCCTPVrbyC4IB8F+wZdTxA6jl/J
W65WyvPPzZFSDrA8XooWpZAYY5wYuCZYxi2KRfOABuwxFzfKPMVE+gWjIxbz9O6v5DedeX3J2i2q
fTTt1eSzW5mN7DBHVeBsLHNH7GePjaZh8486RLKzTerOyeUCbIG3ORImBP4Pv7vnQ22rAG2xEO8O
OXmCaRYPM4VTrikzg+FX/07nLb1of6xmiaEgXsalHoSItFFcSXdNl0Xwi8wz+BYQEHrI7lTDI2uj
otWXXxbYJ8XE5WtIV/lNwmh+bL6t0i1yQP6vS5mqVnpMAIdmjsGzjiwtfTcGk2JBtStUfioAkjjC
MasamZI4vFbBNMZhri/6nqs+DvJ7ZJbV8Vt/estImcZ7Oc/NqaDeUPEPst8Y50pOGWLw4y8xuqH8
j8jzvsMACYkIcyvXQpZj9bWDM53EfF4T24EOVh8Ird4WFFWbDwB8yQbv+qMA7b9lYAbE5GpvdWnu
3PK0R8kmfnVjQbI9tbmOBLaIldLAHQFMXLFssCGWVv0JP8y3qHt8isjDBZbwJNsxkk1HYLl7SwEp
QRjt6LXDQERDBcoKxDLrfejv50aDD7+99mymb15tJ4v0KaXYvPA5kfjPg08le5lwHUKjU6uYCI+1
sZFKN4aAu0n3VA34bBvTAQa2/NAxHVed4/epOH1jgMNJfxV5N5Pk65nPLy5zWsNmfNTtuEwQrvfm
5jtk95g2Sb5B7pw5mAc0Yy7f1LOOlIBG6++vqbY23rmbPxlmYNXFumfRlmDiNlcugj2Ge63bx7sd
VKk4ovMSqn3TO1gFPNoQy0hysXZ9RywglYex3Adf/eCghF7gjfkoLixYd8rGxyhCBp1cAPmPPBGb
o57GNtQdPc3rP8/6P7yJTwzu7JU6auy95Uyg+bevQqNbLBs3zhfBZAdy+5sSPpiDOhq2GPRN/4I5
csKE5yrk8grYUM11Z20nWQaiY7B9z6reMpDJ+kWUqMt5WIkz8xTaePzXHcnY46GjI1Wxb4H8E0DD
UV/sh+uPi99dfgzwkxs/dzksm78j7r1IqXtK0L841qOXiGtCdRD4GaffFvyBNLcJiNyTSTS0eZH0
gaXGjHBOivUD/eDPImjZTFhXkkZeetKNeJKMjB6dVVvRngCUNHLLc1gdUZaYBoYXkS4/lazLf83w
olF6TtbUt42GycwxlPYgTtdbHZAqlnQiZ3AYIjo0GVyIe+9yO1YQCA6YrB9ZZuZoodsCLaVPDjr/
VRBwaeiUJy3N84TWHt3nWDFQ4HdAlWlQ36PZET358iX4wNFcvItlRhw8wu/vzsVilb/PGUTyygUj
sTaeuNetitlTZvAKbbgCcDXWMS6xBlbsGGGYA9QD2Uej10xCMXwjT9DNOalkDEZAujlkWW0iTfXE
ej7DN62efsmpLbx0KT79RP0yO84onUFTZu4sbaNU+FOzAQQEuhM5Fy5ePC5jSw1GdJvErBiYKG6w
xJH8IDWTNw7hXHHuyU0yGkvekjA4VP2CLCaSzGTr1gP4GohGKQaV+S6TaAYwYYt9EbrxfHbmjLit
0coAxOJ5+rwE9uxdBdraQLbzgRK/yEr4KoZt9qMKMFqDoz43uNpXcdubsroMl+spmR58/FX6Vzhd
tyzTz5vZoHTDFP042f7uyETYY3j7kpO1o27zfIXnbOnGLoJQzy02Rku+V9PwLGScZ9YpACmrsLKV
7B2trVTAp7LJ/TNhjbB+aIslKvopQCXiuX4BDxfphDmikF5y4yn3XyvhQtvA+Zpn/lkBvcbLexlu
FCK2KThZS8fEHYSIKhHKLBguH2SIkg4Zp0xKYZgRVU+GheLuqUJE1cnSkDtRsGWHRcq8
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
