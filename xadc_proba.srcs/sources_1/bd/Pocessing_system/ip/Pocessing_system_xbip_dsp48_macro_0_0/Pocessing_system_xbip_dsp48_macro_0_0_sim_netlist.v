// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Thu Jan  9 16:22:42 2020
// Host        : DESKTOP-8BPORRM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top Pocessing_system_xbip_dsp48_macro_0_0 -prefix
//               Pocessing_system_xbip_dsp48_macro_0_0_ design_1_xbip_dsp48_macro_0_1_sim_netlist.v
// Design      : design_1_xbip_dsp48_macro_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_xbip_dsp48_macro_0_1,xbip_dsp48_macro_v3_0_16,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module Pocessing_system_xbip_dsp48_macro_0_0
   (CLK,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF p_intf:pcout_intf:carrycascout_intf:carryout_intf:bcout_intf:acout_intf:concat_intf:d_intf:c_intf:b_intf:a_intf:bcin_intf:acin_intf:pcin_intf:carryin_intf:carrycascin_intf:sel_intf, ASSOCIATED_RESET SCLR:SCLRD:SCLRA:SCLRB:SCLRCONCAT:SCLRC:SCLRM:SCLRP:SCLRSEL, ASSOCIATED_CLKEN CE:CED:CED1:CED2:CED3:CEA:CEA1:CEA2:CEA3:CEA4:CEB:CEB1:CEB2:CEB3:CEB4:CECONCAT:CECONCAT3:CECONCAT4:CECONCAT5:CEC:CEC1:CEC2:CEC3:CEC4:CEC5:CEM:CEP:CESEL:CESEL1:CESEL2:CESEL3:CESEL4:CESEL5, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_sim_clk_gen_0_0_clk" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [15:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME p_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency width format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency fractwidth format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}} DATA_WIDTH 32}" *) output [31:0]P;

  wire [15:0]A;
  wire [15:0]B;
  wire CLK;
  wire [31:0]P;
  wire NLW_U0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_U0_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_U0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_U0_BCOUT_UNCONNECTED;
  wire [47:0]NLW_U0_PCOUT_UNCONNECTED;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  Pocessing_system_xbip_dsp48_macro_0_0_xbip_dsp48_macro_v3_0_16 U0
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_U0_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_U0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_U0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_U0_CARRYOUT_UNCONNECTED),
        .CE(1'b1),
        .CEA(1'b1),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEA3(1'b1),
        .CEA4(1'b1),
        .CEB(1'b1),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEB3(1'b1),
        .CEB4(1'b1),
        .CEC(1'b1),
        .CEC1(1'b1),
        .CEC2(1'b1),
        .CEC3(1'b1),
        .CEC4(1'b1),
        .CEC5(1'b1),
        .CECONCAT(1'b1),
        .CECONCAT3(1'b1),
        .CECONCAT4(1'b1),
        .CECONCAT5(1'b1),
        .CED(1'b1),
        .CED1(1'b1),
        .CED2(1'b1),
        .CED3(1'b1),
        .CEM(1'b1),
        .CEP(1'b1),
        .CESEL(1'b1),
        .CESEL1(1'b1),
        .CESEL2(1'b1),
        .CESEL3(1'b1),
        .CESEL4(1'b1),
        .CESEL5(1'b1),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_U0_PCOUT_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* C_A_WIDTH = "16" *) (* C_B_WIDTH = "16" *) (* C_CONCAT_WIDTH = "48" *) 
(* C_CONSTANT_1 = "1" *) (* C_C_WIDTH = "48" *) (* C_D_WIDTH = "18" *) 
(* C_HAS_A = "1" *) (* C_HAS_ACIN = "0" *) (* C_HAS_ACOUT = "0" *) 
(* C_HAS_B = "1" *) (* C_HAS_BCIN = "0" *) (* C_HAS_BCOUT = "0" *) 
(* C_HAS_C = "0" *) (* C_HAS_CARRYCASCIN = "0" *) (* C_HAS_CARRYCASCOUT = "0" *) 
(* C_HAS_CARRYIN = "0" *) (* C_HAS_CARRYOUT = "0" *) (* C_HAS_CE = "0" *) 
(* C_HAS_CEA = "0" *) (* C_HAS_CEB = "0" *) (* C_HAS_CEC = "0" *) 
(* C_HAS_CECONCAT = "0" *) (* C_HAS_CED = "0" *) (* C_HAS_CEM = "0" *) 
(* C_HAS_CEP = "0" *) (* C_HAS_CESEL = "0" *) (* C_HAS_CONCAT = "0" *) 
(* C_HAS_D = "0" *) (* C_HAS_INDEP_CE = "0" *) (* C_HAS_INDEP_SCLR = "0" *) 
(* C_HAS_PCIN = "0" *) (* C_HAS_PCOUT = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SCLRA = "0" *) (* C_HAS_SCLRB = "0" *) (* C_HAS_SCLRC = "0" *) 
(* C_HAS_SCLRCONCAT = "0" *) (* C_HAS_SCLRD = "0" *) (* C_HAS_SCLRM = "0" *) 
(* C_HAS_SCLRP = "0" *) (* C_HAS_SCLRSEL = "0" *) (* C_LATENCY = "-1" *) 
(* C_MODEL_TYPE = "0" *) (* C_OPMODES = "000100100000010100000000" *) (* C_P_LSB = "0" *) 
(* C_P_MSB = "31" *) (* C_REG_CONFIG = "00000000000011000011000001000100" *) (* C_SEL_WIDTH = "0" *) 
(* C_TEST_CORE = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module Pocessing_system_xbip_dsp48_macro_0_0_xbip_dsp48_macro_v3_0_16
   (CLK,
    CE,
    SCLR,
    SEL,
    CARRYCASCIN,
    CARRYIN,
    PCIN,
    ACIN,
    BCIN,
    A,
    B,
    C,
    D,
    CONCAT,
    ACOUT,
    BCOUT,
    CARRYOUT,
    CARRYCASCOUT,
    PCOUT,
    P,
    CED,
    CED1,
    CED2,
    CED3,
    CEA,
    CEA1,
    CEA2,
    CEA3,
    CEA4,
    CEB,
    CEB1,
    CEB2,
    CEB3,
    CEB4,
    CECONCAT,
    CECONCAT3,
    CECONCAT4,
    CECONCAT5,
    CEC,
    CEC1,
    CEC2,
    CEC3,
    CEC4,
    CEC5,
    CEM,
    CEP,
    CESEL,
    CESEL1,
    CESEL2,
    CESEL3,
    CESEL4,
    CESEL5,
    SCLRD,
    SCLRA,
    SCLRB,
    SCLRCONCAT,
    SCLRC,
    SCLRM,
    SCLRP,
    SCLRSEL);
  input CLK;
  input CE;
  input SCLR;
  input [0:0]SEL;
  input CARRYCASCIN;
  input CARRYIN;
  input [47:0]PCIN;
  input [29:0]ACIN;
  input [17:0]BCIN;
  input [15:0]A;
  input [15:0]B;
  input [47:0]C;
  input [17:0]D;
  input [47:0]CONCAT;
  output [29:0]ACOUT;
  output [17:0]BCOUT;
  output CARRYOUT;
  output CARRYCASCOUT;
  output [47:0]PCOUT;
  output [31:0]P;
  input CED;
  input CED1;
  input CED2;
  input CED3;
  input CEA;
  input CEA1;
  input CEA2;
  input CEA3;
  input CEA4;
  input CEB;
  input CEB1;
  input CEB2;
  input CEB3;
  input CEB4;
  input CECONCAT;
  input CECONCAT3;
  input CECONCAT4;
  input CECONCAT5;
  input CEC;
  input CEC1;
  input CEC2;
  input CEC3;
  input CEC4;
  input CEC5;
  input CEM;
  input CEP;
  input CESEL;
  input CESEL1;
  input CESEL2;
  input CESEL3;
  input CESEL4;
  input CESEL5;
  input SCLRD;
  input SCLRA;
  input SCLRB;
  input SCLRCONCAT;
  input SCLRC;
  input SCLRM;
  input SCLRP;
  input SCLRSEL;

  wire [15:0]A;
  wire [29:0]ACIN;
  wire [29:0]ACOUT;
  wire [15:0]B;
  wire [17:0]BCIN;
  wire [17:0]BCOUT;
  wire CARRYCASCIN;
  wire CARRYCASCOUT;
  wire CARRYIN;
  wire CARRYOUT;
  wire CLK;
  wire [31:0]P;
  wire [47:0]PCIN;
  wire [47:0]PCOUT;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  Pocessing_system_xbip_dsp48_macro_0_0_xbip_dsp48_macro_v3_0_16_viv i_synth
       (.A(A),
        .ACIN(ACIN),
        .ACOUT(ACOUT),
        .B(B),
        .BCIN(BCIN),
        .BCOUT(BCOUT),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(CARRYCASCIN),
        .CARRYCASCOUT(CARRYCASCOUT),
        .CARRYIN(CARRYIN),
        .CARRYOUT(CARRYOUT),
        .CE(1'b0),
        .CEA(1'b0),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEA3(1'b0),
        .CEA4(1'b0),
        .CEB(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEB3(1'b0),
        .CEB4(1'b0),
        .CEC(1'b0),
        .CEC1(1'b0),
        .CEC2(1'b0),
        .CEC3(1'b0),
        .CEC4(1'b0),
        .CEC5(1'b0),
        .CECONCAT(1'b0),
        .CECONCAT3(1'b0),
        .CECONCAT4(1'b0),
        .CECONCAT5(1'b0),
        .CED(1'b0),
        .CED1(1'b0),
        .CED2(1'b0),
        .CED3(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CESEL(1'b0),
        .CESEL1(1'b0),
        .CESEL2(1'b0),
        .CESEL3(1'b0),
        .CESEL4(1'b0),
        .CESEL5(1'b0),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN(PCIN),
        .PCOUT(PCOUT),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nT1iDpedwZFVkRSZDJusiwI7kFIMBvviCRm9M+pZKTgQdGFO5jX8oqNrtlexCu/uDfp0YQ+QGyHf
W9HJmnELyQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
LSiX96nVtTeT6QH6SYBUiN1RW5Mga6q/2lxWqXdOG38n69A/VIFv4MZSHjz1gILFox9JEY7OFwGs
6ebz/mUxmwP3DNumoccQ6uOcSkKQV1eRSlyyHm4UhahbN/tD6kRdHgTGQgjiOPFINjK/bQof7LKF
xQMmQeb2+71XHcPjUHU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T14r4uT0q5iPsUM9da3RnLjqN8Qn724f3Fcj5n9r1n/OCu7B1m+A10bBZuAn11d+eTpUOqwU/X/p
2zzSaUcTE8ijWpgSLXU8J/0wcBVyuWUHOoOpFIkqda/gzGVSmbiUUBGDhktV/P2ktOR9PeMW1pHu
QeJD3NMerGL8xO8RkFz8+37CXz+yNeWbl9EKsnw81po0312geoX3g2TFZsqRUaRMVN1P8+qQzlEb
OAUU+/BYNrtsGGxq57Lea7LASqCQSI6ZVYSocjpQzYz+zpK1Ifn6KpwvU5YLStgDnK95pF56yxWy
4DsarUkJGiFZnz4hzdYJeRLciFb00Y7Z7OHKXQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JB9E+rFzptTgWubhsk/ytb/NrSJBaKLviXMn62i8KWfOUbd7Q37B9GOtkDXor5Q39oNYqlzgkXQQ
9g+vxtDNbMGPBkiP8HfN2tKmqAP3203t/R+B1D0CmN2mK9Bzwi5rAw0zNBanLu0Huhygqeuyv4SW
RjQSZSiUCtH8UQpPnwdKQSS3zlTnpPv4po2tgA8ZzjRNyXUAFGD15dFRCsv3KN9TGY3ySFrBZTpy
ddZI86gPVOR8QamQKAtVPZgLCYSIOtqQrQOt9c7yM0NqlnlC0kVD8X16GQ8LchOJaRRndKljCiJu
T7V6wUYHHVdREAeFxWPEgIwm8uncarb/xI/YFw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
hiRSLr4QLw5/mMP2Zn25/s5s8AF5rzEvu2TjIzKu71zUg0RQR79nm8y7jnlLFI54qMdeDd0ag1F9
TU+c3zvS4L4EyGAGLDGmOYcQ2klSCEkAp0eYHfZNyKQhLKpfpdEXhwpsfAMa8mfqBL6skxrp6C+l
wSbnOqvq502wmvReAdkBa7hQBquCP/Kxu+jlOzeR76T33fKFxe/GKjVFC7CzkdJFg59HGnCzg15A
KPrAj/GAtXhrFFCtzppSIgO8GnVXXMrxXlQOTW8Pa8dpXzVVlhWlbclRL5vPlMcPuo76TstX69zf
yyp3rGNQXyTGQn2cIxCTDQ183lOjoKza3cx3JQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
YGcCI/CcJmhsdgWdOuARrKP5BvDGllkS2MoY0dfL6ioXfX2lO7pKY3qpVerntGDre0ZdXSkxLBW4
1veoXYSLGmDdonWSixQKLqlzm2MuxscRuCLcic/Y885s9obEV+bR2Ys2BljpSBpVcE/Ur6Ywxmzk
LxfHQW2SwTpLvo2b2fY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Qfahy1mSmZHw7posW16zQRrSI47b5EnD2EOzgkKc27KVqFCtYxFhu2K8HcIi4g9qHxVkiuCMS2xv
+leE7EvRlzy778OaDw5sNTj6pKXuDNf0TM9Z5qWIQfZXHe1pN3vk5+JwIPlnKOQNdR/ZvyF/MGlN
OiLTikOABwXxl8J3xz7JkKAD22NG7mPIcFEx4r+67vvFAsaNrRdR1eeZqoEWtdnoXxed7RU4EF+M
gRoH6yIiT9Y1/s6OYskQ6JtiRhnYtAuCfzREnZAh899nzaIcLd7LEVfL5Iz9Ugu5o0kDqSWTin3h
e8cg4A7UdkCUVgAKEJvninJ2KykH8gXo3fcIvw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iH0JES/NvEFuTjt4he9VbmbaRMMC3Az+avxSvav5homUT4AN4kL0Mr4DduP6qq2S7yA9vmLjymM3
vUy0LCnW4MRJjyOhZ5+lKiHHna+B7i67+QBNgfHJZBo1o/MQKyhRY1NEPO2OcbO0Fa7vWxMb0fxD
tLYj3lDptr2t6lE5QyvfRkm0+K1QUewQYhPoottRx/YRIB1HGmuGxAGvtktBkl8YKImYUAhUsulz
EIDhzDvY2mjO2Xcjs4bARB39XehWet0GgDIyU7JIse7GFBpk0KwuFUmt8GyECacKOV3tNHp0JNby
fTW1A4Q/icYz09CCUtWXTEHhWrJdwGru1IPXUw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EJbk8tLLjlKBWWzy9ed/VxLmGUjR/VyjMLsCneAE+j84oRp+KvuysqptdWBdiS7vf4A7tBZ/bLRt
i2XAajV7I+0PfuDBovQu1eym/cWgnSSLDzT/4VP1p2nj5nrtcAn9tYUy9j2vRJKggOQ7q40vZBV1
634FZbpF3fZnOeqhBPy4C+83B6JqHPofffC+hmvoz91FfJThjOv67xFe2cyVlop55qBbOiy+dgF/
gFoSvZBYPkujshJPw4v0bGdz+YfFgPqDQjQUHNRLlqRKte12d/jEOfxr3FTz8UVNsigk6EEgaP78
IiEcx2HkSGbTNf5Gv3xxX0JktYxSp2ciWXghxA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 30336)
`pragma protect data_block
OA2EgJTs1tel4lSCAHubb2c/ClOLJ55ajxz5q88NkzRKPProLwaTC/mHzNLLiLscA5Wy8ogP1Xbb
lZ4XOQDxRt/PISfrfErMYddIMv0i+bDxL8fwIv6j+LiK91NiQeBBzLUbC0GUNlar7156FK4j5u9p
GbH6HDmTl9gQlYWIU4EMOkTJoT9bfr52Z7/3p+Rf4U95mgnJpWY1mq5SZGFP3bIQGDjTtgnhjObt
RyXrdHC5iReW08nfC/vERiI42rFbRebtoiqqe2Df+SvUn/4rcz4rIq/l/N3u2T/eOA0GLV+xycyg
MaivbDLgg8HJoVoMvefH1TcwOWRpHhKxk853OojqZ0USAiwy0a4ZBY+3RC58h/oHeEflVmdUFOUY
ecpZclpgkNm48gE8UZqC+SinOQijitf9+p++Lcn4RLrwdXFiQfu2ldO4eIqtXoSNoIyVTKEBGae0
YO+TD/lMKQ2H4gy39pVnj4YiYzdeUEq4meNbfs+nCTdISwoZdELDySycXsPFW1i/DkeBNX3j+fUC
0zHVvFwgNlMQLIPhHRvByUzajojivgvE3PwZKDIZsFy5kmFqFt+3eX9eU89tLFPakvl3D+/ypRQ6
1ZE1kZ9/EtrxlVZbbymy4EkY5yw376Emrm+A+Lp4jfF/+nEmO72wJMbmzIoTDuMyEwhaqO4SCTTx
oAb1lUkPUyWgVr/WclXJXlrx06OM9v3FJ8+BqYasxwB+XWfiyMgx+J4Tnn8UvFZnURIv4YxO+lNz
+SOiwwMIl/rwfxNX4qSydaYAujYzB+did0pegToMCV9zBbzFKJ4LlffqDQIDMg0GEvPyneLGdW0u
5qSC9yJsd8+XtvLIHQVxCnNDsz4YxXU44KaZW8PA0vYzGG6XjvL/65l1lutSWUVwUxjJokAk3GFJ
TQ5J3uYYRLGP3I/zsUZgSn8lFHhn0NvG96an6NxU/lP9uLzGW3VufFOdF3GONkkO7731ylw1Cmei
o4ctqI4CBqjKq/FnzhLXgNuG9zswINDfZ2OZ8kDxUvSQnB+eIj8UsRCQe1yh9jflbjjJ5/lNa4v/
rIEEHxJ3TpG9LDQ3E3kUysGL5Jqmu7GxftZ/W3tzzkWWId6ZHRNlE6AtRcpkDS9lmfyMOCuijIRz
gzE+VDsLzrj2+eLH81moeAtHTUCatXj+8pSATA990W0AVwC++YjcUH/b8Xmi0fj9pJAkOJhLbMJE
jEaGgYyZy4Kv2pHTKLDMMDWTH/NReRQ7h+o/6O4ThZeHcDCQXf1iQNtyLTxltcS79NDtS99yoqNF
HO8GVWKvry4z6S8bwW8+5/aGaFoNyR02/l2aZ3eDlQEFbo1hyNuBEicsE5+clbhPJyttJvoEr7Vm
YcFtAdSNWDiKiK2BkiWRPoLEECujX8+wvqNTwPNnm/1nc6rnyvsY5s9U0Q/jNlXyWyEVnnkKO4R/
4JwvRBxdA89ycwRi6efRVQvSoW2Io67qonaZaKOPT6uMrS2l901iY0ubMUs3a9MJ9Qh4rxfgmilP
3PQNEfFuIHZe+IpZ4DaVDuvKJYv2IrEt4mRnqIcbjSo8CbD7CKhHCKoWJVnIJtwB650s7isDGpF1
FszcF4tOlV5+AuijYuvgbQqPI4C+uEDBTKg7aZXZXBZwYkg9zVfTMf2NpMjgr0/1WNFq/Fp+PLPd
/4cETE84FdSHdbaDylDQ3mN+lsmWe6jC2d0P2rrEaXQHJeMZ+Wpb0NpR3G1UJNBjWK6sMSbnpR9a
MRWSeC/7SSM/0aTv7struIWFxefzPq5J0HxKAAsoub6uzgym4sM/v8aww5yp8GGKjSlNnjr97QQX
+x6a//tp9P2JSyO/zxitEoudWCCwVmj6pMBhElif8yLm9uWOCkwVtyMfYEL8Ai1QKNX3kBmxdbIs
nrVNsKKkd2LRqKlVzUmS6do2/eTcrxoswcZJXmQ/Fby3suvlDGALCtFrAdDY5p9lm3W2dlnM/QxQ
KvskEPwRsB0nklOkwhC5KdffSf0X4hOiMaFPrRzCb9YKo0TeNbP99DL6yUj192uVIdEpf6/6fpvU
qnPNVOFfES/daQjz5CC0jBnCGXDOttZm5kSAN/snR1moyYUfhxbeE9x3CrsSupTz2zX8oY/c1ibg
JUa8XDGtp7VZFpQlJneB9K9qH+v7MDGogGkTi+qxkhIJRtiBqXsbELcqTUyxR0yTQDhGznvtshoC
03hoKjN7e3f7SpW2AtsY2on6ELkFPSQkltv9DXQg6kd/VZloRDW/AJ5qnndraWl/ncflmSRoz24d
0Snmf+atMINTUtMTnTzE2oi8+/UosVVEU1bFpwkyQmXucsLafOEfPUncwlVe4JeIs9ISvAp1KwLt
PF0bi/i9JDGy0jfgUChqqteWn657JK1radcB8zFBqDnetJnkKOYu2Tr5gl1wtNqsCmn5V6uFNN0j
CfsPpjal4gogssgK/CJDsH6RO0EmeVe5Er70K8frKmytaacv5KOOCBPWvAXXHazz58thsNNbCq/j
cETSMBg6HGX5OUPeNB3wMWVLWPguSduBb6kv/UyZHsEWEdx9nIs2QirupfkrrnP18fj+A/Y2vwXD
+91syi1Zkoe/FZSyRvWObJe3kjXBuJkB3XoNW0vMh66dNSi9Gg+3TuEJKty91scaatZq146N+K04
hdPxq1vhIHgGz/njY8Rs9EJyjPBXa/scgzKe8P0TU7zPXvI1fLdRMTtjR9XG1O19IGh84olPDXJU
YSTrXZ8HnDbrbhRDMMLMqAWTOXL/7fNeezsRTbHFh5FatiIwZGUB8Ev0CzPlzeL9Xh13Aeh3sDPO
dG+/MmMGy5yejxu5iM0jwalTSLuE8mHJbXT8U+JPUlGOphA9gcEfuYQTMF2rvt7pK5968v6HBIXi
rAvwOS8DhjggkS2pt2kDTrIWS7rdocdB0xJGvvwM5Ii0tnfUi1KUaNoTvZHz6M+PqPGoLcstwLPN
MvB0RGgBS1TvU2VFdroSAgg8wXosRyFOpE+1lDjSfPw/9ebYvgHZoDQ4JqfQzKqRHhoM9T0qGUcM
Eo4OXIpsr6TP49Udj68EOco7bt+1K2RR+Y+Zonykz2s8GUa0HCfqedCkBFYrd/IR062EEouDIAbT
1L8zPHm5dFymhsRJq72ZjxfGVvUq+nIu7l11XhCMYn5z52i/fEhwvfAc6OGXFlS+EAe8sfg4kYj0
lLyGybEd/YMNUqT2xoM4u+IknzZC7YmCLWPy0XVKVyC2D+A7Eobesx9AQw2SXyopZsiWM+cyVYIj
5ciWzXFLwKVqCFx+h1hMDmtrr1vMCNUebKrbjxiJiDzYLqAdNTDEh5BOuQUGdQhayevdVT6DQPEV
YFEemzGRQMqXloIh2+hzqHrFowrVHftbh32QFB2p42TDvWCtiFsMj8G7d4LI4iUFsj3fF+6o6XmB
3Zw+LQdH03/urjp0PLUQvfoVxikd3Q3hWBVs/DRDpEyHPUAV72vC4NbrNEQeBf6TXSnc4uu6sd0r
br3+pPSQPUGRso0ZbrNU75+JhyytNr1lN63nUyej5t48xw3/hMAPHCv2AnnHDNNZvIwfJ7l4PfFo
aBvMdwBcIJxCqmhX/76R6leQNk9npJ7R4DsiqTGk9OAzbHX0VNQWABo5tS/4L+Uk24fZqw5IPRRB
Oxozc2vFuxgLJQbPFMBOGTYBMQQGxBvjUSfeiRinfraJ92N375Wrg4OK4biLGH710QS5ij62seZx
ZicrelfKa7ZZAuex+UANSnr7qPU8HD59YPBZKRRZIE48ZZ7dATCkAvD+ovh83+rIcsdkT4u3Vw7u
Y19+y0h5Rfgvsg8qBdMFmtIVmBN4Yyr+J/BiGZjhdMtEnMJyeR239+2R0ARck1v8Xn2EVS/7VjkA
GS2jqMH/HhnxjtujcOZshWJf1zBYp1BLQaji9QPmOWdcMLCZpBeF6t/+upgaaju5Iv7NZmGjXkoB
DMfl8wyiH6CyvKgGX8BC57D1WIIWns2fF6ihf8BlRCVYO+KMvPLvFJ9tNJi/MJzwh1KA/FMLFIJQ
vMxW3u20rbxEc5r8LE7OI833fqZRln8g6Hc2MnzIztcY6CQS2X5pdjUSG0pN3zpGOoWj5lTBxuzG
uCSrHNtTTDkKpi3trul3mDiW8EqjigjPG1vpq5+L53Ozr9Kq/uCs+N7bgLefo288MpzV+ePYP+bw
vBrsZdmesmPAyQfRN6ytrS5Y6KzbuZIy2BMifyIRi6njpBYUFYR4NRXxDmFNTgNPqg9+SpeeDDft
89Tl+ZiJ0MxAikSaZWW+iuyEu4Hs0rB5mLCmo2PQ15YPXAPdS9PEEwMFKrf1H4AgQoWSW9yz4uTg
1wReUG/PzWIwZVeWlpePqU/BINR/JHV/IFspdVxsUAxSPACYryvWnFotjXOBokJqQpeelJwP9YME
eb/Ii32Pu5L4WbLWimirxV3WMR/gshQdyAtVuHYkYzigHRaQLl9sVZV0h3ZwBNPCaUL2j4QxBqHo
MDWzh37dR88ZMa/p4hfVZHVsQKFuZ08AWSum/2mUvS+j34PenLtjCLtrF8XAohq75u3TTioXQoLV
l4oexIsb5Fkh716vEjWuTqyRdr8Gz7JULwwvLs2ds75f03YJ9A4l4TPet3h+c4WKtOXNE3fPggDE
5sVkW1m3f6bWyVjXBuuAiAoec2VUkiiIUKodR+LKR0441c0RUlU1GIg0SmsDt0LCj4fzSVXf1z8w
MdUTfThXuPYBIjwnXNVQutS0ZWh/qPsT9Xg44EYi94SLnSy6KMGT+C5EDZKLSW/20s3b5nVKxo6e
nh0PP2+z51fVbmomSIsumueMxwGPvzUtO9tigA4gcRSCwEO4xdqHjL89hz9+1xE9qs9Ti5QAaxee
0js8GY5L+EP4WYSK7BhZxd3VmdQeU2UTcR6fG3ODvdHbTlHZ9KPggaghWOgMMcXHDNlZ32wSv8IZ
ziOqLIoriOXmpB1QC5LNinK0xhkYVmi/jnedsT4eCWaUvcr/tjfFmWOtZj7kRYHNQXG3RFuXrcMH
oh5l/G/zrRL4umut71c5OctSuZiWkUf/zKFLBBRgiWOhAdlUrDICSNk0CzvVPabKbkoLICp5L4nb
mWf9M7kIIDEYJ3dR2G9cUSRJsOjTQR8I58wJ0l3SdmceCIE0UXn8aoPHbw6+rBU294Xq3q1sOUKF
+LhvIslUb9oSEQ4iLFoteEDajZCIXbHFlN90o7l2GO88OYZ30BMGRUiyZqIaZT1q5lQY71hBCpo/
PvVKmQqr/+1easkTYxVH4DNchbItE5slnVrbueypaG+scG25nlxXDnJc6sL96PuVSYx6Rqdmp+DP
slZfTNVSp2bWYF9OPxXfd2Re3OtKHHKraAq5AqXxRURNg8h0sZtZKHT+Vxt+DCeCkawUDYddeP7m
za9k1rnJ4O16XKuLMSUsZ1JiHyBSTYzCDQGmhzcjtUpiOHcb4xQCDP7nVN2BRz7WNTVn0Mnw6ZwJ
77IPjxM+tdF3Ehb6x164jD+NE+WniOjnYPYC1GD/pPaDVyADLOL1y2tVnp/Xm7EfFVLeBstUhca4
mrCCZZHxW4yVVhzH/0h7ztyOZYLeAs/9ZCeSLhnOT1XLYwLUvzdCNOJiPomQofW45Z06ImplwcAo
9mcev5TIPowWxZACV+fnyhJbK3Dj97K4y4z2vCaE/oRJvF/2RBbxzdDefBVV38YtjKNpS6EgQyiF
lBhNr/PEUQu+9OIsytnuWZP0pxF1j9ZPaxe1CKqkbJ85iA0mOKZkCOfMbtGUWw/uSlXF/vVmon9j
wbrbQ8IeEALRiL1cYKnxkbs4ahtNjMBmXzzbJt6r/mj8qz/9u8FAkWpNp2QjkU8G03p1U19qg+yc
uviH0zA59pgrsZ4JO132cTEa39iloSSRskbB47RneMyufSZlzRLHmypz0M6G8rj0nfswBZbVCeFx
AhIDku3RlCKG3irDpWm6iONxOakyKgUwMR79TEeHL45gXSdgUaXmnGEyjDuck3dxG9NHoh1WmL10
sTNrMTd77uSiH5bavn2AaKhryy8HvmjarBQi4b2jaAv91m6WVGtFYJv3UQ4vVATqQM7kbhx0M1gV
JOWTjcnz3TVlVbWdaRpagwEeycIwLBUdizwkFFpiR8ulyozHcqc8dU5DU933cfBz8KmyVw4J1XLB
EB802CuFiWioxT7NWG9iNEMXwFXfmMbWHRRHx7Njt1viNYD1iZr+Mjh3uwaAH/f2AoB2w1Vh3XJa
cnpe7oMjWfHnSjiFumhoCYGGX+YUqCYLCSEbgcRUiSKzU49wTeOr4BWX8p7JjxtPLnA8obIDMO9I
zMlE0f+peISO708YYmatIqvNvtdU55qjN0V3hB04JZemLIXK5w1kLPUHztDds1UQRs7T8Z0+ZIQB
kLFjXKCoWNXJCSZTEekeHOQfNWdBlQ46bRvYa3LQ3gBN4IfD2TrDCZyZ+RTmlNgXGuzV5ADRHdqD
jtPUc8cuEY6U0jeBw1PqrPYrgXITo1o6WasyH+ef9BDPAxo3Nt4mfn6MZw79JEdHEA9ppivr1kR4
DJU4BEAf5ka5G6KP54GHLmR9o+nbnB+aXKChh21/p11RVFd2Mlr0iBYVeClRVNzd+345vknOb17g
A8rDA0bUcHAjLaJqbbc8MNiRRGhg+zSOTna+zJUAEnO+rHIdU8hytHA4srV6CNgRDiYZqfysX7G9
r3dD29QOkkTGGpsRcryGj40ZP5OZcjWETb8wZC07UWj3EIkPUmK2PYgHM5iSZktVHdaBhrlJq6f3
7qLVPx/fWfEtZ+HGI7wcrhidBLkO0w5NzMoPFoXv3jLyItqsq8vxmRpQL/On2CL9FuvXpj1kkTu0
ZBiProjuFCXUSL4obPy8TIJ6i0afMlt7nwDPWNw4XwdsjYDVyUfkzM8232nq48RWcddpq3UGMTRh
MfxEooHHPXtWzrn2aobzNwJkojQavMj+cFycS4783Ae5J6KGtHWZa6hblyJ21DuUyKnbemnr/fnn
AsjJt8pTysskKLUHuh3+RLKOewYJlOPuE8W4LxukkVdCsLh3v6FKJwKO2UEL48xsFGR5pY+LfBTH
sugoiCOinB0jJqWbg6t6s5eCpcBnUhWn+tz7YPlfYtyEUXKd5fQClB/vLxwVlEcpYsJfyW873tVk
ACZGZ369S9Af3dTzuKKHX8vY7fK7slLepuBvIEhhUwtdNZ1FrvrgKeXdfj6KV5Y8ZP+wLlB745xu
g5yQN5QROfanwUkoxjLRsEeT47U2iZRjl9vAqvGPFArJJ3n6+++UXHoPl5sJspTEG+AgnOfvQ76/
vvo4odLk+beMPzaQU2KRRbgBUz4cEPan+od5B7z50kr+q9GeYJXtCq80a579w+khtVDKeQ6Qfz6I
2/ERtUvqHqSTqxlcNvB3o/hLRy2R8O1ezmI1bqfxGwb+xAmGke3yaDER3Lr29ZCj9wJdkcCIvhlU
oWgWCq2Wa+GLX0L7xDKD6V34SDQHCC5NT6N9aOwnAYR92RRboVFoTlKEG9fqz3DPvhQtqOvq9VL/
s0w5zNUc7M0dPmVHAIKbB16gaPhvPIRwRMrcNy4erqOpqgiMsx+4IEcJEkA3rdBekdjYUIftvi9l
mmBRHmNFhb0YAjH8zG5OabHTJzpnlLH4BrxnogVVMbbhwDRWYSCApENs7Iij+irkXU7EzeHi3PW/
oH2SohNbedRvlKehXVyejIXiFl8ik1AxzPLUbu/ybFw57HJ/5lBGQRuhUDtY4pjnShySyD5Y+3mm
2KaL4Ied9ZQxsFVNF1UD5fK0T4z/LQssYPFaBSPcEEuoLqUKF37HF7qhtHta++OsKoOblU7pmLnZ
p7jlK/ScnVWsVQE0iEuOcdv2CbNHd2Qt4zXxlwyuRNmFxEWxDrQ13TLfl0aleBfBJo7ZSjK1nRgk
TIBTL2CyDqdaI9ow7hO5bQn/UxVtEyS16v0LtirLrLS1PvCWIxkqpM00fSgpRJ83nJHOcE3k8cCr
N6aZGuTWWoKN9eWumBOR+p/2TyKO6j2snOp25Ib0HW2lnl0X+r1hMAaKKxOYphskQVal43jzzRfK
w0p+uwV8vGYGY5xpE7pSsiJfywWqnp/D8PfSLkM3agRPNUyRa/sTUFNJ+3AuqeyjCp0XYeCapOa5
4t+awiSUjRhC8jBhOVZR2iuM2eEAP//90Uaa+IpWZp4ECUGfOk2lKMCRwIGAhx7cd6NhqGhI7qUB
d9wfpImTkfU6+EXFvgSxcW4mRcBLcI8LuS/JKLjCmEQNalk3B9Iy17V9a/TIEKizFHM/prK4DY4/
9gFEgXu/hrFKwfcC+b9ACPGqybsJfxNx8RcqW3xgHIEEnoaMGgtHqHuo6SwXNyddbzI+A46JbiDS
VlR2vOWT5L9BhsHnnxNqGTulXoQxAp/0ed7WEABQAoZdXJnucLBVZY2O9I7DrcpSCyxoJzD+xxYX
0PAPJrQtn/3vxxKEg0GH0XSsdE8ETna+1N243/cT7oapdIc2V/wzmfQ5mR9KGofExmlypYskhIBv
DJfteua2GZUEJLFIGBzOcLNVcL2sBjAOH5vrWinoY/PgmoJTvZSAf4ZhYZrFXGS3IPXqWamkPXTW
DoTC3aBWWmrXt0kZ9IyydHuc++pTA4aXEnm1Vj1yMwxnTrcqaaoUQk6VzqZkNt/xOF+J33my454h
U16yAYfi5L7B5jtKE4RJQCmrKblZCun5afaC3mgZ4sqQyQHg7UdRd4pj8rF0LYNlQDPBwnE5vOGa
kJYo2rTVM4id7hOgqEAYzM/nAvalFH8+UJTFehlbPOsMIk0m0UmAZvnqBKiFi5W/PAw1qeJxGayp
RqYe3dy7B8KrDxewEDx+KMx65h0uXsV25gGc1w0zDCY1JYXqcpbYtnEphQU1tBC2FxqDea1UmkTO
oIQY/nLL1D2JB6unDrFXjPFyYVmhrdGUd5wzqhJ3iSQGforH5/UqXgv5nRo94LeouNr3UNQqsu0G
dqsX+H+pc1yMKoohjMlu1vvYFQ+wXFBmvDOhYvAZt/5/nuSttOBrkfUriOX/t0/hnLi7ja7myYfT
A1Fbgr9HTZVqMWHtHmZ20//UvqkVMDOLYSO6/msNJtU1DxwIRzskyPlsJgCVoSkUyIz2N2/691CN
QaTOEMHutymylIxOVZO1hr/+JoyGSRGrZ8nzAsLTV3AjYd/6BQkMUMH8e1CcOCcYZMh2HIFHzn/c
J5PctT0Y/A63wLSa1vqcpJamUyTVebQh0O67hdCkZ1/zQqZacUWfvX2b+o8xYV+aufYCtBscN4Qz
bZwhx9OuJORqUzrJnoedA0Lkb+CC1jO2TkRDP87XmYyu4KBkKFz4IOaiuTVFO8mobfVU3haIuWJi
F9GD5pM/FzHdz/KZG6w1L8WR13CId3RSnVhoxHNiU3ZhoLDlOi/YP3KWtocYE9MWSKJStvjlFBSg
xg77y3oCk00wAZUcAC0fQDkf1ndsxScnSkGOkoVWC43LJGo+YYbKKuKVixPB8HMx16KB+EFjyvZz
2Nos0kehnfuuB/LzC1QTLuEAiDwpsGh4JkDU1qyGeTDPK0O+H2ngHIiNX0CXml4oown0GGVPjBQZ
GgHtrl4RDVKaAC6SBwmtQaQm1ZsfbbJiBX6/exvhOxq8oks75xS4t8GrxqPKb1ZFOSRF7Nc/9w/B
A/t9aDo5hzJcUSXDKCteDeaATOcGg//w8xr2JTd/Kdl8JEyu+ooAW78vc8O84ZPlIzlj4uiFeSms
1AL1iNtZBeg5n3+APCUvqRO1k/+vqIMlg+UhDMp4ZILVzjYYvbgrEZ7tQ0m7P5SWCFUPd8FR24Xk
gEhbB6Hh6EgaQr71OVXhSwyUiOM9kLN8rnf/CQ/iMIqRWQHU/xf5aX8dnUseJhUeGasCXdUMAuuj
krZ9AIZwgN9IrjNPsgIRHPv0yu38nOqLXtBVWidM+py60kijqobQql1irMI+vpp5UtglM50bLG/T
qa/GOkJ9SgSNNxKqN/XQy0qLn5wgZkMD/njqnCMs7JaH+hrMlnYsMISHQPd9WKUetAUQJCHNJrkZ
AdJJDZ1JdK16MKya3tv7Lr2Aw+SXgTSLX9evtny7XPpvyikIlO7k93YwfjKpkFopDGq6okkMW0CS
3MS/WyZYlNqV7Cuc4yxZUffVpFiDaR6JhzJmzmqKohgxtHtRGrY+rR0THZzgbNYE/FASSoxawAgj
ZGwqtovsQa7hptY7a93isqGBiYDdonmPKuHHlXno+i3kZWiRVVmkFvDJnLN2t342pqqmmHRvayyB
XJWl4clHxh1xMrnPOTVAtqtP0yLl03TXdNM+/730kJAZQYUP0ar97xpdqIALKEvjN9p5BZJdXLyr
UJUjEMFjpo9pmZWgu59GTKGwkSYKOjpa5EXWBgod+x7GxBVolW3cJE43gtQq7j1h+on1cJ7LrTwr
5YkaFCbBVQkcN1yQpWyQ5dLXKV78cd1KnbFko3boQiQPMYV2xgE9C8qcS2s4k/Qa+0xd/EYXOUXt
ybHquhfQj8HZkcF4/YCkXk25XQXDAcoHIxRAT1qvwBG3lKWIkaBIf8tfPZQ97RAv8Gq211CxCC9l
HdjBRa7L3nyJREsb4lhIKWwj7rUjjhd2t+krK+yqF6BtlWiZr3ewuIIdf22tDYEnHc5aE0f9dwvN
XgPtI8HfsQnODSEr43nwaQQmfFDArbgD5A0L1g5aQL+7EdTSnRQ/64rwa2EFUUSf4H8e/FuXAlKK
+kbBp2Bq8locXaWuPgjIvh/eOtFzdiBKgryD6kQIB/Srg9+JgDHCIIiabVe55A+Qqqb0f1ZEgVHR
33uagjTbzQzEn71gzMcqxebGb0TuR832QHQgP+E/gRpcxFB970fEzMBtArMqX24Nik9tVLWqpZB+
RJkw2XEsfa7yvb7GSuMh4UYduMuvYA7YNfBNriFMHcGzpmpkI5eheLKozdiHj7PBfBwxDHlBgio+
IcCSVJs26Na5L4n8iWr73LWYuECg4lcEWkYxGHCtJBlB8cm3N7MMspkWNUM+ZogtUty7hoKXPuFd
Azd6zM+I9oFJYKcPy7Fd/8V2VYlHSltl8nuCSS07MGgP06usmm0e9GtAsbaCEy0UcS38w5HeMHpI
iInTsI9dz0ZJfVBGXXuyPr+0xiJIoO095J7JBaTS+mz3nRM9/G/d7jCjwfQrW5SEgMMc89o+Fr+k
ldg3d3dbuggi5Q/yJOoiSE+/TB4LuXACfx0TNdjE6sUKZZDqx+3dXi8hy4MaeZ4szowy/u6Ecgi5
UzA7rLWeqmROectRtlK4TxaoN8UHmR9Ik3hFi+NEHxm4n+1IOIc45o2g9fh7cSmevCmDqkZ9H8Lg
gTBEYdjdwyJ1gcNvEcnFNZcYEtOd1XszDZmZwFT06bwUgk4fbYyF2N6fVcNHdXMt13d3f4QWy1Ai
z1psdBJcWFKerJUPB8WZOhjroZsoNIAmNbCBRwp0NnTv1LHIMVZC9UJGK4+5IDNmu67+RGB4L088
iLKTz0U9VpLH1X7tdQmYuLogHZsjWka1af31bT62atNh68Eh7UZhPKBF3XFfaeQ9Czfbnj6OTpkQ
WjPhQOpT5MDKYpOwzmh1KeNyOLh97iBTQStdbio9s0CoUa0czcA81lgoUUdknKy3U6KWv8U9iBIE
N08FEAFqquMI8UsQa9QnTArUdAdjpt74qC17Q1ih3NvrVMX6huUcFEJYo1i+EiBGUlY+i3NmSgPY
4SAr8t1Amleql1h1TEXWFID+s8TRxeuaMVtfJDVO977NhLFZJZack0TSNOWAN8mOWVgTh9zNmB+2
VG8JveSUrcugu8+ygrmMl/YzeE+7hqxX8Ru9uz3nfophYLCTriG+NP0R/v8mwqCfdYnjeZjILb3G
7pElOm2kVJynuElX//3ifo2sTVVO+UPQ5jhqu9IOwtcdyFIHbg+fereI0GSUoFcbVYCl+tpOjIJt
Q4qQdA5NyT9+5SZsRlY3nKLe4+tprjwL0rFZ6Y8ZU+VMGrOHBZzG3URgNzfMK0tcb3joaJ98ARnM
DTwmIW5jQ83ExSKQVL0YeUhRwlgc92cngbTMYbBBC6qHaosBHEJ3NI3uDeK/+Cvr7RdFlI2BaYLM
zJsvAC4mwMpZ3s6wHM0fe2U2B6b6S5F1JRMGSz6zb6FVHXMjjQ4DaMlNrQIjIGgqP+/RyBj3jq9M
qeSw+MoX+DySOoh9oKlPeog9uBVWj4l/CuvH0f36Vxgxu0EDnOOaWtOenynVPNnrsWvocdm/ek30
ubvqAn7osv4wup9Yl6QDu1qnwdOQQvtP04y8Odu6Z6quCSxBzBBkahVk2++oE9ghw9ldhzzN23E0
eiOVIjoa8yL1kCg7oLr3+tNHJkZDuKi5cLXDH4wW76G1U7a28bpUvKP3Vi1oq9ZCgAL3+NFvRdYo
qaANlaxYMw5v3PaGSiHZbnN9d8Lvckyd2dq4YstwrUAzhS5t+LVxZenog0zTWbtVjKEm//ihMgAH
y61xNO1jpu+ut4F57hb8db1yY9jJVYDPKygfRzjmLN6VJIt91177HLDPIFl6O+q0wKTVH08nDADo
IXsF9Jfk4vIltsi6uPb0rN7eJR6zcQHJ8U3CwgKKH7L2E4WWF9Do+4AgXWUEJgoIuAu3PvAkLERM
e/49XULjQ1NIJWwh3ASFFormBj/qQelyBtiI5b/h+a6mNYMiS7dYP78NCvoluYXsPwpHafVYO9G3
LO8sbMfntUenX2nsOlpN4JBFNWVAJh+flNz9eVkQXw7FzOjBtjLS88iU8PmXbmVmDx6TK20viRrd
PNfx0ym26h/nOswlzIzZX6X+7er7vQCtNcbi1iT7EsF7gXTdSUoUa1pY29umIZr5ZLLffKENJsug
t7eXodpNvb8fCNj9pwjWCWgvEneAg8eX2cIF9cPGdv8wWkrDjTiHVgalZ2CMhp++rNB2Ndg8YWhr
6xgEKnQYvd1kdnffMwnfEqumSmJZZDMKgu/TyK1S2bRflygd1nfd3P3WBdh50njLPQ3VLF+iVK2Z
cVMmtc8AAzT0eB5CVRl4xHKrnjq2RL6eQewy7xAg80vl/lnsBOGIqMdvnX0HpC6phhcegW1M/+IS
VF8b7srRM/ccyUsAYyiMqVsC/8xklv3VYLHQWUvQmesEh+z3IDOlHvwvZReplCLwdlLP3QXeKErw
JVhNnQKvaGzOtGdJpKoNjudmqAjAETt5PTEry15ofrjBSJfmkhg+GBUOcS97/I90tncP+dcweD9o
jdMeJ5fw59xxdX/t/22jBa539K7qSLnJI43RTPiZRYylUozA4vjSuRX6oj+YBE5D9z/P+kkrjgBo
TgFwR0Ojeh2eZi9QRLDqyZlpw9w7xQB3a45zJMLaknbQ81eP6B73ElOS+/6avpNcyGyMDrgy9lm3
0/RfKWMj3civNcW1gPLetezHAfoXrRo/8AjFv5O2V1Bysiybl3ejdHzyT8E7AhXJbJpZnbRP/hf8
Mdtef3O9+A98Yyk+fXsRIa5BAlCrUr9Q/+hDYSSiLBMMVGTXPNtCl4P3fsy51ZXZEcCdRMD/QlTj
KScuByOsmVDy7D1qbGjCZt5sT9+iF7UnLCiZMmWu6WJVI8St4SoxK/UY2hwLtlsZR0pnCxsBl73n
Wkrq4sUn2UVtzPBjy2IOM8lthV60FDWR3VQdsIhh6sRzx0ut8cn4cQoGXNLSBOH9ej00cPnkr0AY
tSFjkd0FDPC3W/t7lwVA/UhAHX1+oaYncMo6moTJhHc5kIKoQlSoo3TzBtc3QUZ+XlDWwMlCudYX
18pal5OwwnVlen3q00TN1Z3/2giD9Cqz8koKK10W3RnCvZkslzvliLnNL8jKaCWKevUL5Z7USy6g
enBbbc6NYVpile5OUaKeZU89bGgIhoUKxXAfbCFYsBtyffXZVvQQ5mDOMgQXWw9eo8bIcBBmlFWu
NHr0JlNfIF7xYWkoRPSoUXBKGeORbZS5VM4H1X8AS2XOKqS1jDvNoso5Zxe0CVKqSP4iPYrk8eMI
dZ3VWTbcsMgKuCSw/5Tt9uuYZze20zb2MAFrUR1mgvdzAVNHY9ov1g9AiCyiIO5mYmfFOsp9tvm1
LWRFEwbu8HRuoocZrhZmd1UsFFu72RmhKhyWAHeikneGQi8roF5ox6rI8VdlLEb3DVl/UpHPVoW4
o7KiFH9avzXayqIMieBdkWuWPW/QpGunYsKIQtyt6/h2okl/rDf8uYFnn9aW/2AvEdWjPTkOfRPz
DaEvRgfNEI2I8TIBKEkWagiEYxGc7/w6zT3W8ry8DlhC3RwQdsjg5xwY9hMoTH4KsjTkS4FNUQkp
fny2YjgmISzEJYoRf0mfMmGUVXbRyd6VbbAuRnqBTaVmVxEkyqY3FPvZfqWbXLuj4QdSf+9VBaAZ
fb3bW0g4f31767asfUrEg7rODVERglDOscEJcM42JyqvBzxmTYz4Z4Qg6+NbJ5S45YCSgDXBQJDh
vrdre6aAsVtRAngCRYo1Z3+s2VbNrqwrpg8ZyTy3u9P2GL6Anjxwyj6wKIMdZhPCpNcEp3NHBNsA
ac4e/iL+ErCchXbf6G8da88XUJt+GClRJ3IUDj2cgjOR9t9Dbs5vMjT0PL97sYYh0CWB1E7szh7c
4jTtwChehuWnaPA0tcekC3b902N13MtyFNx+Asf6ZhGQ0/EJCNSJCFZOdD0d8wWn0zO5mBjIHSb8
KdaYYqx784bEug/JQDXIEXbLjOVPfAvBFopWvOCrRjBSKndnvu+C0A0pZz/2iSA11Nz/mJqDS8BP
E+g79Wr5idFcgOtwQ3HxEXm6bGylKWBhAvST729rktOSJ6iRYe1hhbIOtyKplNsRvHP6Yo2wm1Aa
yQhrY5UFNzBOK1Hfm3sf/5+5eR3bHlqOzJd/hXsITUW4htc1GRB4B+b3Qg9IqhFp2Vlki1w2yYR9
WDqhf6awu3PG6UscffYXnifeG3YsS+wcw09QbdNCp++iEzSCEfxYlWDxSrAhDeUQkWQ1E49Yf10z
0favYaO6dlLYokmGkviibCJs1AFAN8swl2tOiGk6ecGWMKUGzmTuDYIFCyRBqVY9lsdUNJy/6+6e
noRZwxaxUd8hbCIRBoaQxtDaixYFVcfMGJ+fXcNAcA580h37i00qCMdVB8YAyBWG1kCL2/2sUCou
F704GUkQti2T+GDEhvgBVMII4zJK/Wf4K9knamRLF3L4V4glvlyFhQz01ok+CWl1O64o+7yYiUDR
L/UFvO0b4Xj98nwlxHDcMNI2v14ysFxAX2B3/rnqRz33ErGcjzsKOigcVeMq1JoS9EuoWYqAIWWG
3CCzCNGfYl0qDgR8VSwoIQVMt8ahD36RZsBpJmnaSQCTgYibAFbepXZgrUt8BosRUYgiIQqRWWTa
Cv1v2KU/sxlQyN6pgxKVyLvuR5p6rFBZcXFqgwsnA002oK8aEPIfq7oG3eckFOq6HnspKFUXIxTZ
PSoCVGUAeP/F542rVrXiUhQi64Hmw1PkHnEnYq4PYc1glXHrXruBwWLcQgRZqgItTw+TZA7FR7Gw
Clt0xlL7MLSvQaV4klOyUJiqgB0mwEJxQ79bGiXxTYg3GyustZFBJtP6tUBSQa2a96Vv7CM4tajV
mkSasuz/oZvF1F3oh209WUO7pneoswHG8b5P/3dxAgad17EWks7v9S3IWEbNzUJtL+P2VedOfCJt
HChh96T2ohRgj9NegoKfxptx1Ofs5xQG+ROE/8Kr0VCB1SL65Ov6DwopMuitrxzYqV0AYvyd9rto
HPXXv2emsySyVX7bE/EdWX91dYbDZzyaPPsV7/dvlXkY/7oDr+tUYCLFHeYxGNZp0r36Rpz686EN
gKhHv338tNl/hSP2JsQbtlhS0jnI0kHO7oNMFbwz8rkwvBjukOyQ+aogkf4sUFy4OI7vnCZh3DrF
BvcIu9KHPsI5ezK2TvinCsCF8r18ctj3EOmtRAqru0kLZBw98LNyWR6ClAOUeT/wmKna6IuB1wp6
V+HnrVocgEv28BZzUTRrj7zcq2SvbAbe4dLhJ+2ApEX312V+9Pb/Sw8wS4o1rbi0XyqjWs544EI5
l3iZaVn8SdwtQGYG0HyMccr0lxshwSQMlT8UMyVQC1dHyaWxADNxGg6mWEyKgaiIoCzJNFcqNLgZ
ABMX5F3fAA6d3xjN0jhxXxmc2FnhwqMCPb6chJw3LwWLtNdXa+MG5uIcRIpm87hbo4qVsnNeMCph
ZKiwJKVlsk1drBKFQ5g1H4Ag++Q8HotSWeW81l7wyAnmA7IudZc0cgniA1/U8GFUOhLnEL++tlxU
SBxTS/+bngw2ewyr3FTdKj2GB52RJ1gefnLG5Ywo8Q5+aMNwfOVLm+K46QfPVJCR6f+Ujlnv4BVV
CQ5257IG9iFz5vE9Wu/h37b7UyrDnZh5Bx5aPG7Wpw8RM+WRPXIKhuUnBSChaRTMNoH5i4Vlxgpg
yKv1k3BuNFQ77rUjtxB24PjsguZwSHGO3BayjIupQXefgKlx0L8crmUPLFiDihPiSBRePUuoxv5s
rDYQAEMAH5IHVQuCoVSwLuQM8Nyu7tIq1bhrTnsRR2Z/AxuSdKjP28u3ttQariphCtd0zaXnTN0j
RoMYIVPNZszzsOZ2NmxJMalHXCHI1VPT6wnEdDMk+wiw4UWM3n5n8JEB3eFpWzGvuVDKia6j3hb9
ZYU29rGCX/arUBh+LF0AaCD+Sf9IczEAiGoVoS08+6qiYcEWy1te9rjjXQ6dJPyOUGMndENpc0QZ
C+wXU4QSAfpANfD0Gy0+aIEebmb1XY4ulaQ84QlreYXSWWbDEgaoc64HqfdYDc3aauQdQIqO4Bqa
FYGN7L26lJpoiNeVXFJz13tFO5wcCL4wrIEJO1/W5SxR7UNyQAJqePcLGWPoUbPUW/5VbdKXQ7A2
DU0WeBPxs6arX3NoOO6MfmmJJuLaIBYyQCJSIWR0b+QNo4trGFs/amTaQFRvEN5cAt8YhVZD8mte
mL3zUDdtreqXz5ZGQqIxEkIIbtb0ca2qWWpNSb5pfichGSAtwjxzFIbXJwVcK8OMiU+D6w2jLd6g
J89JS10KdC0e37bb8KcKZmRswL7lH/HkqMFtzXClvZLsYqPr3Z99BLmZYH1vJo1f3BO/NpKDYr+x
mikrXPGw1B15xtP3XztNSL0kOWNv2639BPAsrICLONBBMgu6zfw6+frr/xgT/tEqGB50jryakYW6
BkBFD1ZN74/Io45E/yjwEoFuh5DYqjn4YHYQSqFH9H+Uj2P5v1FbvuMe5zH053yJ2wP2gtYkYmKC
/Cnpf8IVVXLywxGrf441TsRpNKdibwKq1PUBDB1h8LjKgehePAGpPTAWjoPhxWPIJtcrYxlT06sB
kemd1Q62jK0WQBQuxROOI2UpA51fQnmmzdyL01ZinyTC7M65DBSZr0ZwmcfYs4Ye1PpNoe9ZgRZY
d/SZA3mQy+2R/T2JiWALDCaaCEVryK8qIcoCMJQSQ1TbCtFzO6d8g4ewz8nml1CQswLk6kJkN7gX
KHVUKXEharcekMwV9wusVc1TJ5l8YQhtGeJrWMWOhxyZ1BCvke67Q7+zF8lw55M+aPijvSZGhZaR
qFcYVvbBZ/NHd5gS2+TYZ3shR8keijqGNi2IiuvzZ8vfdj7gEHhmsZHbOGGiAfbRPTmy183h9DLZ
5o5t+5mtzC2YtjJGtDnQ4BnNBYQ5BPuUoyK/+NlPYSgTfpAeBzB+26YGZT/qzj61he3cbG1Kq3A8
svbslJj7OpbY4aaVuHy3OgW3JqE9ql82iF19T6U1GLQwNU7lmgcs/DWMXqfk7H6XvDI1UN6g39T2
JxCaB8biaeqFu+9SqwVReVnvGWgqhxEh1mQKtoEHCwxtBiEwaF0wOYDcPVTqlEyMtjtdoEGSJpUi
PJWIF5Nc7GBnJcLbYy/1DHEVZrjlj87O9ZYjrRwIrIySIE/cwZvXxBg2AXyw8Sr/TaQOGit2rSHF
o6fMsxmXtXzZ1bhPPuOCwUtckXZuFHINPxKNO9GtLb+//8WmU4evo02p8o4HKlSs8QmLlLwBCVuH
a4HUMPMtJ2sG6ISLKw4DnCK9lD98IUaIAwSAWV7WRbvNEcjlXuZd7DDEYolNYKI2y2qL0SqtyLlH
yCsgv/+FIYQrv2M/ri9gtG5rBguVa+vl8xujIwcXFYpXc1yt3OpjnU+GxY7z2gSgJTxxAEXRcOA0
9T4b9PHu8YgqzOH0ccRklxiDimNNQXK6IOXlQk0t3jsXKWu98cBG5J/vIKSB9BJAeLi3JmlFMdld
rWiggszlcPVqA1T0RgJS2ZAPUuIvb6j2uI8fzCwkJHeOZyvuuFvRE/rOftnOm7r/ID+Wd1jJ6I5u
gHv4auv2W3Cv1dhectmbAkGYoKDpMOoSKaFzb+FST2XkD7U7YhvEOSZjwV5GK8RyX2U7B/sUjaqL
XRB58YKWIn7iZUzRc9fF9dcN7TP0EmrOqr8tJcgswYqIcTbKTgI9beJharAra43xTnu1nK7RErQ9
9hdA5H3nASke5s1ad7aUV7FA7S+maiTrsTpZTkKOCa//0FXuLrMFReoMDNKX6ov/QkrDaf0inkLc
Ybk+LrcjfeXkIkCP9pU3DOBNpmwpMYrLIbR8rvTJRvoJvS+g0dJByL9mVlHmbDmd2f9+lI5cfaFs
iGx+vS8s1xAMg8Vu838NgEITGng6AEOrrsDSESS3yFREn73UH7NTWDIEurqnvCjvQ0t7rWHyav6Y
74BEG4ovx+xRAqCxb0h+hWGJO0mll8RMYlxIxN7GAOsr6UPQlzpEr+xdfORy2thGqEHsQVy+SFv7
nZAPUbBb7KXTuJeD+vdMCVWIRrh/+S6Q5bSSyHb1SJpETAqZESI8JO8pqYS5pl9pjr9qNge0EG+T
vcfHPm7Tyd9YnhMaf6S7+Zaqtz0xUWDHQDVs3UD5wLNOkigWdn+XjY9ZzeGcGXqFyA9+ZvPUxvWp
vxtUUjbRlCmWEy7TBe2JgngNj0yTOGd/6aSCcRWxMZlhMZVKjPBLy89I4uIlGLrGUqug5YqbUrFY
Ad+RvY71PMOPatx8MSGOKGHyLLtaV11j0PqWqYAoPRO9MQwBnKntH/2iOP3Bjh3itj3ezxh9Xuya
XmcplM1GSEbHPzxSJlXRAMH9Q/FSNEjtUs4LF4wCLqNjLRPIkxA3QSThCHsOh7JDAuZDFAkPwQby
3pmaybuSXJFLUSUARhQUhxZXZ8iYK2VlteGpKh0HYfvxX7XzI0w+HuuQjRhvYnntXmggEVXaNY/x
yMEN5OZhPj9io5BCQAPL1sNZzjg0133BTxMyMkwYX7F0f1rm19l6pCvxXU6lCXiy8IvMHmaeIq6q
8COeuOEJkZL8/EO9T4o4MxQ/tETdJCL0GwAqpqGLDKvy7q4HnqTnnMAwb9k2rcP5gDy36BPpofHq
nFvjWn7Flhn/+GPsdP83qycKv9IYfwYUDgMuzfpu+27VRt5n8SgI9RYRMQKh631g4m0lN5ByKG1q
lReoe9ZMqj+HDV71qqI+qrFnqYMxzFv9XwZZSz81tHGwygX8aFUh/gvJk7yXQ96HAPdkFxM/Ruq8
GPIDb6gvbV7/Q/lK4wUJN/GaLb6oMxJpuRfZR0ydB8mTHYBD3sjwNnitqmzD8KGsrptlishIpKfl
PJaCWPLae+bszhd1X6AQYyrnpPnpJBD/q75kcZIVv3jXTHBNSTFLffu4fsSDDeLREfpDVZcA5+ks
QkwtOUmoC7zDUppRLYPdgAlGdsQQ6sskR4bMKTcJIP6h27OT8hiDcPfuep9HHhLLIjnWU3oE7tz5
lV8gvc4LZ9esndZR+vm0UiJtEHHMLCsxq6OAmLEv4VQoEO9mLW6C5A2wan1lvFCXmbdKurs6ninv
jbAuwJXBtN+ppcGMZY6g8OH+Tim1QxTnzMT1pRvBQvxO4MNmvs39Z/NcL7eQYOfRV7MTJ5QgL00+
70UyviuCKPPusH9xtV2Wo+rsqgHWMXe0u6TItzHYuNO8BycyB53M7+a8qqor6JmHU0xwX93w1NMp
0oZwfm+1dB3yqwyAL1CxkrZkJ5r1Q6lq8JZhrqVVWJ5OSmXWvnW+NbCIJ3Q1h7wQjsHk4V0Cy6Pj
KC6UI+k5KorwnqR0Q0v9jx+U2s5Da2KQDxoi8vjdlpopD7Q4kMxWMS+F9SDg4BqIczwgf21/mJXh
t7hoUQxm7B1VkxcGyNqevsmOjck2nJkIDWMcKVNm0cGOVeN5jJrHy5gxnQKfQZ6Z4VWfhmKt+ibp
SnOo5nQI26iMH0gcXxSE7VX8RKtNyZMAnnU7kXopK0WgnQ6QTSD49ITicf/lC9Vajr1KVrMJyC7e
mu/l2JZglTOURiRkV10uzJM3e91J5CcKxa6dMMFvEO1ckbOGatGoI0L6+ItvNJswliqP4hx22mTI
K9DJJtxUbElWS/u6QtXMinLQfBTu3TxMlTgPycycvhlJXPI8QioxXpdTejAZOBE/u43zsyETddko
wR2hsGj8ezg/TeWEu7lugqFYshaAVCF06cjI+t11fspWdxxMN3Q+KBPkzlz+c14k42XLq2Ux2Dnc
htd2OEsOzA1eZrok1UPD9K67I4O7pnCD7sYhsVVuyDFTFmtVU5xEJQgOKkf3V5M9Q2QSY0Pza2wb
Nlt0iOy/WkClGlb2w/oglOdhTR7tmmNzWfRPyaORfem0Uvy0e8ONROuwC79FnPcTDRsHPkvrQRkc
yRSG5wZchYZC940UeFp78IqT4R9oTfnKQZ/DhjiXWIVaVt/KDU7yPmeQl7MOTyq2jwScNEMOqtTI
4lFFeQ+d2WGKjV50n5SLLzWVc2VouDRg2IGPQ/ZFSg2VF7id6tEoeW+x/xp8Cx+k3wtszws7KfFy
z35jxPHiK3a4olXt4Epm/7Fkvw5syU8XuAg76kMX9Y6yHgM8IxTTGsPd5hP5G6Zk31aGWazMfQhx
CGgdtBUFKw96+OMeUbX5I8c2IotWviItkJOxuA6Mvwa/1ek7EdkQ9YFCXaWRXvGwWVjYzCYTnHiD
+5wI17ZeNMz9a6NK0xoc4NrpvDELIMEgkl7yBR9TsKCCmBYrg+kD9KcVFftqVAKOC/zAowfeNL23
9IqUlfaiRwlyo++STNjOHYMmdBRZxbDLZOJPQtKrOMCdPi1zg5+2UE04rhj4ULKYm1KqPVkN4Su7
WlQ3he3Dqvrcd8BK+yEW6ajobXvXks6k72CcMgTcjLegyR11lNtGtxsWLMe+MEY08WuRM6yd/4/n
NCOOxxavAGxTuLatcfzIXFz+zIUc7Ee+mmntCkGKQemRZEeBQgevIV6fUul6UiwTbyU0SiZdlZeH
a0wUUclPPy2lU0Kbs/rDtfOyv7VJHZIAuTGJlqaRplWWAmnAGBCFTdvfyqA/UFqPGfMFxESthB/z
qtXuCeF4pYCezQewK3YcZ+98E+aMVagYCRtAtAthAdaNTINoxVfnF0SlsnksFThU6xXOdZxO3gJa
rQG1bZLQ/+MM7dMfWQ0tQfVocOVBZDuCmjknsCAlncEThHu+ghK4XRI0xwVo6noF6ARIO6P1jEFg
6fI7Uz705SBJBT5PdqF6LdoIVQUQec2e2ZHlldDFt69lSxgndfJ+RCU9nRO/OPpOFkKbCpPnPU5Z
dFyGzwtY3THG1T72quMXkpagURF8XQfJJ3zrAG2yGCy5Hy9qwgv/4LVpWeTg2JYqwKxNgBXiNZ9l
rrt0DluOh+yOuLG+lEHMvcpYFeVSlVz7sQp4aGE/E0SIDMDPGzd014l93IQhJOD/ebki2A6IgvvQ
QcB0u2e18DC63iR3FC/mlfBl+5mbktNsZvkB8VbUugoepGbUVSxySsTMfBgy7H5m61bu/SAPIujv
HaTwgrapC/+mm8viKNbD0reklU+o9FOeoJZ+c9R28hUiWNLeBXg7X6Gy7NdG/iT8t1WyLME9Xq94
7VMKLoQkJY8kK8bygC3May9IbpnsTjzMxUkn31MP8yUe7ahkfrJbStItfkLeWcrYq1SyR7MTCh7R
E1vdEPzaHyzFxbbyZWn+Kk/0CxSiu8qqWoNoiCdCLSd1CqU1FqH9dHGDniKzGLRM6TlQ0GrJpsTN
RqEscY4G/9ZqTQovJai7/yfCgdCAYjqUP3Fzoxje7XlJrB+LB5aMDrXJlIy2LfxRyq0++MpcvHk5
/I/qhI6fFpb+CmmqTNRjLJSr+eC2CDuH4sZVekfQ/C+hdF6fxTmB1feOKKzi3ikcz503tB+YCzS8
osUvylKaH0NcqsGO74f+aQ0YDSi48I12bXBGVZapgOlEcc1s2eA/DZoqJmu6hFJYDDEDhoCmfP2q
jMlHNRUYwe3KvDNgXosfhlvuRbqTfxnCgV2A44EtEbUKEhgdCP2iFOsac4ri8Dug81/xw9zGbbXn
1sszjkfqX27RpPiQozhS8i6yilGGU0cQ5QmChhmWXBTDW9wpagGrjhhTajyBjkxd8Ui0PnTBEpzg
jNMUdg2Xxp4sCnKl0nqTEoCw+AbbxHDrHzqlByhOollpNwiJKLU1fpc01rEUaMZNHW1+PI12YFAA
mjajh9PyKD3GtmCPqK6y4Cco/QoKgR05oz3ai9cplxQHg2rKJeB18cWG62Wzh0eBFbiVmslJhaxF
vXwIS9U6q1agQkiAyGV9VTFteSMe3PRx4d6JBL1h0YooZZVfgUZdPET4VeNFb20SJgU9zK0bYI3k
cbfu6Et6fMY89C9Fcq/6Jpbpwc6feIefm7nOZm12p32zty/3hWJZP5Fm5lptEZ4wERwc5VJXLzrF
nt8VaTE/8SaV4tpWiPFLelCfxnAGmFF0Iqhy2GH9vyS5fJU1H23K5Y7T5jj61zRPOFrZR2csYJUJ
fGb4MsvDagHV2tnvyO+nDSa9CvCz6CAKMb2uXc9ChW1ZhwsrI3jnffIHsglw9SPTGvSm7JMPdyHh
xKfpY2p4S2T+m6HzTqQeJB1jlqjuL8q1laTZHc/4Jf6YbbpWAnsy9E7qg9biNcapUf3s1JA9cvp5
pM7OogoazlQwvqtt2t/H1dHWaEeNNW3GPCrBl+fuEfoLDKODmA733uPmBea3hMmZwSqeim2MxGma
mgzMzt//LxvUmT2347lop8V2HOVT6bQ13bTcT5SSXtORCGe2y1H5Jfll4BA4k8fZZW5ebVrHRTNZ
PKkmXLieXbqb/g4aBJVTNvulzhgZPggvX/QSyU+umyv930ZVIhYL798SN5QxyqIZPGyRZNSg4zhh
gEeRbQrLHnPdyFxHo41tdrkB15NBlKS0afKQWFWzhaPDozLQ73Fnt1SYq3wOQ6vijbMy4y4Uq8oT
z0Irw4hvdNj1e+AkYEreh7qQsPV1HjKd97OjfegYPqw8x5Z03RYy7zrqh8ObkrtDAbv3wkDNwe/u
ylQemdyRmXlqeN5iBCTR/PnAuOZvHsJrXb5cRlhIVTR1861rfnppc1Zwt3OXQaFS9Svl690H6Bp8
yT+pJJ9A1LIKUpNSqiellshMG0a7mzY/w6nMIPEb0prhm5t7vXvxCeSIbEXrCNmG/ZpFOaIY2oOm
X427VNN84Yz5RmNfiSpvxKIMM7EZJxfxYrmTZdE5OpGqgV/sT4iE0DrMUaY18yT0m3ZxOO3Ck1Dq
6787gy9tVLrracfkZAUKvoZwuzlc5M2dIH9gHZ/eOTWOodnusNKwqxc19V580nqTtsuEU9dNd7m4
Tbk6EuXROAaj/ApQsRyT38POsRIaPWoPIsSsGAHAE2wDfNqajGrHR0VwdJFUstlO7JOG8iIwYJtW
sk1Q9DBB1yUKzdFP2HXsA5UVMnNp1EfE1qzVzaKvmZUeSuPBKStYzML26D+Dbvg7tcPYNT2eKd72
GLp8/maOO1Xvv2J8HgEf3K4kPObwp0zED6/1exdvavSntSiIfFocAOlmrgzw4PMQCgYu5wYeeKUp
YEgxQlP3vbfrMVttw4N8jQsHD1Yt/9tE8KWCOHRjOT0YVbjqAhJik8Sd94Uhyp/DEUQwCTjPDo66
rmm/2KbCfuvsZ8jXzXM6qaor1kRULl9L2BbuoYtvsXD039/YO9peFRli4zuzY/UQTzIBWAH29zQj
ZebJg1gVfbACMyRxGiHUYNlwjpLr6JuBay5HXqlvGNNh8ko9riCq2bIgYEmvd2HeFzMBB70n0BER
jRzZJEGGinfn3v5XFOi0pkbOT724UcPA4nzdRGywXvwqvl85taA909KQ5Juhxl6j/R/3nwVCCamj
brEU+Kgt91udgwLJ6bhKAf4AwNtTsisg9zHDElv953QQgy6uLXAKJHLFlHxP4/u/QQOWixtVFaUy
LM6ka0gGVMceVhH+DF359GBnbFOSBSFCEf/YIiTOwN/3HGhqVum1cp5LK151awSkMiAJs64EYrTL
GHbjfw3pOZl3rlI0rlkX7/1wsAwBl3QMxrJQdWiXd3nDDmRGkfXnmjbfEqMEOiFHsaQF3E0J7Hik
YegPIpykKimGDgE4YoEoGjozTyhlTrBuXTlvNQ1pV/3Ke9J61TPLNfOUlUCLO5bEwEGDndchYNN0
qcrgcs9gcH/wmy4ihLH6CriSyJ0GBYVAboASCgkvMv+zZvedsWH+NmaOIXaCgtI9gLbdfHIThMkU
TFsDXz8u4RVjzU4OW/Pql+lW4PHQS9HsM+8l9kwadtMWD3CKxtr5E8rMObNnzUpFMQyBdSBsUUdA
qsJ5oLUClUBfwjbz/8bsqavv2+y6lZyq1zlidOV8OcWnvH6zD2mv7p5VFVUbmlDdgPDLBjKyisWz
g5jQ639p312Ef8rKUaoyhYMy+xX1Lu7nhPYezn4u0gmhcR0zCkijanMoHfAFWYwsM48qVByiwnpk
ewwV9yRByMYZZtIxgb0mMc6NcyTx/aLlEdN1tebKnVfFJSw6VBPZdkQRnDzj+mAshPpygUQv9hDx
rmDX9x44G1C674gcgeEhgVbeaOa9sCj4+b6ubgDSfrLmhSYXgJ5on6EMw4EJ5w8rAIkojzVG8h7i
NtzW7KetEY/LsHgavdzMx86F8hBNiHezQZh98cqK85Ek4MCyeiAS3P90LQV1yCVNajsMsXk3z+37
w5eeBeoUz8BEYlFBbmukvPx3wKuclT1k0D+B085hHluaYlHnT7JnWBgsG+NtOH6eR4Iha1WqUWcf
k041wKrLPymcnHtaDBlFJL7JKC6EzQiOl62NMbKW+t7gO5n/tp9vZmxAGE3f3ctDP5WRoSIClqJ2
aWxawehGvCev0Mj//p6ovuumLT5KzANM27/SBwrv4WHDj64YWN9z4rke0Q6nCc3KShpNnHQ63C9n
szIOFye6Vv0gqyYedvGxuEkgRA8x/Cqto5UonAHhV4NhgQsab/Dtwe0rHefaS8kTtNRk/BCSLcn3
POaMxYEK2zDuQhLEb2hg53eXl+B4BZ+L6aIITjoBjR9Zgvs2Gu6Sjg7bdzIHt4YPI410xVTClOVv
vfGaNbjKm7G7+6LRwGBidAyFkZNprZ06tzJ2CzgCQqTra68jsp4gOdLl7QWHWib1EcULy5Vvn4lh
yX5NZS1XBA/22Kmy9S2qoYPDb58zCYLE8ojtiL6n4vy1Le4qz5u5oIzK5y3AzQetbPK7/4nqV0MO
92ITSi1iC2RWZOBkPaUIqNkapgBGxigT4FvOUISCZNhhn5kHuUsUp62Wl+ZhgQCg61zRMQTqw9PG
htw9DieDiHvw3i3AjlCuVUwOWnDYR36LyJ/yz3damqWZpnZN3NgW7A7lFUulZzSR++Np/3DC/FB4
hexF77R+Pzvczq7mq9kCCwXWLxbcWPKVE6H5+XB4Z/3VuUj1BOl+lw7XNuZl5nkJ78OT6UBjYpoi
b94J2B2g1/hGDweQ4zdhNX7/4PTdfyhNRPbwXnKi+sXy3+MPRoSYGINmRRH4jE+H9mXsnZ7Vny9/
YeB1zF8ZFOoHcCZdKyExnKS4P4Aq/XV2ubNXedsPXQmWOHhAhPShx21V3Be5D9yin9TDOiwRiyLv
6wz8k/BTNBmI+3Fmm5Eod6W/3LWgx4UdUMSKyjVNokpF1P8zB/3ipTzAjGH8aOs+06lir5j2hRe9
AiNltzapwGwAb62tOAyiBNPythQbMXvKfmerbZjzLQWZ2C4RFzrNpbf8PF1BEcMVPlJRevyWFGvJ
bEVIcPXDipV5iJXc6EYYjWfd2F76AzORwC/RFXoa05KkHi2gQIXNl/HxWbi8CH9RWoQHfEeL+23D
EPxchfdvOGRoUV9WOq37KNNhJJ9uXoxS163Px+Wai98wA/qaxcD3cUecNXKO+UMc0opbcZopmGib
sTRbMhsg0HWxMsNO9dm8OVkLJC5RWU8A38LgWTd0BEsT9z0h+q4J0MDX1u1xwlUxuEbyuxfGNfd3
pzVfBzhmnIvYQe822YxJqiO3K6dSawontfrlCoVOMCJugKNy+Mk4+bZDMdGt/Bl42gXU6Hm5WAcw
CebcWW7963MaOpA8U6H/52jSiYwLCoeRL/MAt6x9gX8tWFb65cSTP/nqGsnIzcXcALiGIywWKiiA
9BpI1Pg1C8QWkeSL+c1OjisGM5TT0XiVg9mDlZdBRTmobtZWxNJ8Dob2RE1egkzatwZWJQ+JjFRF
sYcHhsaEuT5uYtVGoSpDHgaItneIEvroKSAn3kpMRQxXyH1n7U06HTyNsEjLt+LIcKyRGodTLzBN
jnCg8gsewUT/isul5mIMep9IQJaM94Z18u2ND4Dth+aJSUmcyHgxsbHraLSYyGXBjWwhGJKBDKr+
5MEzXU3Szcl1qAWBNUjQKUro4AC09DQ22HT2TzfEhf8o1y1+uosM5KnhT7QUDuZ6TYq+ma769mOX
QtOBF3PyC1v7e5ccwkf+mygl0N8YdiYhHtsDm6iobZZq8hJczi8qe+4CMyxKhlA3s3ZHrO8nlRw8
2zqMHQpaEbCRdNjJTz0voX9QhVMNQ4t+ebAM/4uWJsFqqL5KhuqMebTqc65uS6piS9yIESURxDaT
YrXiLLJGx8i1svC5rYoxpOqNR42QAuXFixzOGboKdkIgBxFk7/IXpc8btQdRJsiTvxRPJDAqqv27
iBFvXWLZfx9bAjZHTWEzlLKYE6boNFuigQBw7eKx/rxgkQBb3xkf+kFrxbBcz/W2zDR05AEq0eAm
MxwXPr+Fu2n4yod2ypfjVz2vFfNox9Es6UcwdwK4rR9BKoQbvykkOp9Pb1d14qpHBlqYjWRuyUrK
rkjIlU668O1p2gp35BKG8IcGlgRHfKp2/7JfNKI9JCluerQMdHpXyWyOt1sX21XV5u9X+9EkkWtd
8Iqywmvs1TFcH6v6dh54lrnnw9mYN2aCBEteBjbi30Z7bm8KBJtGP94wSipJO7uE6emQ2HU1WkVv
l0Y14TJVFKcxR2ZbFn+asBbWxodx1Q9t1YdUMb1PzxshokkzYsaZDvCFJP+1S4eUtJTXFyDbZZDD
8yjJsWn78KbNGyMfLdR/e/8NBhXXs9c9Pq0oqhNlUkPtMqWjfO5CCXewmSWCcA8bu/HW6lfov13E
6XOhgqTRUwCEzHXWvPOL/hAUU76Ki+wsCbD7bqWsa6MJkSDU8fqPVTmFGvun1csVEyaiTLjDr9Eu
Akxbp5NXKI03MmkzN7rP2RaD6cqr9fjEsNPxn4sRrdQp0dzzJ5EGko9rCCUTooiprgF6vVJ9SHc5
ElfB1Oy+n/OVc4CZGlzPz6YI7x3//6N/17QOtlZObs01Q9CMj1x9JDczLvICTqsNK29YedZ6eQQN
LGA4Q8XNz9p+N2QJLkYIm9q7yJJ8//OO9reLl9YE39mlICSkQLG7guMUtdAoeBSc5GYcsoY5Erm8
/8une7XyW5zdTkH7fWQgwO75aKHMiKU7Lcwj4UyA7P05lvx/kRuQcV9+ZsX/b+6YUR7yMOaSmLq0
5vRcph8r+aiVCYusqJmxXrbgSbs/3OaCqKFBn+1ksbgAiW8W8VKz/EyN57VY1Jvh9XeUISNhHcSa
8upENIM1UKakvUX5wZU3PDkyw0ce4BfJNzkpHNdX8z1O4TEHFSEm/3rkduXoPP+tDMxKkuQyJd8h
qCqPPpuFcdQk1QtaSOqKlgoZHzPaafoZCkj2D9BEazLOUFjYL2QqhQspM3qpcCsO7E2Lqn0OSn2g
H5iXHfj0uj6xnO4C1RAoUAGWKCUOoFi6FSW0U9h0kwp0nTI0VOmx3cHP2cXj1mp/q5GOCa6uEYlA
cjIhwobDTmQxkw1Jz6nR2NZC9RUjKR+UaO/yunkwVSVaKpuaPg5yyK4dJobIFpFP8bN4snGPwWFd
VLfaiYr50i3snaKu4PG4JecPEQ+XN5kqy8eO8O8iMDAgxQdP/PnLk6shF2n4PmhePP50dQJkhQEC
HPxfMuFsbtBQrcYsJcI0ToNmXopSbyJsFGMAxHQKKlV6+0czwFBep5ojfaNWrVCaMk7Mo10hXDUy
Ec2KACwmqs+x2Zou6W0nbygpeXjqPeg4vURPie9bJFDk+3id3JqedbFkIrlBHAWgk286u3iIo6Pk
iMWXxxxFLID7phKCQ5uJHkPOXC8SlTCIqKR/XC0GaVDAP3VUjSRPp6hGsJUV/7y+sY2cwBDTc6kP
p0njx7rFnfTIidZRaD0Uax5XH2UwgzDBJitxqIEp6pvCvuZC8LuBXWlC7+ZfQdAfdnz6GR7OvjH9
yYPnTIBCqXvrk2IOIMcF1FaMz2aMEDgSi6r3TntlPcEtR9Yrt8Bd2XINf2Ozq2gwRdeWGyy/TqbU
8jD1ZWAuiBpicIVeH6bfJVBDfiBO5jgoGpqV35B2b184NcJYRORoppIOm2T4EP+BkCtrEnZVdCZV
WzKAN/ZxDeVdA07FcbJKMoHIiG1nxg5338qDlBhQHlfjAF8p7S1RMuClx7FYcVE4fQa19V/8V6oo
wr8gb0e72NIbXX0u36LNJeMluwndEH+Eu9gEfH5F/DjCZAA7b1KQfpsiyaE9Ndt5Y1qwTgZeGWnN
e5w0IUevqzuIZllky6luNCKcAMW1kF2MDWwcqKQ2U22ULcbFYsHk6CBvGzbY1KC6Pi6I/K8a7My9
h1YgOkIiaXoHMOAwLiLiuPqaJPmjO27b1lZtjsf34ZuvTAW4Fc0ww3PrgsIUIPFXph9Bl1iZUV9q
CA/TCihdi797hBSzMiRyfcRcZOBkaRae43L4b/pho76ERbu8T2MrT0PIcKvwS7NJdMJys5IDynL5
CQ6yYpmyRfp+8uNcJYd8WoOLqzEEygo3xJeotPNf6/9F7uraE2fKHUpvApB4+Lj8OvVyePpTXWeG
Sdkj+CwkKYgtDV73/bDRBGRiPDd0JbSLfBNNRMZE9L6+a3MDI9NCWXqkSE2CSY9LraPgi5OSKOKc
VPrUkwm2BjczLUvzjLfHqAUEvuGs4K0oFXpUs1YmSLOvNMLPcRuf9JERyNkE9PC2WD48dJ9DJypn
FaDRzCUY2K9QT17mfzwedtdAu/LbJouK/l3OnVCS/gRrcNHRs8f50uI1SFhCsyOinu8T1EAonsqO
b1DXrygKJgv4cAImLAYDDQ9wLWDwC3W6Kokm7iiQNvIeBGE8fX5WkRuoxnJEOPHSW48FZY3PTNhA
/i/UWSAGdGQSzDUzvRnrVUHU0o610nnfhA6tfv4Lssl1yAj6AWTc3CgW5EW4QvQf96ragK8k6T6u
+VBv0dH/TllGaEOOlXh3KOildBuvRqP0yFfk9CMLv/cw4T9CmpGG1OkfTp3FtUnFYPusJxb5FZPC
xvJ9jjKgYDFGYWqAvONLEhKDbd63TfNFEVnScuLpmpE1y6TqHhnYVpSSMiieYCZqaUr/5xWLXFTP
ACiG6z6kfhd/t2ZUe38dMxKOK7AdrbFlbfKXRBqM/vcB6JHBY1lKUOpHEQu7KLqkitVRXSFvJSu7
N7pK5Vc0Jjn8ZjpF9TJfemhDpMaTr1fUU1h4DeG6ezv/Djt3/Csnzp9vYbOn7JmtajQtgzLGi7E+
FMLmVTNTi8mH3PMtSDBBjSW3OJwPHX8IUYqRT2nhC/BDY9EEnb9OM1bNDkLQbFGoo7p5IzGkHCKS
yfN7UanOUQkVx+hIulN3VPMLM45oSzC7HNCfWaw0H0G/0NVsELxu0tMx8tf+N+knhznoytq1R40E
rWcxJ93sfzU8qMZJplV+j+Bsd8abVotT+WTb7pU8OxBT+jaq72wIsV3/fYvJV3TTkbWL1oEBU5q+
vf3Wqm82Z0KtApxXdH3MlJkihs+D11k98ySBuxRVEamOxQmf6Td+xlP9TPk0WZDiE4FOXYqCyg4F
bkvECA1nnaHXjgBMTBEykbn5VRuQ3Wrix/ZrYijAQwXTSQEyMA9O93RnQ+2C2wn5p3Vtcoj9RuA8
CJyBXYEab7fqxGeOTwlutOlOaQ1lgeVurU+mgI4fwGqvRiVwqsJN6qrmwUTe4+RVTU1ZnBYseJwW
NvKvkKqv4ypgyGP2eDLZFh/HhFkIsdMDZHWTF3i4tBq53Cyk1kRtPJ5ebkxl1CH3TXQbczXClyZh
2RKs8vcHF6HzZ4eKl+V0t/IRKJJkBhB6hvP9xRV90UoiB1qynA6Gq6F/UHaUNuLjcrIPm/keN8N7
ZsWapmv73Voj2fJJ5ArGGZbeBwb37JPa69LBa4Cu2y5gQ0ZR0np9H/zFF40jURsAx+n2AlNEoO9+
Q4RV8k7leJxvC4mhX89vIE/n07JCRp3JVAywMlhs0jDJ/RWQ3btgeeLZZ5d57NOMcqW7foYslthz
Var+uPnOnDszb+8+G8YueHoyK6X53GAk2KsBBOuLJlI0w6mTsZasSt4qW+RCtRL6LjuBLsXR4P9O
lKq/mPz0Fm3X4YszvzXPYJAldwrrLXhw+5TpTRQsR+dEhcXW2Zo7sV/MAEk+fI+sNCqtm2+BGoyP
avWuPxoHPyLS6IikJXz1xgIfj6s46v+P09XoEKNHlNmB1d82/eemdsSisDHA1it8+wTnrDir0c/5
pzFL1sCsO9jqaeLCqfelIDly6eg15F7WkVlmwrgF+8V+Emg/grMicudYrEI3qvs0V0R+In3qjikx
ybL85CrXDJ59OGP2giFajZYZpva68Vd7FJ0LJcLXZPTSPaVuqowMF3ndwYRRQHA8c5PTm7Ayl5uF
TEP+JfZKEDw83NzS4ION9wNyFKdqdlKf3HxeSBM4r/+EmZnO8kpnsz/g52hgV/BAqyRxPyIgREn6
H/ubeiptYVuQzWSm6Hd6ZkGgwIfLjfvHTQ+g6Z7rQJpiSfv+oZq0+bgqBWVEcuIqSZgPoQUcrj8e
kjpXNJpWXXXK/QDMSBSuGuzKBOJQuiARptDQv8eDQYOtJuGrLsImcv2/mpY8MXNK87PZk0BX7fMh
LGTxvINjexB6dDcZ7DuOxBkw7acJM6zUer8FVj1jaDbARC/UTqzyAl113a4I/sIIVNUlKnETm6X7
qmI4PNdt6MXUpb10K6+zb77BArhbpo4zqM5cBu49Ncx8pQseSQxQ2ZRj2o/Ru9GFAyAX0WKUBLLD
/MjR2+r94ZonJBY4nYH6cw7wnGLpodptXth8yGBGTzEqcXqEBrsqZ0KCmrsdvy7UpiG/mJFpQ0cR
Z5svJyoB7xx3VXcklKMmJCCtbcEUEPx9QyvZ4pzBxVW3R6sJQO7HgKL+ktN3F/Ba74ZEkqK6sWFF
JqZ8E/E7bwqY2VAY5cLGHB5wSoIxpG2PJrAsbBpGH9YRuknrpBChoimSvJK4SXwBFCfe7cIzMW8v
XxLzofqUOqTKpq233ZAfG/pBfhl71VSFKp4IM5T63o/aW1SyqLfUheA0Fn8oyqXq7nTVRqtfqinU
Yibbh4PbsHqCRhnBhTBbIqYX6kKGG3hVGQMKa+0slpbojdkz3k9oswf5PBmJpXg+cpVhWX//0G5I
G5t8MaiE8vo/qzAc8F/ru9wt8x7jAO4q+TmXg5KuMsMT+NGGx0DM79w3CWlKwOfTNI2JOogijbpW
M7ftuWztfNg6kyS+OCzIdu/7iyf2DeD8Eu1qhrzlufOMJ/KW+rKn83r+WOStUxp1Hfg4r82TsQnj
l0McOowSVITCbAoeOvOUV3D2ey/TscorkiifTBs/L9UadprTR2gS7/NtQFoStMGj954BMiQwUNh4
KH8woGWVNREDdFDhECqHQGCL8X2zooh+PipuXxodflr9p1aKX6NGOx0Yqc0WZplk7BtpAWwfZdSR
IlYGbBXakRB64ouy53Vu83Ts7CAAFtpGPCeKdwuzXFB57EeYpVq2MOukuh/cesg0jdSnnC3B+FX2
2gCwcPtrcA32RKPtwk7HoW/6zV/38qhYnGtERLT5oQfQGcwBZ2BSbrQmhcfS9hFEXC7N6YMEt6oQ
zTbNtoTs4BWpXf1YWUUiW5xUbaXnMg6tWtEDHAGgeYyZdlZuvSKf3MMN1Xth06qgQcgr0CdSLMUs
vT8JdLnrLfdauUeGsbo30Il5zh77BwoH347h6NfJHltWa0VDye9G0VTRw94L4x3vJv26izmEgnSJ
fA5dXGXlQICBEA0UiGEEGoXR8/42+srQOR0wNz2eHN3r9i5CpjlO9p4LZ6sZPSKUQQlRF3NS1PVU
Wb0t1pgw2zMTGm/gmz5M0lzY1B8NDXDYpCvne1Tce5RUSsVo/8FYHWYh3cIWqdeffIy5L09FS0RI
BjnLFfPinrKRpUfwI2f812+SCxbkOrltukt2Cy5YD1yvDETm0NrKBEvyh3jBhV7LB2fjAsZLmQay
7+2ft8M10Nou+mn1lUfrjTp1KXeDuEdRGGguxIWXwpkNv7chfYllLFgmBXkyfK2XamcG/uaTEYB1
tZ7cABhR28Dd+rhLkDtqZfBXPcY9cIhIbBT0CRGRGIg1UvuqyrOQ4134532xSb7+h4ivZo9dshX/
Lx3ym+oUmsjPa4OzRE1++MiZDpfHolDRHfhIIoXWiwrOYcK2WWoiGSVTWkTkvlfRAv1WfVwcoOvB
+tY9iggtbZmANLCwb8G0iR94FMXQCA5u+dXx3IKSVKF1SDOnoYf30o1Lyg9GMLihVXqXM7QryDes
fqIiK7itR1j+33seWOj2z2Lske7zE6aUZ5VIeqvCV8kMA9kSvPXTlqg9Mw+aXwmMZaLNrLqfvEw2
mWi+bfcmDnD3vyXPZnS+h55uGg0N4wcLlrR0hOi0YlKf5ytjx3UxkRvNxA/Huq0gtGsBOW6aXJko
0uQTGUkN81/4t6eVSXqAw4im+ejX35iu/ZZXafcu2pogWnfdjb+lEWLjFdEUdVMamKmivn8n8Y+4
MZUh6suTPDqjWBR4890ajLtnlx/mUHQR/+3qNslyrUesjIzyRniypgZJNnrrhfOb4P9F+43Edhxt
HmF0nr3XyXiWexTql4JuhlO0kSZsfDxZGfxq3x9ENFwXu7uSIjzaWfw3i4qqsk+P8YN+IQFkPTsx
Ap3mMdl47Aiy5yOhdx+8IlayEitVUUKupC7ngEvwpmJYE2Gn6tkymzAB0nEAlMiCmRdw+/t38z1U
eKQLVQ83QCRZTspxjNvi1vf4RFPlXAqPN8skxMAUuSkmD5TBChN34MUR+od19jLSF+Py7CBd0IH+
mBzE1foZPRK3YJCnhdBYpDKw3G/6tLkityc4WnhZnT3nmrH5Syz910aVHMtgwwZOPUuwRCV13g39
W4aO3H+X2xv0Kq3vDn+0UJa7rdY0ZRm+qY9yjq7t12GLX4/arIGmqTOwOX4iF34SQlsvYAImaeDO
jpyJJkkYcHr1z2gdJKLZvGUVwbqxUXrmEX1ov5MJYta6LrjzeyPtIQYrYgxgMM5STARBIRdMw1r0
X3zpOlnICJzVDf+tqgnEgE5/2icoXuzEjxlufQ6ebpAPRPm1hJXiUj3eTxWBPSVGqwvO7k/KwBbE
6yb1w9nB/b0uvcVo82WUTjhrD53mnvF1n5KFkcbSm2yKrawlBckt8PiWWuInu0t+M6NQ4znVemRp
vxXprathhoXSYRNunFzSa6yfnUAXx0p/LnS1tL2Iy9IN7lIu9JehU4k8mHQtz55fsuhFIDeKtwSc
Jhs1x/MVVk1+RdcH/ERjOODwMS3wDEFeYaNzi8CJEdhp94ypSGri++oSiQF7cxwRuWAE6xTq5pjz
I40XRr7huSvtUez+UGfebP5gLOnpGlfLNYI3ZeEwCOidfFf5z77piY4pE/2LQ6ofxMH4ZEM+yd+V
8M0Z2Cps65udZ/k0zeniKlX/F5QoLrXGFv1qrK1HbnyQuG33lVU5dxnGYQTG6XXD+Uef3d+PF2hW
iDJ4oxOzdeynXf8L1apZI0lu8Uooz9oFGHCKbD18FWFNqWKserYrJTKnAZHpoSK5kLH77GwOtiLU
aU4KGoYlMtyx+K4+UBeWngIeMTEVoWye/qu3i6NMwsFqNs7ykHJ9PxJpO+ZwJKBqGN6LiSy6PYQi
D4pnXLRG2ww7f0AAu1qt6pTuy8YhKqrtxpQZ2wv2KgQzbF7jr/cZcSSyO/GiKWpsG0e2BXaMbGT7
fHNPbpSEXM3W2Ht270R8x68hqooLWczdJ/T2GBqBZDajiADPf1kR/Z6kGIX+2RNBL0eT4Q65eaXG
0ZIeu3/RUZXFgvbioXNPYmbe22prRJRUKhmRiWtqQsBOMnnKIBjout1EVEuIRz6WJSSZEp0FkZB0
lwX86BRC4BBGPde7fEU7uS8cDbtlPAelr5d+wAWYx69GI4F9na45/98LGXNuGHZtT6/J4f0B8kNM
uCqJpTAdy32mNj3bIkM07jWDjNAMMCbMJxyVgPNagkLHtfTtRwjGSUc7C2TUzi6Z7XAJIFKirYJl
SoKBPD5sr+vre7tiKLOQUvxR2IcubMYQt00ntP6z/pc7vOTs+yrZVpwO18PcqDKE0gGKtW/zbWNb
uiKvWAma53pSAdJx8dJPYZ9rrrQG6atkFnfRrd2a1TrQUJg8lR6QRD7E/Lz4hvDWqb7rBiDY/wH2
cpIGBvvmeQmKVqUB2c3xe3aKZD3rQtwkPRhyccne39/gBL8uyZnRYCD68wpOyqPIWJp+j0nyVgEI
/Y8/HRez0j99CzXn8G9DzT1fTc6z78UCvRXQRBMuHKXEL9rKVXaSsAJremmuBtJVbV0TUbttMOzU
2PHj2e+lHnjQG3+TFGuL4giEYTuAMtZPDkyHBDDfX214xyZ6Rjd9YgUzo2F2zjOVUUyrwQLi+rmf
YchYo1AWfLNyRNjB6psH+dKKM9qRPgF7gvWfNkIADA/Q+3i+nVZyagx6enzXlu77tiJlwk624WqZ
355DnHN7W/R3x+fbJ/il2YRsdfSnthGr8k0L8DqoQDk9IaF1C0LqTv1pIXxrJD5OIUVeC7LaGGqU
cu44B5ceJhE8OIWJyjaQcW4o/kF7M0SRNSWCKseLSBUuLpPInrtVB24eUWAaWvslgpbbigZzOY8/
GCfafxC9BiWDMsKpdGDqLzMcfVoec9AWAwmn/dp9V9btYfE6gLRlbZo6wqxA4ljHsv+iAGfvH8T/
qllu4cNPgBnOcpNlWe6MaNoz9CnyRnn+cbD181erKcGOksYJ2Mb4n5JQjRLMTU7w1wT2EUGzD3et
TljZRPiBO5kqxUvUr/p+KX73jgANLbYn/3aTuPmGv5yscoflioUWwx+iRgHB4fz43V24FUU4T5xE
gDAXxirrh88dPETlNc6RzOTcZGmWxJkj3ydQ9BhOi9R//HHILTVuuZRyEjm0Not5kOkDov8U3PNo
XK3K0rZOPb2EDHxaKyieITapIPl+p2QMolsLzQrVrWjqd/IM5dMyQZihO0kP8wXiYoK/KY7JVkHw
8bBdv9gfYfWfsazW55W6Cu4uEoxT0LRB88IjgEw7llXYUo9m2bHybpg1/G2MUaLsnb9DAVRkU1ua
mZ+b/dzJJe/rs5Ywkv0aY3GD/RdM5tGc72QGoKbNQA2lEKbafDpmDEqmXvQT+Ma9NGA/lgmMAEPk
qNE62lW8+PxdUXEAydOQkpkZJO0tJurqOxbVyYdVaJNs1YUbdbyOarMzReFHaG6YlmWPezTxI+yQ
FJ3p8oUHrkcSdxjMM7aDu1gXcynDCIHLTO5yrXCXr6YiPsLD3xx21397dS9O6kuKCy4tRwOzRyfL
G8eFBIPNrtz40uud5Xn+fYKx292a8frdDkOgNid4EX9oeh0CGsTEBzsddCLka07675eksl5vClA5
VKEBvomzM4QNxAv6PuB+RQovIYtG5D0Tv5VCOlHNHDWPSeqMss9TyFHHJ2uDbB6Q+ljfEZqAMHlr
O8+KttZr/DphDYf+VeVz61ZPPiMe/qHL2PTDMNyQGpAStHhF2PaCDBTvTUE7w2/6QcgemZ9BEuL5
QM4BAVifcgXX7EB0avI3KLJxl/tsID0fi/Ex/XY6bOuHcOPWbS61iIp6r36/M5Z3wCR9LgyjiZRR
86f6tzM2v5CEp+Vptfmet+DE6UerOG4bswPfnw5F+mgddSufC/ITr1yjTy3SsFBE4Juoz/dk2ZP2
NbWt/Q5g/RdQWBPmosHh/wj0cmJAZ0i+iujuFf8kGm4SmVWh2DM4fAKVKQmcChQe1qxytiA+LI4j
XOCCV9DZxsWGc4BhZvuPAy2py8IRWDAYxXtidQYwIGDFcGfPzZ0jBKpcwabS33LPmRwWCSeafjbQ
u5RvMvL20PFi15CY/co4OGTCp+8LbibhDCKbnX4KW7G5qU4F5Xw20xTEjLD1zTOxO8uJ5Aekh1z0
5aisLk35JGcVueYpJ1kwbMB8DZ0uWyJP2uiSudql+ve46qdGnwBlWXaMmby7rPwWnxFtUNegonwF
x2ECyjJRdiCH+QdIf2msIpG4+SLBtLDRv3f+pK3M5FN5/sc4Mq+q3A5b1mgaoA8O39AfpTahNZVW
QOj1RVyebtZ4IlGx9EZxPnBfwSbtpH4WDJeKIohvnpIJBKQL1urF/pcfYme8O827x5vc4qjr4QUQ
7geJw8hPLZu7b2H+mJqIVtGx1hYt2kR4wfvrMxqNLA0CivwFR5AwfZhBXitI8s2aiHRMX7Tekbk7
B5IqtkQn+EUJhTXO2eJknKR8qOF4U2buMxchpBf0X6/ipJC9uw6UwyY9J7FFuRlVGYfQaj0X2mH/
yO04G5Ey6KfBYSIpU7L1bk+8BWxlUdv41Cn68Mq1YqG2qpFIrsqzmQNPLR5uuAN6lGxdCwhIfICz
PqSd8ruGLA7eQ2cdD+RTclBcezxlFw9MIx7BjaqcawMgg7Wsm4Zzop6/aSIK5zJOkUmnajwdyjeP
K/vo6uRKagGM/Sr51NAFzrJIfDrXqq71hKf54Q4O/JLE/jiJXIcXY7UVXbJkL2BOvWT+d8hErJBH
kQ1wrl9idNGDIQZmbHft6A/UzAbfuhUfvhdHcgD5EdMjk7GrM8QFSSoiWuNfB3CnAXtPfKg+X8K+
Pbyu65lwj9jfNs0gFJMAPFt7P98TgsH1XFTe/+xkOjqLik48+w14L7GidO4zckqmJe5rBYx2pbrB
SxxrsIDFdxYwYHx84CkzTxXK6ED7KZWMhokVSSCv0s7GD9QDJBZy6JWR4qCIHvNugNgvDU4vgNkM
ozQddLirvNLZ8nIe8jyYCQ7lupWfBgYpmTOGbAVFBCH+iP8+9BnHjW+ZHk9xso9rOgtxWwi2YEnW
0WBMEz0mYXNQo/AYEzGTlz1+ldpUqByaz9TPBwq2eRbYPpfCx9Ik+HFl/zDK2nPGkHAaJ3XfQdDL
938cx2dl1rT9b92np7f0gUT4ybbADkVXjSrwfQEIJKbHlglAdHBJyAOAMOs8H3u4uxTvNSyAyqcC
u6Dg22vo4suvJtsTIvw1EZdjtYRgNgo0j7uj1DkftOWt//sqtBQzVrQ3qLbMLk2Tz7ZFJKb1Kplg
6of9hpIOqiR0MvSIqYqC/EAKdQKroKn76tKqHE9qCsVBnokgfExmQy66JysOnD69Q9UjWZK4qwcr
Ni/5X/E1MbMcbTMwWmypdoNEykFLFORmu65TbWohiaKZASyqolPvxZPPbLAci0yXASjDjba/yByE
koMDpy+Eikyf96DolLomkjgDRq+BmhOwdEq0hrIjvOSOJFoF3DaWJbzXvAznWYGaUtchW+pONuX+
Mw43RulgT3uNFAEpdYu+x3o/WSeCcxC5B2s6muIxF6efUOmuWgvk5LgxJ5mhRjh4vvxmS3Tbtwd0
N+fgkJi+kn+/kbURz/aoM5AjK8CT1SD5lCbe20Ps+c+VG6NELYQ3JM9MUAvtonUdNJ57TfWUUD+k
7EKSrq+DqBwy3DI90RD9zxoWRzJhmQHxrd/k2vMIiCkz3a0irS8q3MC5q56SNaYDL9SZ2mPrfPHM
GZATbbDCeYO8p5kJa063Zd5j9xhTMaQEfxE19nVu6Y2rFMX26VPqfTHDoF5s7MhWk+2l7cmnFK4W
3y/LMVVHX7v6/dP9zCTlVPmq1nfcMVMDTmfLztCNCvBasjSi13q03e3q3gw7zyDibNZzvHhI3HKT
WrIMIOGK6UuF6Dg/WrP3ji1rE9mx8A7l22yLL5QQ7HKljmCxXVgr0RJix0oQ0MYUXjfvGmg1ZUoh
vug3ys8nSfhl9ZgTuJ0h+Zjo7uspnFGU08JGR0/MtWMbCEjgwl7Gbgse22wF9H8BvLXfFXBk3++3
CzbvRzwPMtMsjr8pet+z2FWBM0xH4j1jMtfMZOKQ7XnqB//euvePTtE+7pnxako9xWafsJUsE++6
cDJTJpeg+g827vb/R8Mk9m+TcXrgcrpm3aAyjzdVc3xOD22pQR4fT0febQUEt84OnM5p5K0+Vxgk
mKDYzOhkWjm2xQgf04w0bxKhAnwMmT5a1sBc04/HzAxrQ1aKlK8cwbT9dFhaf2yj6LGE3gO21vnV
q41gTxqwsIFJieLaL71byfcqXtTvbGjV3OGt3qiqMQaBpj7uz0GsNgLqEHcyxSnbZUST0C3gHZeS
15uNVcu13UKvhEm9L2YEseeJk6cta1C0QKw4d9cO/yljSogUXxZGgWuRTe4yJI+wUPXx7JVVo3Oy
s7I7dWACVXihs8MwrhlvMhJNH0OHuLmVBhsmfxVd3GGkomW6VxK7sylrLBovS4HjJT8M+BrTb4AQ
B+L++yop1/YNYp2TWGn1YQY5XEMCr0s/ZoTFVbqj7CF7YEuHwtQx6C0FoENr3aNzajvsB9RIPgn/
NzfccLzH+GJT7vW+mxpJUh93UwzN/kSYEoZmoJm2LnyfQXYbnBgGIU0XldHkfc8F2D4dW2jJT7Yw
lCP6siqKqXfHG05wevISwrBbJGDM9KTSc3Ih8Gjx6pUka4/WH10z+ZLg4iUGLr3jWHmwnrYCRwDm
oJOw4aH41hz9UZQ/nMH2ePfjhXM8tD5kTE8x9sIE9fbq2BBEqCyRLvEP9Xa9hLDc2T/DZNOztchR
NEqmhonzx7GU+XKjmdd2LeHFGMQvV2bRjM5QLNki84ZDwWZj45Ew+ndSV0IBw4nzn6RB+RmU4c3L
ILDvPTUKJyWNLz87YdWCFYjPaxXp+W5JqpIMVPZpjaiNeHeUCpRsc6RnxZimgTjqhl7Dc1Ng6WlU
p8xOZ+3J9mfnh+acvwLjg1geC22wk/6txPorC8x+2FuosLqz/vLgUSqJhQG5JQbO8/jtKVJ4r+xX
J/4E4F9fLj6A4saZftJTJyL0YqAnnEDXRxju30qqA2rlE1zuBTrYCgBoLD5dXYj07bVviAOpnO28
7hbsZB7aAL1F8tvWxvwnRhTXG1lU0m+2vEN3q21ipcfoTJma2XMu1nUnEubjiwfb4nWJSGDwwpfV
hFVsvk7Gl+OFYkGAvpppSBifz/hPq3XJoaxEI01x1LRkJSABUZXzx+y2gHbIhRm/xOF2QEVT8oA0
7IfTGludc3eLgYhVXf+5gf7c1M9qQOrbDlt4x91zsYlaN5yFS/QPSK7sy4bPJP5+b6CZRY9ZO6Js
F4OSrqDKBOwiJwN2k2zziQgH14+51R9CZmlfG0ksGgznb/L6wfANxIgGn3av+MaQFdZMmYXVNTSq
/XoTdR8Rvu2LLZYyld8OJm9LvKITtaRqQnwQ+UN91ycM/uYHJahvYKrFMTonIxN5cFNnW/02mDwY
l8xmCFdzJFpqZfJ88MJEYmNIluUkPPg+ph8GciuACuwfkro5AS9xI1n/zQziSCN4xipZErSWOMSk
dlqXpT/OQsvBTw2BodkiKphEcUIPDuoZEnnc24mdiFU8XeRysAWians0DpP53TcKbS3br/q/wiWp
N7CCnTdAy3vbTH2bDhC+roe3Wqob0CKjzCZC1IKTtVM89F8xYfeobOkVC8WyLPGUq1SIfmJHz580
aXuEJ4w0A/eVfkdnZk9RZF75kkuFi8npSaFjBnoganWyIdHH81pFzpnmS57nhkQQMrS4dVYiPFaE
mIR3+mM0rwjuNTHnrdOKdoo0HuXLZQJ5Ub9f1Rz05JWOH4y+x/wTps41LnILPZU7m/bk/WM7tBuC
GFN/uWU5JU1BJBH7ZM/9DYK/agWywCNOqbiwof/oXUzhlA/kTpXOmLsa3d0nv+B+JT7bU2jEvsuE
wzXMeBw=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
