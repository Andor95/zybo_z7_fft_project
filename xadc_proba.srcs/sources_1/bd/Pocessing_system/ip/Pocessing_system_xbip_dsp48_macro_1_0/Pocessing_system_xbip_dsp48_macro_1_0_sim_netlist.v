// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Thu Jan  9 16:22:42 2020
// Host        : DESKTOP-8BPORRM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top Pocessing_system_xbip_dsp48_macro_1_0 -prefix
//               Pocessing_system_xbip_dsp48_macro_1_0_ design_1_xbip_dsp48_macro_0_1_sim_netlist.v
// Design      : design_1_xbip_dsp48_macro_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_xbip_dsp48_macro_0_1,xbip_dsp48_macro_v3_0_16,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module Pocessing_system_xbip_dsp48_macro_1_0
   (CLK,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF p_intf:pcout_intf:carrycascout_intf:carryout_intf:bcout_intf:acout_intf:concat_intf:d_intf:c_intf:b_intf:a_intf:bcin_intf:acin_intf:pcin_intf:carryin_intf:carrycascin_intf:sel_intf, ASSOCIATED_RESET SCLR:SCLRD:SCLRA:SCLRB:SCLRCONCAT:SCLRC:SCLRM:SCLRP:SCLRSEL, ASSOCIATED_CLKEN CE:CED:CED1:CED2:CED3:CEA:CEA1:CEA2:CEA3:CEA4:CEB:CEB1:CEB2:CEB3:CEB4:CECONCAT:CECONCAT3:CECONCAT4:CECONCAT5:CEC:CEC1:CEC2:CEC3:CEC4:CEC5:CEM:CEP:CESEL:CESEL1:CESEL2:CESEL3:CESEL4:CESEL5, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_sim_clk_gen_0_0_clk" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [15:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME p_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency width format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency fractwidth format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}} DATA_WIDTH 32}" *) output [31:0]P;

  wire [15:0]A;
  wire [15:0]B;
  wire CLK;
  wire [31:0]P;
  wire NLW_U0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_U0_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_U0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_U0_BCOUT_UNCONNECTED;
  wire [47:0]NLW_U0_PCOUT_UNCONNECTED;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  Pocessing_system_xbip_dsp48_macro_1_0_xbip_dsp48_macro_v3_0_16 U0
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_U0_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_U0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_U0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_U0_CARRYOUT_UNCONNECTED),
        .CE(1'b1),
        .CEA(1'b1),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEA3(1'b1),
        .CEA4(1'b1),
        .CEB(1'b1),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEB3(1'b1),
        .CEB4(1'b1),
        .CEC(1'b1),
        .CEC1(1'b1),
        .CEC2(1'b1),
        .CEC3(1'b1),
        .CEC4(1'b1),
        .CEC5(1'b1),
        .CECONCAT(1'b1),
        .CECONCAT3(1'b1),
        .CECONCAT4(1'b1),
        .CECONCAT5(1'b1),
        .CED(1'b1),
        .CED1(1'b1),
        .CED2(1'b1),
        .CED3(1'b1),
        .CEM(1'b1),
        .CEP(1'b1),
        .CESEL(1'b1),
        .CESEL1(1'b1),
        .CESEL2(1'b1),
        .CESEL3(1'b1),
        .CESEL4(1'b1),
        .CESEL5(1'b1),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_U0_PCOUT_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* C_A_WIDTH = "16" *) (* C_B_WIDTH = "16" *) (* C_CONCAT_WIDTH = "48" *) 
(* C_CONSTANT_1 = "1" *) (* C_C_WIDTH = "48" *) (* C_D_WIDTH = "18" *) 
(* C_HAS_A = "1" *) (* C_HAS_ACIN = "0" *) (* C_HAS_ACOUT = "0" *) 
(* C_HAS_B = "1" *) (* C_HAS_BCIN = "0" *) (* C_HAS_BCOUT = "0" *) 
(* C_HAS_C = "0" *) (* C_HAS_CARRYCASCIN = "0" *) (* C_HAS_CARRYCASCOUT = "0" *) 
(* C_HAS_CARRYIN = "0" *) (* C_HAS_CARRYOUT = "0" *) (* C_HAS_CE = "0" *) 
(* C_HAS_CEA = "0" *) (* C_HAS_CEB = "0" *) (* C_HAS_CEC = "0" *) 
(* C_HAS_CECONCAT = "0" *) (* C_HAS_CED = "0" *) (* C_HAS_CEM = "0" *) 
(* C_HAS_CEP = "0" *) (* C_HAS_CESEL = "0" *) (* C_HAS_CONCAT = "0" *) 
(* C_HAS_D = "0" *) (* C_HAS_INDEP_CE = "0" *) (* C_HAS_INDEP_SCLR = "0" *) 
(* C_HAS_PCIN = "0" *) (* C_HAS_PCOUT = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SCLRA = "0" *) (* C_HAS_SCLRB = "0" *) (* C_HAS_SCLRC = "0" *) 
(* C_HAS_SCLRCONCAT = "0" *) (* C_HAS_SCLRD = "0" *) (* C_HAS_SCLRM = "0" *) 
(* C_HAS_SCLRP = "0" *) (* C_HAS_SCLRSEL = "0" *) (* C_LATENCY = "-1" *) 
(* C_MODEL_TYPE = "0" *) (* C_OPMODES = "000100100000010100000000" *) (* C_P_LSB = "0" *) 
(* C_P_MSB = "31" *) (* C_REG_CONFIG = "00000000000011000011000001000100" *) (* C_SEL_WIDTH = "0" *) 
(* C_TEST_CORE = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module Pocessing_system_xbip_dsp48_macro_1_0_xbip_dsp48_macro_v3_0_16
   (CLK,
    CE,
    SCLR,
    SEL,
    CARRYCASCIN,
    CARRYIN,
    PCIN,
    ACIN,
    BCIN,
    A,
    B,
    C,
    D,
    CONCAT,
    ACOUT,
    BCOUT,
    CARRYOUT,
    CARRYCASCOUT,
    PCOUT,
    P,
    CED,
    CED1,
    CED2,
    CED3,
    CEA,
    CEA1,
    CEA2,
    CEA3,
    CEA4,
    CEB,
    CEB1,
    CEB2,
    CEB3,
    CEB4,
    CECONCAT,
    CECONCAT3,
    CECONCAT4,
    CECONCAT5,
    CEC,
    CEC1,
    CEC2,
    CEC3,
    CEC4,
    CEC5,
    CEM,
    CEP,
    CESEL,
    CESEL1,
    CESEL2,
    CESEL3,
    CESEL4,
    CESEL5,
    SCLRD,
    SCLRA,
    SCLRB,
    SCLRCONCAT,
    SCLRC,
    SCLRM,
    SCLRP,
    SCLRSEL);
  input CLK;
  input CE;
  input SCLR;
  input [0:0]SEL;
  input CARRYCASCIN;
  input CARRYIN;
  input [47:0]PCIN;
  input [29:0]ACIN;
  input [17:0]BCIN;
  input [15:0]A;
  input [15:0]B;
  input [47:0]C;
  input [17:0]D;
  input [47:0]CONCAT;
  output [29:0]ACOUT;
  output [17:0]BCOUT;
  output CARRYOUT;
  output CARRYCASCOUT;
  output [47:0]PCOUT;
  output [31:0]P;
  input CED;
  input CED1;
  input CED2;
  input CED3;
  input CEA;
  input CEA1;
  input CEA2;
  input CEA3;
  input CEA4;
  input CEB;
  input CEB1;
  input CEB2;
  input CEB3;
  input CEB4;
  input CECONCAT;
  input CECONCAT3;
  input CECONCAT4;
  input CECONCAT5;
  input CEC;
  input CEC1;
  input CEC2;
  input CEC3;
  input CEC4;
  input CEC5;
  input CEM;
  input CEP;
  input CESEL;
  input CESEL1;
  input CESEL2;
  input CESEL3;
  input CESEL4;
  input CESEL5;
  input SCLRD;
  input SCLRA;
  input SCLRB;
  input SCLRCONCAT;
  input SCLRC;
  input SCLRM;
  input SCLRP;
  input SCLRSEL;

  wire [15:0]A;
  wire [29:0]ACIN;
  wire [29:0]ACOUT;
  wire [15:0]B;
  wire [17:0]BCIN;
  wire [17:0]BCOUT;
  wire CARRYCASCIN;
  wire CARRYCASCOUT;
  wire CARRYIN;
  wire CARRYOUT;
  wire CLK;
  wire [31:0]P;
  wire [47:0]PCIN;
  wire [47:0]PCOUT;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  Pocessing_system_xbip_dsp48_macro_1_0_xbip_dsp48_macro_v3_0_16_viv i_synth
       (.A(A),
        .ACIN(ACIN),
        .ACOUT(ACOUT),
        .B(B),
        .BCIN(BCIN),
        .BCOUT(BCOUT),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(CARRYCASCIN),
        .CARRYCASCOUT(CARRYCASCOUT),
        .CARRYIN(CARRYIN),
        .CARRYOUT(CARRYOUT),
        .CE(1'b0),
        .CEA(1'b0),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEA3(1'b0),
        .CEA4(1'b0),
        .CEB(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEB3(1'b0),
        .CEB4(1'b0),
        .CEC(1'b0),
        .CEC1(1'b0),
        .CEC2(1'b0),
        .CEC3(1'b0),
        .CEC4(1'b0),
        .CEC5(1'b0),
        .CECONCAT(1'b0),
        .CECONCAT3(1'b0),
        .CECONCAT4(1'b0),
        .CECONCAT5(1'b0),
        .CED(1'b0),
        .CED1(1'b0),
        .CED2(1'b0),
        .CED3(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CESEL(1'b0),
        .CESEL1(1'b0),
        .CESEL2(1'b0),
        .CESEL3(1'b0),
        .CESEL4(1'b0),
        .CESEL5(1'b0),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN(PCIN),
        .PCOUT(PCOUT),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nT1iDpedwZFVkRSZDJusiwI7kFIMBvviCRm9M+pZKTgQdGFO5jX8oqNrtlexCu/uDfp0YQ+QGyHf
W9HJmnELyQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
LSiX96nVtTeT6QH6SYBUiN1RW5Mga6q/2lxWqXdOG38n69A/VIFv4MZSHjz1gILFox9JEY7OFwGs
6ebz/mUxmwP3DNumoccQ6uOcSkKQV1eRSlyyHm4UhahbN/tD6kRdHgTGQgjiOPFINjK/bQof7LKF
xQMmQeb2+71XHcPjUHU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T14r4uT0q5iPsUM9da3RnLjqN8Qn724f3Fcj5n9r1n/OCu7B1m+A10bBZuAn11d+eTpUOqwU/X/p
2zzSaUcTE8ijWpgSLXU8J/0wcBVyuWUHOoOpFIkqda/gzGVSmbiUUBGDhktV/P2ktOR9PeMW1pHu
QeJD3NMerGL8xO8RkFz8+37CXz+yNeWbl9EKsnw81po0312geoX3g2TFZsqRUaRMVN1P8+qQzlEb
OAUU+/BYNrtsGGxq57Lea7LASqCQSI6ZVYSocjpQzYz+zpK1Ifn6KpwvU5YLStgDnK95pF56yxWy
4DsarUkJGiFZnz4hzdYJeRLciFb00Y7Z7OHKXQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JB9E+rFzptTgWubhsk/ytb/NrSJBaKLviXMn62i8KWfOUbd7Q37B9GOtkDXor5Q39oNYqlzgkXQQ
9g+vxtDNbMGPBkiP8HfN2tKmqAP3203t/R+B1D0CmN2mK9Bzwi5rAw0zNBanLu0Huhygqeuyv4SW
RjQSZSiUCtH8UQpPnwdKQSS3zlTnpPv4po2tgA8ZzjRNyXUAFGD15dFRCsv3KN9TGY3ySFrBZTpy
ddZI86gPVOR8QamQKAtVPZgLCYSIOtqQrQOt9c7yM0NqlnlC0kVD8X16GQ8LchOJaRRndKljCiJu
T7V6wUYHHVdREAeFxWPEgIwm8uncarb/xI/YFw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
hiRSLr4QLw5/mMP2Zn25/s5s8AF5rzEvu2TjIzKu71zUg0RQR79nm8y7jnlLFI54qMdeDd0ag1F9
TU+c3zvS4L4EyGAGLDGmOYcQ2klSCEkAp0eYHfZNyKQhLKpfpdEXhwpsfAMa8mfqBL6skxrp6C+l
wSbnOqvq502wmvReAdkBa7hQBquCP/Kxu+jlOzeR76T33fKFxe/GKjVFC7CzkdJFg59HGnCzg15A
KPrAj/GAtXhrFFCtzppSIgO8GnVXXMrxXlQOTW8Pa8dpXzVVlhWlbclRL5vPlMcPuo76TstX69zf
yyp3rGNQXyTGQn2cIxCTDQ183lOjoKza3cx3JQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
YGcCI/CcJmhsdgWdOuARrKP5BvDGllkS2MoY0dfL6ioXfX2lO7pKY3qpVerntGDre0ZdXSkxLBW4
1veoXYSLGmDdonWSixQKLqlzm2MuxscRuCLcic/Y885s9obEV+bR2Ys2BljpSBpVcE/Ur6Ywxmzk
LxfHQW2SwTpLvo2b2fY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Qfahy1mSmZHw7posW16zQRrSI47b5EnD2EOzgkKc27KVqFCtYxFhu2K8HcIi4g9qHxVkiuCMS2xv
+leE7EvRlzy778OaDw5sNTj6pKXuDNf0TM9Z5qWIQfZXHe1pN3vk5+JwIPlnKOQNdR/ZvyF/MGlN
OiLTikOABwXxl8J3xz7JkKAD22NG7mPIcFEx4r+67vvFAsaNrRdR1eeZqoEWtdnoXxed7RU4EF+M
gRoH6yIiT9Y1/s6OYskQ6JtiRhnYtAuCfzREnZAh899nzaIcLd7LEVfL5Iz9Ugu5o0kDqSWTin3h
e8cg4A7UdkCUVgAKEJvninJ2KykH8gXo3fcIvw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iH0JES/NvEFuTjt4he9VbmbaRMMC3Az+avxSvav5homUT4AN4kL0Mr4DduP6qq2S7yA9vmLjymM3
vUy0LCnW4MRJjyOhZ5+lKiHHna+B7i67+QBNgfHJZBo1o/MQKyhRY1NEPO2OcbO0Fa7vWxMb0fxD
tLYj3lDptr2t6lE5QyvfRkm0+K1QUewQYhPoottRx/YRIB1HGmuGxAGvtktBkl8YKImYUAhUsulz
EIDhzDvY2mjO2Xcjs4bARB39XehWet0GgDIyU7JIse7GFBpk0KwuFUmt8GyECacKOV3tNHp0JNby
fTW1A4Q/icYz09CCUtWXTEHhWrJdwGru1IPXUw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EJbk8tLLjlKBWWzy9ed/VxLmGUjR/VyjMLsCneAE+j84oRp+KvuysqptdWBdiS7vf4A7tBZ/bLRt
i2XAajV7I+0PfuDBovQu1eym/cWgnSSLDzT/4VP1p2nj5nrtcAn9tYUy9j2vRJKggOQ7q40vZBV1
634FZbpF3fZnOeqhBPy4C+83B6JqHPofffC+hmvoz91FfJThjOv67xFe2cyVlop55qBbOiy+dgF/
gFoSvZBYPkujshJPw4v0bGdz+YfFgPqDQjQUHNRLlqRKte12d/jEOfxr3FTz8UVNsigk6EEgaP78
IiEcx2HkSGbTNf5Gv3xxX0JktYxSp2ciWXghxA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 30336)
`pragma protect data_block
ABU4Lc1mJ7cl7Si2T0zk3JCvOzfYIkjbSVg5JGRGipPY1aWOnjBtNUTwq6B97E+5xbZq/X4viD0c
XRnELYI15mtXrYokJzdAGFM0WBVvqZdAyIV4SPZ73lzbeT7vjA0byJqfDltbMRXvylKRv4VtZxAL
Ly0Yfxz50DOtuf/QW/Xgl7oeuLh+yHkRIDaNleS97UHjOqcFX1kTXDAUqE4C8DFd1cprqTEpdayO
vJkjpKg6bTSCCW0yEr2LvQlfrXD8hrbWoHhf4vbiy04cRg98Eo6eBcBNfBdrO4R+lPJJykE9AUwV
3Um4iHWdenXaulHEs3B1zI2XvJFK3Ro1phPlb9SBYP+zWrUfvsJcnQRUWzkuw23iUh70iVHmY0Rg
Usx90pQSvB7vqV69dsnhWXSiOILCxu0DcAhK7Ic7vbz+h3hTNqXVx8itn2P/XyE9jabZZxRIqer3
Z/v1/FRMsSYqc5rl9raoaCj0+NLzqu16b0B9nBZRPQ2GN6GM7ctUtsIm6okTjtY9v84/DsAnYKW/
SPJke+mmJeBIXqfJ7/EriNzGb5QTuQhsEMipaDThRtRt4elmry94Nviw+pm+Uwm5TVpIY75EHpZ6
dH1JXVtpsdI67UkezcFzWiVrst0ITy1eBjxGw+BhUgbEL+STUx4VJxXdXjAXVeJ/3pQJeyT8A1i9
5wYGm+ShLFDfYfEsyg3sSpak6HRdWZFDXGXq3hd+ak21NlkXRII7/0bztjgjHbU2pjlAWavVywax
gZThNldp1+oYnmI6iEvJGwM+DoK/IAbdV4xHgE5LpzD7tZCZjhTAWif0mhxSxoruwvveInr3NKmy
uivRu653DdtqUooEyiL3C4IQs/zEuOMv8VeBjJW/UyYLjKyRweH2lZ2+mYBz3W/aJdaQQ+HuzYBg
bBraW+V3dqEp21JAMLlpqgvCpVqBFG5R4svg/SMUbz1szXsCbs0PeV08yhnbaa5kwjzfCSQAQJSP
G80ngoy9OS4AQ7C/014Z4J2dtLtZ8Q4izz7OD7MbxEA3O+jaYeT6NOUydYhGwZHUZt+Fczm3yjJI
ALMDyO+B/owzES5y/ggDtwb7WEBlWA2O3WWybu/pMuV7kxaLuFui1P6oWzrfbzPenpQAecLKtjIh
cfoBWFOc5b5tGhLn1gOM0+XIxvo7WoYAaM1prYs2c7a4X1tGguITBU+ADwFAXt/quuePnyolLEuj
990fivc87nEmuOM4IQ3cVeZf6sxeJK+h47uLB5tKZKRIbpyZKmOLl6UMWL1bbQ0p+BHoavX6V0N2
Dg+bPCJynDf2qB6h3K1jPsq70R4i0hk63c0nCQWMrvtJVEF5yyrm/V/8+6jICKfBn9DCovkyAjX+
hpExlXKmLcfxbrxZ4BVtCTKhNEbGxj3ImAMW/Qd/iO4cClCb13QnaKo3snxsj8tgvCzFM4ubw3af
fwIDz92kCq7qix3evv+Q9ZSSdCxMozykaIHaNPMO47NHPLhI2ZOwiqZIV05V8KrsNPZcGyPX2316
/gjsAwdTd0IGMi/bggO13PlfMnWIx0OaALGMQEJTIkR32/eTvovdcc8+0OUYaiyWhlo7ubw6b1z4
UM4gdtn92WVdb4IVQFKtktX/EGbM2CJVw17l9kgxzF/MMTsAteh483V4+p1nDXFFtjy3HE3Dkk4/
c3eEdoy3tMVlR8wGl/OsdajHWx0XVdo2ye1emNdNb3xuGGGwjG06JS6bTg2gwW59mXD6HGDHbq3x
zotpltAZvqCvCEdR/FcQ2pmZQgiFaLOZf+Jwu0ueAa/IljeZ7kPKCd2lTLs4aXdsIZzHx9z8pHVW
9nEunIoJc26y1o1qYIb1QeY+l/2FEqJuDXUYr5m5Tb1EWeqiqMOnFj/y4JMVv0iqifW+M7/wfqT5
ZLMd1cdrpmjYZBdTvoN8hMgl5FblGUiYeENg8adXuAuI290sbWOdPUT/5D+7OjRpwGR6fiN1R0TV
SiTT5Afh6P5mF2OgmqdCBMOmnbKNh1aVAlXD5dT4eK2QKUrfsuYga/5IktrhhFEi8gpXkU/HqPf0
/COs5gPJyKfGOdg6Yb8dS9wKxT60I6J2WHxU9aPLYrs0fPl5rOUba3ddFPUrAmvd2AQ7lWP60luh
/3udwxiXy+rvxkeFG2Bn+5ktgVzyss5Jh7C5Je4tEGTZsW2gzDCdeMkZSYG1NYhdcOEojKIjcUiA
L7hHLZ9YOeBsqZXS5hDwJc/t653jwCMfUxqyrmh0ockCt40RgnCtqUSUJBY8StsqOQMuuIJJn3pj
YI0QU/1LyphH+phCwHA/3TZqGQovqwVnflWsOOSnvkqURFPIaTjR12toPvH+iem3W+bxTgwVOQQY
gK8qktUlBlCsp/ZA+91J/DWwx8XmoRhJQ0NVklYXmpkcHhOo38cq/Yzuf9v8AyW2cnac0FBU0DQm
qDS/GFFtqlPc3Qww/La730AW0e2KzOlfjBIHyYWCSjajKXTuBUDN0rvQVlYZLuv9hITpclAKyIgH
0hmp9o14bOpRwabLfjjJcHG/sqx7hcGGf8xNa09rS01e568Ue157/LabIfawXHch2ORyUBXinW+f
U7NXsOtQqtadM2us4IOedpsGA6nBW/9AMhnks7yBUTOUvaZclhg2FJRJ5BAVmOr4tjD7UfIO4tS8
Ec1aRJbS4U+RwDkzUZr4e1W7CP3ndZCucx/84XNsQjI6rdN/ajIlgx5fQh0MoBeVXAmqjstYGnDt
6VkWTtaqWFLOFwYTPYtDzOWqEI6yTlWm3dOZtFDqbQIF1tacBRbsyKcmfYuOlCCQg1FNr2Bsk27w
93zUhWU6ZKED1yud1uSeERU5S2iX5WiF2l+fETR+A5s6Jkwe0/mcyS6/EXtGa+eqoozXlSeA/pQR
kUgaRbXb/nST+0H7/0uLQn6Jv34dzDaOlmgWZwJXDrjpp8NjYESK70V2shWu4MHH+wltcvhLvNEf
IyvdZfL4fTrrADgcu6XtWc5S49ES8FmWxz1aWaJaZj5y5dCklwXgqwSblvfXPRbAP5rtbp3WR4XQ
5O/a7l+zyXErLKqYn/PfjR8bWhHVO8bDXniMUNHnSkU6b49I5NjcFHdCqr9Y81SpRgNJ60+m68LD
yTO6ho6rY8rcBywUOVI/jerl5HOfxIARtrWsEI7x9kfMu03OjsUxHESgcDQDZ1/SNV0MX6qDedRH
3TR7UzchplKDBcAMozdgCPHAER8GT7/bMwCwT6AIolCCJxpW37iqiwlEqrcfI0SMTdgzUY2eeUFr
jxDNAa4do3CpGEIiwr8qBJEfcUHcawtROy9Bw58bS1LJpgMwf8nAxMoNFzZOQDZzEQ88HnpgNMmo
UstKynm1p7rdLepbGzpUDPM1cmmCXSpIajEJabU/8ohis/FubjrMP74BzgJQhpI+mm5jxBn9V7kC
QZJrSvH5PLSgI1Q0/4W/6Brg2EReWT/WFs+XdRc8F3b4PAAFEcRdQUFrXEb7lgG6rVdXOa6BHEnj
kOU5pFLA52UTCqICX2Wc7CfitTUvNNRxGE2W92kVbnoOXROjHFskhIqvO4HB7hCyevkfDf2zoo0z
ksMFnv3FFL376Fov9YO06DYkK4lTpF/5XDFq9ZsJzEHmc956BhTjDh0Sn7eNaAc33a02tpmERXhR
zVLTVR4iNQjGCrz5O3bh9DmNEfwuTZpITAaFumo6WQpm3FKCIWzwQjHnge+8bsoZWO+wgLZN0xMq
YLvtljoPpzNk4oUGVPFD70t/ayXkcrmVpWd3V37zbfQ7e+6ueS6WaMDyFtneFrnOo+GQWuf1tjJs
oejuIezPiPPbKe0dXR6yHIUAfquHLhbIR1kYle+k7OiwYfCYcs6YCz91bJmN1IQDr44OPDeYLkz+
Jh66ahHQj5XmtIUlqM0jSwWxPC0WqQQ8uhqVcZVXLNy4+3aNpu8n59W8siuraaRxndXgql/CuHDe
LHKdto+4gLyXAQ1PpavlOX9oY30eN88Nb8YYnmuEqQmx90aw8TsNX5SxBcQ5IZcJuCmSuQH8nsEd
OVbLxnw33NkYnBAMxs4b4LtNjBB0A3tyvVdt9u5bmaaba4c0sGHeE7EcJfNoSfyEyRQtFAWNv1Er
LJy3Ll/1rJ7xtz6lvzP3ZlVAlGkN3OlV2XzXOk0zTMGbaIkhF+LCo2z+t5QQN3WL3ofZ3pr2lHEK
xTO4u55s1X3OrEnYVOXhmS/tHrjelmo0B7pQ+EkNraNnr1kZfz6FW2FKJbDfTzoLO+HiNJjUGAzv
HplF9VhU/PnUyVIIYlmJlzif49yyM9CP2hVjZPD6V+Uqh/CgHty1PVFe2CvZoYYDZ8TbfyA/5+0W
hzh9PHjMSmSMZsNdaWf1Kz8ysjgt+LJaPUy4Ar2V7sd2P7FiMcxpVXXQdjGWsfE41xlpp7K/mX2M
pOnnOH10njFOVea5U7nevZakeftYJhlS0HB7q5/pMTye977oEbVeOxEKijH8Yu7VmE+Bp0RnkUiz
q4k+lPFN0HLDgxUNso57nM/AT4DuyDLVvQbKmTVd7Qy/dmZY4GusQTrcgQnKy/UonzWt74gGdO87
UO7Qe19zZeVvmKlLhIW6jPHBbcLggrY/4PMqYPVtRk9kUspu5CM34455VgMxRG2bKzSw1aN19NEm
dbLls23cxTMnivOmcwN4rLmFmtWFTPhu/vXRRWS9HVb9Ej0F95IXmLXKLqAfPa2NCkK+FIJFn9GM
bZeoOh4eyJLVdZrzeyCLrLX1LWGuvz8LJf9FEsKdJ8lIFhT4AWs2UPBAUm3/xeNGzNix556TWn4x
uwCtaLr0yskMFcrHK43cbdatlGwUcm/tg9onR6b5adSPi4WjruRaq4rx5MBrFLfATOnsTZYxjYel
jDeIocCUShvD/gxMEb1wOJrA54QvN11e5YUAn7QoVBqJygBoUow8XmHddwG/RxxR7a5TXSue9F/6
SgXU6/Ru0WIlSRrcfyI6iZUJyowHsqqZ5mY3h05k4M1XeN6AOFxPoUAJRQO0t+R4P7tfrU6vd/F5
JfLD6xv80cDKoRRhoDzk3rfHGkeLdBB4Xaw2Vonn6K+iuUkSgdKLaBM1YcvfKPOcrEwsc6Xz5KG4
ocoNH4tHBP5JISRJZ1fbNrbylqv7rF47sOSNVcFUQPxsGQhDqqw45lFd1ATwZdTPckd0qdwGM6fk
kQQ0S1YYRrXRk6je6S7K9+8Mx/oFadO9vpyuKRVcRwzz3XX0mzGJZVdt7SexRMW0PEOhQXO8Wjpb
MakNXXpt1U8BOSl5Alw+QWBT6r9vltosJO6HcJyHjNm6G72ZK31Hc+f7DFPhYxWiD9FWMUcJZxd7
H56v+ykjqUlbrTsSG+1vhHETi+I9JNCSmEV1EHpHCSRTfSUgUSJjiB13vAdC0iHMRDsxDAMoRS0m
jPC/B+KoYxqFMLbPYnc1S3UagNXB8fG85drkaEk1rnljvODlUtAkk8p1QiyseSuy2jgd9yZYymVi
RMLYdQ1b1BHqizIFCtLhvp8gDnTk3Rh2j1m17qjaqd68EtKSIcepBZR2bsZoJDUlXf0YLDT0UCyr
Gw61GVYLb/NrzJgqpYgvZq7X2kY2cN0OHR/iX7t2PMTRgzw8hhvXIHhT0P7yTGH5mTznkrF658t1
eBNJ9h0euyRI8FaStm87BQlftjPG8XM5/D/Fwv4ERQfkIMH0EWnJGdHx4h8ST6h/hSF+MR9Ua5EJ
UlkUmrFTq/evslVGaAZPoxBSfBOCeg4GToBz5CscsBNSMafcALBcXogBwXhuQubrBL6AecYCIIQ/
GebxZL/sjK2AJH/I9YYzyg5G5ywHPsoLRQECmjVK+whwPbYXwvasfqxlx2DWZl2FWVo0TRpm55M2
1RAXumtpteR+Uh28VXLbwq9lWj8VKwxTvMfYJQ4P3YOnbZetSzO41eacb/vqghxq48tKtWgNeeQ4
m0TDw2BoBJzLRgjH7A52/I9Y8MYYdqbNwp1k7o8lVp9TfQZgcKWj414B55HnqBIROJAi4dBzp7m5
gyxUhmjkhDaP7eNRuKPHLd+Ld30fVCdH+D5mPNXLUwiMIlunw1BOqH11V47wcGX5Ca/j/m76PmSK
/BGUvkDZeOfa/ySOyGGI9v5WKjsWRqLIHoX22UVQOHSCUQOSBvidmZpijXhEaDFCJEMDDvNMTAeH
/Vkx+NgktOWbAgkfGAdAza55bUIZ73glkOwF9LwBVqK8hX/3q6BeAzKTmRqBTwlPdoRuzPgvsc9e
iCNQwrYQKqkCZP8ZC1EGQgzdIt0CKPoiiKQvY9BJlzzz4eYy9bYXpr1VP9PX80RfNGSMbeQBncQw
wXXbxwAA4YFpPxXZ/e9csc2d9K8/xvE/KjDR8JwNgHUUEFNhl4QEOW1JddMqR07Yk1oqUGvJdN62
iBJWK4RKKBcNgoyOCdjWjtyxCGj1bPUjbgqkjpL76Y5m879uWmEnzC/lqO/a+s5V5+9g2qedGKe/
ooio+vjSONAjlFURwIt5xO6KiCqTuRfPrc5CwgVtz8QGmbSOlBJ07mO1TY/f+2Hpo+S8DZ0qSqEf
tu6zUFJTEYUkMiOK5JsJPB+yd8ENF9xa3DZoan6plfvWbplEysB9iR3WViXoWU7KQj8KWXyiQVwO
7HLTEfG5B6O7n3KeEAqnKbcSAD6mom2SpBwnO66Preu3JFGvEL0zTIXHmNI1wPbtMw1cVtIgEaCn
eYvy/GYvGn5cSX//F72p5+Nk5UlLUawN2U2T+htnZfANmkhGS3UceV3CVCJX8S+t2nQxqG3/aeLJ
6cHw6/Z+BiS4Agr1oH0dJZCTVzyjrOkxC/cWRsHeuVgpRxl0fmKLZE89vp7TudCqAkCCKefaY52C
xZNvS87EBvCOZz/MhE/1roCJ92D3tFJkuWuwBl/XZBD1DbF+n0/EJOAk5J7hYtdRXgnlasKRiPPi
K6qxspGmII1W2tVSLIr6ZVV5f2vOx6N4GEKIwK/KUajY8H/6FC5+X6AFPbzJ8sRpPoOee+cH1qjM
uBqutdO0M4GX9pyxtxAzmZ5fkNGvweqmj2EDDPgqesxSNKvzGvCVGi7+Qv8+/HJVxRbGR/eiKIuz
t7lgCtyvN2w9iwRjwqE5t0AnHvkJh/GghbD1lPjklij89kyLPbmyi6Qp9gI7XFL87BGtvY/McYNM
YmuC8ro4nRmHf20eg5R1qHulRwZIerkPDOhxfyAXEdUjXlCGf2NfrAyfKrTQzSdbuZej5V5KBWhf
mc/JTKYxXompL3jKhI9YBojjdAdmCCNAKUj+oOtEb6gqMt2F9TohsT35CRQ+tjrcX5XR7+7d6SOm
z/GxJSCBaq3wyLxRbdwuBtpnnsY3BhX66nUODRA/gpfQh6nWIdKJyzcA/OigCqPJBPCG0xcJhmeS
D8WA5hooiB1MBf22S+8GfT0R6PnXDQZgaKEgn8iRewvoEt+GgY7BD37XiP/G4JhS7I3rArhK1smb
I8BBwTQndKDAAVKHlWLsOsMrOaDCS0y+IsSn9hu/4wb7WuLhO3ds9WIAIX1RgoiKGQu4TNGVSwCI
wS+9T59Sm+EPFSEQivWNO4CkSTicssSE4Zh7jLFjl61vHdMC6QIBptEd6jYrev8A3/cNhHCf67e5
OSzRP0w2BmxVs2Ypvg84OUJPFSaUWhNMRtH9dWRS2SeLPfn6oOwD89G/ZX81veiOkj7/AjnQmyJ1
Vo4ggEgJEuDSArFiBDyeuRw+UEavP9/MER7dUKvJSXuCTfMXTgLKwKRIboHi4HaSO1370FUQWYEN
SLvQocYQiIgHJTK6Se8lqi5hQ97xSIwKjulu+HY7ByVuZD0bC474whT+XOgc6lqdgBqGvXrTPZbR
2W34XsSVjNEZsudYKKwDqgEj9Df58BB7CFlnuiIwiuYSxZbi8bfw/eo1TGp6u4iDcHinyaWCOOro
dFviFO5UQxyfverJ6g2mKynERkKXYqDHt+937gNME1eg8eTyuOGOdGSSNoN2QNM3NeJIVzcTiKV8
nKIhLz7KS0vuYYT+VeJg1B6a/6G1FYFk6ue0ltjECO/L4DZuRyyIUWps4n6euMgHupBFSOM14lJv
gORJZpXEkYUprwD8ovMgHCVApro/MIVxJfRKCG4hlHXpsWhHTaJPGLYGbpl/EoAZKpm9n97iv/d2
xvWDPyf6i+rLP46X0qB7vgeHQ4JOFKKQuD0gVO3Dj4tUBQ7poPhwkuVZDwxAsboR/MHprPtxibBo
pK2PDKYZLWArw+BQ2C0FdqKZsKTYiz15qFt5PWQ5YOy5WT39OXGG7vxZEHzrxTfBgfZkhlVMt1SN
BeZmkXPrEXkXyn7SywjBvLkBmKQs419UgPcxqVepnLpdgb7b5+PPFAReRMIBLt4m9e8WbV6w+6k7
knB6y86HN/afKicpktexlLgefcEt75nB0UnURnyhvtNCcIUnwUEzIjKTNycSBZJRt7jxTHk4ef1I
M1lwwsxzurvGZJDjxYHfRMtvkVTbRlgjLiHA+Lhvkz2wTQjmDvnUssZu/FMX6XHzD2hUaJS5df8Q
ot6aU2Cf3EZzoGyvx9ZHQYsq4aAeKFulXYy4eTbNTd7jSXhMzOS4irclfK5cJpsUbpOB8uTw8DgU
Fv+c+HHZh/bpya+i6kZFaA7kO5HMb/oDTkMb1c+zOUF8Y5QLU3PWSkprVaLy/WWFx73oF67momm8
MthXHSVdFpvLniJGlP+vWih3fFVW+YkyL7Oi9zOp++VjU5E1TXloKDiaWlniDXGuFZYMBLlZORFm
nldPrqXY2eoOjnxJuUqXQ094PLQH4IBimdDcqDnVD6moVSnYAz5YG8FM8hurrIN9P1EdLkDxNE8L
wRbFMvuL2NWyRpSF+OsNsEGTRMroq7ruKxmZDTE3XxHsn0QhYmwGiqr3g1vYr8HiaRcmdwj61B0h
pNsz7KwGr1qJCq2aSsirXYqjKxb3du0S3/pOzOljOnafTpkVwkp8fYi1zzuN8ujD20vC8AW4OnJz
ul/mfjdB+wXfSKe3Hf/O0K1Nuafn7Llfx1Ch1qdLtZZNhyJbROvKe/oxjwFqPolKjrUBlaUGJeFJ
3DX4ZF4CqQLlFmTUj/Qw5lGbhuyngDNzBL70o+TJTu0pAA7BXhEQJA7i93Vc2T0U2irR89vdB74T
KQRweI0Lk8u9EkzICNR9jGrmbn+wfCI165N8DMdkVOfljIOvFzfP+bb/kNTKE4YTxfcr/zkIK9aQ
W1Vzysc1j32W0bedzCu7S6eWlGDErQrNUil2JQv6QgFB6HFpHfFDLQFQesTYM2YZc7m4/eSvs0f3
VLZEtuScCIHiQ8kJZQLq6YHVu+vsjmGZ+wntFJ4UDmqRtK8qXhbUAghBatzt5mhIHdItEItNA9/3
DPYAPWHnpUZpN3TaPN6itoc3cx61fI6p2Z9FSlUVlEA+KWdGR4fEXcpW4wN+uL4z7P3ey8trjps1
W3V0/fQrHdplzUbBZzpsYQytArJ0xUGZ/MAU0IGXsmiUvEVxnqt2jpXCXTWwzebRp4DTgo5RWCFn
RJDfG/lh7CkuZZlHA2h8xXBDMW6SYWsRZIkK4pDlUwJO2g60WU6/PUs6hNpa7fi5EhNBiw2KVOqS
WuPgxH0EDq2iQdTELCfJzkFM7fqACJbDlLAoK91N1voGIOsz4F5WSBGwIsdIXJQEYLOakFZPScH9
OdFXk0Xbq22rSyHmLTGzzn8kEg+lRriy3erG4xhJWN6da3+snQfnOMIw1bBbF2eUkON1/iSGUs7T
baxnzbCK1YLxwka0ywNASsG/0AScuCdnZN07eDdW24glCV1niFk1GCmX6FxxODz8lv6HfpTI2/rv
bdfznEj+1BKPiIVCB1CMcAISSSmy1jMV04lmGkQavrCE+I9fnXXv+fd2zS+YXJiFn0b2cFe/1h9S
Kb3Q32sXn8vQYNYYR2HzK0EHV43RMfY3C3w9StWwA907gNsxq52Y+NKb5W982PLUxr7d8X/r6TAZ
9SMsQzDJ9FgVmPA9Cs9IEbELRheUfN9HruKfJHmj62Ios5aGVrte0UXGgzH19AXGz8ludS+JAKdJ
0popzuS15n3Z+9/vEs/PgY9sSl7HkBOwvVXCEr/vTUfnqXBY9cJpKU3C0lVBIJz0CNjv8G0wzTHQ
4hL+nOkhyd0YZuynezliCe+UwZ3RyzZfkFogBCrb2bt+rmcx3Uo74fqsia32QSV3X2h4X0V1XqH6
wDLYLqoKRO7U+5nLMOnQlnzbLI2r3wdbL7a/hgRAOkbx34+BLydX523FcaAqvMqRX1ONNhcO9WB8
C3Wl0/UtFVAAS4DmybywpUw52aYnLx3OUfLa8x7BGmQnwDOJXd95ynjwotIHSTmYHMi4NIciYF6W
01yLAhygCPqL40xP98ug3jp1wq0SE+KFHnX4kkkS6V1AZSlCRKtbz/Lr+3+cVGnsBqFRuHy39D16
Mis8be4fx326igcvALfQuUF/MDmo96p7MbK1DY4qIC09CZJifO5ocrMGuJlBZl20KoKbWRrD5IHE
wViVrhNEIh9phosctM9tHRs+bERcN4aqeJo5VU0PpeQ5QKblyVHEulntgz3EAEg3W/vO88S/QvHM
cObh1vlQ+NSYe3+FMhfBEPU/IAzZVKPGUb0Wnce0A7eJvWIqbPHziKz+2uAVNw8ZmemZZKOspXWp
adwwkTuIDPMymSYQ3GH6RaKyq/Cmli1jfalTjV8ZJqOWm39UidEYjDJS6C7it7L6F2bJ/Xvum7L2
Kb8btNn5Hc34z1J67KPzIhI4nasOA0BCSKaghH30U2S6Nfxi4QUmVU7cSSXov2z7CW81budThy03
BSPecOFwHemKnLJopxb/5HLu9bpB2kDV8sUzvSd+vY2K9z4l1vJf4XFmkP19Kg8mCrAgwy1vZPWS
l0yG3Y3unjYTN/A9rWYGfcFeJnXBhCYt6rpe7/sPq2zU0RmsVOfSnU9JEVgyJpSick3gj79YDKa5
+xAPKlGdaNe2HvTSBnkgfaYb7w910mPoM6uYmpPLq6s1zFJrGngLT716Tn5xb68U2xjeAiPaphDg
KBbroNK0T6/grmi4Q7Qb3VRn9U1R36GRv8IctZpFjDmGpoWVH90K8QsaqWUx5RtNTTt2cMlUQxGY
MEY1yUETTKfWPjuQm2IO8pnf4z6KNtOA5kimuy6m4nHTe5I3lpMYmbrnvPH0RL06G1ljXL4zIn1F
oNEDo/r/NjIMQiPpVubvlDQmZXOJmqzK2QR40mcbBEcY/mCugLYb587KEIz19ogkkOLYlMJr8Cfa
T/Bdewr+/5SPprcatYrZFdyfEW0G6SLxg5GCxGW87+MZ8Zp4b68vreOa2aR+MdLp4B6zreITgZBU
ixKsMY4ddZT/VPqrKabTem+oQL2iZ+yTF/Roao/TYSM8KipllrUTqQyjgWOq3+29+KeYa8+Ghioh
EyJfDdzMP4M14QgyLvHLkKb3cXGqL7GQtWNPyFP3+vdxatsZM7fdJjA3CmFWhp2tLHxqVr/pYpNQ
nodTZwngIQwlyc3qI6Z4J8wiAd85yslF3IihjW+9uPFXtBJ2IFgsZbWTnQoumRHrm+49d9K1QuI0
2fZBM3r0OWfUR6xi6XGBpON/3BkqtJ4On6fuD1QQdvs3toUSOoZSMlanE8YhD2YdwhYGn0jn7NKr
lT1W7xnm5yYCLuPW2/zYlAOwRb9KSsBRqraAdK5Ei20R3d7eYOLNytdqIfB0tqYfqWOiZVL9HQSA
oKZW1VCQnTdjmsRmlM5i8C/IE50jf12w1FQeyQ+VYHybVgrhXuBcvlxWsDQF2Uh8dWMlNe3gMqkC
RAda3YI98AaBWvwuR+seZq92ZgmlBE4zH4v6Tt3+e5UnRU1smoQTABUJ9wYEFTyvw3yYoQcz4PoD
8erm5N55kZX8FuG1T5I4It5UJM4o+Lxr3wPdIPY33eO9fzcDbjb4oLYkGUKaGeDdh5KuworTv57I
KJGUj66WlaNGzWkkwSbLB0n8rTi1aey8q5XWsjQ0Bvb38a7OmovLNADgeZdgNyg3OsqfT0II7rAi
QFGq+DfjccXeRCjZ21nkxOYsptWYhlnD6kvCqBe20ExY3sYzX8HeppdAgnPY/gQ1KmT9oMp+WgJb
o0sLgqvzV7Fdjcfv/f3uEr/AwiVZjKscgQNZpHbdxeLthLgwnIfNc/w4eHMxIRDc4zhlRFVcIVt+
jC+Vc0wNPj54czIj9pokZ2SCognnXBawrcJZwkEv4WiL6SfL+cTgxY9LM48s85FUmir391NvA7Ai
uWm1q9wVQHQUCRz/yLfTnhe5MGg28l+DVMIpe+qJu2o2erRP3fYJlvBsvyKa/VkCEAQXOJSWmnML
FCxwTYC7BFiuPrFioUFalHDrSPztivNvLAk7XycjDDRlFvhHVWg2LD5Avu2LJlV6OQzhBHNoPrWo
IDhTb1MIlhVBmcDhJEgqVptnDrx9oA6/rMN6KCKJVSmTRsE21TvivYW2TIJjigo7DGk4/y0LbK+s
vJqf/lZzd7QpL4YlcmDtUcPSjHpVleHHe5N4T+uu+6oyD727itIkcn6e+PLn/Pccw0TDz2HbvAZn
KNymO0l24mYR7avrRCgTdPH9v03zKlp77swG5L1VNRFdcZ5oDf0/c78cSACOYQapgc0HnRIsWMtR
AcPlgaIyRndgss/q3PEni6yKQ8QmU9OJku7NvpEgtJf4w3omHp3sMdvlXl41B7ZERcqX0QptR62f
PlF1ygEjBvgWjPFwenHPEV+lkNCs6kCSOc/4Yx5d+BPQu3ClGjtMckHp+DCFLFT44HvRaIFnm9sF
mMuiW/3yLhkzOpreUVGeEUYHtLKmnTx10pHfNtk06n/LZadikXCKjiaGP2McEtMfQ2JjjfkQiWOk
RaQlUUrm+C1VPqOB7+rQGBjW429O0nd16s6kIokYxT6oGMfN0z3mcbpYMHXSQdStFfT2lqurGIip
qCmwIBrwvYIHwx+nilT7CFrITUw2BbLtoWqnxs19QPbLY4WX5UVPJQlC4uBYoI+CgI8yRwIFK+IH
YVkIvV4tES3A8RAlXnZhD5iU/e6FsD0Ij1V4xGOWCkjkQ8s9PlCRidZEIOwX4r+2c/KrrMz/87tG
vRPU5vxPEMJkwW5PWZ+bKIGDFswDiq9sLYhdkVlDaDkLUhS+/HeG7OJfpfg6m31FKupUCLvXrOsg
6vevOVm2yApWeH2MSmydOi4G6BUvRJqcGFdWkym2tgpNLNUvwRep1hmWD5/6CDZl6CSUKJONXdqg
HjfSQ85sYqN8/z6YjkSAf+0mEZtAUJnEIWlBdiln0Iui88tQOmKLG1GuoR4IGA8NC9+psh+cucKr
qCaumyv57D2ojztFD4UaL/jR0mNHyCbQeSaAlFe9BCQ12dM2GPe+wkMZ+y3FduGaJoBoFPRAU+B2
p4KuLFYoV5G+xNcJKPB2bWR+ahMU4FviLZVcUnJyToSqaIYzFg/zs3W9MUooLKwqfnWQVk5ownir
ycScZ8IY/ovfhTt6qH5CtHuM9u0+Oeqz4pkROgOlyVu5Rbw1kwLb66cqzT1mgMHoy2Ao3lN+2fAZ
5i8w8pIcOvr8NK6shYNst/Wr6PXIzS8i2ra141wwtNjlgQOdlY7C4CZCDvSrOLBHCl9tQZ0cDfhf
98Lp5tLlIC71v+Wc7+G/OwVkLpB3cDzYNRNvpcyaWj8aVFUdXPy+N6I8S5FNw7Q2evBgirs40XfE
bkLB5gTGzPM7MbcAeDfzMzrL89lekD3KbP1JoA/0zOKE5UPFXJrtyWPaW7fyxK3CaOHyf2xPMIO2
8cpCRiXu670sYEU7ZONkgq9Uv5RFNTm9hB152TfWHP6U60B8TEuxppO4poIJYrzNnWWjm50IxTZQ
iI16Kjm0jhQKHv09oNUkMiPtvvsq5u4DGvpO7vjCxqEbG6uvOlErNeHHvsrfFMvct3y9VVhSO8hk
VJ85Q6yZNIgOwSvmKHRWiSIhPRrrbjLqaXH02RjcVvCN2oSpeCY+EratyhfcpiqJdi+XDZJrvGuM
AAn3/ehz7uY1nRm/LJuyAID+hQJJo0jZQqltK1ycsIYJ8AZwwU0/2LctkL5Dy+4sgr86qjgr+0lD
mPIVwMK5tAyC/nQadQUaqoAOurWarAlcsxBD3vtqaG8QS7st4BqHPJWi9k01i/9lDhV+UTpi2kwu
yOrEo4OMrfcC7Eqgta6W3/ob2WB7Rtv12ZdkzpgwReqexslG/gKuWw7roJtBi7gttuYGmtNTVnrS
fVrzC7rfKXRSGQNHcx3qfNdr30RAmlQX2EW23yI9QN3F3XF4xskxPpB4F/vnm+0/Kf4sCb0O2w93
Lkht8YDSk280sK1+q/fbOSxU4te2S7RrquISD/HznRcAMsfGPPqwAi2F9Z3hzH//X7mJ4hpplgxo
AMLYVy7FzTp2uiuo2ne8+eA3Vpmi/G5MvgZvqSW5bBTyYKxoBZZqIimAG07pW2qYcL/Ro+3XI7ke
JMHH2O//5w9WtlS4OCBdiZS71HJgtFdhIvbHgugl5g/4T7hFlJXbdCf3DA4CL1K1mJLK45uYRvRb
h6lsTnbfQ409+fT2HvRXepTzZBM/49qC+nL0R8wJC9THL1oYSKiLsMSQgWduWGE9xxskQILvgyMg
rVMA7gaJVpRcdi4gi+LZuxhwg6/PH33heOA4XhYkdaeutAso7AZuYXkVECZHDUbXM36+US6SKsWm
8xU+JzyiDC2C4e8n0RCWfEn4llX448Bunb7sN/2MlFlFn3IHf9uAWCHQugPY3SCe1jOkheS0C4+E
4bm0UAcBHSFhKYGRnZVQLoAj/eFkSaGF5fDz3Bm4+zRQZT/unbVKG1hc5nXdJDxaoClMDlWqD6JD
WV7Vsghm+fJYr3aORnC0lqu3r+AQsv/6e4r478bCoNTygUHL1Ng2jheQJhtK3Wj0VJiPCDjakH7O
DghPHcYpPwmEduEnsiq+p0o46tlaJ/7scqKSnevuOc/Cfic99W85dMET+/ConrcMe5xkQ5dijk0C
HO2bWmjqtJa0JsuESP1p4MIUn9PfWmRIHDCeuZAvWkop1A6eTEEQH+zzoyjRSYeablt1qj7rbLRA
iSD6GGC2pFHJWONBa55usUuVlTS2BCO/20K9v8cXB3ygXK7Z4yyzdmsTruNKmjxM75Oz44jJngO0
LqnkZWaHgRF8M47+qjBhnEmqenF/Nfq/38e7CSWIM4OocgARSWtGsCXbwI4EuPTCBDBd9QR1EhDJ
xN07TFvh5VShJ8kbFi5HPxNebuWlMVscMoOC9YEG/tAZnJSV7T6pe7iJEepk3r8g9YwdThL9bd8X
yniUTcDIVrr60YtoQdC7LDh+P6h4X6nYb4lQZy9pJvDqjtxFPxuPTAR/v/2c2+0zXBGt1oPPb1/x
r/6jpiLoWkfydyF0iRoUuDjDBF8RHzqJ0J9Tvw0jcfyXRr8waCBnxZx8Qx1wIwHsKK2KVx+Hqw8f
eh1oZ9ag4kduquJBm3inGSUuxrTEyG5sAkKrl/PJHc4mdHf9B9dpSfkZWQjtaJtjEATs78LHTVc4
/pn5NT+TtbfHfc6mupF3L6EAZfQX8FRBlgg8nsf7gxZd65gR6i4Wrno90swBfARMTsA9vIdLX8xW
GwWSPEb0HhAmA+iKzxXF/C582hKVW9b5SiDLdlPPX1jgnhdvSGDfmC7o4G9yoRNzNDe4MHfeTHoG
QttEu89edJ8vtIhd5JkWKTXVc8oZ32P9JIBGAwmil1ogInKBGyOMmmLqSJYOMjrRItE4KlvJhAsv
q01aw9meEBn+XSWwsM/LlIGq7DZj5z1zfUdEiaMwHkX12dL21muPw0mXCXCII066b+lwO5Jeq9JE
Kcpb82QKVTWQovf83kGlfL3IS8SKcgV03BcNSiZzYKSMI6xJRApo+JCzccn0sY41OySeql5RmqHD
O63HvXXyZSxncq4Y13zsmaH1z6z5gVSDYqm84Z3tNLMy24k/GIOGwIvQYcLtigof9LBvBw8gZY+n
x9dR2EwqZYl6yJHqeFB3j0JU9lw4geKx5L83PRAwou0LBzSUiUHvKL+YxXC7iiBX8e+VSoXAue1r
wBdhfPuwNv1ndtPzTdzq0KjCY0A0I59y74DjvieGrxzkj6ql1vCCzQkYkFOygcm9plXCP/bUqy2p
WbmOkdbRoo8lQkymHWb9jzkfk0nZBg2u1hLj97QFdV7E908EHRSet5N+w9TxjZKe5IPBC3k9J435
MM0wZXcoc6dQkhlw0wh3QAgsI7L3cYYCqZfwO/j1DOzHJKh/VQngh+1krL91X/+HckwATU8GtmWW
VVa09dzsufU7sQbFD3ivbinI8tsGypazufqSlN0+JwnN3wPeKtndH0YDs8Qv+IyEQata0bVOF0iK
53uFBxYhgU71ILM0eqaRG2mSx2hKGiSheFqjtiu4jfQZoDk7Q8xIDLy/SVdAVA/1OaPEA03EMfoc
9O/AgHKyr/Rl7IY5Stpd58rmcgaEV/U0LGnuuEe6z7wgoaa8lOFjvf4kKhN1uO8UCBwj/Ck+R7wh
a3lrwvOglnEIj/0aCZeuA7sJi97ETcy/tNKMBAokLtMC3ewn/YGCoRiPNaHsA/5LNN/SIKyqCN7Z
u78Ow8A4mOZuKVYrEpmxtAtbZ2KHp87hIQdx8fVB+qgDeb0wyJF08VDPW2LSMQwB195gWJfDmoCB
S4pQs/MBYPURXYAh8ZujD/zNHUrz4jrmPUa6eXboQljzUY7mEiOyS44/Gmfa/BiDGPurYVZuacKX
7BUoX2MUAbqOohcZpksx60ML+Rrl8fnFa51R4D3aWULVJMYmN9IINuPtHIddExV/ve7ADhCaKWlN
TiQC2Av1G5mZkht2gxgq/SB8cUY3tnnfYc7bGXqDz75BcLijzMqrJNO6NnhJVFp0igBRofnMQ92O
ybhSENXnJgq2Guu6N6C2/zmdvInnePlmZiYwUOFkduntgSQft6T17+5AEK+jXyWr2A598QEwN+Ry
Q4xtY+twj2mtcDregImc+zH3x2XUwLyd9SIO+E0fb0yEBOQYROynu8L6Su36if8Glk8fBWzYWDPL
0f1h/PpmKCCsQoDYaxxhCNnqlMPlRv//jVoXXrvfqSzrYn1xRZjV15EFgEOrbB07xuctTscoEsO0
lEPZ884bkizusY5sQNScqBmwi1/qmO30pRytPp0kiAUnHR+/UUvyRRtQ9AjO1ASrra+qTyfytJP2
RuKEhZsHsWCVQbZ4qYtaGkshmiJpnV9u77wdgYzC/CRqgUi+tmbEcUpDmU6kiixl8LDzx5No2uaw
kkoXeVKoapDbjVCYMNLL08KLmtIK74GepysTzfOJXUnDmDdeqQTyalBmP/q+BV4fXsJ7QUouGLdt
tK3aVja9RDY7IT/OogkRbt35IL8SxaS42JMw/FQvYQFeEyMkx386qFbBDW94KhFfjv/9qAMwGRCI
eQRCkw8MpwL/BMebPWFCFSrzZu4te17TtILmodiRaZtYDv4nsi+J6e+t7lkOiViegFqQaWBUFZ1b
3tUo3uDovhMl20wQhTcfeDC24bvRXenEiVw3wKhmkQ69lBXivf9NUaUvVlYaBf+9QCM/wn9f5+au
YhoFg4nt9mkpOxs2/ARpAsgzJSW3ua5fYZzSOpNWctVweAwj6vVEmEXK4Pt1RsVilFmfM0hrEnaG
oLP4YDRpPNnoS75IajB1e4kei1DTsvTdW/sNRYK8SJ7tqJWHMK0JMLQFwNu+Fy8sPqMQYivrHP7h
1X7ydkxr2DbdUs1OW/CV3u/mXmcniH18qsjw21NqnDfvO0Jti/R+KEdIRlpk+wVgo9YfLgjmC/6k
Rb1ae0B70bmERoslA9wXoFtSADGDgE7ItbhNSh1txWIzqCHu6PDy3a48R+FVQR287bcKMysVoUII
jltavKOVz123ZgJ1ttafgQdMN5d7BWTCmFyaZVUVvpmoD9EG+q74xiYww/0jKOvl5jevvVO1gqCz
EvGieRlzRedJjx8CebFO8C163QcxFQt1c6XgBDpDQ34l3C+4OiP/QUg89o6LNK0Bw0Hhw7eXra9f
vjEak/ObfW2EsGzEv1pBKKHiozrx7kf5MHJaYFzVMaBzaK2Ect8fgqZ7R322bWbgqUO4Ng1Cgc8L
B9fVMJKkOaiPDZdV3HNXpxlsfvnFRFY9xqIIw+yuzlFFJUPLYPuriYJIn1/JAeJ3H+yoVEpWffBi
7MBab2wqsuJ3BX13nAQK0T1o0wbmYU9dGPnLIhtIP9WdSxpBI4Z1iSi0LHrJmDXheyq+9JkKOOkY
TnyhbLAAf2H79IKeaBmazomA4Owj0pEaOZnwfs0Ch86tHCSzUWGnIIz0ZQ81wFH2K3Pjbsj/X959
4mfDkkbAmix+0q6o3qHcz6ZM1kLQI21kyasQhoPxUPm977zzNJpNYmwd74vaK4FmLLQeyu2Q4R4J
GTtp5Df4eFFz6EVRJ+cLfXPeuZAjNhFpUPsoFh26s/AjccUKzjJd76tpFqPchHNpA5QsRpSZVCvY
GPu5OXT6xWiXq836PslsOpL9zCd62LgSaDLBpHRkRj8S2OUgzVNrt1Vt9rsztLGqVx4Wg3zcX0cx
HAYV1h0pcHjMbxKGLbRNkUoFp6tAXTXMFM2pwvFBupTemzeJgCv0MwRxMg1q+yDQtNfYrDqhHnlQ
qc8LtRAZ2+MKb03dzglEKSj/Diy0uEwAtWxFTWU53mYUC8LdyUu0nBuPSAwdxjI1sPZ6PaS/hm57
kQ6JhtQLbw80aN//HAvPEqEvCWESH27WcKQxSLoKDMttqS3ew8k7wd5jwbMtJPdNfuthrdIj2V60
jb+9ShpXP3Q6yxcpHyx3NMgc3gRVuyv8Sf6ujVi6uy6k/E7KVDKCcJjiqF2ywUD1YffT5/e0C1IH
94mYH3s1e7/puzT8gDH11BOyOPvISafAb4tzYCUdJdFC8NXUbG+lHRdo2WWynCXOQ+VYTzKd7l/X
AU6xQlI81x9TxDgoPiV1rZeDGXaXfjxv4cDi9m8N0G+aRVA3ZJGdvdjOVzthMItW9GIuWDBL/1h+
YcGaQ9fvGPizoaRWtiwpIJCFB99mR6Ixkpjy5n6Pi6NS6z/dwofoEhnxOITyZryDKFlBc1qYOZJd
A36sN/xhmMjg7fJLynucEfl7YW7AiygJfJykb3fAedoiJwM3F3kGFSL3Z0EP3CH86khxCGGMq8Bo
CAHqMdlRO7//6ZlI8Ul4qZwuDk2cKx/wI+ytGRtwGZE4qvQ9rJMJjaKITAOYdZbow79ZftMXNjEL
FUUSQrqJgeJyCj+ruY6gVR/c2Og/DnSjT8k1oKpOjRzEy1/oASUX+zAWBexUxTK/TrEcv8NsQwER
ib1gcV++58U61DZ4HYYsEclGXefJfREA9+pSVPvGWXVeNo87vqRAYxtg7to5fY6tC8hWdmWKsaFN
qx3ftxbaxQy7Tc1DZ1eOW3sa5jY0r/XhwTo6gmXMLxlumgKZjCznF2NqUHJt5aa4gB1ray9qnNKS
gFfsuapZfDJPYCDD5ZjC8ZiXFi/vpk2faj+i1wAcUrcc3TDPoY/9Cvyf2FFy4JlpbOnzBcicMkaB
ndL3YQjD+cvDO9fAh+f+ZP5bOZwUcIdvgJM/g5kHwSt4aUkb8HPEUSLCjykfz5gGa1hjUBVwV0fW
6IuXgFK7GHDPlUACY3mZADSduR0rTyf1GoDZNHNyjs9tVZnPr3Neezl2z/xjOBn1u3tHZe40gVtp
bJ+HXWVlfaW2gtwbLBfWqL1H06vMIrF8w9xzxWOMeWE6Fx3fNv2zGsisk4p+57Z40p8uy2YQ2vQe
03MaIKYJ79Ivd50PdMtM9s2i39lwwsRNbTH7JlbIQC/i+1ZJAE9E6WIvEQ7mP+uiocG+V1OGtPYv
eyp6xRak11wpKhrKyC9JoT/xscioD2s/6z6CWAyYYk/IxY+rFjf6V2T6bYFGjJJUMp32Pdqk0bAQ
QmVfYySgPPyjXzuoj+oTikXjBaslgrf8XFyNIymU0qqUFt49ijUr9EN6QcvN7C6cHHC3j2z79Jlx
snQ88alKXywDHJ73D2Ja61CvnQCGAz36G877Vvd6TRL/++kq6Ep7lrriN7geXnxC13Hl8IYMygIQ
vn0BKkKw416xzbJKyvnRfOmcJAiBSQm3uT6mRRsBp3yVuEK73vFxSMvvJJFdKrVmm06GK7mDK/Uk
BXewEpIZmUJawXytEa2ACYRjGz5eoUy4Tok3uccukrROyB70JUI8t7X4zUUKcKfd5P7JrEbxPFG4
gT18cbpWH1A6UxmUyva8jHriLy+hyw97QlCZ2cM660SzH4/EgijnZ6EC29dN9qS8opOnAiVFXfCu
NIMyIv97lZPPhBUdkuB4vN7xlNcvS1N5elMwgokc8BvgqD9nr0aSpm1NHHn8LspKvbOe574X88EE
32SCX7pX8dbiVUuXz/LMXMzaSUDwVH4Fbi84lPswkPBwpBNQdnRhmAnlvO1DaW97g85wk9NoZe9o
tYdtZW8PsgUkJSoLZTP35Pg8cPQxic9blVgmjP6lU39pnRjz1/W6B8eRhrnsftLJxCk5xsnOFD7o
WXVIN4DGR9R5nterNNoqtLPh5MdChK+iv/+SfhfferWU650Stv+YYTKb+LHCrFzMmDY8qxyOloFO
L9o9aU9yGhY3irrMPZTs7jXzmPXqsR7OKwdRCOWPFFA5tKAzwNkOGr5+DA5N2/JAgeXbddt84egN
DEGblfI70Prymnm0a0mqQdC8ZDgvOFTYIzR0g9KTI2jgL6wdimXIC8lgilYpKlHO0etaSftp7hgb
ktpr5Yldk9xufAd5t9H5TI2OzgZV03gM20E9qab75jjqx7v8Zi/YE/rAJ1q6N/RhwCF41HHZA6wB
lhLmP2LGk5wwlyyO7SIu+5MKrz9GXWKF5/frpc5lWWaxv62qon76wx3gTGjibhXptH844v4GvgZy
46nbWkizMXOg24rIfLv++tr7uF27oYmv36BL6xHIKGnY/9J9zV0dzOTSPCbVVmseAWuYZdIWJFuV
77ZfnGKkl/qoo6tuZhLmKGMuHRPj9r6vDunhCH+VdtB9bXOuF4pb/6wPF5+Lqs0X4wQ2CNJtcSw2
gY+/URuU+K5HZka7hMp8qDRDW2nt7eZM1aeH7QH6w6EOha8kdCHV2aWCcmE4fdxMoBPNJpa3ZVLS
6quk25I0iNZ3a+N1bQjMefL1yJfKtoLlxBDBXRLRp0J3xCmwkYV5yVwZMx4UULQPbAz+74ruxJCL
t3Z3mEJPkDi8yAwOxCvK5u5vNO9OwHdS4YbcrJqdK+IMpMQN2kd5X0ZyrpWFvgv3XPjImD1RV4eG
Zr8WFjmINAbjTmFIDLAXnxSC624dXg5eIUEt3s0jxNw5wJTgfJFUOEk7z1UkaiU9Hjvd8w1Bk9F7
p5P9fJf33XH4MU55bYti3Q5fpMXcqIDnNZ0e5dJKnAyK+v6aLWBgV4cXlyPxW42OkdRiJvaAC/7N
GBaMPB6DE11HSRRnO2Rjw8XxkB6VjxxXrexKL2I+VHth57syPp2dC/av+T+nI13L0Tk/mVpBcEMr
F39/XH3O6iboYovuHXib71QZuy2erHLB5I2fl0iLDAJV+xcRSL/ABd6V/ZiGPFgowyoDQL1QROdR
9rxRMjeRMeXOF8zSEO647CUkT3amDFGxXNF/2VmRNAgN7wqq26BE6ZkUZlx776npUxXUvXj6fKjO
YAlaFJRRVIHLqFWvGr32YgYvs4Yb53tBmZH9gu9DRW5A644Vpfd6WafaYVrohzy2rBi2yJkgjEu+
eDw21FsHXZWDLiVqmPgHKAcxcKVNWQajRO/F1LeMbREOAqJMd6JpO8wdIlAZfcQTLPznrEunzBm0
VKsoo9xpuJ236YQaL0w2++B2AGjaZGHcd9wakShXKDyg81G3BvQ45QpB9EqYs5pas8OPW2QGj+qI
7SvJAF47OXENifG0NQhDQQ49/dQSi8yGrM5j6rn9KfSUfw5pteoEHr4tWqUzR0GNpbSECxfEVUXN
SCV4v7OQYS3+wqiC4ScnhEVIRqS4tIYw7wCeGdZ/nkKKcmMRLDH0hzeuRDyaBfBgtTu4EdQySBby
3m5W8DbZZVFVPt60sKCJa2X5dHaeNUNqAKpaMXDLEq20TYmvpp/TX7i/7sIDYSza9Hoz82eLhd+6
gDkSwQus0zeu80EGfZVIv9tHkcOWe1UWrEOzjPMfU/WR1IVPhFLEa0AgqnL0QvkmkudLObJQ1gFE
qFIojBPGiBxqGlo+VmHQsdqhP0GcLTRPsRg7FxORaEmccnYLJUixit6ryL6hIeNUEuNelwiSuMs5
6vaLBgheqnPZDdWjGRXRNqq6UbxFFmC5Parqv0t1GDjjOskCrOBzEsNM5QpcVooZhu+p7BwfVNLp
4Xl00viMX9O5yRhKQWxX8jT0jRMra9YQnN/G3aSie/Q84QEQlg9RtLdHlSdZM5AQnoGQzLSnw69a
ykYFfJRuOZEW3yENX534E1ej4xBC8SV+oltHNMjp85fhd0xUZn1AvzeV9jY0ktLkIxfm+13FM6NK
LI2H8eXkmk/l/P7YAN6pPk5BBwYO2uCvzde8KCmUWyyzFrmkIGqK0PzNambTCrTvtZbusYGVaDqu
4whLB2l+Gl54h+NB+ciADNrVVzORA1IlgYJH3yWXDVlvq/+EA38ahP7Ld/Mgc9LlXmXuREp8wkQm
4ZegvFDDYsbSxV4K9y0fEPSdMp11YIUY19z6mOc5Bv7gdGWKN0A3dbObJsOMy0R1UK8ikg63o8kt
nny+IzgtZ/yZ4LDf+3KdVG+vahA2TL+/Y3Zl1ZcON5NAjy4rPI0nE5Ire9x3cPhDkzocWWGu2EzY
MhYXhr9XkRjqAePapfj04MbQeVpxhQuwMQmCI0M0kgE/ilUXlsJc796awtBJjFs6SrjqRErH7QY9
Ow3peM9UpFw0U86ggExWSJJ6mB+LyjEDv5sc235zmftvPJuUOF3G+CAXgt8cOlowk3OMbvWV6J9d
5maTaGn+/kaSYKsnJCKc3o2jqI8I5BYA3NXSkjZqBNTH9eZvWuKqyalKoKsKIGrx4L8J+7vdUEN1
BMu2dJTKS1wAhLglAQ69Ru4iQE+xoJ/quBHxSQixUs3drwfom49+lCtebew3gOGJpDx2eLAvSUjT
qUkcNbZxQ4GZHKcALmYsSd4pWDvYhvz2XNIYswBN0K9N1JgFpbGW4FQgaUuYix+sAOHMcMQZQOUV
c8N7K9KS0ulbFSPCyi3QjJrvZbCKgyu3Emis3lfmU3T8a4AyLQt/ETmN1I8t7Rx6eNv1l8vOnkMe
1NP75boCpcr7PzOgkf9GaR3pAV8rkqZmUvNy66DxDPxshbBKPoaCS0/M5jD991prxiS547ZdTir9
Vy892gaEWHLl1B9DAFqxlaMM1VG4w57M4hpqUB2dk49TJeynlYEsNriq/qJsAXBNb0aeNaVK+lXy
7CHv5bYROh/m0CiWEEhueHJsOvPLfTZ21KzDlAIi6o9G+IMYQ7rp4TJy0mZTtjQJN9CsndUN5utU
diRyPQpi1b/2yK6N1L/+wBkrcYZMax41IF446fTQauj6mDDrtHVkPjOsNEFxwfFw+UffZf6w4CzE
sZ7RXJOREHKdoHurvpHGf152yRH9/EqVgqzmXJoUFuh5HAeaBTMJUmCT6zlpVq/E7P67g7P+bhvu
LiaAQlWHdr7SXBr/9YBzPvNKW8rChXsQlwM9AX5Zgj+7NnmkNIZD/6AxApOVFIRAl1zRfUIWUdYK
QOcgGIHUY6xluZha+W1pQKPJXoNN74CwBP2mGETmJJFy8+bB4D+LulhImukjJG+3UruTLLmz4Bnh
d5LKHFIBgE4+7O3czjG+/lK2umhN9uJz3/y1lOhf78+kE7yr7Cya3PqerZFvcC1qUWpa3Nb6wbNd
8Yqa5aHwqbunHdkVAAN6o/Pi+cNbd2j3Eh5JXKkOszv9dqmQLGnu47/jeoADmT5Zmly0IhMedQ7/
KEK/BorrC4v14nVxZKJIxGTVA32vaXrLDOgm3qAe7qV3aYtsWY2cIkQC90n73nTOW/GYHNfjhjtr
JzAPnAjZ4u3KrIGFR/CBlUeI3tS/NWnrUsJs70p+2clPtXC8KdPUXmK+8sn7G0NCJtYZlR+VtBiF
+5yEz04/cthpxGSBeaB2ob70inelOROgz1HwpyPYDwGAScXouar/rAbg7+IFNewljEZ+Lq1z+4UT
dBzMa//M4ggjGvrG1SJUlbORBwHqhMn1DJb2N4XfOwo5YELE5izWJQpSZzW/3MR3nB9Qf98LGgb6
99SyAnvu4AXG97Zzm2lRwEBRlyfxmP0mYIpAZ8OCPpfSF8tTfsvyUFSZnXUKmQCX4+4OjOJskR9t
MKMi1vWf72vu0IXDlBhfHgSRgNPa/j7l1hhZEKawUXw7SFSwiLnUTJAAuScN2t3+lbwBZru1XTHt
Aa/fEdh+muaLaC/vKtral7s0YI6pT9u/v7TeVBSunMgDORhO7c2OZ55vx27Q8cTo2G3EhKrFlum3
y7nTwV+mWOCCLPMOAbY7pDeS9oIRI4PnuDUD+YnF2gzXLkl9b9RyTts0Da8nsuy4barfj/bGY4pe
vIYnLQUlBGTV7Rfsosy3nKXirpritceB8jESXv9XQClhynML32EfVyAaet5eZI8tJmgx72EMsG7U
Gt7TeagBCShCqts8O5Q6JgfuWOIDubEhfxNtCqbzRRgdIwoEabV4Xm1XWt/d3CpWzRFvm+ku3vIu
lhXH67uXjIe0fQpH6WfIlD+r6Kcck0LFQ9YAR7lQ8Sc+4CPHCY/ih/mlhsF9IYSucHf3g6eV4hsc
VwUPEMbIWt/cHFjelpK4TixUrGX5rUtJF2F38uLdWpYEnF8QLdWpfhK3h0pltmzYDi9S3Tnycaed
/X4MBkGAs6toNK8lelrCTaBy4OZV+4u/LWHNgR1e/7ZXiEbwawQk2eZz/Npq4oRw7Gt+4BSfK+/n
ue4w8YEvMqExoSZ7MuHXlDIjAygcMYOJuBuVssTsEIGEd+vaqHf77UoNhcA0JBg2P0t6gM33unea
GIF+aY77ESMgW9SQxnBGxWHu4keb+qbHFj2dsJvYG3nP/DxQeHDTu0M47wqgPn8NCKw+T0SPJyHu
oB9iPRwEuA/3NTs1gPmkCfVSOvpkyK+M6/9LLIoms+EIaQFFiQufzR98WvtAvGfiU/qZv4uoC/Yu
5fwkZPMNYfZgg1QR8alM6LH0Nr/Uxol3JG/ufuFgxi4W/UebpFAUSWriUg2DhpMUEDVh1i8fYg3o
jz/EoCG14ZmXyKWMt3JUTnGcNlYMFar0+e6DsME4zZiFIsQmteCUrzKyw7fKyQPbNc0ucFHj3O8X
6RjxXCQqmxqdG97DWbm/a/IXgXv65jkOybEsPCqWFryjWlTiZj3ePHrYjYboLKV8I4EGKq0tv/jG
U1XKSDK4GBDyP5z+xjsnxfovHwFsdcrSsHrP0iPK6U2h85A6/nJb/l0bgEMDLH0dY0lncElf20va
C35RTBeREG9VQk5QSlH3xZbX/RFxVdJ2tLwQn29M6L0xd+XoRIq5kJRAZL7bk+IMAyOJiKDuyzjQ
kbIrxCiUuM3HkM/rGWItFVJQOcG8rMAYDtGj9dYtW4GIWzNdKlzfYb6/UY0kdbib6WuOFdvMbGIH
E1J2az1vbtSTlxEyZFTIvI3XJX6WeRF4I+WrT3voK+J7swK7OxrcVo6AOA4eqUHL2Nhn1w7cFVCo
3UOyI/xdthBPw7YPycdd2lKOT991JZtC/iExhk1ti7rfOQ0pjX+lwXqDVrFLKFAvBoAVS3fgZMcg
WMDqlM7519mDgqOpJJr0TluNJhF5d7jDJ7jvvPxIxYhhMBzdYCdnnP+0ZddFdbD3O/xX9nj/n/vn
zEiLLFhuT3ruRLdph1oLPEaPozagpKXdu3tTWRODGZY7hEblORgIjuXirIV0CHk+H0WniZ6weg76
tnHQIQhAQXVxknsholtlYLLroonfuD2piFPoXpHde6JQvWtKuiI8H1K4gEgderOA0ZfiPCGTa2q8
ty0FCxtmZxwcV3fMa4rxn9OKQD/+6lEu7vFp6fGBt/5h5T3aaJTk2ryfvaE/ssy+Lq1N7w/OKbB9
EkvUi3zuP4sL6Z/h7I6201WgFrCiowsuussjcfkWhfVWM/5zoLX9zLrzEEiAj5QjCa6Qj/4qiA1J
EuN5ICHfUWi9Qs/TfkZgHj1VxrI2r7MdAFnnFAOLfQseMg0JbT4XXMmaZV0EIKZLGtuqmTnFLSd7
jjLNgYAYuRuKrsjvCXb+siMZblI6DopjOfkq/7hI6A/GUSMSN2yAZjblnPelllLlteLau0O40JFo
7GommuEJ4sD36wfGniwED/T6sE80MsL4QtI3Y5914Gl8ZdZ6J9L5qujC5Jf1FVHmACR6EukINCTX
qx9Emo/cDzFnnPx2q8akLU+8z3IL5T6r+vF9+3LWGaUMteUnN8r0JBOHVlYcBljTNT5XVJPtcq+j
w3ICarwpNZqzy/AkOiGaRk7PaEN0goXv9PQNLGzuZz8tKAFZnMazkBnyCN69NOY98lpK3mku7ffS
W7nNnjhQne+c3W/sKnZv/DOIpGxLy+RnMlF0b62Z8tW9nV9re7sdCxR1VO3QZTpOwlYEBvK42Tr+
siuWgS+cZMoWuAnq7lMDQVoQ8ib+kdy6SWwMfIkMAn6/K/QDf2qkW93d44NQ8itJhb0oxhA9XCvA
qZik7LcAkqGdOWGgDbKFvVI1vy+MxMAQjp8N6SlEjYTwuMcmw6q9u457ee2ZX2cV3m0SKQAZTtyP
Vpb5gRf7+6LXns0bYvsXdKJBQkJaqYsyNSV6KPg1bW0A6iWWWVQ7M2XJKY2KtntzgNPxRIXxr+yk
R3JDXwXjL2dZpy/k4lzj8KmdiAFHtROUGrn+bMm46c+B45kha/4LJ6qqkQhnXKNLPvsoTsY1S4Yb
6dD/KDnIKRnndwCif2dBZRZBPp0zZVpxC9jZaarAWiPxOhrlWUiJDKDelVjx1X6oS7zMNWosS4/3
uBq1gUHpDD5ohWgdkoopFyNckUaJ5+kyk7DeNx/puHhRON/KepiSzaARPiqHD2vgLa7TRJ1aDzeq
V14LGiQznIDNaWAdQrGYXRY01Rdj9+VS62gtl2c/MYtUSRSsejF4D1PlT/RkcxjyNiN3HxTc5w/v
lO6TfqiZLHTrItyGpoyPUYk8WX5lldbUgakmZaFt4Lz22+TrPhjzbheU5sm6xN4KSTa81OINsf1g
3m+ypHOaNwRIovpAbWGX0KFQ2mDIoFX4MN7FWfj3pSxZb89crG7c8p/Yng/fn1L+hvf3vPtmapsF
2LRYuQug/7xeec8iXqWHi2K2UK90HYclpjacIyn65HHYAI6qdkecRDm/Jbdm1K1GMpsjbqxhFMdD
jzEDlCVRJNLIHplrtq4+1o3mlHyLerBVdh9r34s21uv4sjDbrg2tub8PozumOGoUja7Hznqmoy58
mNQoa08HTC0Ce1oL5hEQ2E71DLDnAUGUGlkHaeT2m/QqlCyP1iAE+WJUPpaaSdVQomk/PRmrdP+z
+IduUgzu9O2HczAOaQoc4cIlL7yLZSnERV2s6qG6LUI/bqCcLTGbsPJREUZuBTEtzpjxFEs9S4Iu
diXW/Iw9Yzm2EHajNdpSUYslWFhcCmfbkq6RamM8wa+mGQGBg/amJ9a7bVVWitgnuYTSZAXSLN4y
PKUNy0hz5XSjj10wm73/wYoxv+74I1MiuFPPTMlDGbh5//cDmxQyAtqu0s9NrNkjE6FflT6Vu1EN
CI5KHfA6T4lKIppEHMwrjMXZIPvv05HGaHY+gOYGJ3hI3myZUZxI8nFrZtjoiPZV/fOHg/jTMOtL
HdLxl4vWbyMPKRrmDAdu4b/L9FGi0oT9Du8LVvHvGjani+SpkF+9mUvpcY4/gAhrZ14MZvFTeE23
oOSiHOlAwmYt2DEmOBDk/yv/Ga7JOdy91jADu45QRF6ADT4kM9yNWkPReBiQ9CWd992dEnjLajQI
pGFjsHKpRl1W/Iqj3hvI4c4dfyeCCX9U0dVT94DWSAAqp33ETotrd0M47KdGCEzPBEzQOStZ4B/i
J77AFWf4JdBroN3BCJ691jh3MHcjfmFZzSsOHeu6x0+ENvY1ZULCF7BLhdYcgbloX9HUABDTKSP4
jOqkuMHicV2g9+Zswb/OSfdHUkkz9A8KVIR3MYjFKeoll17p3jHSSa8Qb2WYvhHhw72PI20Rsw9s
fao8kMx1DmoDtrCLIkHwL8rV2dDuEQ++3PUq1xT9pNomm01L0AHLoeOa95+ZldGMnsCdH9iqRvz1
our8OccqQ6XOUHQECti3Gceqdyz1QmHYoa9twtix1n1nSZvuAbpp75ydgsL9vXdbgpBAl2PfeGYu
wDZXevimuk+Kp/3iLM7E6iiGhzrhQaQ5l0T7+LkplmlYf+neUEAeZS3ueYARuP0UbhW8MtrfD7xQ
PgUC8tO3Y+GKBaKXja3bGcMBPk0jA0hjEWyCyCDa/QVOMXVewjPCnT0epKvs70RMVN1YrV6uC1KP
7GZMfozaLPjgkGll4ycacCamI5n2FzpbV/gLi8UQHFP8k8QV+UBasXcH7NDhhvLeRlSrqZB2bKz7
Rda59Q9mkFUrX8HpycN9LjvVN30CqQvcGVuOiUT/eygl4wPeV2drGgMJE/Ndn9XTaNO7/DdzpOCQ
0NmJooJcr2799HK8Iue2lvlHmZ2duBQH7NMle0GPmN/f7DhnPjh14V9+nVosaWnvqhFn5Mv9jCiU
BU0j+9mGZZkweaHBzdM1Gv4g6fok3NxXa/u6S1t4NRTXkOsFah4F6nRrh2NzfMKGWr3A6NEmpTP7
ro9WuTMV1UHto5XaG+UwOMLOxhznpVKat38Z+KnhyIhKbUo1KKGoxpbY7vdTb6IpRCzQixgI8t26
CvhdfF1XvMupJBhpbb2t2IUMbdjNVDSRifQSV5h4L+5Hs2FEUeYaJOFEMmO3rUu312RQhLKTbPf8
OrrR4so/h0ywZGnTswL1ugcmwScWcXWu1/y2YumdTvRfrNUaWtzNJ1xWfmbjoREHnpr0ad/F8gqo
mhH4VHVzSxKEZstotg5sgyoFT02pvbfmHUSYm25+7e8kf8l2lyhrC3LCBKRpKzBY+f4CAu8q381q
a1kVnaB8FuBnLvQ7mNrYEEYAV554eXdU5muHe7+dLjJVYjPsAr++ipfw1rwH3H4EJQcvr/K/iYIh
T1KK8pnyEuCMZV5PlpRQlxcCkMTz+VdLY1siZPmjXcfRS3Q/K0TEpy4Te6EYrE4w5ZfZjl+jUwQK
qEQoF5Lu1kD5WsgQA25xGRtySkaKUaHkJwnDnOvuwtPPwT0Tz/zQC+lfVRR63Bab+86iHcIeHg4O
DTSDYjy93KA0TOt0onyHuUSwAiBDhlsBDFZ0jVHTEarQrjf+5AlTYcwpmLgrKvKZYmGybX0frzDm
zBg6oM8WW7G43hpXXPkrqyZfr0Ow4F6CbCYl5wO+F9LZB89DYeoPdN7QN5eS7JsIfG9hMSU+xC4m
VK/KobyQ9oE8HUAUjmme4nQeDbwuXlnLLtsYHkD3z9rFdXcrD9KleYhoJeYzhVgfwUADzIaiV6oy
06u3r1dBpX3jTBSROLhZ95g9V2TN2cNPMYXOQ9nC5e9VB5QYeeAgyENWWeo7uX7zZI70rYQuxEge
Azog3qHLbK5I7Kva9fuHi9sV1jKT1FH9JnYhRKPCm/q2EojNbpkQOoQrQA/AOdDRU+8wIWH7pRoS
+YS942weJygnRhv9YK03wfclCklyFN38qUSd3QFrHAtIV8qbnymmKJSe7MinjcVvKSA3bOwjHdFT
eqx8yBN612SaSDz5AZk6GblMrrV8yLJ7wgFDRrOgV+H1IFTXIJYH3rOKhXbwPLWuC1a93Isxn+mG
B9vfp5VElfJA/O9X+EizHCQJggw4mCZ3EexemngdkiHGbJEHyVGppfGCoox0WsnOOkOtO9FIW1n7
YB9NdBB2WfEUY0ylmNAtu+TlSu26FPePtAXmL3rDf3QRpGLxEkOCUF5fVgEK2EjAM2/fp1tM05xT
lU5abZkgrZWLQbuP8azm8MxwyFy0b8yS/T9gNN2nA0P95Tl8mUn0DLi9lKio6TyPKcT+CUNel5ab
Cy25u0WQ03NKW5c5zvpz+mVffMBvK5PqphK63d3sTnfKBMHEVb9QOZMi6S+rKzDVr7eWRA5F4FyG
LnqGEOpOqUKgv5wGv5r9R34Z51Z+/kRjxfFoGaaYdlukK7zYerEvAuiF8hXmlkOIbUyO/jAgaJSr
d9RQJYIsnfSuGLD1nZuh/uZ1Z5D3QxbCs/Tf7JAJxgAQkkEVcQe27XWltpVKKjlkZhKFaaLZ+qYn
IS1aW5EM17Xk0JlJZhXiyuQnYCFyBfPhTVwq5MrM/YyHxxgBhDLbpm4/GFtVeFxc/GcYVU28LUaN
WA4kUyB6osTD/50z1wWIHh1GOknRyb0cMiivuvDnAnQFDVQBJR9z7GnTtn64RTQHwAVH6jXMFAGm
8RjRUz4taaXdZFExH0k/6MZyvEecLjPet47TPraHNC6cwjMxboXjN3TiD2PBAADwWhpy/mqbv9kN
FJuJYGJuHljRPzFvOBonUuLAawKzDBJJq0XUEoqWAWFWcqEORTXVQdUuUU0z842s9l+7Uou51NCE
1tCJS1S3NP/XbHJqdXk6vOZI98NA4NwBmji0T9phLLKR8aA4m1kVAXme9fpFfBWZk9zCnRTZ+xO5
M/rdvGK28n/URyDC0/bCiW7PUO0vyuoqfJqXLUHR0U+tShf0O17IBnrtMmc4CCzDMfsHVmiXTUks
1d6i093UqZ6w8Nvhw8SV1T87v7RjaDN0zHW2B942UwnSncG2JbfTpkANYjfTdxZoR9JaU2InqTJx
PcLrei+mFpLKmVUzsAIAWZ6v/C4+kIB+KnYy4W1JpJtfXbfD4Wkj2aiKMe+U7F5KHflu5Kp8mZSp
xEZ/372NRyl1noQUgq9pCv052bDb07UJ7VnZMgO0gGmuHi17EMAA+c4VMrGfrfBPupfsDacqFDWK
x7yqklaVtez9lfLAPiI28PlfhoW7kwmeUpqoq6vg0nAEYvZayTG6IUzodh2gJ80KKWLVI2Z7uFz5
apt1/9fsq0YJtLDDk5mxYAXGfqLQPgFyqvtcexSSJmfuRK3FSf6WCRrGKh8yFaNj5tWSAUySrWIj
znPxXepq5+sMY3FVjZjt1wEPmwUBZWmifK/ry+7iVCZEglWIafaPWemB52liQPPymHvey/PLuA7d
elR/9MqYrX5aUX/zLN3JH30dCY5Q4s9TcuHZ5rDv+CESJDKwahwLe0I1/iWrGRo1jUGgl8FrEUfm
4K2CwmKhB3kc3cGVkP4HmNQjrOWp4vgVW6miLSXtjMtLOj46QP9Qhe+raEsfs3Vf8zxjdi6akOFH
7EftOzfqMQk7t6ZW+mjQqWOPfuoL2W/fyNUbF95uOduziGTERbILwRw7sveSgBSQ0nAZpL6LtBWQ
zGZT9kokWzcza0L6ftt1xnLOnVyKuCCfzBX1HOa9Pyx3z3V/D+niYyV0+ui7LiOUMnPyIgoRUubu
1jDJv6rBPlCiLwK/Z2iBUuxFRZ02bvqZFJHmqzFtIvuT5NhNma88uBUj4vHTFJXp++UhS1Bn0Itj
RX9qT+sHf4sH5fdqGy/eWe+9A8CdC8DhXjC6p1BizAZSRvuj7P9nhlQHvFRuUtCcfd4KRhTv3ao8
H+x/yrp8gt7RgbB32Ax2yZSuBm74hC9BFfuszRSTzmgfEMogkk6V/n8qV4zUoGocj2soVLgRwhND
QG7B1xpCEbYfo1oC7cJfpJFzWx5q+uiT4MKvI8XGzRaBMSXX5cHuBt4om9e6OHLvZJlXfXUyaGDB
E7HebW40XSvigV+J/q4LVV2miW9P3+tubcBoylDzEzQ/TfNWhDPG8n3be4UF/Xi2UTYWhBnP1bME
e5/0E2RxHWHjyshw9tGy4tdM4uPQRC9CcONz3LC+QomgE9B/n78d+fCDU8Av1QktBzVsgc5P5oV2
Li5JV1pSh9aI984gLeLqxuAFUS/AUYGzvQZtRdrKtV4XXWRIL1XSxAtRIy3sHzDUfrNbNsLRtcJq
EC5RnuWSIhSiecw3nImlIePXMoOIo0s3c9tGa7CVgp/qR0UjFWUOvHolvYIHV3xdjTThx87JUj2L
Uc1b6ISguUaKekMVk13+Da1/PjvgzUjkqZkNwW0t2pXZVx4PZjW6Fxm1HxM7FXdwAp9BwACdcTcr
BhOTCMelg4GsDzy5HyBovz4GuYrnt/9/ZMbJsoPALM/D+zJrhiX9I+hZpr9kpUzVbVNdpDEcL0wq
4QCFGumSn3OvINzwC5/tHOB5OIYemm6/ZTZ0cLUn5KBJs61tuQaGZc8nxU+FThFn1zeNe57MLmBH
FsYEVF3Ei0HVUyeTD5ZE4xA0YhcBpfxh3btMLrDIvA5HiBYmDdAFb/CTBVU/+eUvfXQAaZO9bNdG
4U7gdUKKcqir4QyMArxhYcwnQkBxsP1eyKrTqfF+R6ItATA/22hVaN20uBcghpvP71aRP7IWqfK8
upeB9KrvPdawPFt7qePLoioaPiDQgaPQbCocNDxDtQyRyv5IzTzUlTb9Te9ASHHNbyJBDRF/AIt8
NDAYIC+6mIEZWxVgZt1WKOCQ30+OflWTAPcasHJVlxw6kP6n4QtUhmc/X0sBCFghpKVaoaEhEUus
b3V54V43r9ucW49pN32ynD1woDg1+905PqVvFhNhExKqrsZfD8ZXDoljX9xacBHs+PaNY4ReGAFV
GH6Y/GiF/bAQZnIR5L1NEmk7AFw3dSYvo1DlMgB/ypwd7eMPKNw8CiSyQI4QqsASw8G5wNp/O/Gn
mWotA7mvkpalPUQqFOZmocsxdnTYeDzjqDwl/P3CqkX1BcpwlanfxBIaByP982syPytTqZH8R6FI
jyBV7zohM3PtzWdfhfNagUi2IhW90RD9XiIvixA3L3ctcA3E3BLs8sgn3YvwdTWyFSJ9ywnauJR3
6AftDHuwME1TF67jnyvf80rzR6H8O5HLVHLuq3g2b11bOxCZKyu/n0qVkWOzGX3tg73ZwNzdU50a
hTM3PM+mNKIqQPzWY3uEdgJosBgnkOI+DPjdqP5AwCpwxYFmORXJssvmLOUzKUEZOfaRt5FdqwjN
R5Y43dc0A5cVmIq0LIBJKZgeI8ePtqW0FmXqX+hMe976eQTp1kTQFp8mAoabbrGoVvu/Kza1+r+Y
9wlD7ji8I4hjHsMe85uupjQACyfR4bpHElN5G7rRXDqLDH5fypjyWhwu5SCnYj4IQDL7gqpkpgww
aZmpUML9PhIwMMsQeGfyRNmfQJLhxR9/3O61uFLYaWTXJLH/cL6p/hJuiCM9JkuIxJjf/L886Mj0
3sMCXGW5ybpLWtRRQBZ12d2j87U9jQR0e/4xyWCWHKf84CxJdj9LSYPO88sH9lZ3VVMMH2r2x7n0
yjq6F8a3gOLnx15P0GmjbaW62aRAM+8GisRPeLMeoUwW1SNyjWtOJ0qkAG82EuDfgwv189A0qWH5
X5N2U1Z6P4rEewmZtWUgzpXsSyf8OJpZoo8ihFx987Duk66I3fryqd7U0THatYfdtFOTWwKixGaJ
CdC5ZtUpz2jJTH8TIjOXFyHPtDBEju25JKcAzVi1eexRCDoPEyIzUat7ZFV2otvJoCgTm1URZxHl
2J59VitKxpaOAtyBve3Vj7LHl+M8BQHlaZ4BgYRrlKpYyWEW9TaYvWpP3E2QEhlH/M3UaWiCDbkA
BGjcNZrakQoEhJxleMr93G0zO4Vpp9XWdNwtwwwe7KPENxZktTZgv2b06eNiFoHvVLIkldW/E0lv
6txvEHTDO3fBr0K5lUErBDLnOkXNGKEBo9rw+kMCykUQaGCJqMo7/dOsqKLrEDmk/OqyYDGndwHr
ES5YE3RrDg88IdD8qrNcZk1MueCz3oRR3AHcx8uCED+i0G3KN3jxRAy+re6BcAoDsy3BIaZ2xptL
fqIsDl718NBSLoGK1UKL8ZNwKAE3iqpLE1RpLrQHQBRrDYmlh7t7B5Ln2xsESvgjXTnhUAon5Ql3
2/zd8GdUHdKuEtEXjh6hOwpl99kU6E26FZ1o8e5dAF5kMnC5oimircBxfIhztn3vCkPg0ZNgFFGm
6wD7ZLwZVmrdVhFaIGJQ8yAQsBoYEYD7aZ2vSXCEgFBsaxyx6kGH+ieIinq2NOsC7Yo2OED1m+DS
LR22YIJy79Ry8ybvsxQC9TrY1GGrvjC9I77QdhTL9CK63iejdPTFcfbxTNYT5F00Q3u6BgWHeT8T
yAsdS/y2YAMjhCIrvpclbdlNcIUZKD9qVs63+HHq//r6jk8/dvLo4TlbG6lTqFFrT7cFai+F22q3
mYQa3n8cLMz5Yh8I1HqxeRhjaGqn2jcR0hbu6OQQxQjZqizAE2TkqzJRFCenHVMVJ33GGR2YSIjg
IBjz8EK+dUnZ0v0llBtM1vBfM9WZxYe4R4P61ubzLFTdfpjaUINrzKl46VPOvTdQyh8WLhstaq+m
SekgdIplaLQOHgJzUTUpjlgabYGli7F3fQfBiTFTrUhD33XvWDCNQubCXm3h6QbtLydH0cNOgM1X
/xgxXCFUM9ps9ixeWjXfN+7ELg5yC/CFF+TF7Ov72HMFC0VKjoAOoVoWE209UaamLaR4S6wIOhmV
wVOgu93/f6/Pd+n5LMGhaTKOeD2wzUt2GO/XbR0YDjZrY5SQRfrdySzLjBZZBoSNEgn0EwhU7EDA
8nYU/OP7AbagnBEgtVIBJlAf3+yy3GF5tujo6LIAjjxfXR6pXEuhGsqXFerNtiVN7OJprR6stQp7
hOV+68qc6bI4k3TQpnLiACOvw1UNUxxL0SUGJOH6atjdDArrK5toihJtMqN/rABjvYg+OZldUto8
0G4KgpiH5MWMmHTBVl56KZcSCQArxmYWI8tC/uqdyw3mChqn6Hmu8VdcxOGz2tQzxis5USsW3Qs4
bg8hq6CXiYnXk4tgDJE/4qCtI1GIRXuIIiw+XznwJZlpTQrFBRAyMgwzBiBdZWb7PedChWxOj7OK
VzEHM0/3F8NM3+JSU4upUYGLB58kTUs9VgksbpYrf08HVWOB3SL0z0shvTjBEMaNzYKsJgiBX2E7
Gpwg99sZlwLyYCLPy1vuWD11cd9p7keripzehw/qcKkaaaVCj7P1YchUIRPkuF1H7PMITiTsrquQ
WKwnIBJDsw+JTv9K6AOrTA2+HJFXVk88CdX1jPyHsAo8tHesqv9KXX4QssQ+92ORVnjwBC6TG3yi
9458YEv2/0RpxOJubzGNhXC/xsvgOaEa5RaHLML1ZuwLHVkevIsgC22G99bhAjHXNp0lTdW4glUF
XFaWMOSKw723ZYV+H18Z4QBcE8NQlZ2CmdMOVgf6G/kfK7on3uSEZCye9kcXpi/zov18FlE4/Gy7
GjPzgu09SpAXefRc+wJno5f9hE4i0jpPd2k4eSIgcMrj3OFxcRnxp2FfTAL5Xed4kzscuVhtQEHM
3RrtSPD5+4Gz0JYkpW3VRZlJcEMEdNPpNjL1vv/lSGrq1Cpx5R7jMcIT82a0iddYpawSlYLlRLPx
/w/TWCaKPRw5jSLCOmV+ujQfDaPtK9P8+jaTTayTN6SbzVkhDN3qX1BDfcucIp7y+6Uj0B/l3KzR
vb/JejDihYZIFZPsDcyIH4ffWzZnYBXclblg3UAr8cMSnIPVCS7Dt5415lkKTLHj27JfCDmeN5qW
82EvJYF3CPmLRpBSuXwSbRuLcRwvVGsMRwte5886scmX/hWZGULUy1DYwW5crqk0C7Tg6Tsdg7Zo
UzPCXQW6CWOSAQ/LAohTz7ltxtD82qCB4P4HAtFFjjtwSS4fBt9l9+dJVlf+P37j3/l+nyrvhJaS
YMDxbYQBqPFkr+CxCtAkbgN8+r4ER0RVIoSNMdrHkaiLaZAnVUvkYQmci0t4m8iXM/w/hRVrimUh
HiVX9NYK+y3+Wcljy71P8a09RD7TRDcxXPQS/pFaRqwuCp6r3r9v7zkCXg9noHXf2NQuH2kE2CNM
78iR6WO/8LCXLrdVTT4JpXPI8eSmloaj82tMvv8ojCcQAQbbMhHx500vYe+5/EJg4SgeZ5kp0hVp
k37uELZPNeI7aQrwxUur8omzRqKr4q+YiltR+EGk4GYPn2CPAoQ0Ftx9SrdForV3drS2mqFfxmgm
3TY4ZcDaRwG9OQu7+SSv1Rci2b88N2DbKNbH5Ia8YmDSnSuJGDoWGxau55qnDW4N+jOVIC0TgjY5
N7r8NBHTTGrYjHdmWBkRWf3CzyNf7EeUVRj6J+34Vn9EGzeiVgvc3ngema6BYIz2rT2SNaXP1I/B
hh7ub11B8+paZckIpdmQF/ZEfm9InXvMHay+6bX6grJbzAcRBE54MJBpOIxnwYbLqOSHwgO7GzvN
osd0Ki6YwG6PbPGSOpeS0DMCqdedc574jTueRW+vncv6OyU/7hWS4iFhsEI1GQe2F53QWuPwBCe8
K9Y5dcxFaOBEhmJgIaZX+DQzAIofBT8dZfv+fP4lu2jNIrVvLmPKEDc74mS2nctmZQRhfYm5NTL3
jbpvg3M9CDif/fJwwQxE2E5f43d7KwtuFVTuKH3j8ZN+TiWleqh6hS8DcXztruuU7IP7U4QMMj8S
e7jx7gNVkphA9bG4f/1oG+N/IjCD9vmiH1KaFyAUVSJDqt9j0qz2BBAD0aQQXqZoyrRDBF0oGS2Z
TnJJ0d3lNF8PNbVenGJVp63+iSqfZIS10pcGv6t81DbZlrNLSdA8e48d0MjxGqGubAunpOV8MadS
qUCynbneLwxH78PXUYm7rVtjNP2Zo/4x5DXsRPksHbaQlJbiS9TO39iNJDoirhuDp0Tpg4bRYiS6
8S8H6A72hw6eIlBpSpf6pvqK+dSkarAggn/ppY3IYpOK2U5qSmYI+J6/d4ZYR/Z34cyTNT6/7mVl
jjgygtEd3ecQ2H7UI+ReGKZJ870pqRJIwSjYAkFTLA+FbQMWmF1KUUKf2kZ08x/IfqSByhF9hN8q
nseV+nY57II+yiB4VKQ5oBIF8IFs9eLldsU2gkt9UlXW6BL+XD+tq8Y277NO9R0nC6SyGLmsG4S+
jY2EwZqcP8y3sqYpuSHKLoNAo77nLlFs+jcxs5Iqq+p6xUIZfA2W69ZhBzwsmO4tsm7YDGOo7ymH
8FX/2AYpTK12ftZyChEBsazye83sopp1UDTAVPX3huq7d4SRXimn812fO0MSJefl7Cj3ErX4fht0
aosrpCplzbfodbf1VVDLDQpWE+NnRNanYL72j51/D7EBgVPS3s8KSOwflGYS0pBP1swH7j0TNzdi
o775FtnLo286BRSaxfRpeYVeot5puWa4uzwr/n7i0jUpScnilKt5EoT6puEt51CkBrzQLoKiM4Vj
x/SHy7WzHZgaTj5+33kydqgtND8g0Y41hajU+5qcuvuLPvGQZx2otBaJm3M8Ir2rqykDMCUIXSg0
yVx/rqBLHc6EMea5yCsBLyz/nLFBqHFz/40IrJKjO3dnGZr4N92YBKnmZctEFUl1RmtANDPyMZnd
BTHDO23syS5FHHyQNFLlHBrIulV6ykkPcU+APyzdzvVHFj6o8MBBvibNRnz+tLvrzqz0hBPSgSEm
p0qbBRE1ptOXFBsUF1g9GswzD9pn7HIHxfDmAr2h1bwpfmLcmdUtpODhavZRJLm9k71sy1hApwnV
I53YuipP48ouIxkwadhexY0cSL47CjGcHhloSZdg5iE9DlR5Y1wVnyY4jvPfPUw261qR91wdvO3E
nm2lwyKLOAwbdDcsC6hpQjZMga6sZJyl4sFQpco79pqNDrM/R4q05OPDiyfo/RpjCNs6Zb8WOUAm
Y1cpOtY0j3J5FnRyt2ttvLf8du/NggGRTMDmEzeOObBEbcpowwjZq0+eMT8uQwV+OH/p4bVnEOIR
d6IfcBmXD9xbT6BHKTLUfuEWZpZIWlnmp2D1KKY4o6u5dprQjeUYdX+rV+OKycWZz8qC+l6zPVN0
XMNPZ5rgRGkV74FWlY/OV2iYeWOXN9V4HEBtlmKgbQ1WkmdiqrMok4KSJkrR/f6c8HfXzdWVEATt
/MdYwMW/hvHv1w7Pd5LRKfv8DAZxfC8t8b+wfdWx8zk58MJNle4W4wADiqbxmFFwmL1DqjV9aDUw
rLMJ0zOsHpGmf3pEz66q33T7JcscvUdV8w9hz4gG83edfoNIWK4TMDjc2mpyo+cGO9ix11jqW1rU
wy/GzCuFTzKTOHxzvOGNKkdhy4waFPP0JxBUiJ0deoxUqxnk8gPFV+dndQ6QESbarpT8qZC5UPmd
zlAQWA8s1t7swWeg05ZthSbRzWC8TM4KiibCtdX4m517L5mEw6YqwTsP0bIxq6XvYU2OfFJomVcB
trn3qaptib59QbjFBhEFtKOqAb7nuYsGwADho8GYHDWaJ7m/+iy9YF7D5MQ3u7OY0gjzHx7mL6yj
azf/fGgHFaalSXk68UeQI2chqrjoGy6gFUXmj2uIb9LdZtjPpxSjFwz4Jc7TEpZIswdp/mHiu52g
aA0GYeVJWmUHGdzV9F12zwf/nKWlTc0c/CMUZOD8gONmJEDiXOjncQHWczfTwCuqgXxeFEegePkF
n940ZBd/TMJlquY62V9RsestnOBO87sim/psazx5ZK8Co20Vn8y1opo9YoQeFWAggmv9QY0uQgxQ
tIopIZG6h2Hdob/BdbQWK9rl4kPjV+TgVq9tAJk+1leQT/s0V1Wd9Pvi+nk9BtkGItw2ORW1jtEf
4nkGFeN9wkSvX6pQArx10p1a5p/eLAiWir2oz7MunYdl/IYUJxyIfZNBFpjlAELlr8f1SJrW/CUZ
GAxW0Tv0ndHByoV8YgV3KVTq31edgZiT37earNi97j6pISOGcdMBbcUKlDww5V/Tg/9e/gELf4r5
x1OrM9PFT4O6bAssGijeo2+Ky79TL1oUl7R1bYuISFrTz0hcQ+4hwZOABlCeE76zn3tzs4O9Bvxx
gunrSslLA4bU7kqMImptq2nKksAgpcl1BMF55vUtrUzpmdyvM6Jw6wzbB4wmCSNzYvfywIz0jXuY
EQLkUEBfreVUQ8YHrv/8oCE47d6nH07X3Q89hoQlLnEShui/Iwk5UI6E1xsHl4qUkjZKw40gIT9w
n9n5A47ExF+9sbzrYLJikkp79Vk2d7bqKRSJhHlElqOHPc3lX8EHFpFp8JAya5yfA5kj45lDK0za
c32fEN3ppa+sUIRnfB50ZrqqnX4dlVVyRy7tDbpxoaoZVBSvojVpoqd/4UqLAkRl8kufTABEA1Og
Ro8/VkwUw/sTu8yUr/xlla/iO1KaNwf3WUXXKkk/3Qj9g9d4dXs3LiHY9mNstr1UzOAO8meBY4Kd
8TKejvFa6hgZ9FWQAh8MaLjq7S2CA58CJru4oRdEuRqWAl3LPSnv5SScMheZyD/0XUrgPjY5HY5a
O8jIYkj6rYgjXTI3ewmnfSYfdWRWSBhHZNbiKFk2El+lbsYNOCQZIkFY5tA/+5nyrxZPOpEa7cLa
LsP9P2eToM2WhGWUUevgriwCu4F8TM2zg8K8ven82Ug5OBPpHf6Fn/aMQAD82ffrAdiQHjU7g2Ls
Q/hqRwHlw2Vieszwzftr/DsAByof/008WwdalJEB61vB53iz/cYE45MLtMa0ZlChfNogfx8T49Sd
xhulapYlqS0EIcNgVGJ9WBvKMNm9Lx5auZTUiBYOoIoPpqpBHkTgaXIw+QRHGJPbVmgnQQAqupum
MAEzmTSAIhBxBO+WQWOhgE84dYS6BM8zvMlAH765j9ba2bkbrmzb3WmkG5XqsmojB50YWfaMIpnv
v6UZymcVcn1a+6Jnw1wdZhpH7D8qr0gibm7pgUk+eFvdakGsew5M+yCx7+WmO72DSQIc0IifIC8Q
k/DeZoMsUDiPfWcKOvLpgxKjkD+jkkIT5tHHBOq8dUmBwRyywpoYlEmsTxDQ/CiRPCM385Px+KXT
PEPImcrxzxw3tV5curaaERsswjsSglyRkRJOrFEh3oyIruU8tgc8NIMfALACIkX/Hv6DTzWMn57f
3GARcfyRwoYTDuTPWQ05axo+2kdco6le4FUnp6281zZL76S8E6S+E7mfIoY5RnceLMm7iajDllSD
od+6TxaS/D12XcNnxiimrSNAdlBrgncwErRzh7/KUrfQNj3+1jD8gSHS6vORMPgIX18c//6eTuE/
vjzgzAK+DwFxGQdNf++wEzYIaisKY82RVijK3i01kEpXSyspe6F1cSXkU7YB3ZvZ4pyMQ43+FQ6M
NVknwMTLxFV9UBnkdxsVrl4agDZwbZDXBpHqAkfsIzucmfwgySf2Gzjuk+aa03JzyKbXM5xmAeUJ
jGGMb+4=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
