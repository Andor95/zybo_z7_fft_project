// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Thu Jan  9 16:30:05 2020
// Host        : DESKTOP-8BPORRM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_c_addsub_0_1 -prefix
//               design_1_c_addsub_0_1_ design_1_c_addsub_0_1_sim_netlist.v
// Design      : design_1_c_addsub_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_addsub_0_1,c_addsub_v12_0_12,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_addsub_v12_0_12,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_c_addsub_0_1
   (A,
    B,
    CLK,
    S);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [31:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [31:0]B;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF s_intf:c_out_intf:sinit_intf:sset_intf:bypass_intf:c_in_intf:add_intf:b_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_sim_clk_gen_0_0_clk" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 s_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME s_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 33} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 33}" *) output [32:0]S;

  wire [31:0]A;
  wire [31:0]B;
  wire CLK;
  wire [32:0]S;
  wire NLW_U0_C_OUT_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000000" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "33" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_addsub_0_1_c_addsub_v12_0_12 U0
       (.A(A),
        .ADD(1'b1),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b1),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_U0_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADD_MODE = "0" *) (* C_AINIT_VAL = "0" *) (* C_A_TYPE = "1" *) 
(* C_A_WIDTH = "32" *) (* C_BORROW_LOW = "1" *) (* C_BYPASS_LOW = "0" *) 
(* C_B_CONSTANT = "0" *) (* C_B_TYPE = "1" *) (* C_B_VALUE = "00000000000000000000000000000000" *) 
(* C_B_WIDTH = "32" *) (* C_CE_OVERRIDES_BYPASS = "1" *) (* C_CE_OVERRIDES_SCLR = "0" *) 
(* C_HAS_BYPASS = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_C_IN = "0" *) 
(* C_HAS_C_OUT = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) 
(* C_OUT_WIDTH = "33" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_1_c_addsub_0_1_c_addsub_v12_0_12
   (A,
    B,
    CLK,
    ADD,
    C_IN,
    CE,
    BYPASS,
    SCLR,
    SSET,
    SINIT,
    C_OUT,
    S);
  input [31:0]A;
  input [31:0]B;
  input CLK;
  input ADD;
  input C_IN;
  input CE;
  input BYPASS;
  input SCLR;
  input SSET;
  input SINIT;
  output C_OUT;
  output [32:0]S;

  wire \<const0> ;
  wire [31:0]A;
  wire [31:0]B;
  wire CLK;
  wire [32:0]S;
  wire NLW_xst_addsub_C_OUT_UNCONNECTED;

  assign C_OUT = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_BORROW_LOW = "1" *) 
  (* C_CE_OVERRIDES_BYPASS = "1" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* c_a_type = "1" *) 
  (* c_a_width = "32" *) 
  (* c_add_mode = "0" *) 
  (* c_b_constant = "0" *) 
  (* c_b_type = "1" *) 
  (* c_b_value = "00000000000000000000000000000000" *) 
  (* c_b_width = "32" *) 
  (* c_bypass_low = "0" *) 
  (* c_has_bypass = "0" *) 
  (* c_has_c_in = "0" *) 
  (* c_has_c_out = "0" *) 
  (* c_latency = "1" *) 
  (* c_out_width = "33" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_addsub_0_1_c_addsub_v12_0_12_viv xst_addsub
       (.A(A),
        .ADD(1'b0),
        .B(B),
        .BYPASS(1'b0),
        .CE(1'b0),
        .CLK(CLK),
        .C_IN(1'b0),
        .C_OUT(NLW_xst_addsub_C_OUT_UNCONNECTED),
        .S(S),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
RhedCTftnR/lFLJouqVu00X8CC4TkDlMiW/WeWJSYDyQHVO1xW1z9+hmgAziXI4s3uGElBs9cXRS
4yVMV7QH0w==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
qX90FYlZfOA3sZpcv0rrvWRKCSlr3skWpeAe5OSxHcZPsVQFyY0hhWVDtP/vB+dV9hIUwAN29Fn9
+L9OEXYMlIw5gH6s9NmquAs3lmPRLTrrpKJWdvf6+b+LeG9CPwLz87bXAk4qQW80zUHBaDKDLV3q
pd/gEf8Y3st+mLOGjNU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
chpH3Rj5RAirYZHkpJvTu4EJpydPPSy15v646y2lN/1w3bwHI+M8EpLMBjimx8uIWRJZ6dDWPR8E
zkwK1TMroEKFaL8IkFMSHPyzqbrH9l1jxYFs0ee8Itp8Rg8qenv5RXM+h4JRTtRmzHk1A8s8zeKY
MgXzIBCAzBa/vSgzDuDaUnO8r8f/5KtRjmE28JLNXXAxyijBrMTCiIHMRJZL5/+LgUyVq7/Zr5yx
7kuNGWv7Q0wESEqhsQbK6UNel5ak1cor7647dYJgIxnNZ6jGVJPkqi8ydAIZ0z0Dy4txBvVaMyP6
2kYYnbVN6S6q6yr/QcJHEOgefF59B/8JuWzdoQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Z2XauOF3/9FUIv1kEFfEtdy93+zHY5q9dH1pJCNLytoWWhhJBfCI5Uc2w/fQLrvVw2Ivy0/rs9qH
9fomTNECWeCphNDVpWGNcFDGE51jt6SDWt7FmgfZq4iXN7XWrfmkQa4DB7QbtSBHCMcBT53TKbDH
suKNhAWMV0htWeNEgN4Y0biiz8U4RLT1stdsMMtEzfYVhtrTmFWLihJ/9gJ01pm/kv4OB2cJEslg
sjbxCE2B4Y1uSj93tnBAw4/f2RYGfPcSrkaXkNgOYK2dc5NQgd82XvP8siAK/ETcZQ1lBK367Rxv
nlr6QUMwKALmjLVe/IThpCRGbOSy3QykkwnpHw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
F/mF2RV90mf5PFUZZUjMej6jQS+Qx16uCeiDQxUX+H+O2yjP6UECegDbtLmGk9algbDuGBCE0KgZ
HzSxX5pMwDq1J7nFyBsu1dGyu8NeJxfu0fFA/qS/SYJSTtwnZ/eIj09mNLJ3w8wGR87ke1ETTRpx
4Ni9DzGJ/aaWFaUenL/x4UWS9yqlaNi5Utcpa4kcUHC6fW0asZsThZJBqpArO54TF40nxZAD+V82
6mBGFOKUzgmHqXNsbVif4JqUd63LG8YWxjrOeesq61xzyjw9caQMuA9v/5sQslCFaeSt2atgqyaA
y4mcm0kU60s6mYqJr4KZt8DWG7LPGoIjhjpnMg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
YNrNxIZl8p2OCPz+4Y4awQ09ZZ41X8EdKu2SX1gXfB3qoV3EOXf4eFHtYJ+bFIccfEIqPfZ+dnr7
udcSDAJMcxqZe+YNk5hTq+uX4PlnQH/IVlkgyYiDhQ7aRIS5pwtIRC3biXn80933ov4zlWLI+ZBq
bG8lodZjxKh6HZPWi7s=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eWToRZf4bzAQhHTj44yEkOqolJ3BOvlBPKnKoDCpSvCHSrnRcJKgOd47PnboABTqLVstQenz8AFe
rWHBbaad7KOIh3LsEXBwDHKDdby7iDAB5nd3j2Wp7qiHOv7WpIJ5RG1GMQa8+QXHMVaV4jSvsdmt
d0D9H0WEVaqrSFW2ucpsMYNE0LnKcSJ1SejKOcgISzaGJgXcmEAOVAOnCDPc4slwc3dt7Jne1KvY
i66An36Mfhhd1b0RikMf4yqpM+uVrL5XWIP+TBtw3iQRE0ZkUSn3K0My73/2vKKBfwyV0c+M/Il+
aMxNaD44Gq+/f6zGjkelgMEI4BjB5rs1KRvSCg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
MBlV10adNaOmnKO2bAOqD8oXZgxnOcwqR2gbJ0tjSkvCJetIou0Qjq2CEGHrUYbbFNZgDM6Q9jH1
3VVJjfHAW4SxXtN+IH4etT3ZFh88qd+Pn8fXghMnlbNqXqmwt4MbHwy67Qn3acELwn1tFrJzOkmS
mLy21wEiIb2H2rNtNtGoMLBazng/cP3AoPyWrO97TXPw+J+1FCN55ahKMWyO34aiyrE4wdJxRvnu
fHPa3UXmgWCwlvVXp4ZxMyhQBP+V+w1p+3qSeFmp8fdTVNIDjC1f9ThIpcrnMoPm4H9F+YmhZ4xL
LiBL65B2+Yoe5jT6Y2FPlc/58vOMSPbNbmUBEQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
2nehwoOyFS/d185H5RlyNqbbQ5TAlEkFX6DF4qd+YjcONlc3E7WRlkpERzHPQqppWkJszNR7UgzN
I9Wv/67dUNKpV32Qo9waHC1v8D+5/zEsfOUmRebqTpCr7dcthMrcbqVEjoR8jWFz2lIYbc/MM6sg
C04fKSBtJYlhBfuuwBHin3UWELpB+IodE2rmwKzCy/8VTr8+dONj1UBOmgwUYjDkV0hTLz6fKfQ7
mTJI3TsQ80Nv2lgOaU4t1+Bb5UoFwnhDqrtJYeh0rpUCUupP5t5bsuXOufY2FdL+miuVrtuEvrTE
PVCkFxe55hPGaRxkT6jBv5PluapmeYV17G3Vzg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 23872)
`pragma protect data_block
EPyWASHDyFfRdFUYe+NOtJduhkDT14q90hBJZCfiYK+AEywQ0Y1b82D7tkTGxdejkiAHEH6eNdin
wVd3m4iQhDdcFtppdFpoMSxjcWYyPL2lRN/wqvzmLT9C48TJAPHIlH/m70dVLbOtyvqSyRhO3gGt
CYtjyClxTH/in5QAMhyTmjyeWJLD1L60Caa31pRlcfCx5vb60s2JXl5jscRA2rS8EL4/YB4qRrsm
7t/OA5wwkWnj+hOV1Ap4sKhwUbzH3nO8RcBDlH2D567FFXDy4Z2W8T4oZ3WRQxJIOBgRnh6jyqxV
8oqpqGDSCsIQd35BvFKULNiGjGd36bmo07mIQUHlkDJPy/sqn4iHeYRKGF2EoMNlP32O9UncGGXg
JS1SiwPEpgYpo0MmwsYDwptNvLGMHR0+cvVMpf1qnC9fVa4Zu6G6bN5kZ/QQUo/rpPPiu5Z6Y3PZ
zmU9uWafVokipeX8cCpQr3gGIPQfQhKSZLgySCH5CWSUd2BwG24lN86tznvMuOkObxw7ZkjHuEdz
MX8yeXI9BA/Z0FgDdysNQkd0b7X9J33Zy4+7EuDn3i9QLvZXDWMFKfhhK3Qzdl6SwwsDDgtKFBRH
gTP/OPeEbWmwh+jZh5Uj4xrpQzP+FwWZeQZbU/lgr2loqSiFRB2pg0/zXMAbo1MTyICykxcmZVKH
YO4sKCs5uCVLVAc9wF7iz8K2OyRSst1LNAiALIHEV4OriWhaYPKsYwAlZ+43NrF8MxUgjqFOlhA0
8OUiURr1U1e3VIbVSMlROyT8+qJTy7Fmv9pUwBKrFBe94fmJxT8E/JfHtwcSLEhmtAjWcvOvKa2m
W8AqecSAglBtyhgt5OPP6ZvXGbDvmAxzUdVWWUtoSw3nj1R+g/V6+o5Y74GyZjG+NXE9HIlgWs/n
5tbOyIl65xZmW4iH/bW4rJ4QcvxG5Vnmik0z2g69sUftE3TL0ixj3tB5Dc6OVb7Q7n62AfYP7aTA
/Ziy7SD+MWRE7/ECkPXAmZ+kD6uc6uevgU64bVwgBGx19GqXh9jzMUAMLeJ9P0XjqOPEcIj4tlIX
QLNNZ+EokCj2zaDV6whcWfQdLpE1yEHjwMxKUQbsoJZ0lUftS58mOjDORG21fLqWpzT77BBhNAcr
5HKVtHp/g3lghQFLPI1azSV97HFbSLgLSb1VThjZbr2s00mBXDmzLb71BKzlCB8sZbvYOdx8nZCN
B0MZiLt8YbFXQGxL45MDUY/KyEy9/8ZVc03ng9Lcq3XrauJbKvpF3Nw5DdOZ0SIJzfK4sK+ONIXG
PKoOhGymg6kmjDE/lXMxfTtgbM2kPLG/sKnBd3bJAuqJ/tBBkq9aZ/ewIvEyzny78JiqIew/EZWD
YsznQH0WGwafJM5Ncr8uKtAJCy2ovOGb3ra0DHjVDmIR0+1EU0cJZRrNiK9YE916CI7SuC2hS7HG
Es7KZr/KcWAEJT5ND4PmgFM6dp4gSinpsBdYMWL8HJIwHdqQXNwHGf4QIrFzh5Jdqyr5lCU1+grn
DWOh0X75moeOZBQtUdcXizBqfiWIdC4yoF81wrh8b+q3b4SVrpOVSNR90qth/f6oCiPZaz3MaQ1p
SgZagvfsOXQcyNdS/6h3BBh0ylz/H2rUvoLJEehtPHX//ix1nSXUupDAN1mB18oeOoFoFRE9Mw/r
mUmsY98Y3fhuz5U/dyBF7Z15rXCSaOrbQGqYJJV/6IwfBlnL+5pDO/jJhjxqqjFEPumw5sAIFKdv
UOz2vjQa0H9fi+cLRFO4tdBml8+ER6O96SgPGn2yPOuJnUxDQnftVCzhtE2aVROC+StR/WTEes0k
PLgvp+X5++Mobs4uX7g/Q1uDxY0aiUmAXjfeospVYJJ4nC78DPRgcGid6DFkAfiTIpFogh81pL6p
p147r8IrOozAa1+sNB2dWn12/NC6NC3mcY30J6oJeX+qUR1HE7tmgdYelmeX1VJvWtJRfrnKzj2z
x+nRRx1lsS7Xo4CZEPab1JrF1TJJZGshYqv6SQ0SgbofoxYGZJCH7Aud4Swxg1OzlUgwxOVb3rAR
A1Wld8xWOI57BeVw1TTRWT5CKP1i+ciO+FV3t/TgDxTolz1rnpmh7kNDt0NWyob7YKuYynmuYW7C
JKHzILI4GxCngq8Qr4clPpi3WLjZeD0BlY8J3Kpkh7aqxVNwQoM70shTsvKf12fgeS1KBOrNvNDO
oKBrcMfk5BFH1yKiA0hztWbePRBfUA1rXGq2VJXEa9Jn+mmonTk+x71nFBUvGjeEBu93eTW9k0VD
qQeYoQPfNKb3UsrZx7Sddb8r+C6G0CAAwEnr6XnjqXoPDmJOruZUal1aB4Kbowj5k+ZlzpASnAxf
mRnA17jFTnR5diQnZlSKtt/oFJJlf+Z2nh6VNTgHU3S0e4ZESFN2/sZKuiQn0pNHEhrcJ1GzJwzD
Ub6nkqROYAGVCSf2KxP4+A/M1eu9bgnz3stLvRtkkE4MgsRolnPXU8XTJKbpvg5bw8M4QXkLgx/W
eKFFA0Fv+tLgwUfjtZm5D3YDUy9fkpHSm6yptu7PUM1zCOFpDpL9PzUc/nLWsEO7xjnnV7MaC8jL
QjNqsxmzIpJuT6JnqdcX8SVe+zRjwrYuH9zqHyJ6VZmSs3B3FuluDX5D7vKj69CguZ6HJ8TcKS64
zInnWIAfqWiOy3Qghv+2WV81LhmOAZ+Kcy/wfrQOlXlqqpt5Y+PnpMFHZP44qs/H4aQ1PvwvCAja
uVOisN6cxTZprLHZqxCjyeo+d0HwrAR9VGptEBjlTTYYdMljXkjrxxs3yurCPJJlT+WvgeJCVluu
SonnKIlPqtGBGJE9DxicG9gB4akyaCszdWVxiotjD85Ta24OJzzr1r+1/36EtXRi4tZwXJSfs5Nk
i5dIOhV6/lKSnL9ALfVCw+Z3CzDf1QxXsDv+O2qRQRghBajfi9uaXeKdGxGHHcNoMMGyo0Sac41S
fjBdK8+RtPUMXn/WUFWnTo+Ep0xyy0Y81HvD/yOiolsq++jPiS2HwplkrltKnMA7oGtLGa8kMoqx
VtM5GcKkNnwpShM3pkzF6R3ekNs+9ZQdJYBffsdxIRfQmvpwChPRWH0SsmimWJho95OylnBrwwC7
mINzNbaWwqmLSA3mg59WJGG028yxAeW0BsCnRkHzESi+XJzVnB0/dl5fyvsGo/uNqqIWXVdmKFa/
lKrgwqforacWbPL30b8Dk0qXHtzG0WIsSQmZlaRsIsnFpKq1CCDvN/QO26Wx9Li/mhKLDER9uIzO
OwWwW1nfI4oYERAGc4u0HGkbJfAFJveBezqhfAXv3JMiwhrWuwVsAhZTLcNeO0sHrBSx3bS4dM99
ELsTLIUJkpZtat1eaApSJmx85b59Y22aJpL2rb3w251eB3E6nkMz4zmJIYiwJtZb42G5tZLzsxSB
pPVEh3R9BlZEkExt0SOYPq7VVfH0by3PdfNAXDyiWH1FRA4r05odOp3JQzTCdBHeO3NIt3t7P99f
Z/uhN9gapJKCai3KMRMre1PvP+GTfdneNbPM41YFy7porlgR6mIGASwhlbEScGxP7Iw04Fj/brMs
+b6xltEqbY8ZuAajawLJHhKwrhiy5pn62TE+3KNGjoKkKsILczmZRPj4NkPLiwEGMNt2rdoJyOEK
AX3QDhjFZWIK4QbwfDOtDBjczFWTnomPHS0yCPipdaEPtzPa/01xMrecCzMUJSPioXbGDvdhDxz1
ngrSCULzHTuiaWsfJtPo9xifwGxCNWFlIA91q034wyEcSetEawqPXpEFOk+0qX4tcLAUxFVg31Bm
g/x5+X2ZEomrkGzcqOyyhS0NQ7OBnorVpDVgYdqSqIvcaTwirXheoxlhKYkY2CZftWq8mGdN5SCN
gR3N1rjO8YZJ3SOxCY2P4oUbBLrplafBAM/EJVGLKpuHn/Nsrbh/4nqU+HLDfD5LI76khRio+6T8
sdvdZ8VWAowGLg7awmzmCyjLxbfFRE8+4g9bWx/gOmn8TR72OUJBFrBY/cWUikeILsMMpIwaZsf/
XAEaXzCxDnKwIpOyzM5NGP5MVQ12qKoeJsgOv7S9lItEdrXJ0JczQXLwQmg/gyLYn5WPcD94wsPs
9Z16fbUCnRIoFB6pTmSg0IZVezdS92VNblKQtnNC5jxH7vv0WE5BrlY14YRpR4962LnFoP91+wxX
BRUC0cVHds9sriqwXfVeuOZecrusG/U6N0PnDuMrr9j7lFx4rgWVJbwulCYZ9SRDNSrgx4s127RJ
oDk5vtFy0jjIkJDvPxU77wGocVwTAWC/0Uj0VY2BI2WkeJ03YuF1HYHOWF/5efqrNXZEPhM+saaT
U6NTNOQbDlYJ47Y59C2uzhC2HGqfiFEr36VRFGZoQQOx3h/aZYEuAqRxlwTsyjbNKCoewvUUHjDZ
j27rw3pbA8wdKNOpBPojjRwgdmg/oRZwatwM4fwY2L+hJCsiJxJ7MeDLj5gFXAqp/ZEFxpzPzQ+c
KxbK8O4ZG5IpWV4rMdSbJLN2Tn4TzAVsj1WqZI6NG0U04dlq3LKB3HkRpmYrQCxfuUDHDNkLj2vK
E1Z2zA5tfRAg0fhC6HfQUMUyDC/s33/REmM+N8lP6Scakl/anBG04cf0CxxvNBvVrkPgKqd78tc8
u0RZMg1VCMkP/h02V9mK3SBxJChSQmoZaNo3aW+HBoRcot01qMXWc4vq++hwt2heBP13OV5QM1XO
V2+m8p946WPjyb9ymqHNZeuHM3MvG5awJbhJDfw39e2/kEUFedIqLLseI57XLlzhgT4DusFir9BY
5IchJAGx/NmXH3K1SJrXwkLbU6E8y+aE4AwekHDcSUfKG0Q16sOUQYSXCRR/RHoHXS4wCyEcGMAU
WXhaHhYUTENYJcd+V91Uc0xoZlrUrUuDQKoqnwE3d1e9DJxjGZV1OZwQKsj2J4tRClFtz9b/lTKz
ZCazcHdfOJ/1oVDqps7gsYzsyZNDqbNHwHNt0i+FRbhNErFam945OCdilueiMEOCUPLuW67dmsrB
JVbqjORvvOOJxIEWiOBCdJ+T/wGuk/Q0og6Amzgwd7lKRpoJyiWtPBjr/qOQF9VQY7DJ06VACJ7/
Wtm2Hx99QJKh/wSGtOUkOU/jE9OVlvxlYw6LPXdGt/ResUXLW1NSYt9X8vcFq6SRF1bO7Hg3RxIA
rMc0IROROKAUY+lo0rtjE+8cZFrfMOkdgDfW2HwhZ9k01I0SyfFlDMQhwiGgjj+qfVQhq+u0OYB3
VAAa/2sHyJccorBgpxAXHr03PhktoBAradqgWnDMoI6+n6wBc/DiruvuFH4BV+CShJMvPM0zLMdl
JIEsHehzXQaCIVKHVL0aTPlQStm3xSimOEfVOz9LdONb+m4kHO1Bp04hEp/1JHgHHYdtUVMxHbDO
AxX+rDIcaErpQu94v858EBkDoiIwS3OV++0mKpIEDlgZvQKjAc8VR4Imld2FgmwFPzvzCF1OH5sY
RVUs9ij5JP112PqdJMQy2hdxuIhddRV88Xdua8uqO29EOElaplkJoc/wghzK4ODy0DoPgHq2f75P
NEhdxRZmYoG0imDBUvLGHZ2hjnCcForIu7U6es6TFdx1B7YQ84hNfGbz1yhxJqfff/jdg5eSqjlQ
7tbZ9UbjF3QFHbc5VDbXpo+jzrgVAWUefaCa++TRpT8RpHz/lD8hnY0LJCw5hpbxbp1xJ1utAhkW
bGZWdoLRAh0Rtwcuqnq5Kto2ANX0gBybTfRIF0GZVATyn6lgyaHxHSeyexccRG1a8rPw8I1ZPK2Z
5K7j4H3xiMgx9XQYSUzDWt/LDKWIdhIh2N3YaXYLOXrHY72/IcCeTXbQbBiuCYGjRsHJwbWiqByp
ur9XuvO+iFIBA9RE0Q2bomrxoiUCRvCPqFztH4ZkOnSNP7VN1UdO9M6v4BLkJmtDfw5UHEjS6vAC
CwfR5iud3DsUb3S1DqEpZvSg8SPcs7iZVZ1/wuduMgialqrXhT4dKCLhAvQkRTawpUickvFNaYOy
kk3HLA6BTOpfKpUuvcx1i3yuixBbMjJiXxKfPwKn7ei2dp290ArDfCxRWLqiIdWjSE0/rj+El0Mf
q51w5tBPcDwWE+GRQ8asnSki+tFUT2vIs/n52dVe4H3ngnMLWSe9qGBxfQylpT4SnB3V8st80dpO
Z1CeLJn29YhfmA7PG1Sd5iDRiVlvXFmkuJRJ61LlBaWd5l2k/4wvhBWOpT25Os3epnwA6ZeTjovt
3UCuZxN1PX+RZ+iU23G3odKEgho9adxTksbsvj59l/rwYXzo73zDHzKZNfdBKiSg6JkIfNztSvbL
MNNYESAVS/XQ09k5bUyTaJfa1a701ewGFyD3O6pYjhirYcS/LpgSZZuM9dQDfwyK8P5qvIAjXH/o
9dw0DiiHWk7YSURSiAkb56KyUSiG54mu4MZXBtdLBktKkt+IgsSgFAq12J7J9DHXJN/jojenJWOD
yOCmAtT68X9+xoi8HehADXBrX1ZUrQtQv8Jpya356olyyRuYfEHRpzkAohrQ/ohzNi6u5YuN3tJB
0wQdMhqGtKMmtb4QpGwV1g6ii1IEhgP557nNNWU0eRFAQF+u20R5isytRJFDsQYuP6QCGqis4vUu
NA/9EGsx/SP77ByOjdduSrAyQ+ai/qT1+U2QKk15nrh/qeejMG4zgqQzQNKKxNzevN2SaLOS9kAE
IwsoNehpdLldlv2NtZoWANVLoHjnqQRWpFeLlHi0QhqP6x3A+9rZ0czqlxMSBCeEAT1SNXyhDAo8
39zICW2J71iY8u2IeVJHkLg6P7dQc0+GEMB4OpekO8NfIHTKGYC7Bbm+evS8G0w871KCjccolXwG
Dd2JomezdQ/tMgREmu2qv6TAjw7ICBo+hVVGUz/RIBPA5z2fmhFa8J53v6nF13H6d8G3YBBeiv9x
oZJ94ZA991YMCnXhbp3jgaKV+SMXO8QwSdLH7ojZOHGK5A21GoHUzXPOES++dHOO71ac4i4QMxiO
W1w2DW6IAULHcqPkDKDKCaqqwkFx1XBI/zDhI5AnIyQqKF61bc2Nvjc/KkcjLisbHxLeR5Ov+Z34
kxa4GKA+G257t0D/O9ns63ULz560AXkE1TR1mAzDsreg6szRr/cjsj80Z6O7ZGi9pnmM4uA5/aUg
1uBlLOpd9NYazRjTVu2GMgl7yruK3owatkgqeiHRcmO4WzGCxow/Tora9crU9fQBg/dlTxArp/ic
rn0EkQMxDzq/Q/PhtHg2WcgC73aeEWQhxSRmlp47QzhaTZykjnXHXdqzbqgcNnYoD61G7e1IK9Ss
nILa5aAWjrGNHERATQhImbzGWcb15QxhhOWaA2Ke+8MDt3TJD4CiekJc2+AOWCHNrQC+kt/maOpC
8LxlXxVQQH44E6jbYEBirkkXQsvFYFXhUx742oIk1mKA/O3KXI+uFbce03aQdKvVxPcN8guJlG/f
fotCNDe5f33QTiHqvykq9Ib7m0kF78tVvxLtVnYs8ebVindcBJRXfUwC1wkz/SRwGyEzd/MM5H4O
gAGQwZGBwHJRod2lYF8ng4iGMDZWl9+AUeqShVHD3NaZ3NJqPNh45tAZS9nDPBszfW4c1Mtgll/D
R+qf41XG8RrZGVLlTylkfUi9iEE3iUPBCw8SMF28yTBeJcuV/oxym17sfFH/hWxM5/HYhXqM+tFF
qg3b1qvSxsjoykELkmmKVhEmLH6Nyz+feQRohHPUyjKztUu8LDeUYt7ppMjigC279yktMuTrYJEs
eb7Et59AJWOnRWVSLvihOUbwiW0WC+AGKCdEj4Iq6N13nLf6w5mLf6YaWIySBsCiStdVaH9Uzp98
wAvr6Gys6Dma8xwOlyeMvpFYIP+peX/YzJSoL+JJ0hAi7mRiidj8c+fA39R/Q2QRQSR6v/wyBgjL
T2PTT4lMC8FXZkTGcp/wa9D1Azi0jmA/y/IwfN0ETF2K+HAhhLvFEjbtTj4qaOOTKU/Dm45xkOGE
w+waatiEUrazXzmJMTD7f4xxyPRAP3VRQ0lBefFJj+ehOYQfPlzVA+zsF/XA2+rUbtOi+bFlcXrC
Hz7gC284zImtZn8VKww2q4TBlTHm/6PD12SlkYc+K4qvwaiCnZzrVcbt7ye0jhymeAaUCvZzgdUS
UaCaE6FvMbPleUrbLwclXD4qrfxX14hcdIwzgAx89ekBWe4/BojseI9a9teMA6b7/SX1xfsJgcDV
U8iY9AmQAov6Ev0tXnu6Az44QdU84MDDOFOdHZ9qZachTCukt87lXZGBjxzWOxX91CIBvATZvqFl
vfYxGtpgNSFB/iqlpL+d3FYcVBmnnLAjlg5u5+4e+Lpoj5nC4BbTaXqAQYjfg9KShz6ZQpQA0cpT
JunvNL4oPAitp9C7JxNT4pSPFuzKo/UMzaiPnLTlntRAqeIOFjagLwhTeTMKnkh5S/ywNJUlC/BF
NX1wF4x+uMguuBYK9zkxllfeHE5SRUXAakNGHT93RI5rtECkRK5OXcUwREexADiuR4snz63yE21G
WnaoR/p1tdKBLTNTfdheZ+LwLBqCtGHhUpsxbhgw9qNloqhVKIpZ+1s0M4exiJSq+2E9CnqO/bD6
KMYRiE4Ti7+yOF0EewQSSzNO7WkW6NXL3XN53sroj9mdD7aZDS/fIFX4vx8nKlv4ockx1ycCO5pe
E5GFsB8Si1BQMSLlOE1oGHvm9vdrblra6+PmvgV2I2w7QaKybRm4DSg6kyjWY6gsNnbsyPDL4QmU
keACnOLv9b7+481Q/Qp9wpqRTUlNGkKqiohcMmR2iddNPPRh9B8j5zIEsfYR7ymm4MX6lmbDf2Jj
+QQ51U8UPOlI6TBeCRli0IqHUnWpen2YpG548YUKlnzTZn3dtEie/FBd3ZZGWQ1aY49ZOqQQfWZv
Etpgd4RKFpdhMTa6xJF7FpowyQ6xggzRHeDHN7X5Vdhu4SA1mc61lqh2frsnHArs0EIzFVpPoizv
oRMm2S8jgfQ18idEzZDKRR/GE1mi9FVE03jgFIqZI1r0Z/zc9HTVqmnHua+ttx3nH52IWZavkscR
DQj5ksulXIVrGy3gS/Xitms1fqzoe1HrwOLNi7OelypfvAW9HQVcGyXWY0/MWAoA1be2JyglQvpg
Z3tCi4KQoLcMnm9YrHQhQwf2hHW6n7DUpHf8wlpBHYMDmBAHRPpYoEUL5We2yiNnGtrC05yJfkk0
zvT/4aAr65jUVh8sEKNo+5HE6lF/Z5MfGLhfnPCcSXZSvcC5tAC/0plHg6uYPkLdQ8lDSjzjuFtV
QrvfPBe+TByX1fJeuLkbix2geRQ9JkrtqpS0rIIM2uZmUjz67beWItBVE/V9iYZQMId4PUxmqBhj
jqvrtuf2ma0N16V3SuT774Jdq5M0Zw29ql1LjalAG2npy4gJ+Nwser82R6ET+xv+ZpgQkRtMjmre
JdvHu6jY5IJYABRUkrDVYkVYWPRQWEl9SRYaOdUDQWP3j3pfHPuAm6c4SROS13rrjJTgjv+7Zfbg
pgDtFVe9EBqIoT6et7o7U7tSKNZcYOr7N95QEwtJamz7Dul3dcwH6Bk8xirF0UrE0qXP1iVi06Ub
fwnxccsQfgLuI9a5Gv2dGyaYCygzKwQHlzU5iAMwd3MSzoGhNE/QmgI3DaxwN760z7PKttrwTOyH
J85z+AK2s2lNw2FYdb2ZOO+qGs7ZYXKBWXpMd4kCLWCB9YOLjQCdGY4iRvfUZsyuHsPqai+TvBwD
D+2n8I0qjGjhdezTkNDefIWQXAMl21JPPhUL9njOpZPlTP7lwSwAwzOLPO3F/Q2124HS27AwqAvQ
fzf6Aq56zoMGGGh/9Dx5N4Rf2NIHe4cNbSYh6rKzHZ2Ykr6ttMmQMpFfo1qHr3B3q33wiPz4xV94
ooBSfJi5Q6mpiOqjZ8Jh8MSy3n64+bCKRyyBUkSKjOL1lhfWjMK7SmwmC3c9ue15R5PKjoKLTu3V
xIRwVBK+0kzrc1bO3Et47WpEo67xxL8ElXPVRW2mWefGB6IRK3crLp5rTweEKsy9t+YeXp2oA1tL
vo77QVkF8CvIkZ2JR+zpiKjvTLChCB38DmebeR6CZFaJuFKUu5aWdiEZKekjHRIT6189djNPkiCh
DCLuB6OFCiiSMqmQLHuDF8TuSwSW5KUth1HVhhEokZEhpsTi2PzS2rBLWZWJ1tr+ESxJNQPmn4vi
GlPo3U5JNI6zgQo70KjW9JQyJpEaGXJBWKJoUYwENMxAau1+gjcQgKa8vs+bWRFHeIOEKWpNETTi
nUXcV79FW0/bgmMPJV5S24RMxcxzdMQ1QCJZ8ru6LO06u5S9t2X7+G7zNICPjJBg1MrAEGqXiUdY
V82BCsP/vWRF1X9MbQE8SHjMkvXNlf9NLolqkVsXQb41hbaJuDOwEqWyfDBpAtt2FpR3EOavqiUg
/TzEsZAocB/HGbOOtVqQh18hOxmuW2tNMw3GLmtcPw3SGXHg/9BBfZ1DZ6cke8rDD1EYPfn7LmLX
n215aSvzY8Gswkry5g9e/ACJhG15iNWmP5mHw1EM1FIPs6LV+iSimHT590K7AEK4k54sZReIHdPm
o8PO2BhhT8VM5uCUvnBTA9uPjh9VYR6/CERXL/4THIqEWCdP8f9bG/AaEUzZ63g7ym85tJBVq4C1
fc9w0qn/sBmyq/rG74+bdST3xxGJE3NYgNaY2cujb+J6a2jIsZRuyvkN//vkhhZOoPMIdDficc9/
zS57NDyb9JQKbxkS39uC+PnL0n6bKHL4RFXi5OyYAuMMrVgA/Hi2jl0gv3j5DLZ9p0GUWnB/88EG
ZTsAVPgj3eNkXZQg74K2nEP1DHxQjWXuvkh3ofuSBKcoGyMnqTTF3jMs+gdswlzXxxsq74OKRLju
oknMA9GnpFyNkxR6+6fGXJYbs8PMh8Sk5kPwws5xSr0aSCsIaACUEUkyCnqQJei5Uze2gr31L+Yh
atk1dWVrmX2JHHusrskcvxAlvZ/zRolAWO2wYcV3EglisokRZIvEazte/LwP/YKHycQfAxKxpsbf
SF+WTL1wxoSa6YumKwTAfNb2JTxOU5LIwyEjeIbmqr5twwoS6GCvJllik5HIchQw2J6gzcbNFI8R
tpU5tg2Tpt56OWF3shsjsfOlFnT9CDTpy8owqIvMCU59G4DFONYm24i7f3SNJQHB2EpTdJIJFVcK
U0PlaOFRfD9iQ0rgEkcuhVf3cF5RgxsTjp7gALahwSytOQVEslpAUsv0yxZ+nUaw5Zh6GT7bBPi6
IXczGjE1HU/Z0KRneHeY2rkgtSPoOBK0Z5TgnIlHr97eoxJPoA168nPP1s8ugZ2rcyrKmaFcMr3k
DrFYc6tdcOMsxLngOy3Sh7emk+CWfvacDt3629AQZC93c6EFnDALZryD21lZkmE691ahfMdxy07J
F7PYB2LcbHW6DGRJ6v+IEbbaWNBnixVywwzqxFP9EfHkXftWOhaW/Ip+BKdIdaG93bkPt0w8NE8d
gOawiXvPPjlubnAUGs2C1hLp+eNnDCnJNbkE+IdeU3sTtCL3UP2oOOycVj9kzIq7BrusejziOkBq
YvpSKDHKuLFezQOC9NtmnEMIRNdyndfFZM67OYQKaxZoF/a6wKsl1FkpvzKbu78vdZWTrHVeOdo1
mjeVzp5TKLiHT3UIMkTyglbOvj/roLTMjk2ZUyDKnkx2bTs1OlZHXmzLfRDGMFl421wrDdPImX1k
lNbOnQ0CENJtvKiX0s9dcqBL07pq/rcF7PbfOLvUDbCN7hUbWr/L/VbTB/EqySDM9fC/UUf26zOl
QCU5k+AAJAGjJVqcYAklFUiaZfdgXKgV8CT3lfxXvVisqeKsLQ9V6MMGSshO9eSKa8djFkya7+qs
kzN+vxsH8fBswKseuzM7MQKNK83RQZsooDbceLT8rPU8LDutdPE5JVCjtcmz7HXnvwUdNzWX5Yr/
43GcKKkbzCOH139YGPgpGFUOyz2VkZtjYVE8irt4bt+Sic3eAOyqGrf/kyw5UPN0C0jGSarswgsL
bjNMV0kqhLr1ple2jvag9pj97nZoh5VStDeq3Bds2MJ8+dAWIEsqZ5r9sCRGWY24PuYv6l8WNCJA
NYVGwTXoz8cdbeh9m9EslwgOiV45omsE5z9i1bRXhpW0dsTVc9rJQQA+F9Bx0sbTc3d+MDduNoxO
h3wQP4wsE/V4yLv6Ig1T6LHEOsmAGyBbdw7uc4aYwRJeHUij8Fy/J9MYNMdHklMngPJxRHbqUJ5s
NOoK3c99Bveb2DnoHD9QIx42GBg3i+zYYMfO1WJ7MidRMsTK41U6FFavTYah9s6HQ8PXT9oAgHbV
2cMh/naPp5xRo1QSHZn/TkdBU46ts5ac3uVtUboN8btU9fQm3YQU7YYpQGGqLSxPGbjPjYd7/E34
P6uB3fKoVb7NcE1uRGBOtN6YZegRYCqtUHcy7AUYa76tTCaHT65g+UfSMxp5Wtr+ViCEfSbSD68K
HyYGNnSaTxeT6ahH+MKAXEWLrRndUb4m+oFlzPikJTy6E/3prFHsTrHioA6guxCveRabXQcwsBDp
aoVDgXCMO0AHBIuBtx1jrSIl5ANCyES9pP4w+NFF/wcVpWu7gz1HTUoeC7pE6hVPT/grbOk452fS
7/ZEKfpvctKvPG3Ez0sc4Ka2zBbnYkorDO/ptWgxxaOJJTbBb9+7I1RvDrn2188KS2Bs746lUbeu
Cz2FxmXcGdnUlDmzzZkw3bMgmm45roaA3HijJuMb/qL2bBMCNoRWr1z/RZWQXhsds2TzakGeQ826
+tiHbIW0g7uEHF8O/r9reLRRqjSBDqXYctlCVD2Jd38OAB3BKU82u+uzeNMtHIZKHCtQMpJD8naE
H5ROcDiD9/0gLGERZlm7TDAxZSnX1YmXLdCgsRnQhF5DGA931mMOvNAxF65rgDzqyM7Yb94qyZCC
m5VrqXLdW+aFpYoBNk7Mg/ZkvPaLkv4iKg0LWRl57pah8y7bHH8PzB8puMqioUUitdwGfRPeGJO5
RW/RO4FMSJ3weM7osRRKL9E4Nw4ke4/P/42Q9D3rrs74KVtw20Xr44PecvxgBC7eI57mRGaaM/8j
04bLZ3Ff7mf5GLdaQcOMsFuPHumXOg33hYd7Xu08/JeTKCZTix9UH0CUOGUWj4vDelQql1EG/XT+
qJ17Af8MMxZ9KwUG3neeeJLH+zm5tHFX+k5O4vk0NjNueI2YaP6opfOwinBpbbbU2LDJkKzDypNR
i/sgg3fT6Cl+7BgU2ZHmXc+dmm0OjtOVOAgQmIT/w/TXnXOpIakvSDkUdIvOE4vDgFxlWyoVzPr/
2o+/Ie6FBF27ttn1wJOAQ1kLp3k1RJDD4CTfEEhD/V3txWWSSLdo4LJ6CvUYtVoZ8EgTopvLYOM0
n/viOnRhljYwRx8HOmFzP5ytKmirE6IShimQYx5lTZNw0INWr6IwqqDL1F2uCL9pQdI6gFrL7lVo
D4wzJS9Xrub5Ya8ZAPLXqNYaapM3V5HE3YVX3b50qQIvDiSespi9ufYyRVcJCAuBCfBl9a4FOSGU
Y3DUNWXGlGTY+UdFTUAJWoAg99Rm3O9URHHXI/UsdNV3rvd/qYmydReLA/fn93lIInIv2vlcDg67
qmdhxxmCLC68nkieNnq1iJAuXydy2SP0VYH8aAfS9CnqUCgvkdtfWnV8cQbMz1EePQ6tmelVqjs8
jCq4BxjwFdaXdtseS+l2FXcU/jMk2hJbSEte2oGEIDOGbmv2zxWTz/m9GORST4Od2hCmg+Q2y9/B
7DyDdhMnpigxzbD6CazJzj90XPo1laRhiLBZpcaRZdxBmh3/NvIxWbXrzqG1U+guO1u24fjpC7zc
0vjSnEEeLoL4pwLa8PGWzpDECYSwAyESoFmOIOJZ0YrY93RY6bzHQc9Qzawq2yRxyvXkRVPKRwJc
RZrauW2dBbmoAu0r2h8Nby2FK1PyKJAURot9nmN0LmNV8Aiwog6tGrig+v7Kwtz3w/XWEMbQI9pI
v7coTDImC8BWAIDhTuvPOYMwoARaYsv4rGbyQRioa/Nkou+CzVhl1CUSULsP/PVrq3yJKGz6Bg3j
vfq/VTHPJc5dDTqEi5gDqCtd08XwqE/ZiLmLo33gWRoJGkwdIy/AfQTjyEd3sDtvwPo6NIh65Cec
bjhJsrVBbNamNioG13tgiIKOAEC7eL/4dUpElzcruRs149oBGUvVjlHOarFbxvdHZFlw7skD+PQL
F0gIMff03nI9GF4JJUd8er8rSofhHxiFk7P9jPH26vcyhl2s9sxcQNxMa2yin9hzXo3D2J9ib+1V
yFhUj1MAOQqBrNcFMNAZHyCDN+MAgLPqiu1BlhdpxarC9KK17TvvT9fMkrqp1eeRdKqs5XyJu08K
SOA0M4lnjH2Pzx0Z42hFVIqRgUalmYRZLX3TIuQHS81/K23HJiSlvIybYqc9/0RRxntcaLRnILdq
tyDy5f2WovtWf0Pa5rQDgODh81m2ivOhPX9q2XZRa29rvYjSTKtUqtkPxgvHTctCXDsJhgaS9NDA
VTE6OdmZTNoaPM01eZRvUrBacG9uvC3Od/UYAw3jkieoQuhOwelkmnxTaWD153aRrl2C1IlQUy3j
04CuWVB/MjrxIQpW7vau3W20wzPiCsPLTud1vgJs7mPPyxfOZjRY4IjqhEUVyz34KSH7UWMZVy6t
o7/QPHIHePc+dvIVuPK8VH3EFnRrXyStjAQJw9poiwhsvDCRzI4Nt+RaohixrJC/Uy2QyCp2qDMK
gxFD7Zvcl4X8t5NBmcA6uI4AKzx1AV5tmbIDb2dXVCH7umlu2BlJIuFjtEalhwiexxZr0/mFQVzm
Vu7uAN0H679AxCdWq1w3pR7u8q+zCbxWm2wOQ+CJA71xUrsDG4j79VjPm4povT2pykdXk17lzV9O
iATRvh8sPY84FeEKKifZErZYMlvZXUgGRRbcmHlrwnOC6RI7D2tiX0crVZuvJyJrqR9gSma8ZIT2
u/UlvvqXK9tDcv6kPIXhoKZ/aETYwCdWudQ3C/OLDvvjhe3WHUUquOAveD+p8ywpOYz2WdStwh6e
FU4kQATukKla6ydnerPLPZ1Rb0XHSXSEVIInvwtPwGAB0c1tgtXGb9t5OGqh8iL+HUH3wuFIPwtF
qP0A+39bCHGUQbGa2o4nM4RRi/KzlJDuYzXFW+wExAdbLfkccCOe1gv9gVCb+lbHVGDTYgWxa4Du
auX5C0xrONfh0z1+imFvNF5HwuGfAgBK/sGQtEF8kkAyJM38jSu2mnPzfNRxaQ6/69pOBDlMEB4d
6e8J/RqDTnjF+MjzGBIgW5h5jhupq3O1J7xAmVkvBhHl+DByA7vSYQL5cmQZ9Bf2HacPqLvY3jP3
+96vlWNH/GKIUPp1hlZi0KHy1JWMass4+sN2sRViRjWW1N5RsNs3k5EnISVIZL/gB6vpu9QpqXVm
jXGoWhsqR2j5eHW66m0JfvwR4btnNdrmK3Gz9SDdnhujfwHFNoqg591SGFIi06t5SPhmO0MG4orn
QahUny/wpy/46+/8ZhwvYD5ulR5JeYZi0pU4GYh0fXWyzH+38exIHt26yM0iv3NbmuyTVJYRBF2L
sJZKC+gjGlRwK9N9I+T3dNe8i8kPz5jqW4IZYE204XVR0S53QDXnnOOYHB4Pyh+r4UYcHT4T7+VJ
hlL53k8QkqYIFVdHnp0fCzUk0bEbIqtzhwsZBwrmgr7xrq10305xRuckg+tpWBUS4KVLPboLajP6
l8iW+8IU09GZdQzcR7T6cNfJPozwqS+YmkXrNc7rO9yjsIM0iaHWuISQyZdDoLICkZV3EuMaY7MN
oQEM4eTK+ubF+a7iKQraY1dEORNHXE99wCjQGzgSy0cjK0XVmURttcwXbWP16eCz/41dz1SyYg/0
K61hWKwyyhYOBOW93BWq1J8baSgvkU2eh3SZ1fb/yqjyaWYO0Bnr8E2EmSChvRRioTXaJPEAaSNb
AyR2UJZI18awj5KK9c4b3gEKxt61OG0UL6fJ9xeatp+6rkO7/48TRSlvBdeTBaiwUg3OXbucdh3B
kNOUi3vCWY8Ur8L1Ed9U/X4LtAG5ifyM+2EQ70y2rl9sM3w92CVc73wy//pwSfyTKW/7qFIsEDIN
YtYOXMjuyvDyKS1wOqrtfXOn+m1tJ8vLsZI8fJMPjr2amtvcm6EevPhu8Gn0zIO5KYRLrM1tpVf6
UCqimrSUoQaa/L/ctkzuveqaaaqtVOKpVWQoa2e6LWPQ2RdX7LlxaK5G4l/wcFasFLoGIonagGUQ
gfRy4nQWBLgozEqZsY7p154u303G4u2lnYxyvGcYxGEZ/TcnB/vFWXdjU3b4pb4nswAUsP0t+PeH
scdbCsSA/Y+9OVyAfYeQd1g/79kncX2+MYx8l/QFsaQ1y1+ffkTjNthMpo9gXs1WHbmye8Zr8IOQ
E1wp8hH8rqRHTR6JL1akVeeSL3BIo/BRpYZe9TJxEBuu8YpT5gEX6F1NotLyFp8tz7fBUSWQpwyz
TxdXpQEu+59+0IvwP+5Z9tp6EDSek2wrJ9rGCY2ZYkkuZttyYztaWpwXQY09PvzJGmzv7eXeuciL
40fjcHVlUsJigO/Dzc0JLhJbpTt1MWwxprTayTIdObhmOIkJUFtKFCJcokJysEkDrAAcb94vt/GH
Q3q8+VtVqpoQiyGeWhcNp9+va86mivwObnCBtfl0u2RYOZM+ECqUYxga/vrmVYzaIQiKU3mpRhre
KflntrSyZjiRM++b1HS64fL/8g10Lj+ISqFKsJxLq4au1OprBqIuNmVjBD0vexkECAolcpW0EZ8I
4gUlnPpOdKhqnT1ybhsP/F9yvyRw8M2m7R27qg0AQq5SgRurY+IShfaxaSrzwaMhOVeVxF5rOH0G
aWEQXnvxVu/JLwjbHKOERPca7dz9AUMJomacy3M7wrL2SPIktVl1Wx25LM1XNV3Jx2IC+UB3byJT
BI71cNNpRyHqIEWvDEx42+j7xwvnGva1QJrsW+m5K2jYvRvxstauxO01HPfrKDGmyPVqel/Zm+mf
o6qtElTOZ5z6Dj+aGjya2RvwOtkUjDbsFnkt8c4tbW220SLNVk8jcbsJQfeBum0xXmIKwgS2cwYr
nnRerUp95V8ovvmpDbRqJZ0f52V6E+FsJhf8X5vUw+HFhgqm1okxSh6ylHISeNN8JEPrsAlQgO+q
6YyJmdVf9CfoSA1Frmpb5mcceXU/Yeq8E34BgAmwO4EWK1b9QK6I4N7yTLdk7B/Bq3HN50RhgET0
sHN46n0P9uCMG9jIjOOIEnICHOxBVsrhktNZYbc6z45ugiJsuHIVH/VwInltQmT0LocDld0/zLIS
7P7TrS39D1sEYqTHYZT7oQtXK8SrOHex7gPH2zp+7RNkLW4dKIZy7T7phrRBGeaL15C207IRPB5W
yWGcqm+SEUKRCVkRsBPySBFqd0Inca0U9CcIAgpBqcHkOSJG7YrmxWvkLzevAwe8cYBTmiQCOe6C
/XtultQr/cibMCPsw9t2ssS18pVRoggQtUa4XLTQ+LZIdlLoVKHXlMKzzxaI+LbU8FFv7UrQnHm+
CjBsGt1LNkkFCH5qhSEvoqqNl9m0o04RQGweXcXNSpTrRCqkTDuznJQCTjK46u52zZBmtrzmTfrr
FbraJok7Y8Mz9eCqgDvWbFDGofbq1t5AgJSqjpESHloVEK+8fjFPGUUrn6vB/ADrrC7pSQdh5vWd
ZikgdjnVVTc1EqyY7Sjzn8lAMZEP9hbtJq0IIHb/5d2WQ64cwLCZmI1QEWmVQEac0VmzEZ0/dVpq
vPzErlEpAdDy7rWZyo3TAcIC/Hc5n9v13/3AojT2UErZ/wct/PF+KZl/PDPovjCWtlrWul/Qm2Uz
1fWn5ASyOK+o0VHTOPWO75dcYuLivUCQGuynnm03JW4jXJty4IsV4Sy/x7ZNZkS7QknS/JbIao+y
7S3MTxAsHeQWRy7AUsTgyOIBFxXi/N9Nwgkgcf0LGh9arCTwN7tAAy+kY329Vh9OjfcX2KDr7a37
GbtiazRsRJu4mgRQq0jhv9LUcHOdkqn9xJB/vck+jvETVSAkCrE6yutlaIT51zowvVvMMMZhWn3+
iQa78gCOHxzW6lzUSFBcgFXRoCA/uAwQGrFbRqaUCiifpKZ4W8d9fjvQG16sVnU203H+NrA7TfUi
0kNchwE6/rtw9EyQB8eUI94YmyW/cSglfKEikYWjKWI94n/CdoK4JqPtUeoS1AlqbkVh7x3Adjq+
KFNw3LvmvIoYPA9aUGSZ4oalJVoZVbV+2NULViOCSUKKchroOH+gaywsMG8kRTkbHGlastIm0u/Z
WWsiB4tJ8jM+AQCb5lFAcmHyZzEeFO0xAkM2V5NhozA779LyKdIn6sH37d6kuiPK+Kk3nVRuDrfP
vz5NY/46pHCowxQARNA2O7UPpg+iE4JeUaT/gCJP3JoqqNwFygZZFYCLxug0WI7ps3jlSCkLIzj6
s0hj1GPM70L3chinJGCpq1qRbQjZt8lK81470eJYKJ7rcC2o67gQ40lDYkXu3YKja9Mtn2jDeSPR
m3CimWGJOPcfU5Q7MJRL1JJKeaacRZuAav3SKwBJ6elWNF3rCaNKIfqVOMJycj1OULee2tX7VMZ4
WjKYAYVvIC2mvSSB3CO3HrfybBJKhP2OFAW5aHaIAxd5JViyl35pS3Jq26gWil/CPlGBp+GDaM/Y
84uZZRL+89Dbw1497vpYMQp4gFqUBg5JyksTr1qe7n5RR8IupRzDq5GiyUvWCzx/EfzqYLsN+TJ3
cWDYkauHAUnttVtZLyt0WMzO73z8R0YeNU2LMbvm6jTGIy/YMfidV8kMt16HpkSq3wbM8wKw1HoY
Tzo0UlLxyDBtQM4TbhI5vnphKY9rpxIN3/5kX9CUjB/6TpZrNkBhn2rjtmO6KnUofmS3VfM9pRFh
lrihFnH6y1m5pA5sx0Ua+kJ+3D49Eh8m0zc29zU9ER9PDzpg0HSjKqGxJE5ctL3V7tREWm0GnWdp
Knd5xtvmFU8nC91+KSOb93t5niiUx9lNyCnrqPVQaag+QYDMwmFoqr74PZ285Ay6ymmph0BHaeXh
5FkucqPAUMkYwigDElMuWgh4cATjA0bdpMFE8MQRmcmGK3TbpjAMUTdCRzq/KP85Hd+YprY0ycuv
xOuhd+IUKf+yFgVbXe11nvIGitr4yJVxagazrpDwFLnx2hfOwwBAcqS9JqVUO7qIXhfnPv3Te2Ud
dBICGOqsW7gh5qcT/baHx7IVgRC/nU6RBLBjTKmPicu6l0HLVVXFhAgUuIuuAKWcbg4LDWqxacxJ
z++D9r4dTbj9EnUtC5PhnfguaNoeKjIaIGwN02GbgRx4Ar6TEE/Al13mJ98EKZKaRa6MffHezBst
urlHxLUgEaRiti5wWL5TE/6Il7rV2BYLLQPcNDwCHZcX51QxqG+u6xjCTx4JpnoVP7bYD2Aulj+5
CTi2VLfd5YLHqfs0C3n0O/b0Mh27bSa5ZE2iRNa+I5EKh8DxYTz+3FCWDfEXMloH2CWoBvmHXrW/
3KzXfStEhXskxrH4+4roy+SDlCm5uYYAn+/4lWj+jXAXQuyxEehDPi0HyYe21lTjNLW4nLr3abWp
0Cg5UCB43/Ky80eddpNJdu9hE++k1Q9qdBsDCZqP3hLvQJD4+P9rlte2GRWaLshwWrm3nHtdl4Qf
9BKFzXmpn4e+VBmQQ7IwxtodLO4KxejtV0xGAvNXb+vDojYIWTcEmjqWujcdkwCdPPlOGDWH5Yaw
0+Ww8zFblLe51JPwOSZ3AuTtgAbVMV9qMMnjVmgH4/p1FfYRRubfQ6tf/9jzvMa4D5rsIFSDJGPp
9tPVs4NdCmM3UaEShKIk28wFW6SPLcT0jeF3zJYMhKsROHwlP/Mp63VyicSqkl+1wwO/oi7XzM0S
bHAagTTtQOnl5Vk7f49gbua3O3r1iqPgRG16hj7jm8D6a3EqptUWTSyKDJqVf7nsae+SIhpY3m7o
Gw1hxVx46ZpWD5RNRSrLiaVv/BZIxj7cd7WYGJ6R6qDy63+os4iMKcAdZLXDc5M2rnGmhx37PAou
jpCmHAbty0ZZpOa2QEFODwgx5AotJFqLlMjXD6J0z3VF6JZVijFIdY46b7jWRSerOYht/6hDHxTS
3FXPHt2BEjJLl8Wf8/RblmALJrRn8W8/CkhLSAH+f1CgMTZAtJ0S/SdhZsL1F9ZRyeZw6r8L1Uu2
kd8uT9osTn8GHzJUJXmVy37NPne5jEFfKuSHz3OWhCGATt48FwTx4rUebJloTCimXwmTkeUdLFCv
WsHOvc5Za4D+ccwEdxi5twjGODXAnBDWaxHrmF5I5t40++LCAqjmcGgIUDV91qJXzvZrv2unQjAr
rFNUX8f8V1GHTSGOFLqHLsz45BTOlmHPMNclxaeyPX7ImGg56Meho+kkKC5OuvQQIHEge6VNvZFh
tCzEqHQ8mXSfBr8m4ziCkFOO/6ok7RwzOPxldlxc3YBlCar/kv62Xl8xAJ9OiseT68F5wIqzWhCY
iPNfs4l+h8m83gMMh1ljctRzWeg27p+KcNRVoDLrIEFV3cd6aMgiHds9+7WwcdbJM0DikcjgVrft
le7DC1H4MwHOLOnQNwaMJlkVdqyY/BTkvLcwRFwWaB3PiKIQJx06ugxr3wDHzQgMb4AEO2BbwKO3
oB7BLxEfu5fOt+1WgSCtx75gH3AbzzyzmQQ35DEKWxhTkXQNV7bzEGrfTplGK7teo18fHPUeDYlO
fTMie+mmHTJzdJ00ZiWiJV16/a4IbgqQb0kqZJCQiqbfZgvDiTcgy+ebQ3Hl3pPhGAkeP7cya2KH
FOuMxyf9JJRGbHWI8mV5E+CgI2LjJAamKc7DqoZG7eKn3LYkXBZp6/4zQ7Ajr5myfAn1p/J4+GBU
MLLLxyaHcpdCqpuC5BjFocIXWqfkO6G3BH7parB/oZyRvQft/6/tykHcbaTyf73zhp0a7Y71hEgB
7PCG9fj4Qwa+h/FTjMywLl2a4R5STbpNLfvjqDfnXTXIikjqtnr15EkdEhVQ/fRrSHmXgMeeA6Cn
wxcxxIv2tNVIBYNG6QlKUBssGFXvdJpJJHCyMNJzSidxrtJKT3lZRIUN6xy1IYuEptxPSFNVdxXv
UqVYMaJfpmMUrfGw56r+9cGjVaPewEItiS/q6JOMNuNSsff8zTdbFUcujBZAhWlr1kNOeyyP9HLu
KRxqnso3qIhyhcw1+D1vWAV5O/a3lDtZE+ApW57y+DNmGWvObteO+KTVkUE6lYekqRTpoOFlX7zw
x0Sk/aAT/ijKMNAXsT5G0uJe7Y6uhtfccPg1do6Kd4VojKUtj+vil0jITFEM2oXxNMNAlLma0h7e
+WR8NZpmtFBDQP2plrdwOTiMYFo/yxHFGVtWkAZAdzBwIHa7kw1d669AM2Hpp4xTJrVqpjTx7DqC
jRAZBRPcBEaegE4CBYb9qLZ/08BTC9XHnFUV7Up7l/fp2adzoVzuEzMUUKsNZdJLuzr7EbGw8z6I
HU4Fjr/PDAK3LF0QMuZhX2VsADxFYkZSFoHwOoJ9OS6azVPTvWUffdGGMFZnl7157UfgFswUHiVY
8Y+AocbeMpOuGhMCSr9Cmz99KSxoEEQgVAjcQdhHvTg/fzdoTjv3p1g4yA+y3qt7TRI+dtAB8q7G
b3TFjpINiVatd0+rkQ9/PZjuhsarELzBRarf0c+AdnWRvmndiPBk4n/8uly/my2S5dlFkelwlrk3
dCjGOmt2dka010mHwk2a1m0dc6+SwmsgqnxNWkFctP1c+xMTxEele+R2BQNQAXwdrCDu0xxf8Vcu
X4j9zQ5WQeyIuWN9hS3oQ58r6C38m39HvLpwW8j0DaMd9euBtSXk06klM+V3pytn/0hEfiXWjRTQ
jmwuL4QurCYlfWzFMX+pzeDdje556JO2TX9AZ9Wz6dZRzTjAWx/eADUuG22TWq0JERZwjLOl/fY9
vrvEXRaI2lmUT76/qC+4UF5HC6PCva4xf9zXgFMsZDj/6V+rR5YQRrKBG8myb+d4XkswZDrAEFXb
OdiOrT1WEhWb+BjqumxWcVSrt3zWpeX40BjYFBzpbfgm4PXLY30gJzlrxcdBDh4o5wgd+EqHGopc
0JeK4QczUhAjcPJxvj54XP68Jdpr+a5Xxa7cYXT1qDfZSYIMb9EZg+KjCu3hsGsbj/vJC6A0ogzC
6DCOHFp9jb74vCBdROE8dIlSDKh8nIu3lPifzMptiohw+u6krnTpdJnPq0w4GV8QqP+p9jvPR7CR
mNiApLwQZIglaC8qC18jEJvPo//ovfI6QDYOJNAPQxH09M67hVO1OySHC3kUwl82MeiuR9ftGzAp
+lzAcwMsrSHs/wdj7QIo/61FVA90UHyrsUxOUEVlS6mtm3Rc5xaD9GwfD+lf7s2SveQ5hRmfYNn2
ZopJgrkEXFinv2Rqf7D+ISSr1YxKLieDQXIq4B9lmFsrsPmgMKs1n41tYHqI62+JDKxkqOI9qV+J
bO9jdFIC3EF8o/GFpYwqiqTm4MeDKigu7dDPRbNcDAvsfT91pdOjdc8EvvSSCyz+tbYzfVrF7RIb
xhvzSHusM/TrXgR3LVUwFM8hpZYTFna/I8dEo2miImHi08LWKzAxF63Z03QB2+eklM9fvHfhfZBH
MkhHjOiBWPV6janiIeqKBYAHWqd4/74Jar4kEA4vdASVGIRvDMfpBJg0txqdFJXC65uI5Gv2io3R
qjVbBIbx1Jjq0+QWgRNb7450J91Es/PJ2f+mYFRxhUcEsLQJnZs3ngo0ILV8oE4hlPpzM3mQTtZE
ERBoqgqb/f58wHeOdyMlvZPOztJiqVYDl2nxg9Cejv+auPj3vrIL/DDV5sFOce+Nj2NfNIf4+5RY
W09dtaNc3+AwFqNeJRQvCpNt56YyT02Kj1QKLtRuPVXP/2/Glio9h10YCrf2GXmrrB1O5Ava1QnP
rRSZEnyqSgl45M/qKhx70TL7oSvcRAfRsxM6EgBJ1qyn2mUVDD1QOaxd1NxdY5GCoHc33e7L7J81
o4ZMEBeJ2To8jLfQkoQqPFwT15zXygXzwwnyjs7MeK576tcR0Y7mqSx3PNJiYs+EBLWfvu+ZK5Xy
v4B7c0fXkdG0Si+wgf/ytOudEd4uXIIzL2z7LhJRlYjsTAlY9yyX9A8d4a+xAlkSHXcqeKb59Mpb
dxSM9rU5rhUTcGWtXelKXoqeye19QPxYcJTt5ew0/PqP4xuh9TFRzfFwlwI2xbv8vWrBpaIW+nVn
2PV/I3O29YMsgfMiGikcbwzvQko4F9MNT5SyGOgbxGWBU9c/Tuc5ph0UlKSuR1PYv00vGgcf6yEB
xuicOhcpCWJUs1p7jtBpRPsiKal3/e0V+e7B1CfwNnrUVewchpEuPBLP32lcOEklLtDztuY8wusd
EIW2aOIoSvhit6qqUoHqE9mwSF/xjyROUM3/2hm6jCSKvLQUYb3Z96bpTAY0149CwenRn9mnVcom
nkC26KHybyngJaZiuCLyhyVqREEaqBLy+dw29DgmwwDMlPnL+52vbK9j0YsfiH2RlLjjzkY7E0mP
i5XaNgezFiAlOPjTTq6gLDqrIaijj876anNg2wagDnPK1zUluFS0g+f8Bcnel9mBa75VLHmAVro6
C7VZGFTAZj9zHBaOuCMvyiuOuFCLRI4kQ8c+hN3citLRUa3GUfRG2n8rNkqoYbLpXj5hinooK7Ff
kQ/Si4jms+dPidWiHfSqQC0npUBNpjglFWDFFHqI11+cCH0/CAdrmxkHzqnmn6MdaIuCsRRrAf6q
eHpUq8kPpjKfqvqYIFKbRelaYPySElLg7ceWXcuEn2dl6ye23ni09umYkTs52Ek3BVPspuSEa1oj
FSYJtiGcQJu9DQaDpI1VkMqRUqS9U+o0rhOFOTSY/kR6PvT4S/Vdt2L59GKu35NnrKwcLVhSHBqw
tnw1FMp+YQhYhUjduGb+kPHQOHlDwthz9vR/Qlqmx7YdLCpDi6DoGzrzP0CNLbSe3LNcaxA97Thm
jB7hduq0g+6uRVVm+Z2y2Z/xvYeAw0JNuSE7aLgYt1MzyWP7L5tJS/WcHhIerpx43vq0Xwt4CHmd
Kh88orzm8KdmIPkWheib+0bJl1ADSL+Vvzk5UT+tQR8b2sBK//IKzvD7ckqpL9LbALMuVK89MQP8
AFNg6WvFWKfhXyslIqMjC/TsG9GVeCdThASlNxiI7tGlSipzhUJdM+WeSxwvXy4GnQPbaHs2yKJ/
3q5K34fztKWpmXNzGe7gJjhfVCcFJsjZQ7T6qnfovZvmkdYx+NfN14Y7YMj4ikjPzVhM8hUYD0L9
nuPhKvQPlDxGeq5O/YlkSWlVIIp5/o6vTjZzEz8vjKDo3nq9nvlloBqGtXNq5bFzSte2DyEsz5R2
gOx/WXWQOsBwU3onrHuhEQnVTahU70AgogHKO1/DfmvzQ9sq9mtu5n7fuR+iBmjpzRuVJzc6RD9K
TuOtrubTi9RIX4cCv+oUN+GqKvI0phs1Oj1G4jt1+IHqt0H4tx00fvJztw/ZTuHbbZGHMV1s1rXo
XCAkVafp1dVyQdKX1DpY7fvstMNjlilEnLm/6jUeysWYZC0WmdU+xabBgpLJ/uSIWYgGGRVAvLbS
UNDLRe9x9JM5Tr9glRQqguSlSjSmc1YS3zt4SyRB7BVE7U4xtEpRktCTv6s8sqAB9OB1aQFmAv3v
8JKjuqNM3Jn30K+2DSlyTg+SGLb0RILTaxxnFCfoTx5tqVQzrT00pXWf6FMU7MHm6Ku/C0CfMYmb
F5nLl4e9FKDYpQHZw0jLjTMnPDxMFteV4WvhOMD4cpr05fitluRRd4auJ54t5hfM4vIMWrvPwME5
AD/hCv7I5VnVe5aF6kWx4EbvQRrE6/rM30lfLky440C2UCZOmoeO8kZBc1WFrowKn4V+By5UohVq
cY6/qoaoc7Qg1isbxJe+9GigzEreb+hBob5p4JevLZeIN2l4FeG4I8bCPZgszbPm9KnvP2r4yCZw
xy+xatsWi/xwUEejnEU2R/cUPvq49nY/LDM/hD/5Jl4g4pxIo/R5dJj7Nqz+l1BOq0+1qw26MQ0K
pC7toFWHZyt7aiCcfATnpDLtFHnFfj6VCIo3dsY6Bo2pJQZadICAQ0xrcJDtJH2OdcCWsZoUu7xY
7P2OhiDRDb/eQLxtkVsM5/4QfMgpTb4c67YM461H8mr8mgZhblH5G0Oy2ifhAAJcXWrvWuZm6W8g
tu/AXE9Ms+Cb9wtU9iO+a2rhhE3TTwaRI2e0bsxqZUNgZjIoDloiDuvT+mOKX2WVomNVd1ACmYys
iki0oSdRc5IF4/p+3lghlgVOWcm/z1MHFR4B80tdm65ZsPuBx5i7sx2RWe6g1MLpVOG4/6gMeaOI
76Gakr++NIJ82rWZGhOBEGf4mxy0MbLLVihyQyjvneBXfGfHstQrgRaujx64rmY4AdoAZ0KAfVdq
V3+BfSdNa7c2xPR+3DhdzRrtWEBl+yewV5tVb3ISt6Gg1tWSpuG+70xEOmSMN0PjAkPq70zrJm07
iF9ivJc94YLbp1/qjfttoHpo+jNQZQTP4/+1wBPMsGjnUQFDTw5cH6Tqb65225nVUYh7TIeYQb3u
lKU7tk4He5bzOs3SgpER8psked5Y3jIhtzXd6OGDm1bzvZE036ptULqT3qt2HywP7fO0k7ym2gAv
D4qMlwnmyhgK38mbuAZ56TQ1u1XWjCASF74EIUSpGoaHR7InGZtxDuFlZf/y18wnR6R2kdKI0735
oGpsfJqWG4JGpH4VQg0ntxOI9KXqtO+mfSsT7obrWdmE1lL1DivrhJS+3Tvd6BQJI47IfWJIcOXs
o7BbBik+QLdVWPBoXA8lmwjVlZIY43YuOY2+mCqB8uY5VP6Hjve6y3iIc1gXObOBTxIdGl5LR+bv
LzFs2PghEJ/2zUDW9g20vISxgBycSlwENX4mzoUHssdnvArEbm99xz2v5w5b6SX2hMo5ZIM64UNB
8L9t7mO9SB3mWNvtuZz31FdiCfvtk6Y0jaGHD/YWmyLDlz8+D9MJmxVPfiqk3CpgpWeutOJX8AD2
DQ/UVXpzOcZLqhlFI6lBDZjlRyM6Ndmvy5B32rVY7HC3Ux70evuFAarkceWfKkkkUK0l0bLyTkOh
CjX3csFhMOa+CMSbzjpwvWvYlvYdvvdiWmzme5pmM65w7cPy1xCoBQP7K3jtR7oMZ+Y9YwD0+aF/
bgXq5/fjuU7mveCRc6uWcDhKnYriX7JpNTucmK1yse6wI3KSaDJ1gOWUEL5bv/uZRFYjYqDEmn2y
POnvp6OzQObwVFq+kT+JidfziJLy9aNiaZXddXiNH2PKjejZ91TnKIyXL0vXZBlEGCyUAe5c6BCM
fAoWTjwNOVmQKyatpL3/BZQZaDdDrLPAdOzpfsUjkN57d6jPGZ77Eq3tFH7gVV5V0NMYNlca1iTF
hhMrwFHfUg5zDzOgYBevj2mO3yqHJu05PoqP5af1GIbePdybCNpVGeCcxWZcz4hElVnyxZ2ONK3a
TdrcwxwW9ccRG/jAQ594uosZ1KuNZUtQtlxZf8r7H9guKsM2xHNJDVcnLAyVN3n4adqZxcwM5m1f
a+VtVJ1duBzTc6bD0gc+WaNPWm5N4Bblnb1J4odqTTLfJvEP8gOrhIubXoM9YFp8WMCh+Sv7KNW/
Zz+waixbx5g8IejarSo6M53eVNbOYpuyzhV3CGEuXABGNXLcmjSyo/jy0UG9U+N4FnQKz29CDj3N
5wblyBCyEd6YBP1tiuBErQVInIKrRV3ken5YZYvvYc1d9r6paasc7YxY7ZcuFbLag82hXTgspalq
jINKP9Gjxrvqa8drZ0uPekFVDTbPCZiPjmvXeZ+feWPJp8QsbJcvr66emHOuhQJT9LXrzaUDeotL
x5FuYxq6/lPXPl0HlaCA1ufsh+vH+OCho9SfKaXC4I1rt3p0Sh22QZkvZg3sYvFzNGAdVjZBmtqz
h4N9K4NsYBXsu7LQMup0/juKKpFTmTpVdZ6wYLgGvZYgTeLTQY6mLIFhGXnxEScR6W8pL+kFZ2yR
lTi+R+thJDHZuTVXVQbvl/eXw/efORarESwGx/VMmdKl8jJ7GVqkdqudEqfIbh1piHid5MIPmMhK
mXi5sddqp4la98lpVnAdjsaAtIlh8hqLuQJA5zjmX/2rMizfc6SOGVSLlxstBvfIuX/atupJ0bCS
LWBd4wl8DJBCxgvqEWuevgc9XdWetKPXF8h29Fd7H0xzGlQVcvKEeo4o07fLFwtWNKTqguwu9P7w
lTMv9sa3j1ptuT128v3OPy03Cu17eEJTFGViSYiEmnZ0uQoL+KTsHzam3PpX2L0pZFxMwavRJeep
GgyWTltIcOGql7/3s+gV3/030ZrZqzhrm7BH7e0Ji29IpafZzf3YHHHbXyDRY6r+XpWNlBZ2jWiC
06ijnEOoWl4lXQ3JJi+V7Nc9U5wknsfmi0vFEbn3I8dAbp83PW2+I9nj6SXAMKYX/4fqGzzLhGDQ
6w1aSQsDuCFsh+tdb9hY7e5YUgPePgNLJZl94aj4Ex8coOHP1PBpMRdzlHsLMOkPBTdIK61maWAp
RCQ5zJcB288HRUre86de3a2W175f2xfxQy0UGWFhZ9tvl9rYJiWj9KBNhk4O06r6MuBtGBSWF56/
hrHgoMaQOX0yMv2QZozpADuUk7sPMRqhSxqwZWLCy1zu4VJSXPI4UudurU4Os63kXBr62QbFHgfm
DOjH5r2X4UOquTkUoWZsxec3oeT+tT/0zjcHdDu+Uen1wdrPKCV63MZ5F6w6HxGCANqPcLIXCYDz
Iiq6ifP8hlVRaL1IoRCvnCyuCwVkRAi2qzNwPS8lPsJQtZ3ScuJF/Vc1NSoWLVr0jYDtSCAUBTib
5AibQdUfJfkhohnR9/7icO5eZKZKpqQrxXS5n+NjeOfZoumBuTvvocjK5UTHTiGp2LmvornnZNE6
ACjO9lW6WHukV+iY/OGGuUOi1SeWvIXeMsyV15JDXSiHaI5TUmmS1xtoMJ1Jz//ARJbDgqEAGmyr
VuTOkUmLK8kYvJA0wgaJSiN6i3Xya5vNX6h9e5BBHyYGOdN8griOLX/wi18FCIDRPYXcXn7YFeI8
TJkcWgW/sF2XvINY370LmMlwyFgSXE6ElsCv83LVk7oRD9JEmOLmO7MV+zwKYDxbBP9nbcxJifN8
L3fwmy0W1QoJlO7NALsi36+JLwCS/28S19LOxzznRHU7s94d76E2hCVGC9JnjAzz3am5W89k71mW
ZOc0orFryk73y55vt9gyN0FkKGGnibHWxNFRA8eXKvXQsiEDahtcVs3JhXfiwutebeZSKmScxjWi
za2DcoQsv5KPtnsXYXQEFLuJFm02PMyc1fSmLKbz2UmarTe2iakb4sIi+ZPw/Z9lRB3t97Iv0zPt
JuXUnXFV69GoSL8BQ8zxa3n2FK6707k2sYbGUxYW1T8MP1YR5vFQ5EJbC50bIDdgVcAFWeHhnZfi
R92lW48+EoDg43HdW6R3e8CGcYXv2TQwKy+u6Zd5RlpJhQWAL7sYEXMgy+lG+nAivtpfYy4jtQcA
w2uRVfF29sUK3Fpr7TNtzZV9vu2fwuU8dOu9Nx5L6qkccxV2GdvFXcTTyuSEnFMz/+poDGSYS0id
ZES6BW0dVshuDkctI2wyIqoi2m/ufakgEQqB2g4sSTAPRiIyLU6lYHtHiQ0Q1mtzVVDHEVjTadzG
1ji9t7KTGKnfJavF+yqlvHjmyqvY0FsVSjtfBzcognzblfBLwcEOaEENf++BwCHd7cArjKZvL+qM
lwzHAPl+XCckeI6zm76TzWW5Vknti6YMEXBY+WnhZbhs5kq93/sL/CcBPXtWwaRCVMrMm77Rdg7d
MswXqqbCljKLgcgCXpWPPrlfY/ABzIZmksH7HqgwcVOZb6iYhkxLG/oYG45I+8GLRwIBYUYOL79H
dFAXGIidV02EXCNfryQ4bqULB67l0K+UTrSVuh2v2xou1JWrMepwX7BXPDvNySWvwzBy8zfCumE8
8r84HYHezFbD5bPxP0e3ugvlfS/uwyZNrZCG+xkHbLl7U4GYMgGguNOD8/0Knay/Kno3AW8Vzywz
NMG6o+bLNO/6T4kJl7y/08gquKkpRnBVvUtoXqH6dkAa3n/xaGBoRIIMH9QwpaZ7zv8bXkHkU9m+
jAIKXe5C8TMysjDr5hoZclmyJOMLFZO047B640LD3AnWc2MUf1WrTvNbPWTFjaXpTrUFfZdX61AM
qCxRgNiZ1xVBUbAVeL3C20PhSMyvKjr8mKYiQrdAJ8MjDSaB7oGhsF6xt5PtljiuNuYbMmQrK9lA
Hoy/Gf/cr+Bwfi8OIkF7/oGBI2zKyTOBVF+I0LeGaIjbQfS8WEtaFMidXi8LKfSnWqQHZmXf6ETw
ueZi2gKVMq80E7vfF5E36o/cOFgz3KOhXWwuVUqWKpIE8dnc9e9UCOg71Nre1NFQJzaFAHNRfsrM
5xbf1x4hdBAasDomktsgJOo1HZAi+U6uyDb+nNJQoP1j7hIEYG0Hpgtb56Y9vHpXDtV14Z3w7Ajt
5PuKpjc8aI7N680RaXGX2H73tCsXrEk8fG8FopxMZfWoom8wQOwt8n9XhlYqmCQT6jeyqZxyTh27
Jy+uiC5CLn0mcyAf9sEzmlOS8u3sJJhvR1yn5scY0R9aQMO9KZ0l6cf9JQkhot1TZfxHm/sRGHZ1
ZnVSwqTnl96MzkgMqUb5ELYIQ6SCeiWPhApGpJNOYhfDRTsBDv31r9sA67+PRoPmDcKr+05KSzk+
JCAHdkrjpfbUz2lkLvMmk/wG03+Yh0WLlBSpuUUoWx7Rcc13sNtR5HBEErVyUIg93cZme4xrfpE4
yjTpomWIGl0+ra7xYnzCWlnasc1lVK+gWoMlJfPiTAP1O73f1LzKhnmmUsCGuvO951T8U7/rLzAr
UERoZTabsqaEYeKqif+HlsAYAbSxMSQTO65+JupAWbujQByPuit4Aj3pctcAlaWLi1ASn4JfvMMc
M5m9y0bP4wpm9uBK/YFbIMjNXQjkQtqow0pr53q0I2USySX+lgeCVp8uyJA5TLLrbMVetU/qJeGu
+JaYGdtmcGQmOa4ApNQ9i7DyR/5JA/TjolyPlTad1T9oVvliypoinD4eiX/0M4gx4jHHMXmMHdz4
xiaqgWG65mBfo0FTtqbKT0gLh8Ow2uZYsNw+LOqUhqVDEiBmUv94J87O9O5NORzsVzLHz9v9ss+X
qqBxAvPUt43B4VCDD75r/4QmB+gKwzYHANqTuvdV4MUgazegCTTtvythiavLMzatt8eecGVRhDER
kQyKHvMrRGtJ9KHqXp/peTl1KoH27eONNASBqDaGLUZFuXc4KROoBKJthxuy17WYb6x6GQFYrctZ
IjrXtnUkTio+Tg6rOHfvUobO4bntd5iyfxo2SP0+t4/f1nTRL5p4QQAyLWiH56JkK23ULc+4atZa
7I5TnWx9yuCIYsSBnzTCBOKeplCrNqIcjZaQbu4vWZG24e167c2iMgwUJtjOv6OXRzx0VUjgTysf
/LQjSO7QCK0hor0b5tGB8ls+L9j3KbivlFZ6SBVWfmYJqxmIPTWZLt0bfdKeCcocA/Qna7sS1XL7
A+buv3Gqn6/GFUsYIhbgKMVzasLvQ3DeAEe9uUo15Swac4Km2pYdLo2AIrfKYu4FIQ6uBTeg2C7/
o6cLp0gBZGc+Wbll4pKjxD35r1jTM/tTDqA27+U5QwZGGz7matBCOgKkKdKG+ybYDzD0uUsnY60R
qN1l/VfsMnIJLMlnqERyIuLZ3iYp9wxn1rTW7Hwgj6mjazbHbzjS9MhJ0y4bzgyL7oyfJ+/d3hb6
Vd4NYFl8ZeQ5cTUTSWsdMLEMU048iquIoBS9IxvjFrWFmbvMfVo44K0FrtULZpB04kbOzcFrU6+d
N4JOpmRa5+dTBQgCrQXA86jI3AcTKTDtN8VRHS8eU1aoEVZHfBrcz6/VVdd8St2KNzR4vqXfxsiP
2gUgJ9Z74yEUIDg7Vd1K4Ty1lMoIlWWHsvhzV46D1tOAfYK5PuuWACLW3y1Zit7KExmbijnRFt3q
M05GlmTjMQV6hLUc7TF5+6NGUGvry/29D/A1ebXf2uhN6h3dpUZxRGAP9LoVUZpw1zlNsBF5h6tG
S4JrRESbxy5Kih9TcNzYvVnVw4QHg2jyKNqOGF2hwitZ6Mh8VJQGGNxwkfbkLgFb37ApDK7PzQVz
VXoSDXe0xka7je9JhIT4BNgsLRREjSrblD6/dWRKZ5fZjh+XKSXQbx/ZNTnAZHrRNaKEtJeaEGUV
l07NqKY3+TKMOBpV6+vVk700wM96JoFPYZSzXnm2s4lFAzc3oayVwjRHiMYAAMQK4y0+U7aAhPMs
qEmeYZ0uRvJeAkt9EwlD8fE0tKrtd8UUyuwpCum3HOomihgA4loAE/kxeTqQDPCNQLJIksKqrKbN
e8C50OKhyA0U6gGvAm0qVTmd547aLWz4n6GQXAGjotSv4R4l6weZO8kIQ2HLGa7BvFmmVlXmtTQe
H3e2SPY8+q/0RN1ifJNG93MFR+xqnrn/auW+YdwLkx/6zlA+WKqorSk=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
