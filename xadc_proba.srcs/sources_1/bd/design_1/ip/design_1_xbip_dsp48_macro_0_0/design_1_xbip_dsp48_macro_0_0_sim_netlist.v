// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Thu Jan  9 16:22:42 2020
// Host        : DESKTOP-8BPORRM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_xbip_dsp48_macro_0_0 -prefix
//               design_1_xbip_dsp48_macro_0_0_ design_1_xbip_dsp48_macro_0_1_sim_netlist.v
// Design      : design_1_xbip_dsp48_macro_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_xbip_dsp48_macro_0_1,xbip_dsp48_macro_v3_0_16,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_xbip_dsp48_macro_0_0
   (CLK,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF p_intf:pcout_intf:carrycascout_intf:carryout_intf:bcout_intf:acout_intf:concat_intf:d_intf:c_intf:b_intf:a_intf:bcin_intf:acin_intf:pcin_intf:carryin_intf:carrycascin_intf:sel_intf, ASSOCIATED_RESET SCLR:SCLRD:SCLRA:SCLRB:SCLRCONCAT:SCLRC:SCLRM:SCLRP:SCLRSEL, ASSOCIATED_CLKEN CE:CED:CED1:CED2:CED3:CEA:CEA1:CEA2:CEA3:CEA4:CEB:CEB1:CEB2:CEB3:CEB4:CECONCAT:CECONCAT3:CECONCAT4:CECONCAT5:CEC:CEC1:CEC2:CEC3:CEC4:CEC5:CEM:CEP:CESEL:CESEL1:CESEL2:CESEL3:CESEL4:CESEL5, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_sim_clk_gen_0_0_clk" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [15:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME p_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency width format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency fractwidth format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}} DATA_WIDTH 32}" *) output [31:0]P;

  wire [15:0]A;
  wire [15:0]B;
  wire CLK;
  wire [31:0]P;
  wire NLW_U0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_U0_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_U0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_U0_BCOUT_UNCONNECTED;
  wire [47:0]NLW_U0_PCOUT_UNCONNECTED;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_xbip_dsp48_macro_0_0_xbip_dsp48_macro_v3_0_16 U0
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_U0_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_U0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_U0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_U0_CARRYOUT_UNCONNECTED),
        .CE(1'b1),
        .CEA(1'b1),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEA3(1'b1),
        .CEA4(1'b1),
        .CEB(1'b1),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEB3(1'b1),
        .CEB4(1'b1),
        .CEC(1'b1),
        .CEC1(1'b1),
        .CEC2(1'b1),
        .CEC3(1'b1),
        .CEC4(1'b1),
        .CEC5(1'b1),
        .CECONCAT(1'b1),
        .CECONCAT3(1'b1),
        .CECONCAT4(1'b1),
        .CECONCAT5(1'b1),
        .CED(1'b1),
        .CED1(1'b1),
        .CED2(1'b1),
        .CED3(1'b1),
        .CEM(1'b1),
        .CEP(1'b1),
        .CESEL(1'b1),
        .CESEL1(1'b1),
        .CESEL2(1'b1),
        .CESEL3(1'b1),
        .CESEL4(1'b1),
        .CESEL5(1'b1),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_U0_PCOUT_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* C_A_WIDTH = "16" *) (* C_B_WIDTH = "16" *) (* C_CONCAT_WIDTH = "48" *) 
(* C_CONSTANT_1 = "1" *) (* C_C_WIDTH = "48" *) (* C_D_WIDTH = "18" *) 
(* C_HAS_A = "1" *) (* C_HAS_ACIN = "0" *) (* C_HAS_ACOUT = "0" *) 
(* C_HAS_B = "1" *) (* C_HAS_BCIN = "0" *) (* C_HAS_BCOUT = "0" *) 
(* C_HAS_C = "0" *) (* C_HAS_CARRYCASCIN = "0" *) (* C_HAS_CARRYCASCOUT = "0" *) 
(* C_HAS_CARRYIN = "0" *) (* C_HAS_CARRYOUT = "0" *) (* C_HAS_CE = "0" *) 
(* C_HAS_CEA = "0" *) (* C_HAS_CEB = "0" *) (* C_HAS_CEC = "0" *) 
(* C_HAS_CECONCAT = "0" *) (* C_HAS_CED = "0" *) (* C_HAS_CEM = "0" *) 
(* C_HAS_CEP = "0" *) (* C_HAS_CESEL = "0" *) (* C_HAS_CONCAT = "0" *) 
(* C_HAS_D = "0" *) (* C_HAS_INDEP_CE = "0" *) (* C_HAS_INDEP_SCLR = "0" *) 
(* C_HAS_PCIN = "0" *) (* C_HAS_PCOUT = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SCLRA = "0" *) (* C_HAS_SCLRB = "0" *) (* C_HAS_SCLRC = "0" *) 
(* C_HAS_SCLRCONCAT = "0" *) (* C_HAS_SCLRD = "0" *) (* C_HAS_SCLRM = "0" *) 
(* C_HAS_SCLRP = "0" *) (* C_HAS_SCLRSEL = "0" *) (* C_LATENCY = "-1" *) 
(* C_MODEL_TYPE = "0" *) (* C_OPMODES = "000100100000010100000000" *) (* C_P_LSB = "0" *) 
(* C_P_MSB = "31" *) (* C_REG_CONFIG = "00000000000011000011000001000100" *) (* C_SEL_WIDTH = "0" *) 
(* C_TEST_CORE = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module design_1_xbip_dsp48_macro_0_0_xbip_dsp48_macro_v3_0_16
   (CLK,
    CE,
    SCLR,
    SEL,
    CARRYCASCIN,
    CARRYIN,
    PCIN,
    ACIN,
    BCIN,
    A,
    B,
    C,
    D,
    CONCAT,
    ACOUT,
    BCOUT,
    CARRYOUT,
    CARRYCASCOUT,
    PCOUT,
    P,
    CED,
    CED1,
    CED2,
    CED3,
    CEA,
    CEA1,
    CEA2,
    CEA3,
    CEA4,
    CEB,
    CEB1,
    CEB2,
    CEB3,
    CEB4,
    CECONCAT,
    CECONCAT3,
    CECONCAT4,
    CECONCAT5,
    CEC,
    CEC1,
    CEC2,
    CEC3,
    CEC4,
    CEC5,
    CEM,
    CEP,
    CESEL,
    CESEL1,
    CESEL2,
    CESEL3,
    CESEL4,
    CESEL5,
    SCLRD,
    SCLRA,
    SCLRB,
    SCLRCONCAT,
    SCLRC,
    SCLRM,
    SCLRP,
    SCLRSEL);
  input CLK;
  input CE;
  input SCLR;
  input [0:0]SEL;
  input CARRYCASCIN;
  input CARRYIN;
  input [47:0]PCIN;
  input [29:0]ACIN;
  input [17:0]BCIN;
  input [15:0]A;
  input [15:0]B;
  input [47:0]C;
  input [17:0]D;
  input [47:0]CONCAT;
  output [29:0]ACOUT;
  output [17:0]BCOUT;
  output CARRYOUT;
  output CARRYCASCOUT;
  output [47:0]PCOUT;
  output [31:0]P;
  input CED;
  input CED1;
  input CED2;
  input CED3;
  input CEA;
  input CEA1;
  input CEA2;
  input CEA3;
  input CEA4;
  input CEB;
  input CEB1;
  input CEB2;
  input CEB3;
  input CEB4;
  input CECONCAT;
  input CECONCAT3;
  input CECONCAT4;
  input CECONCAT5;
  input CEC;
  input CEC1;
  input CEC2;
  input CEC3;
  input CEC4;
  input CEC5;
  input CEM;
  input CEP;
  input CESEL;
  input CESEL1;
  input CESEL2;
  input CESEL3;
  input CESEL4;
  input CESEL5;
  input SCLRD;
  input SCLRA;
  input SCLRB;
  input SCLRCONCAT;
  input SCLRC;
  input SCLRM;
  input SCLRP;
  input SCLRSEL;

  wire [15:0]A;
  wire [29:0]ACIN;
  wire [29:0]ACOUT;
  wire [15:0]B;
  wire [17:0]BCIN;
  wire [17:0]BCOUT;
  wire CARRYCASCIN;
  wire CARRYCASCOUT;
  wire CARRYIN;
  wire CARRYOUT;
  wire CLK;
  wire [31:0]P;
  wire [47:0]PCIN;
  wire [47:0]PCOUT;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_xbip_dsp48_macro_0_0_xbip_dsp48_macro_v3_0_16_viv i_synth
       (.A(A),
        .ACIN(ACIN),
        .ACOUT(ACOUT),
        .B(B),
        .BCIN(BCIN),
        .BCOUT(BCOUT),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(CARRYCASCIN),
        .CARRYCASCOUT(CARRYCASCOUT),
        .CARRYIN(CARRYIN),
        .CARRYOUT(CARRYOUT),
        .CE(1'b0),
        .CEA(1'b0),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEA3(1'b0),
        .CEA4(1'b0),
        .CEB(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEB3(1'b0),
        .CEB4(1'b0),
        .CEC(1'b0),
        .CEC1(1'b0),
        .CEC2(1'b0),
        .CEC3(1'b0),
        .CEC4(1'b0),
        .CEC5(1'b0),
        .CECONCAT(1'b0),
        .CECONCAT3(1'b0),
        .CECONCAT4(1'b0),
        .CECONCAT5(1'b0),
        .CED(1'b0),
        .CED1(1'b0),
        .CED2(1'b0),
        .CED3(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CESEL(1'b0),
        .CESEL1(1'b0),
        .CESEL2(1'b0),
        .CESEL3(1'b0),
        .CESEL4(1'b0),
        .CESEL5(1'b0),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN(PCIN),
        .PCOUT(PCOUT),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nT1iDpedwZFVkRSZDJusiwI7kFIMBvviCRm9M+pZKTgQdGFO5jX8oqNrtlexCu/uDfp0YQ+QGyHf
W9HJmnELyQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
LSiX96nVtTeT6QH6SYBUiN1RW5Mga6q/2lxWqXdOG38n69A/VIFv4MZSHjz1gILFox9JEY7OFwGs
6ebz/mUxmwP3DNumoccQ6uOcSkKQV1eRSlyyHm4UhahbN/tD6kRdHgTGQgjiOPFINjK/bQof7LKF
xQMmQeb2+71XHcPjUHU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T14r4uT0q5iPsUM9da3RnLjqN8Qn724f3Fcj5n9r1n/OCu7B1m+A10bBZuAn11d+eTpUOqwU/X/p
2zzSaUcTE8ijWpgSLXU8J/0wcBVyuWUHOoOpFIkqda/gzGVSmbiUUBGDhktV/P2ktOR9PeMW1pHu
QeJD3NMerGL8xO8RkFz8+37CXz+yNeWbl9EKsnw81po0312geoX3g2TFZsqRUaRMVN1P8+qQzlEb
OAUU+/BYNrtsGGxq57Lea7LASqCQSI6ZVYSocjpQzYz+zpK1Ifn6KpwvU5YLStgDnK95pF56yxWy
4DsarUkJGiFZnz4hzdYJeRLciFb00Y7Z7OHKXQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JB9E+rFzptTgWubhsk/ytb/NrSJBaKLviXMn62i8KWfOUbd7Q37B9GOtkDXor5Q39oNYqlzgkXQQ
9g+vxtDNbMGPBkiP8HfN2tKmqAP3203t/R+B1D0CmN2mK9Bzwi5rAw0zNBanLu0Huhygqeuyv4SW
RjQSZSiUCtH8UQpPnwdKQSS3zlTnpPv4po2tgA8ZzjRNyXUAFGD15dFRCsv3KN9TGY3ySFrBZTpy
ddZI86gPVOR8QamQKAtVPZgLCYSIOtqQrQOt9c7yM0NqlnlC0kVD8X16GQ8LchOJaRRndKljCiJu
T7V6wUYHHVdREAeFxWPEgIwm8uncarb/xI/YFw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
hiRSLr4QLw5/mMP2Zn25/s5s8AF5rzEvu2TjIzKu71zUg0RQR79nm8y7jnlLFI54qMdeDd0ag1F9
TU+c3zvS4L4EyGAGLDGmOYcQ2klSCEkAp0eYHfZNyKQhLKpfpdEXhwpsfAMa8mfqBL6skxrp6C+l
wSbnOqvq502wmvReAdkBa7hQBquCP/Kxu+jlOzeR76T33fKFxe/GKjVFC7CzkdJFg59HGnCzg15A
KPrAj/GAtXhrFFCtzppSIgO8GnVXXMrxXlQOTW8Pa8dpXzVVlhWlbclRL5vPlMcPuo76TstX69zf
yyp3rGNQXyTGQn2cIxCTDQ183lOjoKza3cx3JQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
YGcCI/CcJmhsdgWdOuARrKP5BvDGllkS2MoY0dfL6ioXfX2lO7pKY3qpVerntGDre0ZdXSkxLBW4
1veoXYSLGmDdonWSixQKLqlzm2MuxscRuCLcic/Y885s9obEV+bR2Ys2BljpSBpVcE/Ur6Ywxmzk
LxfHQW2SwTpLvo2b2fY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Qfahy1mSmZHw7posW16zQRrSI47b5EnD2EOzgkKc27KVqFCtYxFhu2K8HcIi4g9qHxVkiuCMS2xv
+leE7EvRlzy778OaDw5sNTj6pKXuDNf0TM9Z5qWIQfZXHe1pN3vk5+JwIPlnKOQNdR/ZvyF/MGlN
OiLTikOABwXxl8J3xz7JkKAD22NG7mPIcFEx4r+67vvFAsaNrRdR1eeZqoEWtdnoXxed7RU4EF+M
gRoH6yIiT9Y1/s6OYskQ6JtiRhnYtAuCfzREnZAh899nzaIcLd7LEVfL5Iz9Ugu5o0kDqSWTin3h
e8cg4A7UdkCUVgAKEJvninJ2KykH8gXo3fcIvw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iH0JES/NvEFuTjt4he9VbmbaRMMC3Az+avxSvav5homUT4AN4kL0Mr4DduP6qq2S7yA9vmLjymM3
vUy0LCnW4MRJjyOhZ5+lKiHHna+B7i67+QBNgfHJZBo1o/MQKyhRY1NEPO2OcbO0Fa7vWxMb0fxD
tLYj3lDptr2t6lE5QyvfRkm0+K1QUewQYhPoottRx/YRIB1HGmuGxAGvtktBkl8YKImYUAhUsulz
EIDhzDvY2mjO2Xcjs4bARB39XehWet0GgDIyU7JIse7GFBpk0KwuFUmt8GyECacKOV3tNHp0JNby
fTW1A4Q/icYz09CCUtWXTEHhWrJdwGru1IPXUw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EJbk8tLLjlKBWWzy9ed/VxLmGUjR/VyjMLsCneAE+j84oRp+KvuysqptdWBdiS7vf4A7tBZ/bLRt
i2XAajV7I+0PfuDBovQu1eym/cWgnSSLDzT/4VP1p2nj5nrtcAn9tYUy9j2vRJKggOQ7q40vZBV1
634FZbpF3fZnOeqhBPy4C+83B6JqHPofffC+hmvoz91FfJThjOv67xFe2cyVlop55qBbOiy+dgF/
gFoSvZBYPkujshJPw4v0bGdz+YfFgPqDQjQUHNRLlqRKte12d/jEOfxr3FTz8UVNsigk6EEgaP78
IiEcx2HkSGbTNf5Gv3xxX0JktYxSp2ciWXghxA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 30336)
`pragma protect data_block
1McCiiO9Nzb5oKvoYXhu+bHQWNa5khEaoW7D5mi5UI4q55P/iWe1H9X85LY+ApRVaR0Lun9CFTKs
4MWxDPWlnYEi0936qT4nS5wgYYKI+YoGMv3eAtvDY0HIESYCt4kmmnfB+QI1ZFwQnJOGBrAJqO5z
n0Yrp3zdLv+P2/hK0jpdlikrCIC9yR17ikKM3yPGx+atRsS+Bo7KWUtvQ95aCauKbFxRAhG7b3KD
NGJ7+9Ytiq47eZjZO+lAJymP2TXEGChj5MDiya+whJzI1oSr0UKrVRORHO8E2QFTKTyO37AGC7FM
wRq/T6RR8M+BzrsBzq6d0c7K4BV0StStVEPd5a3TdtlqH6YtzcfNZjdn4nWR0b+nv0KSIHPLy19a
VMkMpxN4ZBsInO6SUNPY56wz88BGNW/biMRt96wLnhDdUah0WKLCK16ftTjLP/GdmMnynzbBaXUl
/QtWHz1KemRSdF0ZCqJmGzNt6fyJYvFp4daUv02w2EGcgsv6hzFl5VvCMsR/k5vm2qsL2qthM+sp
GaADz2xmDRNjF8j6EjQ6dZd9LcGUfLuVhsFdRLNhpsfmMjDhJ7Z0gg3LE+bx75x39Sq62aH+bBtD
WO9EhU0/6XlLtN/elWulJF8+5jct0vMSZ2IPdJbo1auHckNH936l1LrAjILENXBFuE7MDgIYu3Dl
yxzVUFR3Dl2aH4XiAT3IxIs4p7/hwntMW4QObt0Lw+DYkM+fc43anZgyo4wLNZSBiCIJnd+IBhhh
insTKTi+KP1V5Icg+8lA1RFWCfQiURCfMGWl6w9e/9lJ6k+aKFOTxl6hdT9P5QvW4dcs4GWwWGR1
qZ5BzuZqo1iu82M3ABDNRSNrDaN085AGD2YYA0bkATxkg0eRpM8DGETLskxJsXzL0q1gbmqt9V+r
aLj3jEMC1fL3RkqtDK+puwxzdJ8eQUJEeOGiu+xbBnKlFNEC/+ftaI0fqhOX5nfqo9WTru1o/870
r9uLdWZ/seGKtC0G5aZVGNB/hOoUjzCYwvOZv+T57whADdEXrCd3p4pgB5oB/dYhL5m6tuhs/P2C
XBLrjyrwtPEkGRIQbgtAI2bOP0knl+rMWhXvF/Y4RaYHYClO6cblmko/6ic3cVwI+iWSxpc961hB
agosqe2bsSWpfvi3U4EydHAWkIN49kFBc5ZnJ9mqVvbXYCM9u7wJLQ2YIOEVQ9bZOolUEaq+CuzZ
DmMoFK7o9h35t/iUDA6PKfBYo/69RPXHVxGR24c+Nl4l/g97UeRRJkHeoVS2AHJKm02lPY/U0UoE
VBhWkuw4wPkAnFAQ+1oZtxrtb3iyzvVF/BkxIW1+JLjWigm8gxcjvCatreWDBp4yPcBXEG/MEG9F
+9mF7mQmkxxW5THh7aKuMpJsvpRr43z+nbIqJsJuGAYnO9k+9EmNiP/IALf4vIfHNL64V/WpZLY1
JClpxlCKzkVf68YFpCxPnW+f20qZDiP+AA4o+iyIk7sNPcTdF8+JBsRR1R5M/E6KAoKpkwyBnLHB
yFiNI6w6wp8XTVjNZjWqAbXeCDS8iBTsrx8CQba575+0qN/18Anx2oqJYWn75xPWzms4z8q72YvL
xdlUbOc4QCd4KPaWVAp64qBV2E4q9UGrBvmG98CUuXEQ90Zy4EMUB/tF37eFzn0hfpvmzB81ilAv
WZf9kvjooj8q19WWKNnyu2aiWYBe4xzi2pSYF5B9+FdfIPwCBmuIirtGI7wVN4fKfbrBQONXhFtp
mJ7ljL7mqRvUSY8Zx4IGw0nd29n/R9sIRGkJp32LNSN8citnbKo4XhI4lAD7yjH+hYiYOmkkIEEu
UNefiOKPEuRt7E5lZbFlsbjmbbImdeKS8WrKWbj5ZbPwJOA3hy8i18087AY7SWhCPVdIOODmJexy
QypSk6fPEjupg33D7wqlJLrU5halDlIUkNIvDX3/CmxhCb/mh/6PNmrXNMhdPbj3dfN8tT7rWjgd
HujJ6PPBjsomuPbyk0EN9mZOFS49u9s/wetLgMEcPe/esXpcKn1CZmExaoeGW2vFMmbuSXKQWiUH
tET4n8XehO4pmtvHSbnXQNsX/Bugyi+aFBvZRSmO/Rbb0+n6rm7oSwyGUZLH3X40GRr1wz5upmnF
CnuUDZdQ82GdFrUF1OJOBGDwTr35V88FcJCPz1ctW8fKoagjjt1OCLMYYAeYfWd4lgD4slDzg/yp
jimKrkW3f0Ox3Z/NxB79+MdZv1pENvDLaP6iN2y7Hg4nnz6qepHO/RoLAlqsahEOAISlXWf9QXtu
CevvrobXxI8ZZkTnqTViiRGhb2vyh+ugNSEd1hwt/YIYEaTvc2zUJ/E2ToAv+komVvHewU7eBBYP
c8BByjV5vL1f+E6M6wLd5zfqoWpejtUP1j6MsDtFxxfGZoLGUbrNYwuXltrUHDhpc9ybd5j7o8ev
1Vu+aZF8XvqV4lXi4B/BBVc42JvdugkHPQoaLodZUkX2/ijyG34dIOjPr+g/ySLHTvatGbh8adTP
Xr3Ytol6bsh3wrDpXPqIy58R1bcGU2XTfMhiQiw2OkviYf6mZVD+pljIKXggGcSy9XJnTJKLcMKP
2XTonZtyS8k9ub479dg7Ot83KrUiU2cWG4rJ3wkoEDW8DqU+E+PcSOfR5eli+jQVChT0Vk5G2sRD
F92Mf1lUj3+IFmi0vQLIaWzoSIh30Uv2IPeVRPjXe9PoHAEVxqk1cFVQYtpNp+0F0MGnnXt1hpst
volMiI9J90FTZ/cGrZltIbrFX79AJrtdjEaEnHO4HlfI/jlmXQMHftSt6iKpbEW20n9lC6OqurJW
z4WFR7F1qa10CeGRc4AyHkiKA4hkzFqjFIOVFNgCwaA9Kc3p7wrsXwrief3DZQOeFclSQzmv0rgX
oytERJcCcBnq+B3HMRokXvMNa1SVyhy3qZghEwBVIrt4nFEMRSh1L6yuaR6WDj3zXC1mv4716HxM
DKspEYB5y7hbYwqJ4+01XOp6koI+UDAZGg0KOuG0KRHti4DC0kpwP/eY9YE1d6Wz/SCJBhVD1okL
+pmrO+vy28fz18OqFl4im7PFwrpEnNWeuI97+mOQRdnTwmMIKlNqUtmHwyINrxTl6MftndRD4wKL
85STYFBJklICfJ4GSL84wj1piai06N2J8Nhy3ol+k8VOU+opYmX6in304C5XyJvsQ+OzwZkJi5Yf
3Q5LU2cjdg4PfB6hSd9vbxkN15u8G/XblCVV8WND61cbOCCBAYhzpPn0co50/JCkIgOBs/HhFFgz
cAbk7RsFVURHj+EZ/Ovv/1q59X+Ih2VKg7EDJmy+e7jpbM2awczsESyk7UOvdd0UM5kIgp1k12jw
oDYaa8A9iELW53Jz2bWJR7VyFB7r5TfypNW/6Q00/Rw4VyegqOyA8LzAhlQMcUfpTekiyXbZeHP9
TgvFYzJ046xDfoq0aclRizdz3ggxIA2l/uI9/uXxyWYU13ZVS+uNz9NwRQzwzwdBWkViTugfAABO
ENH4NixtddtRb5e3qfrrKMhnqESdOcnfhprQa9ZkYmCjwrR78v83dMVlJtmeDPpTrE5QB9RjPlYu
qmv8IYbC55CrrrJ+1BH75XEKlNa1Ve055SG0qZV/QFUmRrln/JV5TPo4druwAxwa/jFskw/xWqeM
YGicaacfhTYSK+Ue0/3NKOqeYlHriMlLlLAR7Eu/lJ7GDYlWuLkZEnePfiK/svOwdpGEwXOgNiw5
83x/vKp3kcm8JfHDwV1ak/qM6cAvMIBG3cpjLo478SYJqVnI4VEZDhK0H5yB/Xm8RY4H0W+gY9Aw
TJpE3Lm5ZoQr5nqx4IetVCUgn4ZUQTG8KTojeMhJb1iCY17TlrXNSo64uVuq9OsRcQEJE+8OZQD3
kA4A9OXRvBKLJi4n+J4tqQsQmVLK1ivGWoXEK3dU4Wq0Q9PY1UoDMR+R3bn4ier5IIpdlOLjlUgl
mlox7wE+B34cFm51z2XD70Xe5vNRvLKRmUMqVYqJS43KIYWROkC/tsGBM2ARWTXFJFdT/hHyrwMj
i9NuMUrsk/9Z3Dl4yVh2FjALjsPIF/79qoSxxEjBonRff4z0XsJIfzMErEp3IrdarlhE3KZiII6g
ec5sGXtNTv78O/PUALqzhQWGxNEy65s7Wh/dxq6M72hnFlE35lUHzh4uMlrTwV0fJUOPSpQEGaAs
VAoKjQZ7deZtXbBnevHMw4TT1jc6PlZ91zLkJQKLJDfL4FucKYOSlxIB5CBheqcD8euyGrAp/05y
hJnoSd5zRB+ML09erf0hsH0FtHpI0I+u5N5BDWJ5FxDPPl6xtcIkPr8pgZ6yCZvdHylkxuFkC517
aMnDQ/zDpLTqMgxZIQFkOCgi9n8H82AO3PGZml7+wh7zz6CJ8fcQxj28u9RAQoutGEDFA5UYMvHh
usBoPOsg99zQs14tMfe95gRMjz5nKwpscsArwybUmYhNCi6O27IT4vj2MhX1BYcdrY3FqdlDEUQ9
evNHvx4wnSM/ss/jYhjOzyuT17hGE+8/zC2bkBdu/CorGeTdPBwAWFWjSy34ibW8dwX2XR6jwrca
nBzg/ZBYzqVfN64ITEXQRtLpU9/P/Qakqgv1q7NhBdqFE4zfTm+Kj/jjYFPaEKBv7WTh607hD7nK
FAmUrbnzxQN8dYS/DanLqCRnM43EnK85LF/OEmluVEGLDxRUT5INxfSwanjnqF+l9k2hE4uSw7eL
o2LbALhDrLyuQEOUrpZL7JnE4wdl+8RE83SqSTis5ndKO8cvx8xSg1Xs72yf3ZWcxhwlNqLxXJfO
n/HzEgifRxtVVTISxvxnPCSUZRLpGQY+McXsUBO9G2LXmcFCQHbTHmQqGRUJpe8oKkYDNLovuzUj
o4mx+p578t34xJ33oqrHwjdtavqMfW+/xN9M7BjPCNQ69bKDVACh45HatUF92HCIiiEVPHNdqjuj
PneeXXYm8FayhDx3K/vRj2CA7hfLk/Jdn/Of8VdUdl3K0yDKVzQbn35R3HqHZ5cLMMWHOMlunV4S
QJ4dd5gZRvY9FPR4SEnLFlBRZFVqpgVWLgquCybpdDbTiFIu5O4g/oKlguwTPkv0fkb5xBHCrtEO
q6ZbLC/TbyqlGGVp9RuOx6foHsviCxEShFdDO/xZzjSlLAYKgqx97S5+ib3oSe+gauBbMW5p5MpL
Ah1o2gzHprzI/x9tmnavc1IhD1EypECMbotTiigxVjzl48PktRISf2ahNBI+FMlekEd7C7a/u7kk
7YgBeSw/w/nWajyM00nYtgVvWsAV7w6+pzYcCrOFeihqI2UMnUkyIBVrSEoZZh+aRRuOe+ZuMxDf
QTTs62/GK2CQ0plImHMh+DSg3uXG3w/kVAFDLY/DBV12rN5z8fl4GBng3ytXmoyz+CpQb2HHdmh9
L4f/8+S8TrxnIg4tMUwtsT6PIdElrzuGP4knZhOk3Xa1+wrt4xIfg/3bDG7YNDMglg0H6via7sBl
q2/EotkrKSBMI652rR32gMnyXuSTDsc2QqWjirWEtFz+I0Q2LfTmVKduABaikf34QV1NJDQtcSW7
oSyPbDyU1WEYFnYeoToWY25798cb0PZp3LSJdcQpiPA/QRRL4SnEbY41EBe7OTGQFi70JsDVSVjy
u9nsgHom323Qaebh5u18KZmMyhQb63qwEje2dWLeYjap31PIdhhmRslwf9gs0Qt5yGwPJiGy3x0/
8Myoc0+LnAAH68pR/55Jl5zmFM6BmfoExLx6qXHfFJ5MJrB/l7KbwT6fVrRIwlHlCLd/jygyjVl+
csvo0axr5NqcAPEyLl0DtHjy4LOZTJu3yBZ9ZFhIVNt8y5fv5Co4PkOG1vmcxDMn+0akI2JZDmAo
PfzR79Jt9x8FD+SxPAPYCZ/1BINJaDq0vcPnFa11FgckxPwFxpc/r1UnzbMWcZLMHOjVaF76vb3p
0Cpnt9dNcO2BLZYRyC3u1+UgpkWAe3hY3uhr+amTRWcMCq/H9fpeN310JSqJgNG/9fPr2d2/3YPR
Bhuc+BRui81FkWLa4amDXHD0qfHyCpvhUinfFiIawyhB/DcUVSKQpayyYKTYuuIWg3F0OjB0gpAz
q1xB+K6S9f4jETkagpaaWK3fCW4fQLgMFwCaq24wcAKmEkztRZPS4fLcG3gBpbC//QOo0ORextzc
/g5IZEh/kl7AI88ew969NCQv3HHD1sBynlTDt6zcgWU5YVMSqOalwFCHSzhpq5343BK+/ellgm4z
BbQ8OsnU6TsmTmZzxwwrH7f2ZGefQUpv0y0DAg/XmKSt+DisO7feCkReSPfgeAuyyCqDimu5t4Hg
J5uk8d6v9ut7ut5UVXA5fN5M/yoYJBMjNO1aEFtQfV0iX4ULJ3eeTM3yr47cZUylQp3Fq3bnVRIK
eBVuZo8ioF1l+t9SJtOmQbz3wa5YU8AmGGMWeu+8gBH99WVG7L13W4QucsaT3jc6XnNzI8slvTyO
j1yuvVy3t+G+wgUV61mPW4/1vsVp98dSdKK5LZCkd5t8QS/N14LCZiVCeD1b2fdAgKYLwVKxsnoe
/JaE7fd5QiCzOfATs+CxQUTjsK/3hY0dmTIL6L/UwcScOfA7izgjVF4yzSPdm7/u1a69UHTd1w2Z
XMNj7Gjm8Ojl7Ot15faOm485JcXwgtYcW3OyTGHWDQLU9j1ks7xOjEiuA2tP9FrcJYVaIv9fyf5k
hdnHebSGzUxLPit0EV8Cce0MFFg8RxtfUf/jcWCovZR3zbXveCe1flSZnwLrL2mYZoz6ElMIpc4I
JfOHaqlKs9Ob/KnTuLQK2cXtoyb+qIdlPhKCCsUcPFatCjsbmhdAO+enZiOcq0ZVEPgBAWSfT6eU
PHEc4NiDskuTXAWaRT2FeFX++h36oWmDXQ61wrJ5xPr12m0fz9guuo+pLN4J57bErauSQRU8Raz/
2ewDHzlAdwuoHAduVz4n/7sBX+EigQ4pmAjhNwKiFDKTFGgfMKUaEzthx6VrcGaE6EcYa7SHSXmk
RveKY9nSreNiSfr+ZX42y5nVNtoropluurgReJgat4FH2rBPHWxyl+MgN/Skm4zhz9fhkPsDgf6g
aRQNzN729F9IJ+ujqsD3EBWiCfIpsoraBhC32vwFN1cn7Z/V95pswVUFWtqCROISlnfaSCDzvpcC
sXhpIepnPaJSE25ov0aOCtO1N73QfzkVlAMZsz3uwNDFMWv+KlKV74O635KFcjAlWWE2kUfMU1W7
R1arfQ13jajkyErJr9wxkDE6t99dvoXECoaVuSgCHXOT5DBwuiHGG1V5QoUMCjSc8oidtYc5n4dZ
glxKruNq8bA1LjhqGPN8l1fCj57+TXg+mjv0YRcVVvGfUGnShRbVH5AZCKxMSLXzonVPQRj3LZ9J
PGn46PXQ+Auwb3SH/FpPlD2ItaD8ykYqxCb2Zgm1+ry9g94zs3xcYhtlyZrL55k9Tdd4s30ctFPz
bFR00Y95+vyON2JhTg4Mxb6uiZCzMni5CU3o5Jou6ZomdJiiRtM5fNZ9wdk4XGUpx6nkPBhBrY2O
uuLFlR31r29pvjG/3o+VaR0Wr/KtQ6PDYezZ9ZPdp+qrpFQsLBlomdbIb2JVQOQ6P3u1Q9X5qKN4
9fObuLViWvBU0moCJmZQnITlr3BNYGcN2Avg82yHQc9EFEOmm+yOssdmSf9zYE7Zk81HMx4IoTdv
XvcvwlpNHY4q402lIFuXDPoH0pYKWnMRfCtmV/Kp6fA3R7g6OrRsiyYub5Yp1t2EAR8fHzPjeEN9
jMFVL02PYwELKK+/w3jujC/1UZGhaKcTSnEE54kjycAhT+boYhuyojazPlk3a+mQ72/t6S4sfvA3
v0OwO1rU2mu0ONemvKlkuiolLKkK9pCrNtFUbuRUyTrXr0t1prjWl+y0BZTBb+h1EHqHIdNKhgx8
HQ4IO7eqvt2P6Be14EWCtajm5OIRaGtLK+VaQKeZiCn72/A5G+MddbWMX7oBeMY0+ixPfD0vj084
1Muvu89MqxbMoJvWEL+5o+v87z+zFptZqVvsA+F0gigFXG6XY24NV8z+iSi21LTn7yHu3qmNkK5W
ME38z8eAvUVtjyVXETMsYSc5oWq5pniUwQ1/PQ71Zle8n6jAcyGrMREVb2cqmjfeJWj/tz3EYodz
a206yIaF6jHb+9UxbEnDtgR1vytYgAz0INJM6fYxumysCVHFJ9HjGsAQgmyoP/FKVQqXgRi+towV
tARq1ZlwywCuOoeMW2Syl6OdUWJv97ltKg8ler7tCviWmEsN6qK/r3irKLJtzRwjsCx+AeDHLcSX
AO/UBmLbLeXcvT/b404MHcJ1S8vgltCm4e3hymhHcRF/5WtJXq+R1DfsvCAYzuAisC4h+EXRMWy0
S9vj2XPV6iZ2DVULlKnpfvOKqylbPqHjEnF9h4ZCd9UE+CELgIjsjSIrI3fcPK8aIaxEf6YFLD0d
+n9uzbo0Dxlik2c7nrhl1L+uvMNZDvO1Y2NG4Mz83fY0ovcv1jDxJhIhKaSBurV9tCNpjvoCgzq4
Dx63DqxIjm8ihp/zJcjeI4MZFpnx8oDLNg9uTLzbCldoxqtgHsvXaIIDI+imgG/ebxUBDz3aM3GO
oAiQSy8C3OaySKxoIkjlnZUm2wwKWoqODPZG0lTyyaHISxYMFL1cNzKBrLXBZceaaFkJc0tymJkl
vhDrxueAXCzuad+uiiDCYuqB/nbTJpP4MqGTRhwEOQDli+JgjQtxYCpD7OXF+7OEEImVOaqI4IOT
l+QvUkgXzYZUhwfQaaNg38GM+duupQw4iJHcSkn0fz3VnAM3+XhdMmpLIVSAvrkCfPq9GAk95+Zs
9fZFFxkQ896Igk+HrHnvK7V9R8UW+BYN6zpNECoSqSqWOLPYUlQtQ7g7Dym0gBj31SZisM2eEUE7
d4zP6IYOG/VrxeYNmkAvmZwdxC4D+BXhkjyr1/SJIMMwe8uFjdo5NmD3Rb5oluR1k2sOllmOFA2h
CnjTBe6OKaysNkqVEDkxjYLD4lv4OCierjRr0Nj513+RavCWKg7OLcha0QlyyX0yACnw++J0Ckv6
FRQ3e1k5Grdf54R+vk7tc4DbsDj7scF1GlgPqrXM9e3mSCZyDejyMJ9TGaqgnxixUqBFCPfT7m4r
AlFIVkaaZnZwGCHk2ERZoRY9BKkY+yjgOj7Jpt5fcfCg0/kBMZ3UImupQW0aTRBUyAUtEcYqGG+f
bl53M/83yaOwhX8jyWxRzgG5WIBCZjFad2Qn4QWLi3DZVrBA2qbFm082kKjq8USYmH84a4k9zivh
mlHReulaUP6MPcNeXgvd8u+LaZI06ZsshCn60Sbxg2I7RKsLziMaYzxDsnhkVlm1OhTxuP/Vc7Ii
SMZkHYpP4526JsTl1kO+2HsyoEYsP+cKXCAg6wu5Yx9+djlV4QhegXT9p2Ug337tVFrySp/ZUyfV
oO6Pnp9hkKA+SkXlysbo5RHkhL1fi8cZeJ5Ebi9u17tDeqh3L8f9r9UKk53r0ghQxx4o2NH7SYxw
c0R0Ktfsm9dbPsPMiUrCz/g+YRHXkfrTtHLj9rBjIrgQKpuHqCMkOy5ORDtWtnEQKD633zxesRob
1KUIaVjNzEvErhfZ0gEy2TCFbO1a0Iq44DaaIp/a5KmnxCW8pPqwEYKKSBi6wgELPdUMr79kUNut
Xe3ab4xfx/+tc0psDY8s8U9clZvwwqhM+nAZDuyRNzaDTw3B7xJIHw5lQKFyvxyTnA6JXIMx2WrM
0LOXbtyyDGvhasqIqC3T1RVMGdeI6mVVCu6wSHJQkdvJUPvsA6ThpPSreNAUCvRUSZW6QHfoe6GH
1/opL2NE44Y7s9oPy6Sz6Si02E8DNEqVINgZLJBuVL4LZUcNcpMCiklacr6NqHUfodvlmLTy/kiW
JwbgIPj/JjqvdZf3qaQxcpvtKTVW8pMLM5yNug3qHlnEpWq60lcYWSx9mxA/0Jk59sedF6bICaCp
0c1PijwVvfmqBjCjba7aefhN3UXxblTZFJD9mp2zFVC3qTUDMCfZQVhZxnKr/qj7JmG2UO09vd8e
TKU78DGllG4ocTl8L25EugdZ+iJKpIAJlEveXmA3+2+ZknkT0VfifU7yafRCxHzJYxiS/R+QdwDD
bYlCOQE4h0zVVQIoXxbd9lmhY0kxjenvR5fa5mcbN3xqZJMLfQQ6Pql+Y79omOzqS7OHp7xwhlsI
vMmwTaVuGST7dFpYIeHTmG9mXtHiu3hj2DcPs/pUXQKlVOoGcqc432TUnTbbPMPemKl4Q9YtnqJW
1UdQubvUQ7DDx+efuZl+cpYNfm3YK93VO6AfG97SOZU79lHhCehD0JvtiupRaY5kcKms8cQ7uhmK
sT7HEduStB/2SR4vUrQXvL+kGhs7OLZd1GuWLeD5Re5lfnz1wCHzwRw9wj41NX1JCMMK3vpgGY7A
VhzH7LruIxJvqS/WwluWf7IUkUwtjtB5c0xMnW9SpTwHj7DwCjjOuI46Nn0vaw703Vy4nG2Ue7XD
2BfRHBQ/HrJ7MQh/WyZzMTzEFwu5tMrYwFvH8kvuHRMCvKd8A8gDo57AZbRmzngogh92OKfPoEY1
pxGPGUH86i9365XmcBFDrEd1drvVfGkmym7bJvKfDZoh7U4jVC7LzxuchSJjTWMHAd2Gb4TIbTss
0n+6Ecs916y/7z93TyH2YW/ZbHPEGvKty7oL6sWCERbUvXI9+m1UzfGGkhCbjrpK6aj9dJtg+xrQ
CyeHuNwfav8Z6znF5kfoS4UpPX5rpNqsV2OgWafhEWENQK4eD3JepyGo4uX4h+0u8rCystWzrft2
EVnC1jEF0Wl31kjAznLdY7fBPYl/8zFhg4nr8KkPp1Jr+QV1aItfUeIPMZmRsyoRZ0JprcYb8fL0
SLxD99RwDUNOpq6L+CFMM1+jkCg6EkM/qvFX3Jbysr+zX/aOsFQ4bUCMxLhuYheXZhp9ugPk8JNZ
Slmw6D0t8EnpxX2UXYMf+/HTdXUtOs3cFfiYuZ9+cT3ITQ27uAj1wQWKGa7IG2CD/hSDohu/RTlS
TnECBdcQG/Q4IOoS7GBujkfFiyMlulIe4ME3d12psGK0JV1D+Aa+36XwrxnC5a9REvFpcdis/qRp
00xiXJakHjBYueD/FptqdPb8wKNJz1/6bG/LwKpisCIYwH8LYWx24jj7tcIwQZynlRhxOBHS6B17
xg1km7ccgPFtI0TnIEqgPbLClOzJ/mqPF/A/Os+ycHIc+WCgj8856xCc9Q080OTNlJRz+PLNBqrS
xaZTt+uzmSYyiJA/547n8C8Jnvq6eelEtewVnvqfwe/iSVVoUcWB8snhuf9WSzlznYOyu47V339g
lFMh3/8cH40SvcF7W4rFaWVfBGu4W+pyYsHZtElmCihzZ49ZmW7RZKU1oMpHwzgxgJYihkoxDzkF
Rd26mAqlagVLadAGX5qexNyUJ/R49SyUsoRvXBOJR7o5M8W28qsN7kIgMzlVp4PUAPCK+ANTi+H/
rU2nO/3BV4EDznYrgZjbTMjvZGFQmcHaGVFuYPVc2XFVargi5t9H0VCxNa5IPSJ6mOSzSptYywcq
N0NMetSwmf7K+AvfDBn082PD44ynXpXx7ZTGlVxUDv8+1SS6x0NSXrDmANhC0TImQcn8Xn7LoY8Z
33gDvNKP6bvSSpfvQIha3j8ElPlQU1C+izN9xuHYMTrQzK0bskLenntczBfDfZVRchYft0hc02gp
SxrbeG1zBt7phR3pAC2wkves67HiH7nso+wK3REmDpN0FPhVCNJGDGRxwJPKwDGv3RsKNr79A6xT
LzwmvOJzoJtnWHXwv3dYogPpyueeo21RuR0CQzXBSs0XTMGCKj7qhlib12q5S4JON6Y5bQ3LhaWs
wkotw6wPT7kd6qZ3UCoiohfigQS6R/1CVmRPZtzAqYLOBR+7MZNXi9snzhy4Jb2B8v/pm+ICg7PO
n+hT0CuuVIkZYjAZoZ13kAittcWOEIYKoQGmukl0bNSR5o4nv+8YXH2rHKfLfScZ1U4+DuzLBEGw
wNCWGzCaM04mLNujfk/bYaDTQEpy7cCNfTsd1t3crQcDaoegAGPxvhp0uSbuV17GyE7dNRoJ2Pl6
CFwOn+RX1vh8T28b2VLs6vjZOHP1jQ3oHteajJgNF9tLd+mfLpcBCOLwDtVDG5ypqUNGa3mG5WXz
lSVDeobnctcCOrcMjsL6JxDYVEFrwztue19flWaXx2OaZ+ScJLGQWIEiXfBqOQrgF7rRlAzLobsG
XDuPQBPXue9ijv0tRsDrE8xhycBViWY9M4ANt33Vlqxl6LmnwqeJIfo401qD81Sue2UHlIEcuqFs
WyRqFn8Lg+VwwnmpBMaGc6GEM5a6VGEpULFsxWFX8+8Y/j8JdUeZqf2DVseNmB9X9xP0QArFEIZL
3zSnCKqpE/uDQK31iaP+hWiecwBdegGO0y8CzlSLEii4Bpm7O35tQLEhLUtx1Rqw5ciHvVRG52Jf
Xmgwnm3ZecgKvPsyw5Z5+ERovbBwdXjF29HITTQSLvpd4B4lKO8CUvkZB+Xs8Hu6V7VRG61H3659
az66glo/VFwTfxHCjKD6Z9Ryv5o7rsmgAwqNeoblJBUj+Lo4qbL36uZ+JkXU5DqH38Y8FUEWIKtY
8GQjm9AyDiw9eu6GNHldAixBn/5CEYy+GciYrijffAX+pE2DSqE/4RAQIe7wvjfzuXO+kKg385cg
ulQQ6C0BndcPuugnMIrU4FF0AJhF+Ijf4BZy1aLVqtnWiPANoqwuVTod7tQZdZIZrDN0UQ2wFGMy
zXYD8Df66eVCsocf89/+U7qBupDhz3CuXedfmwF98lUa+n7kt1wF0HFxOLpG/rXUN7A5uaxNwrWF
j36FjvU0jPxUFr1cqWTSOJScbfjDjIN5ZJ/0kwJfNUn62TL+LTo9dPsVrvI4Mit0rfpvNHka6WA4
kJ+u8fcbTXfortea2LArHWpm3ryUo/v04kaMtjEYaV8YBPh2XxGjuCPH/7LyjBzVlqmVGkgT9NR5
O6Lq3jLd6dfS1Av5pBY3aKWea3aVgOV7VMpvGY7fMkVJLjzijtylTKrc1BFzQu+/1wdkkuUNiHpp
BXMrApx/suWvsT8Kzf/cp7Tjugvc9HqFIj3xOsrc7jWt1O/X6FmHtD28KFU2duY7C02faFhjJrGQ
bYN4SDLglO2CGAgCUJhf1Ol0EFzP0EfY5s/Lu8vmZnWVT05Fr6e0jbalpqtHz8lGxCBXpSqouJqo
5Nwg1WooWIprXhKVUVUySFWepJwujkOgXxQCXgtJJrmb5JdpvUtVsKDIoa64C7tdSmt1jL9DVPL3
k9+VVf5kjaod9fXtVH3Msy/sjZI71OaqGgnk/qEvmbyGMMIOWXSd6up9LOlaOngPsteKt5KxKDWR
X4+PBYKSMgsz1GOcoO+sMwnWqXcJbDNtZueKOe/sIXVr9e0kH5ja4J0Rjd+bbQGj//t9pmgSgNDI
izm7ra7QcY9UwMcW3zmvWnVQhenVXtOXCShSYsZerVpCXapCUVwzS3lRBTg/yn46dwI1IXjrCkM+
0OIY5u2yNx2GRVtSK5IC6uwp/uNTnajuh74Atrv2a3PC+gPlb0RIi+Z9FfJt6MhJLhUtYNlW7jKi
g6qoW/moD97981iNZhLXjNaha5WltZL+l1wRGI8CYJlxCJ1SlGiTitKl/b8/05a7j3RnuCr8CLYC
xZBrVsN/IUzoRaYtFY9sFiHG8iEakQf94weLgp5aN1PBSAyqFl0q766D6AoLX7dIvGHnOzROXPp0
o6pBTfq8+x5/1joNBEJHpOHDfqwnM+ZWpfq9qjItxYxoe+QJ3/uc6lC4KpIma7UiBTXuEPQGsWxm
GT8vSpfeeMJhGNbIoHY6CmN5WyHm+SBBBvtn+L5VjPnZgaxbxMuSO7Y369WsFTmgQyWGl0ZcAj+p
B2BVI025eZRNlWKSoUNGWyHU/mHU2LyNB9OyVy3MfHE4v1DqbUkYoS5G4rgDViJCv3osWEOYQHgb
6v3DAzkM7HP25f6tR64hDkq4TJecKClrK/aTGP9h6Mjl2b95pGjJC+43ZFCUlXFDpWVZKjCPpV13
7RbGkCxNH0bxeK6PzoJ7jlXBRJznjqJYUmalkVF45Scp03/9XFvEz+UHygs8AjWg8h91UUOsQ2z4
KvJv4q9fVrWnbxmRMKO/UTqn3ZF8WsEa7BpWG/zVKpw5PTd0uQtSvaPhmYoy1LB1tH/mBsH0Dv1K
eFOUDakm303Oa7bGloekxfi00D54pshaTyEZQ8LdxKqhGQyfSwZdpQbwyiMngBAmnvOV48FxqzL1
n8QnQggAjGEaFUKP16DNUfD1CXKPaS1ibCkvMLjJ2Itb/YUjXiQKtu2MQIqRsh72fCtlp8OQgBF1
EFJrph/GXHJxq/DNqyOkBLMH/f/ixCc2jriW9E0CmKSkNUj1glSEp8o9vUe282/qxcHIqXUfamBQ
gD9dCMo7nVVQR28yGzboy2AHWuvMLAVr+MUSbFx1ncV73uosN0YGiq6zciTMe1k59apVpX/q+gKw
e4yubK71FpZbtxd+TRVL7GhNYV1KahRJ+kSSSKn6pXE3oThdEI+EBo+5WQVMqTzSkipty16mbxYj
aWf4xpwLwAfLlNGgxlI8LdRTe7o+peqrAlCYm4pzYcboBFr7hXKxj37/s3NITYC38CJGnzeYsfAw
scmy/Kgyup4YuHSQv84xMe1OefC9kt58is0OtkrknIKaUoxMhadiFpZb1q6LCGjZCDLvEt+odanj
lAbrFQvutF6stxwxrtbyNkWlVefo5mHsHKUdUKlAbvmFrOD7Fueo7LxqJB0zuorR/CLPvR4mXC8v
eYVlWCltMSQ/Kv0nrnUmMks6Z+4DfifByU8oeEzGwwMQwV+wSiAMR8M+vT4PJPfbaiJXDMwgwMPF
MOzV2ChhYDM8gP3pzI1hh3AnXBLO4kaiLDH+tfeOEXlrLiXMjl1o0efl0QxZXEKGgtauRZJ6Ddau
NCX/CLPHsY9dzsdLmvh+W9IX5Sxh5toIvy+3shn+M2bfbLv6YF+xue0HWuUu0frpBaFuGJonMEjc
VBr8b+vAPMQznUXeuQD5D9+mZR0npdfWp/0FZlXzul44Gfzqm0jcRVkaE/MRVyxTtXkERcU9opyp
g2rhV/rRNnh5HtAG58DIqA8G/WV88e01SZ8AbkPCAkG4zk98ufWu1cfyU1u4etMIAlGk0Olp10y+
euBirD0faI7spMc48qrW9fcFBUcy0SDsImNOMh7GEiBcLcSJEazLplZZT8qKM3llBEFTSdZxKeFM
I9i9mpW4rikZJ3iJ2UZ0zyE1JRogxNEfwdlwM+ObfnMiAqNG9hwIoOIMuIoauwsqMta/CF9e19PL
RCU1P97jl0THaS+Er8LKn3rn+DDC4Rd8oFr4IIBTpZzeiD4B7TQYYrRINOswo7Mk0FqzZdtcycvb
4JeLF6gNX9uNiZutU10q9IliJ8+DlDdsAUYV9nxpmQ+B0BST4j5Pl8e+MPqf9q/IGzJgV98GIsdQ
0ixzuV/d7D1ljAOYSydLPPY9Ke4E5CRnvVDHgugIo/FAY2iX4kisLIpH/+0MwdztSacXlQORhYau
P3J4YxR+xGJhUSqDXyMGhGby8zc+P3qPloyW7kHjrMUSjCQGQyLXOn998edHyeEwn0PFKM7qxhyG
ToOEBw5AV6Cyd7oDf1krUh+R4JWPImbnqt/LiuwEKCufELwQAHkTwsmcoFL7KmMvpB/ftIkJdU3+
baGtWB2nkwbXeXvhJRuoiOu9fIQtwlKd52TmLNWjj8c6ta4z5veWvNKW6gk2aNcGJJSdKyaQ6obS
WXTehbRfGR5IS5EX7lx31rlZH1nJ/xP+HXvJ+sl8MJVJqomCnIud5FP0xymKvn1KZzCnGjNPdp4A
KRQOE2XvrWmfhTJuY5FwdA0y6xaUatmZ15M1gJ6IzHBv4uZSYwt5cfscLi/vigzpTtDWZxmK4IhH
4oPg1JnTM/2fuezUV9yvmraBJSc5gUqtFiUaU3VgIWfzGriTCLXYRErAl2fNj/qRkHBLmemyYX6Y
H+k9963AXvkKi7gsnhmgKiDl9kWrSx9MppikiT6w03Kf7GmwaTTi4vJEFbNFKNDajszqXlkI+xAM
ABQusSP0/DYPL+C4kVCIgFpUYwXdeN5SV5AH5Cyz+lk8u5C1NqxVtU9Xmk1K9tRum/bAQ8FYM6DD
uH1LSH8Uw50eTriBZpf/3P94MB54tjm2sk43UB66fdv8mQkfS+V0gy+AKzsQX1GlDClXpxKZn5pk
lZkYeTCUr26Y7mGL0O8mtwO8iZpBzVuRveLJMJjBL9KEccsg+lBHAj/GMeSbUdjkzhy+msrx0nUw
ftT0mqOvACQH6Y/qR9C1UoiLVUwq8dNoD1FrcdwYro0AaLEH4fRtqTVTy4jZ6Pns/Nc3+wXESeL0
9+lPoVLimM4VjfLwsbxhCOzaiHVAY/Jhg6x3jD5aTdR2FG0Lk8hItex1/VVJCM05HOcLaQnbV1l0
LfCxMwxaaEa2TDxpfyKzdT6vfaJ2N7NWCUoUskpllPmopD5unuVKDV4+8QalC2iqw/jxiLyQZifZ
V8Bn5wDFX6BrUTCnCGsQ/h/UR3tF1FtCNRFMzfOP3fivCG6l2NjFp/+d5mtn9evr8HbK7cd+Yjmg
WEhz5b49h3PuaVFhcLiPELSsw2+t2aZQMOosHDglsgjKs0hRc54D5IvwiMtV/x8PrYPO10PyME9G
B6UMSU49d+EPRkWTmANZ/wbTLoZ1sRTyBAo0zs+C+xBTw8+/TBC2wdDqlHbvTZNUQTFVsVhczN2N
ZJlLRf5ffk9lmOmYo5rnCDYx70yVbCdwe9/J1hazOiZbxfIXTk27Djjj+5g4TP935X/QbKp1266G
GTC1o/Ed3QCHjs1zwYKa02Skyrav8ebuTXQQ7+UcbKMzf8WgK5ABzMcMq7aiaSurhsh4Jn0MUKns
uhapplM+NgjMRmTWNnHniWfwUX0wOLWWHIWVKOm6QofYtGV38oweFcdWcBzpJYG6TLGiRG1WQnC4
DHDV3bq9YWcHO0fAy7FbqlbOSm1JJ/KzleZSr2uaZur9Mzh1JSiaB3063JyKknZJ0HfV4qBBKaKX
qTt6oJ8VC+w0YA0b94937wPCU4dxQJ5fIkzt2k/WtAVe16kpV1Fkor4bmjUzxJrdG5GBdOvYIYBf
16gh0E3UkJ5M9QzeWHsE5K79VG/U8yXg+I0ZzFWm9KF6W8so58xj7eFJ40KBWmoBYFj3Ssf831K9
ZK5GJc5Ce5Repd22Xs93sMbMNMMWGIjF7thHd3zQ/IgV3XfCWrVsEPJ4CM3VWktN+sBRVp2DCpzb
k/aT7SK8Rm8VUUbB6e/ms9AOrr1ianDGz+CZvZ9AXHw7B215R0QlSisgA7uLubRZNkjLfuWGvjjK
sJGyMiaEt2zIL38CdgcgZZ629qBmR2u0qKajgnNpBa5K8LliYSQ5lDYinmqy7BA/69b3LNCMuQq2
ZRwdVDzY8NVez3kL2kc5FIqF+Afr2AuHtoDKgQdaI/yiO/ysev9Oh4pvv7O/FtF0FligDCHhbuHI
3IU3SvpQK1wHxhfMRX2zFWft9IL0N93ST9xjw+wpKGU3sPklWrqkuIagNMYv1vXo48soyaSHldUW
lB53HXjTHIu6+d6kXlAywHl7ORUa0sZsHaqygWY8S18yWro7tRueAI/+sew5C54i6X9qQdl5FTH3
SLi//siMdqZBJtjYvYedOizNNxPoPcCtB6+8m5INNCoKbsKo+LCi4ptjxByHVMpaxNU0R8Fp5Gsf
yDjwzsUABmhfMG/G9jgPo6WMmU/3NWXSwrFD2gJKmFxbHw64qfYWwQLZWPI2H8YzJ27lkkW1m7ad
REcAHfH+f78kE8anZbfRKbpPN6HYnvWRXZSE3EBBZnbaXSOvTeImoZcMlvNA2B68aLezEw2yqxFT
Qb57m6P7Ai1+EilVYGQUOg9r1/8iT2KEOEHgQI76YH29rl4E6u2GAI/iO5njC7eFrsgwJKKwHNho
s/570PVtX7ZxMR0kW1qoVN96vuqeZ5YZM2rq3U8UvsFSENuxOrM+Ns0KTIuy8qFwKkNxccPSrZy+
e+rScaZlnXvIDneGxPcG2K3kJJslXi+buM7AvAgSx6i/617IWEJiVwBLwSXANpD75GEe+5gDy0Gb
B/4G57fxf2NlTFLTevW7vLkslCjn+kDjrsKArKIYWoxnUNPXFdApewObJx6+xw3aUjjOmodPnsdH
XixhCZKYtaiXfQPDKXYz4zvxLdi3PaEjH6dZTfpUDqRSzEMql4WPcLblDsymprimnKWujXbGIgeh
ODjUg2U2FFf5zg524YjUn2vATci9pV/EdJcwr7ttLRAN+xttqICMPgk4QE1Ixz3ZEiDe+TUXbaAS
isN9DhCwXBNAJJOQAUmKV9dnGZq+0Q2/cJcbQQIQztAHswIQTAKyMNNXiA+FzDa4YUg4y2Ie3p39
sRIDEizbJVj0EbhfBCB5bMvveqhbcd48vBJFt3kuSeEfsWfuMi5/waKsN14vACpy0hFQ+Jl7rbfq
85xLNDHBu8VMN9LrzVjxJ0Af7aKFF1FRqNPSNh+GImawY88WK1wbLz/yanKJ0AFy0eBGnudXNkkV
f9JSBRIf/qDLFrXmNdM3UqUd9g22694DpGbt80K2HIRgrzXpL+oGqsWC+qzhZWUt4V9nhwWlm6i0
SY75v2trII2eq2/tPcXCtSddkThXb4eLOtLOXF6s+MXkcT7xzkHLSFMG1CTTuwWjTkKnkKAN+8U/
IteCUosggmTrAntzQV9qHSkA4bzL3ihcCII1foVNZ2jtRJ66H/WxD6TaJI/2RwFOuTzg3cAviSiS
xQsEmagB1cS12Hk7vL0CFQiCLnEs+S/BvEa6MgELnP9NXIUi0xMRj4VUWc3tEbD34n+SC5daNblX
MpwKqW7bQ5HoUW9Lyav/QBQf0oGe6nlQOPFNke8VOrozRixGly//IpjeUgm9FkQf1hZ4OsHnsfux
3VnaYD1qB2Cip7wXtXYiaJ6zLWZY1pz/JzjkgJ0imM1a9rQ4TDixdBNxs3ATdJYe44pJ+W3X9T7e
6DlT+1p6hnQycdW3z/gkGOHA1dtWmidEfmTbm1pz4CybsNqX16LBCLKxoLe+lfPPn0LQgJ1Wyucc
DLAD+Io+/Uf6TQfHW5j0HiemqvwVualGMvCpJ6BYLQo1MDxGyMsZtQMzqks9Y8+yui9snhbA4eik
6n/EpCHQO/0UGzeaeyAbyGyzeecG90+dEhizuPmx+nKY+IuXOt8eOwaVH6DNsgvq8weZL0JvekNa
Me3aj1qwancliyEWcrc+kFQEoKIEK58MJqK/+E5gcMTIdZVm7/uXzdC/4sHqoqHMB72AmSbvyFAH
rK9Un3Nbes07VZ6HKxfK1+mrjXPe8U1owLjOOisht3Y1qvl/1qSl0v/hOQ10qdkiC41qUuFLxBt2
NLOKggXP7gVBKyQBpAXoOu9T1T6d0tc83NZxPvU4a8l7y2joVjHG+BNgaL4sqJWe+qglUFicWecy
hIxKi8XUQP3V1G0HOIfD6BhpUet6YsV0jSk6v8w7vvzvg9D2rikMIrtA/DYBCfyGNHMqN07MS72s
ubBMWAfe/RZvCwWfdihH39vK4Xd4gF3mtEWvKGvOBSs9T8/AXDVQdYErGTmk9sR/+A2ReMmxnHMs
xygQZtJ+E2X2f+x1f/nKQbrWDNk1iM8gfoYkQ3CmGdKlmSt/jWRB55TEE4v6mshp1qzmIGbsN9O0
RU0nR9HuwZ8u6lmYxfwaePdjX66P8uwzKSylRp/6lIxM51pGdTuhn+K8thu1C1O8ThzR7I0dxOlU
OVuUtWoxaICpAJKb9ufg8W/LmaHyuskqvUvoMX7M73S/u5Ditj82EvyWwckzczrMdwEGn+zyBJl9
uAXlLVA+50iB/EnC9DOhKP5c2K4ltfqcFTZChNYBMCcwuSd/Kqb8bmfyGyO6Y611a2vc3yN6VPI6
SiaXSYS2N3KDgI/bFIZyGAps89WYsUjB+A4O6dJaKzRF3iJI1b3rpSKZVfv/dpwcQmGbsP42eWvi
tgqyR9gzi2J3vBRtPRpIZHhSIV7zBCIlJbjy9XzU+65o3k2cnyk9m7mIO9sF1GEMnvg0BQT63O+P
HZjY+QNaxNFvWx+Nyvn+WANR8u+LntRMlUB/CvFObAaq3kdAx8ttoi9e0yg6GwYm89+WQe931Ce7
kanHsZEIjYvVRI1Cm1wzVYcgpx+sHuilrrFEX89GWzHSx24JKgtSEA77Bb0Kd/CwJGVCQkqlcgqo
B5l1E8SmCpXF/ReYZvDjU/dxns3c0WhtOgSozVjtsyghnpD4HJZku1jTCJVRvNp7ok+Zpts7Txjs
zzLGKbe96aV0T9sebVzvuaacRoRrdUgPf1Ma3ZsTSOO7wJB9MquJIvWUpLv4sL2zy9Uv1/aAKsh3
6LyY8M2rPcU/dAiocBiZ6WHY0TPimI/34nKhr/RvubaADjA3/L7YlnBIV/7Gcj716nbqyLkxaRO2
q55PECHCNQuqfbs2IM0OG8r53eSxlCTrzegKyNVgNA0t+8mhYk6zGyNy54nEViCcrUy+SJ3HRNEU
uQjkVhrJo4Eihvor7PtCrhfWKtCYqI45m1XHI1GVWtugWHVqr7Sso1beYmb7sM+8t+5lxoG8xxn6
JbshzIpDmwXwt3BVwiHiw0sj5MtOJkUnNP3wDgWI3oM4kz8iEMZxvlaFp469GIHmdwiJ3LVYvmn7
oI4q5Oiua7DpwXA4t3ksRuaXOMkaNRldt3/EX2YoN+HQ0ZZhuWWYyX2QGrTDGNjdXd/iUjOMkwUO
aXybfZtepH9L64xZ/Mo15d8hQvVpJvuD+Vm1v615Cns+WM52FvL7Yxeb2KGFncAtY9s8Nr+AGt+X
AaLL1wPC+HvWw1D2uZFSYeAx/2EuPCClCbJ888R9WCfp78GMlUwBxWHoB1NH7tRMUhMpDgv9TuF4
Au92RYo0ybHLCk+8sdyNBKuDsEdnprKsTsYiagRjaqvQH8OP5Isqi1V8npawGTEdt5iIqHahuysQ
bwp4Bb1K3zecIWxRw6CWnspiFfnQ3I1hBwPEbqRtOz4wjlrEElWwqmOGdk1BQSuD8g740/w1JWA9
jOC5GrhciRos2uvAbPoAhLHIlLRulT5TfNK2/Vzz0OgKFxigJCtpAIIyVMxUlsjUSsIoD26TcO6F
LFhmhegGADc3Paf0WzDaW4tRrQzcZpcOc09zCH8Wr68DVmTN73dgKPIEBSh5XVgpmip3HBGhZ5Nk
H9gmScBC6vAGOWrnmVjKqk8r3LdDS9l4XWPljOm5QpG+xnqychpqKRzhFMR9XmRyKTecR6h16pgp
vDw0hXbvTpT9/0sTxLl+QmKDQso2N2YZ5GAOtJclIieDK0LI035nUrWfj/jFw/0FSn02ITMEnXcY
r/lxBlIdTHRj+lb8FeehYZrrFre1SwZQ/DtZmsW8j0U3uPwe+vY6R+1LInpDnWzdRMoc2q1dd8g4
TngPGQsHPZp6DIJ3aQybfwcWXNTtCnwXf+dujUiFaT5+K43BflC2lL2DG5FkqoY9XIkfhNfbZ5Rs
P7LQk+YL9J4RlrNsRN1KM/3ZnjSg5BB55QP8i+WKr376l9Lu+A0R+L1XRTATR+y6OGRuCYOLcQBw
bLDxSqNoHHSs5x/OB7zb6YEEWn3qy0TJOO/m7S7lDNmOmo5lyESalGZAsLPdMeTeXaH+neYUUmYS
wG3MLQzxoN7IWaNBWYyY7z//1oIyY38ZLTJchbImAUEVW+ZwZllfMLE25chT56Ukb++NiajZ+cF3
6Gh8XGst2TstfQfK+eTUBv35ek8XiSAgrFISW3n+YQvnY/MCY8BCElMdWdZfpzPYUGTFcpZsy8f4
ZGhwFC/gqKuUJGZtWmKeY79VkCSKf8WQYDqRK+HV7vQ8Pf2EulNtMqd0WkIXcbiQfprreOI+BInf
zOKKOfoB2E0cQNe0CLVg7AyKi6phICKVEZ48yw3yOYgXM+qJu1ZZeuOuOfWhJp96D/DQYLYhV0jv
LQyFGMw38FHCYQHqc210puwuBybmMFy9m53613ERow9SN7MjHEc/1DC7Zipe/cVvO0XCkaRhFOCq
kRcjTTy9N697aGyE6vYQjyfdHeQSE/KcdJyXIJzo2wSBGUsgJtMsiUxjA8i+qn47mUR3DtEW6WIQ
dVVk1RYllHbL3zRKRAmx4Dge/PcsN5HSex8W33gJscBqWIIc7l42xfRbckY2jQQ9t/gFosyqOQKz
CDzReGauTF9n7U+g5zF81t2LwxUnuc0PFxMt5xOw+NJkUH0LUltH3ui8/jHfSjS3/V51T3PIrO8Q
n1XqakyMaWHF+pu1IEvfFV/NQosTL8L7yeYxbs7qcJpbFzV/GlKlLDcY0iJJ/f+60ohyHPVpTu7/
wu0BYNRQfQlVAU3IELYHSbPuTMwj2Aw25I4I7wXklHWI2L2NWG+483RSqoHaWU241KtyBkAbK/a1
qSG46XBIoNIoVv4aiP3Om2AkKbY2vLUVb8EZq97U6y6sZn+CNe5R9locuuDYrfDVPP4DFfhqAShP
Q8yUg1k5gk+6a7u7DQqZNncxyJWeL/EbLfZDwsdScVvh0YwuGCrFzk7hSrKspF6dsXDC9DMX7Vki
TcVvloBGYzDeH3a3MXXU2CCFN+No8XAk/kdnr00raB1h0LzD9+VGbBHoER4UikrZDoyFrHxYChi0
D3q/yZNhgy+BGlWRxj36dKLRCu4X0qvIAl1eU9PdwCrGNcQeZ3V/naZmqZ1MpSIA8w7Mn2+Rpg48
of/Bedd1dQsmOdgvQ6KgYWNk7D6nEt7O670MhQam5sZJd+aBXedt2i1febRet5bWfG0tfF0sqwEl
xTH+QQZD8G0KDgI4q81AQA0TkZiwEeCxMOcaPuqcHPwr1XwBjxqo87OY8B300iyjr5CZq5gAS8H7
FIFroPV9PxBVh+pO0oj0s5ehU9A7W1RocF99zEBCa7ewfqDBelgMDfuPP6KOTY3z11tP9XbAbp53
3J0JAWXfsT3kBoBB94IGnEc74vR6hMJW9LREjaP4xUyHwffrdMdqxvHwoBk89QadNsGw91IpjuZG
YDNaMRon9wWxwCSoL+gfOa+thzd3AIkr/yry8bmYIzjgPftFdTcWUXKcO2o9yznbVp6llhznOy+d
yCezPTbCeYjM50bNf62DBeWQmSpcoYISR5Gdn6ozTlzmD9XqPJOHRqaXaVdO/tqqIF2JSpC0K0zU
Yq1zC+fDW07EE0qs30v/0Wzja6TaHPEQerFw8kYLDXW0dP7TG7gSVuAols7k/0YwBSX5pHbx2UKv
DOnp/cNMbLsyv79D9P8z15O0ZZd+vh7nQHOrPdrXkkf5tWeDw6FwSnHB4XUXvVWWhQXghfMwd7HQ
sXZvJ7vKzQ9hKGaNn4nyfE3qAo8J+o8PCEtONG1++CXcq5TR5MzcQAHJA4A1g9uimXefaRVPE7s1
umXtE/GCS3Ej/1mcXaQAg5pk/ZXHWwYHXU+G6YVmeiuKhKqhCElp3cGL5VeNKF+a3j6sx6BFRz9n
zHiTHSG89qOwQzJf3YbGV2N7EfimujHnLsjOg8OeUcicfO5iUveVXPf5QUAQSDBBC0jCjfUGaZHW
tyUd8vgo6vaJjanYYxpX7kD0/nESv8omKxC5XMfZeZdRU7fffp5PVD2Ffg/N+O5Q2pc0AKFYOz62
I5blK/k1eajvu8vKdVsK2X84UGDLDTwI4psJYB3UxZiNlbv36RrEKvGPr0tUBltCKIN8CxueaW7x
WewLwnwwQiONF1N1ccdpXmfrrW0xOYORK4+hfci30HHbT9uWFIXssgCQ3AKxqnb5+4LwX5PkhnQw
ULaGMNRxdsMBOaRteqKior4W1nJ9gSXKcpKgf31Mk4NTLK/NPRpiM3GvfmemotsCE8suQQRt4kUp
hEF+vpr/1jNjRDu0mVM3INcUfh1dPEW33gNp15kOzT5/1wNBSna8sfuqDhyPTXizUvx5cQEzvAjL
1wjDqsLBqWW3VotfDAyUqbEbZRnceMzYDdog2M/twxWg+t+g+BHCm8xrEyawdklXbT/1ilh5GLHh
TxkOwkg3TBF6JE6ybUiExbVwgHA3K30zkXRqng2PVVjIM+i9fkjIL2EKQcCbQZy+ZDqF65ceK+YR
hEqejNEgnGcW1mYyZf8Q5tH99Ww9EJPDWjxzwB7ym5Eo4ikHqbkDH15RmTcRNKMUaH52zA/LbZ/L
NrFUaEx1UXbax1SNSo8bg1iJW5BTsuY5I+DhSkOOvYWWGIsNTRN2Rkp5ksk53UN3k0W0UosYtdKI
krpkX0cZcuPyBCbOT3bclZsILwigHZwxgPqsUYipMl82vkLWEDxTg7iqRfG3OmbeC0HkDw2shKDo
RoN8KzHCQD/O7cXYgXK71HtyE84hOQnUWcg2TnF2HnJEEYd0l73QBY4ERQql2SifHJe4mVBqgrTZ
zzuH3XsI/NfM5Dl1Uf59KqvByUHQXO+Bm4Wkv2XVnZpsZeuIROFGD8FZxW/Z2y1ZnTRjEhfZ7T85
RRL9ws9+aNmJMFZCPq6ZOUrPUxZS+a3G3eo6SqaIzuxutf5lAEk5chUyK3hbfwfepnXFtKSqDO7l
vV/7U9uYlpaAbdV5oClH1XqFxRy+VFttEUD/uzzgFb/COVuhtsOHsE88KxO9P6lGjQficAUq2TjV
pZLQH4xjqM+6BjKoN4ZM3RYvOsJldTfrt/ifQ1IyA0qpMW9QwK/hY5pi8Yzz2fUG59WigcMnXn+i
8G1cG/9sQS6dq7cUzdA9UENqeUhA4IgHj66ZBqc/DE8tChEUPK80WRfRTwM6/JwqHefFzfoH5oGx
t4OgJAgNMcV0Ml9o50N63VKjJ4FyIWmbNMooIFJdTDPE9JQU2BJ9/a9eWHH/0r3yFFeq3kbDeJ9o
cBN89sBEwggWPFObmwJ1T1WqSr2tEcgUlKGcDHXj+J0Bd+Wax3JzrWqXPeQoUNxSOEE2IRFKJvlR
iYEb5Nw4O1PbZjS2K5620EA39bROrtflp+Ch5wsfb1HmM6+qsB0bvdxHQYq7Ss77rXdBqBhl4XLr
t2idzDsuyMK7K0fRvk9sB2+bgsyQqNDp2u/dQelQyE5eoQp+T4ZcGqWuMxpNopWmu2s/m1QbmBiQ
uYSRQ+s9k9+fRb5Kelumsh18tsXIYwSkJAI0l8ICsXKgqRYLWhi7Q2oXkzDewpASeIRgsodq5jzp
xIo/SwAbT2SOLMeXGgrxhiaUd3MfrXjVUsT6u3M1Q8nPUly0R7QcMhJFWLh/AbPXzMFT5qTbRp6p
xlEGunyPHQGuShOOWcDF+uYCDw77ADQ+xc4xb3GHKjoc4uSE1domhAwnsWhTp1IDL86WsbUvvW46
RWYxXJaFhyAJvlCqEB79X2GKMO6ObeII+Hpd220atQyDJTbB19g7b+mwbppfZ0TS2IRLr1jiIgO0
ohTZc8j9Ni68Z39JSmvmqbRkv7++vtZ9tPYGnfD7gxzacWUkCW4yTWuZIZ3edcu1Q3EwnzJ+1jUz
cqmKdDJmWZxSSGMOQIurAjTD4XvNCw/wBKw5hQbbdQ81MDSpUfa2RMv0UPgfJo8mUNc8imf8T3nq
c8iJ1Pxhxjb9wbxkAfMLQsYH8m8z5BlV2cRNbDV6S89QRoX/cDqgQZGblf+Zqgojev9SdfSUbyZz
QIhDaQhMjnT5C2mbi6YuJQgqVaQhC7pb7zleCRdKEyyyNi3WMnizEu9ZO/ndLu0QLxMDljC30R8P
NgzN9QBnlgF2SBwejc8A30N97wETnJT0T2DXCLqqVKOYEQ7FWroFND60/AqyLJ8/P0uXku7xWRWK
d9zFR3JkOPIhU8yMmE2IJwtSOiNx1KpTjB7nI8wof6yLy91IvJSn7n42hl/AC3uTb1ddjM5GkeHq
uc0Kn8TluTgzdBlElRFrIY8bL18kBdX6H5RLb1m77uQvFUjuSTi9s8zC392ebmZ74pxbsangvh6C
XqELdecoJOUWfw8TS0DLzEZm+G/30zyRBafAwe+CT1H8DWzyqQdseUtjYZt5TdrZXToKbE9+K6Fv
2Lkf2hh14PxSE9jXw3W3oK0yVckK/JT1KZr5YeB35JB5xtZ3DovwTjd4/Ll4D2xNqNUp58rOa5bn
c/kx7GsGR90rczQSVUdwCfaSW2YHw3EZSQyLpxCl+VrWMBgnwWhL13hW325MCPOPx5ZSAe3TQ9x1
PnyU1IJjzlyH7BZVDUuj/yM093jYZgvwivShPWnoL0mlWYPWAXsCKL3/AHEq3PqNvv7WFmqfEgs6
mnsqSkUO6P5+AqHlCzImLb9dKO/deRTkcRJkSTYBdD0nB2zy+GDgNRDUljyehdmTJbFLhYbo7kdd
wUNeqW1vAsvrHzxTlwsEx2E2SCf1ZZ6xYTUqQhRVpOgkccNhqsYjQlH9akl3V8yzxGBm92SS4BHQ
V/30m3JO4NGo5qZxV9OAnI04LJEQA0yAGtOJCW9cu9X1CGPx4Rb6qDh3uPVpO+YIMdlTXJ8E5Omv
er5Iev/2hn6bcEhxTdpDPquiI5aunwHQ3vNSH6/YvU2w/OjWHr6spgscjRLftCaI4i3urWWk6pZD
UuxjC6/yiSAhbeQ+z5XKD/P3ylzfETA4vJCexMvtSWIWVfhnbcibkhyqYcPnvkdsBwd5OwR3W12G
gQJF0IUoMCoJEwKg+c05k76Qb39cMbu7LU1MM/JUeaKZikSE2vAHzonpIjty7sQ9UepQr7o8vhmS
L8AsjQd0TzM/J1TCOyzRWHaSvfeXRKtjTSwLRBs/LfGN1E/54ojJckAlONGYi2bupmLvrqiuEJB/
qPCyzDVHIMp2HddelD+3ISc1j+slgo+AHB+/BO3oUrZZXSaPe0LzibjziSt5jKaE2JMcfaK8HKs6
iwW/HzTa/NTJHwIDsDzi9IrsTbTbnDd2tKfhEg4HEpYJjcWNapAxKL2YNtEaK6ZFgELofoO3NjTp
OKp72JFKObsYFM+iXBrIuBYXBQbArdDEm1rdpMHT8+yCwwbQmcn4ZOOf3czMcQq0sr13b4ZBh9CX
ifv9TKty+R+zCOUs/GtkrfyPISUU4k90DwJl9WKIaSXxMOncEHLzdYUdgzoeBCbHMO138/fB89u6
s/LAfEh2biBcETPZPojfO4iW++xNqGHSLXfPJ4mpP23oBudzHgX92HBSe3pc3XeJz7/J+IiKfNJP
gMBFtY6xoUxPOWfS9AOoXRa+MdyuRlGCgMnPO/qgLeFhNaMSoOfEUYVebR6NLZFltGEP9GHY+ZBn
LywGFgVMQARu/SVSo+DZG0VFm/hgJFDet8wrWOVQf8IW3Z69zhEjE9HdgT2urE98mMWjRuoeEoDU
giZf3U4AgkwqQ+V1rQ5u1WyWUPSE0xRVrRRxLHiwdy6xDM2FKo4Z/os7LVIseze5V2GJsV35sUVs
BxvFImvRgnziBO8wayzbIoQKiu6XHABrm52tW1RDrYwbJMyqZBjpMtzF7HB0aL22U5zu25s46Paf
+sg6Ss6r0WR8VgqzkCeabeAUsY5N1vD5xVvjqG5FgliMUiSQsE8/kJ92+h70WrtSw9weQKhMP4Iq
PPbD1U8eVj3BS2Qy3EsKUPAzhjdh7ToakgTN063E+fd9V7+SY0+w8oeW8+V/UAQ0w+DOroUP9YZQ
VrMO8W5rRQEIm/TS9kUGCJ0kEE1aQ94/arlHhGu/IbZmIFhEuuKxH073mfhQoLXMt+87ihraMbNW
Yz70b1DzenIskCFyqZUHpiRIRno+hFCidbvCu/uErWFEKSRXhjkRE5hrmr5f8niGZ7yJlszhvUO3
CmcJcYF1mCbDJvhIrh/Rm6ayCAbKlvkWHhZI6BXrry9RUrtJNGoeEfoSVB8jJ06qnfKcsGoYDIb0
zP+upLpVn9w2iErhrj/HSMXO5o7KgWHKkXo47dVSppsTOfklo0m7yd+LaZBQkVXg3DJwyJUltY2c
GGK5s1ZknhVkSyEoqVTzf+blZym/FkbH7uEdH6ZTxmAeKoKuJPBB/UkDpqfY/Cm/5KQgC/Hve/OQ
dJeo200KlOubT0wBRK52ILUnsXMFIPT1NEt7u7zAT5jK9fsDJK7aNaB7WyBDgG8Hp8QpHQ2exSV/
hz+2T9lzf0zLOzEY6gU0zuiRb1LfvwlryHXTtr0gCf8fGkeDkJDDcpnzxnJkZ4m7YYRnDIxrreeE
WHHaKA+WI99sIRSKDng/NHSTPwFMn6xVdByIQ4SHKrR6mjFrG11ugkjAGufjDRp1XyS+w9llI7c/
qVgwsXYD2Kd+ti7VO0Q4zcjbDv6iGY7bLlTaM3D1F1LAdHj6tdV0HqLhLohjgomIy7/mA2ReEpvw
apzEfK4T5kEHOyp4dl1cpSraMOEzla96qTEH87h9U6eMu6b8aUSHJVARS93x1bzppP85YhHmxVlh
nWLruL+XCbrZxspFTE8g0taaAdqPESd1Un1lCRQu6no1h6G8xHfDv8ostc7A7lzsaFfxx5yuT3BR
wMzQ34JSe+AkAFQIyVIT087HWeTnWcAZhEs7fh99RxxsXknLLKZyZCP6bUpMxjIXbqal8l9BQy2A
Vxrj74DCVBuf12dKdEPS8fERTpPX6aLvD0oeGfMCg+Z64SZ69pCBaVOkz9QU56xXcjC9gbeQlL9f
2hk9dmw30bhJCAtGn2SztVIfheLuR8d9Ka0CvAZW2btKyMoTN0R6hDv8Hm6bq4HO8D89pWBIOUzF
LUqSAIIgtWmRoi8AwsQGQBwtq22xdtCujXNmc+2nlRwJQG9iKgwgyZVsxuGpzku4Y5y9SA903juh
Db3QBYGQ5gFWNsinh1jEO3qxmZ7KGk4LGYOLqEAniWYd+lZtPMkKPICZTZ1bk8b55Sf0Ok6RxurI
t3nxCJ5Z1cG+oDWW+1E0R996E6MNuc/PqkmMpcgO+M6AN+Hv1jt3RA1kpC3yEzzwlYptbpB1q8hl
gDq8GpNEIECmpXCVWYVv7a56ja4YjTYuJay3EIVGMN0PDih5J3FeKHyx3tp7cvhFIlpSigHuUJCd
0ycKvFX65LOeUVzGUMQcXCKV8qHsZzi7/WUJVE4HZa7icNVgPeNXeVEjPiIQh32d2aQUj8ntq8CM
/CUszxjI979Alji3l0GBn5fLFBmPGdDf32OUEjnpOYe23mTuSQQRV2qsG8ZlUzLJ8iLrWc2DlrKK
yBd3IWt0nWQK49RnJC4MkRXBgU83jeKJQjlwYPsOHGrlTP8+MmOVPt+owM839kER95e/0M8q6luR
Hbdgf2VZ+Mis9idrau3BRX2Ldnci2Y5DcIQY38PRLMjean9N+YspMZbwfbYAN4qjSybZxmZ3sZsJ
nF9fur9jT49c2Iam8/Vm6agOyu/QvC9g2r1Onx+HbwcrQhZL/nJFpP06/cyIn+h2Vnc23mLW9u4d
sPVnCjXe7DSNaQk1DWifKj2hx8z2v/l4Gs5QBymOEFLLs4JBGqqLkEd/B9ya9dSuCCJ0Ux04sbj+
IUKjKtZNr50F29mhZUdMlMHGuqK4Hu+rpfgD6DwJFCwKF9zRVT/kBOj7EoChX2xH3gisWCYD2bwa
/IdlgdWSvnN3ecitFkyesbOulrAthPEbZNhC177EH+hrZ9+g2ono99LblVlON6qQGZuvK5HXYq4c
iVDqYILiWMepvsW9I6Rw7xG9Rov73e5aVV8nPn+vT7lwG4Pi3F6XViwNXgCEPttb25gkhyEW51F5
xIgX51fKzL6HWBO+Ff+jEc/GXHDrmkwUEAp4uQLm+/PhiAJlLzWkJNg4za3Hnm8gZNAq/3ArjEed
PsmcPcA78vqpTSHv91FgQZqvpjYiRBey6nUeo3afckVc/7Vbn89LGsOdxyOcmSK1B/tScdrA8dnr
zNtwZTb+SkHSNIvYshV6t76eqN8R3TgLXTIelBVXgiYA+uGJxoEXIKy+fMrE17+cx9NmDOq2dXo8
WKi+XWImPP6480X0uB75neHKfWV3dJtVJFrdwPubApDXVvIr67fO+pWpFKpuKNnPq+lb7ge642Qq
7JSE2QhPImQxJmTI8CnLxYC+erJQaf6ZtWyF0vspjp21MJY1QQrXFaILgtJhsxUeuybjVgGt2hfq
4nXdBi7vrYCyZyBJTw/sA4g52cGDb/JI3gJ7OzWkPxPILDC04y8qO4mlfarZvgNAhuXOMDvw+DDO
dVaG4UudnLeqZ38NAFLjWA4HdHvel3qXwMfv4PEto4XbZhIqNpqqcF5MFzXR6qsbKSbGRoLibo/w
4/uE7x6DVaWdULQ/4BIIQtvJPSbAP0wy6uzgWvj8I6WD3C5prI6B/bZ6cKTm9sZSy8Ybe2GM+ZE1
jDPBleVcPTg5BzMZl/ZaS0ip8O6jdFGrEHYrFB5BZpISdnu67TeodZcM7b4M8EqAgEhrNlwwq7Cn
FzyzV5JlpqXy6yp99JknB5aOHPaHYPxfsH1vebt4e2jxkBI7sWqvNlBC5IROfaABra1cesMvY9pC
7r2wEz6KccHcOTrdnFIW7eLYovc0QANMnDuSyu81ZW0sopsJu9jvNRbShot+y9NImnzWO0rafIuy
EqWF6G/+Y0b5yhYMRR4w5CRacyrFyVmMs7EdcazUDj6R7f/DnIvIl/SpCgDrZNqGtLj3J//bmbaF
pQ82ho377eHlpRiSqwQRxd5brLEFP9bGubUFLJ7397322NYXAoHervv2FlnzHnjfVTH5xynhXvoT
Uu0625TQYtrmToZFBF/NZYE9qAnajAUuQnO0Yrt+DB7ArAWna+NXLlrirCdcxd5Q70l+PoURuBNH
5+5Su2GfdzbdjcnDTyolhW1m8e/2rDB74I1ePBkUN6R5kyEwXn83FH4SzWeZGikXMgtIe0c4Ki2L
7RNx7nc4V8PL841tGyOIAS8eMTb4Yirg3R2sgRrSrcQXyrS2KJVJPBF88OZmvzbkW0ZU+Jayk+6F
i6P3mOHwYlJrEWKqVx1Tn7B0NG505PNw+Rf0hlM0qd2zDFnGK/ghalkl4uiAUX04a9tZe9zsE79T
26+K7Vn7JU3oyUzJI+oH4LW7SNdD6CECsxm0EPKqv6DLGdZWO6tjzxptjW6LrwGyIc5hGfl/ZYiU
QtDVMMBAGteCwArkyBHzg+D0n20zJW0q2jsxR167iCcx1LIdJ2v0DI489NwHZO9OtHfheTdEb4yJ
H/TJ4EE1rOhlhk3g4C5Q0bMozbl6HDVFOc4X4Z5tf88xeq6gSWrzxmHqP7H6afmCSG7RLdtA5cxg
Nu/qadIm++2B79p6HdkdlyTyhbNljEREEvk8DFu8u3Nx46Pjw5neXibUrAlsFPP7qCAg4loF2E+l
8EfHsGnGQVTvOqR0Ygfe8BbvujazVkNAW0GJtKGIBIkkMVAnnzqCXFNRruxU2ttdBsYslcmouLSt
oLpRyhJtMOWUit8JlvWDaCqCIEgZy6PSxQGs+bIzcwfZN/QFTFWQbvcjQgBk8JZguNegd9VQ03be
PoyPAIDJak0pdu0RLxSmGyANNQ00UkRnYe5LXCpaMDmlkFtlJys4Yj2tIHlK6gyyCTWAXtWUw/Wm
O2EhsUXPYOyX2ROYJUqzmPLlMAr5R/hAECKFnR/eMZ40FjWrSx9b9qzGpKMJsi3JP5q+94joHbIZ
65Dv54rb84GRLO2hScdKopwbRByJn7aEGoJKEyrHDO0ofDzfuVLwTma8CvKo8ZFVj4GZUObKWxb4
w3LqtCEm1afVrKt553x7J38haSb6Ta0ZkWSISGHZHVZIaB9C/MISKQdwq0rrCwbG28nwfZkYi0Vd
JVgr7V9j6mUDWAgFx9lTTFlsR2Yua8nG7i9zKcbWxIq1jMQmleYoL8uTfNSp/aDEXtzhbCvWcvRL
CAEATW/yBklyiSv2ykoF6wZkFB/xLt3SdSXii81ZK9EoUw7SgW7mcSzpb/XS8UKAneHjKUCx+OkS
sNeMODn0GrMlsFmr8GLzFEn7AzCK5Py1Xn1u8MLgk3LzwHNIRIjUOE3WkPRgDHBSVJL7AZbOOqrY
zuuUBZHX8Az5FY2y0/MjuZaXr9k8ySVkJ8pDs0tzbulmf1HxIQgah5YjxfeimwHrHCgEh3SkYh38
6iytrax2dlcyAg+QYIvH8IPoCw0EjQ1kKwbA56CvalJvGtO2KzXtvUbtF9GT5pzk5Rdpln1fnRJX
HJrGSW7MGp2KuvnrqIVKcfJrupVm8dojYQRWWhsxxSm5bUKSbY64J6D/+pjy8aW7A7nYpKTHpHn2
zgpzK4vFUMF4egV5j2j2Ip8rCDNAZgYHYS887kzXxv2ZXAUqUPzCEH/8LRaAbg1PUnXE6PdEEorn
03J6MSjbhyHanciM9svNWePKhkt3sImt/h+3QSAm/rGNCBy14cuzU6JzlgI1gD1kgF7FIYTWROjV
wQIL3GfIfh8TyJFM5/DHO5Jw6Ils6Qu9wGVRBu/+0Jhl+aJxqN5HIv3/6y7FnqRfPPIO6DUCJqJB
vGUqKd4hBl4pEHMxpvO9LsL5DMEranHj7ecqh80q+zrT28oYUzFD4ckOt85cH0CJl+gKpERTwxp7
4V1qFZdUnwujN5+SOjj4ikda0SIcD9+MthnyNNP8ME6cffFsRe9Dd/hnk4HhS0xFcYJ3KT5AIrYL
qDbtsts1sjxS9f0bTSZRkleZyiYO7M1h+iSKeWo67HbS7DOTqmruX2or9/8jGKhKQNeRURr/ys8D
Or1ZIwGZWoJl8F0BU+Khz6WAQcLCVcdBEF+dCrM7WL8UK0XaZmUpDO+s+pXu+LumVk28FPe9yTJr
fbq0r0wpcxu1SEvbUMX+25wkrR3EHNl8HL7EUlO9bDZoHBLQAvjC2B4tUjYzWwZ4SGnR7Fwoar8O
vPWn3MDxc8w+cb7aW0salX+3wpZLG2I4rxl9fZfpO0sH/uS49GnM5UQXMfgM8IKtimaDViYwm4vm
Vv7z7g4SA3+VJwEUq1ym2Rmvwb7JikwQCOJZEX/7apVI+bTZOAAwnNYONEZKy4tq+/ZSgaP6iGmD
9TACmV/8a8NVKAe51qCuztx9z2pN44XqKN6HfH0HiU0n3A7LwpFTNQYYuUc9kMRpcszDH8W14VlA
pLeEppVqzZdIGSez+eGkzsJhDy78XKC04UKmV6JYchMN8wA57kqSlhiSkMq7cq+bNOuMblG1pKW7
XFiKSI8pkR+aMUCvHiYmwrnRC1b75PNS0KZjH1lHZBJhRs1AqjhtGR9cju8HGbdRbs3vIas1umHY
zy7mUOIkkbIv7V286eICE+A7lgphI2Y+kwBr+j+HY1OxwItT5A6Rr7F+eQMed6W3RTI/cX+SYCck
f9A0gJWLsxqFHQrn57bcJzWgcFLkKtAOUJAG/fQ8PCSO7MbtyYuBqDxXlYrzwIvEkAxsO6ZDPGV4
CdY0e2gDoJy9oXkSFpHwV51r21JzJuzGK//V4vWbHBBU6ewpMFzF0H2OW5gYjfGREsVgoi5pex8K
E79mDJh+Ffs6n1Xuh2mXf6oxW5/Kv6N4M9mqMwLe97cVFC6CJp9z3cbSs7NSPvnQ2q7LeHWPJPLD
FkwrxRwjP9yJmLAljJoozrrnRbYGp8mn5AAdqaaJw+3q86f090R1tlQjjLD8S6CcHkh6aCeEgtkU
xu6OMKdfhl06rhepq505ikLm/7oE47+OKQzyO8AFaF9eprRgbSYV76K9sa+Me/P6PpJSAfcGntru
fBejl+Wx4EB0BHM0+0NXBTwi+SrQPNdpqk+/F6W7DX500juwcrZmaQBBuGaYQfcEGtmLdpRVSgDe
/VO4Utjkd7TAWu4Ws9M4B1CD6o3awWjisUpL8ICwUAS6duxx6MBy+Zga+dM/auEH8QhWvNP7sJtM
5/XZkMPtI4B0Mctqhhe1m2B+vprcpNi0X97Tilfvmu3AlW4SHvbC4Q6Ct0WJik5JtBaYz0zFwes1
XhWMUkEff6BgQmyIoCsIyryAPL74RmyKGM2Pef1Ra+UmzwdT5JEV7xeduZ0duQ1G0zgVgsw5p0Kk
HUDBKjP8DFMZ1d6p6/Zvxdb+H9RCNAV01a8eaLY/qXsr9WguP2errKL9Ji0e0gZUDluh0eiwSkYX
M/tDe2WF07f3waCCR9aiKOhF/XUC+nLQvFpnOaFIuvPRww1GIu7SpvYyJ0RQ+ccF6UUYzErsOItS
nXQ25/Kyn42H+ORWOXplVA9771owsZy0GVW3fDiOd1IdJernqdyt9uuwMdR3IVtp7cBi7tu3oxWj
06eOTJmXpuuCBvrFRNJikWjdhDrfMq29+vEL36l49CA8r33EXSo4vVKYVm+nHx/EdA+JfRHgTysH
rvKsDD50mGM7B+4JMdIxgOR+eVArVD3P25pVm5wsUUPKbIjUT5B4Hcb3YxTrkFVXKviVopV5eBq/
NCa7ngjlx3tUwYiHX9ni2vHHdb0BRfy+5sJgHOUAMrY3tiZnL3QtKCadB2EIcJpkUyFjs1TU/u5X
ReV+DVwEv3ocFl1rBEs0E6Lm4BUCRPN9mtIgW9OyqzjpSkLvCkPFB9KiiiQXgoF5jG6aDf6eT8lb
wQGXVrSYPL7iQMWWzjvZkxPCMdh6qRp+6ZbWl1Jb5ZFhWU9zfYuLNSyZbOlpdHZivO3/FDftgD74
7Qhr7tffYOWkWZ5paPVCBiMkOV3kHMfR+KJDw9DFu1Ez6v3MUDb5B1DNeCRjpzLbFcKHbZ+QJrff
usW6WXTL39E9hM9fYcpu53D/ga/xRRrY235viYDjWF3THk0InXY3uuBOZOxYv5xuRdfoymQvtP0C
7ZRh6ZS+J0JJOc4jc+rv2diqNdZxgLEsqFn9bc1UwaN7/rxTVlG4QAMkvHvbO+4LxG3awcL7dGtQ
XLlXtQBG9g9qpnpDijGpY+4RBBsTBSCgj6Ir3VG9A1PBalgQ2YHC67J2QoIjzU5jK6TP+rMLEQoo
QKOVrj2HBgxflPawPCP3Vj6eoAFQoPDqwZdwvTOlTuY72ppKkYy/iRwRx21mahpXGczbqmrn/RIB
L5IVrL6JtW3+oZWaMyHpjB57L8YIPERZqr8AqKBL8bdxQMnEL/bVtCIl27H8ChQqCZ8zGDsoTml5
s9LS3A2ah7mYayf7XBgxOjdorJiCq2SDP7wbi1rBqj5GU2KQev3I2TD8maL76Hh6krQ/4uMMFkVz
521NuK2++frAWNsOd8Vk/1dc8sXfxS6hxbdZhAgtAavGP5uNi51dc2/cmgJHjQfB8Y94KGLWfHsg
rpV92gyJuii15v2DxzEqGmYK4tigg4ApXkd6KJvy/xtsyM2l6JyYzMxXotiKbmmgsLxhHiIyxxVw
KHQXAlJrqurFp+9+mjwFljCORv86eb8HUHhvkm5FA/k9VDYfZGkJWaojuoEnH0H/eyjBjbnv+qrt
zytzASDEimCQ2EBkMKNTbyDWmJylrAPZVf0k8eVEbVw71xOaX0HOi3yQcwUC2lTJXhepUBVjNBn+
aUHcI/3Y5mdzFlU2pwQZpoAxquO7IznSk9nueb4Vwr0HquQ7S2fuvmG9w8X1a442ypG7Rfus6/yw
7ZMeigw6hGQLMa7RiSZcXfLK8haxTdts1amRpXAtgriS6C8OVtyyxW7CFqseGb0Szfw307fc6ARf
v6qEZzeFQK6w58DNb2Eg8IlmLqtwo5JTZJwHYtAbyIrHM8oPB8FYVZLqVpCHXSLjukTMiJzFKMIN
WYC4FdpBUE6GACjsQ4I8DsF+hYEfEvVjSjoj++vlKyBUbfXL7cvRhKz31wr3UEgZuUBxMuSbWCAX
/hJNh3g591J1eJP68vTSNNHlGZayHvhDeCfmcSpoh5cBjXCvQ85CqKooq4RK77GPYWRmw5fnuVdN
uKVRRpKJVGWHAeVAMudKChP5kAA1EisvGs9NvKCzK/kiyD9p3lzOAdPztoJEQo5sWhzIuBWBvnAA
VnKqbYTNWunmmU8Y21MuWX2ACseraydNO1soGQuKk2A47LY+gmAjieTjDC5L+fpQST8klN5PmEIb
NDiEpWh54pUsLp8QtBS8MbfS2vD7ykJYWH/bjMvyZTrF7nFrKxi/Gs+rB5o2k9inrzlBe+vMdDS3
AQphoHPguBu6EY269aeWHWSzD/HK2FVi0OA7G0cYIO5n5HAjHd7l57PdkMRlnypxyLYSkTVyQ6OZ
y490K/HtyOICgEuGQTUXUYvw4y2E2XiPHjo3bw7Oxi1yfRDSFagzeVCIAUDkbrDprMxM7xD1u9ZU
hCblHJ9kAAi8LTSEUkwFtq9z1zKlFsJ8rK21jk3xYLw9/VajrBYyvT7HRO0lRZfSMMzifsbsNPif
XZnScqraYXQy2cTLGTEfO+TmwvHCFmQqSUyyVi3xLs4oASqe+P7VU+xGx0PsspzrLt5ZoJceL9Nf
iwnUv8GQizNAF6NQ6wY82jeFi2FbPXXs6/PBx2eTLjUGdf7rLNLsPwA3o5qAD5LnFaCkUQXJwDPd
YUiEkCcyoRakoOCq3alggZ9oXzkKm4sOxXzo5I2uFDO232kdU8m+C7rm3idk1jIwcv8ZBa8N2eu4
qusKZuxF4yp6kjVbJ9iWYDua/F2iHANOwMmpa9h3jOLPHUzkG6xOsj+FCBqB2pui8cTTpno8Tqd4
/nZx/m69L/XaMKvntooKJsOWnvwtSxf3to8YC4Y5N8E15Scv2SHqNmeGgq3u6RgeTuRezKDhRENA
qC2pWgSRL2aLq//rHotNQIKm3ERrabzFSUZKk3jGBvkjgcu3jay80K+351P5Io27KnctpCbcn9Vp
dkAsakRe+b4zk7sYatxjzsLEmbvh/2Lha4mou7gZkTLghiwENqUXgg8hiCXs+hwB4NS2xlt1iLEO
wJS4a6lSFZ1EhlGVDaHC8YFSeWNqHMViivYQe0mdIXwhkNm3CBat2Dmzzp+OCej2xL67iDylAoFu
oU7ydiEigpT5zURI6S0hYr51vgaSmVqNs1p3+S85s+rvQN4m52lCqfkKrHe/eXs22bLb+y4gkIUZ
p3P9f+caxDrBufD4HDrvRuCf5LCjgSpUK7PweOKIXLcnaTntBzyks35cS49mDsOYVt6msH2No81L
hGqhQEpBWnFwrphtYAlJsAD41nPQJ0Up4AdHvUDgCtiD/V03aMDIi2E7qdqizfzi53bPReqBWeV7
409Nr+qwouNi5S98GAW48EArLxfWiTd/HwLacUUYKp89GREBqiPuRI61x/icm405kPwZ6VCiW/pA
UiOI2pVffd1GQcUucLhqaqTRCjIJCfUWwaf01j1TDaa4aZ7ahe1Fs/LDX9q2ATPrm2N+Rm51KeC5
J3zvx702A2GoRt4t87nIBYkzdDqrCGg9/zGm7cFMCBEXWJ8hgkp9DZZ2/XJbHL2KVTs/4LsZOWh/
8CdjumZ9V+AgzvlpDB1RDITA14TMLt3jZUF+dxN8+KCL4L3Oyg/txLzC9PQ4cCgpfNQ/22vrCPRk
HaTjkM0hGKzb6jQNuFBxIWwpRmZbrb6JIqBBjMjylKowa2CDxKef6LF9wVXdHtOARpKOfHZyrhz4
Ec1vysDjUJDD4grK/u8NzNNj2z7DB0BzuzZfxmV09eVs6S/0cVxdqkr6V9jbNMbzJiCA7iTtp2Qt
1NTBtmIBqpCSTTbtBRfgHdG/3U8cX5Rum7UpX/AxWxkzTphVEoY6dNVLfHw4fEDRiXrlkc4JcHgw
drnUJiMCHnQ8ozjcRty6NBuLltTHUpz/WvXe8192OWZsc3twsnRk7G49pYBPMp1iePffu05NuEYR
RTLVF/TpaSu3N6ZEZ6PHX/vET/keNYptjmDOPa6+v5BoIX4Yn8RL2Sed2hRFxlLTSnYxH/XKvAfI
O0wA0NZWhjS0a9FC18ljsiBC1N7Uk7+fXmI99uSHiP8xxOERSLhxCFmeDcUFZXr22H2Q5PkvVjJy
gjHouU7zkgmhennOfe88j1kKknn9+YtNKmH824+9vwz5fp4vdniHs6pPtMutSrNuA7p/dTCzN9KB
KIVVGBeDXbn3EieFwJtx+7ndU/y3OZerqOhZFrg6cuciOEWrQW8m2Jok5ta43gUFvozqlfxbwD09
K5aaxEUqsgYJPqvcn249h/zBPB5lsFcTFy01HSxA9kH7Zwk+PYdAFzZvSmE6OsnA+NjsYtEOWbSq
0l8tGt+rhch3ZTapeHwL1eGE6EZYTWvKlEqYkSaJqrCCbAEG3S9s2NkDfaCNAWu52XHqlWS4HyX2
OeMRpgWoFsofnsRC4DLeAhIwh5yrpgBIGDopCqIX4rSrPVm8YWWgA6tMbeb4wHfEuRuZmdP/YNGY
jjw9eqFnk3r01jAo7hkUzHOgjPQ4pwrlTI5MlJevzjqKKZCuzsinQ0+NIGLg7WUEHw/wwCSU4ypm
QWH5yf1YZqSCocvyRqjlabnDsu4IRHG9Q+uSF1mTE2zwUoczcLRccZk1EDjDuL5r3J/qKXiPl5Rp
bRO0AB0VEO0qncvDb683ua/zr6QdAd/8SM7ZZEeoQlRzoqzJLXE6xQdwOpFcb9s/H0JOqpmT46PA
zXUxFNPJS5MF1+7z3mnafIXm3f9sq5MM4KfPwOhTt+GqSErgnlcG9ie3Baovq1/RlwDmuTS9/R7n
nLQLY4524M7g2yHOQoKg6T00hc9JhgdOq1ydmVE7WMc4V5CkvzWbPj0P4Y8/KmB/LwjfpMGhDKu3
Hm2ne0Tm+2+GEEBG3qb8UHJYYS8e96wHF84N7h0ommrv36zTWh0qhJr96oi1E9SkRHakxmt6IuLi
lOUL+0LSfwVxXhgqElmOz6/Y1z6m6632wzI9pZ7jpnu2VDct94qsFjm2TKgH1ZCu8NHI/bDE0XSD
sxHtc1/IHLTcAJQj6V1tyaw5occL5WbB1Y7DG9C9lFDXvnEFHZcSYFwogIEoy/NSWvNydPyfdMBa
/NnZU5Y+hZNL594osIwtlWcWnO1MdtnZ9EhR5IVAM/jq/W089CG+oTs4cZCfhEQvjCDTqri81fRE
TQ2b5r7CZarYoXpuupz4PszEd2AzaVmFXz1AxgPlDKAr71zlHz/CgO1SF7tR1mqg4g8M23oiM5DO
61VG3IOKchDh4XTjrMlJjQKSEBC2BW1wFAe7JXZ5/5p2xkW8yBa5vYGhjXxJcXx2GNZX/0RWVs1p
eQwdqi6vlIS1YKGjJB5RKrzqN+hphULq6wcj3B/Syz9Uwxlbt4oVF5ItOeCsuZBGvmUMV7v4PSQ+
1ombzuyUu5FFDGdtSei1LQNnJO0ap1ZlmD3dUJAA3wIeOugbgvXK9JFjPANxzenzdUjBi7J2MOaL
mQgJXeQW7nB08Mlhya8pFIwiYg4fVwLEQul73eaXW9+SczU8zrx7oxRgRb3VDidvpGFABscZ8s6i
b0AKF4ivDaztf1w01K3Ji9ZKCYqVELbu7yVqE8NizuInlNbommonOVAG5gAgn4t2UCfVyb1/wXQA
edT9OofWFjIWHa8dMMrbWl0wzc2DSu+rHvvdPYZbeFaNtA92RkVG4NATokKBT+l6Wtrx3P/5mP6f
8SORmruE4jNjd6+ped/+WCW+X7EXGB9PqyUmcWFmbh1A0eHsNG9JiT0ulmXIqUPdwJiecsF5MojM
HNrKO11GciQFU/oG6NZPYI9KmmVw31mW//revVu7qSiEk0Zme84nG+3nlRpZjMRTUs1Vm/hJa78P
jUh+oDxRbFZ9sw9QBogcXy6Sj462lWo0TkH8530FWR+WJ1YQKLXBz8afX+ny9Ihp2mN5QeYsgh//
OeJW+P0sUY9TX3qfrpsuMSGKhEAogkRqAVxn1X1HpcE2FP5oRhegrTmUzHtUbSHPLiPuBFJ+pkCW
ot6OyBUCrpzvVjM++9SpFFxFjw9sZmL3e6ykNnudynvJDsoQdZ2C0nP6XW0kE1+rc+AtoDzowYQS
7rnQbbsguCkQmtZLZsdDRG7wZ2qd7cTCDb8sJC8c6tBKFj5d4DVAGOvPbsnFHnZPha9iSTJtdJiB
N3sAWY+6WZZbCcLcts4VjbY13bvBNIyJ2/HQyidvSj9ZZ2lbu22m5fnvkqfBz2uAs7qcGkvmwqrt
3sK3QOy2Nj9jj+ozcCR2YTO/CrdyOTR2MYrubM/wcDtK7M8LdwIV9dq55LG4hSIQTtd7jQxF152R
1zrlV1M95tZTpso9B4734+GKeMut3f+2HTxlvCsfhk2bW3mXwk8/ETX6vF8/t1kF5PNFI3Ch/EtR
k6x7suewwrBWMoY8IRIyhdhFt8F18ZRXN7bxBq3ypkfJKFW0jGOFu8l9EYqD3ni4lwG6MlFrBgVw
Xez9nnP3CQ==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
