// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Thu Jan  9 16:22:42 2020
// Host        : DESKTOP-8BPORRM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top design_1_xbip_dsp48_macro_0_1 -prefix
//               design_1_xbip_dsp48_macro_0_1_ design_1_xbip_dsp48_macro_0_1_sim_netlist.v
// Design      : design_1_xbip_dsp48_macro_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_xbip_dsp48_macro_0_1,xbip_dsp48_macro_v3_0_16,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "xbip_dsp48_macro_v3_0_16,Vivado 2018.2" *) 
(* NotValidForBitStream *)
module design_1_xbip_dsp48_macro_0_1
   (CLK,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF p_intf:pcout_intf:carrycascout_intf:carryout_intf:bcout_intf:acout_intf:concat_intf:d_intf:c_intf:b_intf:a_intf:bcin_intf:acin_intf:pcin_intf:carryin_intf:carrycascin_intf:sel_intf, ASSOCIATED_RESET SCLR:SCLRD:SCLRA:SCLRB:SCLRCONCAT:SCLRC:SCLRM:SCLRP:SCLRSEL, ASSOCIATED_CLKEN CE:CED:CED1:CED2:CED3:CEA:CEA1:CEA2:CEA3:CEA4:CEB:CEB1:CEB2:CEB3:CEB4:CECONCAT:CECONCAT3:CECONCAT4:CECONCAT5:CEC:CEC1:CEC2:CEC3:CEC4:CEC5:CEM:CEP:CESEL:CESEL1:CESEL2:CESEL3:CESEL4:CESEL5, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_sim_clk_gen_0_0_clk" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME b_intf, LAYERED_METADATA undef" *) input [15:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME p_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency width format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency fractwidth format long minimum {} maximum {}} value 0} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}} DATA_WIDTH 32}" *) output [31:0]P;

  wire [15:0]A;
  wire [15:0]B;
  wire CLK;
  wire [31:0]P;
  wire NLW_U0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_U0_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_U0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_U0_BCOUT_UNCONNECTED;
  wire [47:0]NLW_U0_PCOUT_UNCONNECTED;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_xbip_dsp48_macro_0_1_xbip_dsp48_macro_v3_0_16 U0
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_U0_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_U0_BCOUT_UNCONNECTED[17:0]),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_U0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_U0_CARRYOUT_UNCONNECTED),
        .CE(1'b1),
        .CEA(1'b1),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEA3(1'b1),
        .CEA4(1'b1),
        .CEB(1'b1),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEB3(1'b1),
        .CEB4(1'b1),
        .CEC(1'b1),
        .CEC1(1'b1),
        .CEC2(1'b1),
        .CEC3(1'b1),
        .CEC4(1'b1),
        .CEC5(1'b1),
        .CECONCAT(1'b1),
        .CECONCAT3(1'b1),
        .CECONCAT4(1'b1),
        .CECONCAT5(1'b1),
        .CED(1'b1),
        .CED1(1'b1),
        .CED2(1'b1),
        .CED3(1'b1),
        .CEM(1'b1),
        .CEP(1'b1),
        .CESEL(1'b1),
        .CESEL1(1'b1),
        .CESEL2(1'b1),
        .CESEL3(1'b1),
        .CESEL4(1'b1),
        .CESEL5(1'b1),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_U0_PCOUT_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* C_A_WIDTH = "16" *) (* C_B_WIDTH = "16" *) (* C_CONCAT_WIDTH = "48" *) 
(* C_CONSTANT_1 = "1" *) (* C_C_WIDTH = "48" *) (* C_D_WIDTH = "18" *) 
(* C_HAS_A = "1" *) (* C_HAS_ACIN = "0" *) (* C_HAS_ACOUT = "0" *) 
(* C_HAS_B = "1" *) (* C_HAS_BCIN = "0" *) (* C_HAS_BCOUT = "0" *) 
(* C_HAS_C = "0" *) (* C_HAS_CARRYCASCIN = "0" *) (* C_HAS_CARRYCASCOUT = "0" *) 
(* C_HAS_CARRYIN = "0" *) (* C_HAS_CARRYOUT = "0" *) (* C_HAS_CE = "0" *) 
(* C_HAS_CEA = "0" *) (* C_HAS_CEB = "0" *) (* C_HAS_CEC = "0" *) 
(* C_HAS_CECONCAT = "0" *) (* C_HAS_CED = "0" *) (* C_HAS_CEM = "0" *) 
(* C_HAS_CEP = "0" *) (* C_HAS_CESEL = "0" *) (* C_HAS_CONCAT = "0" *) 
(* C_HAS_D = "0" *) (* C_HAS_INDEP_CE = "0" *) (* C_HAS_INDEP_SCLR = "0" *) 
(* C_HAS_PCIN = "0" *) (* C_HAS_PCOUT = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SCLRA = "0" *) (* C_HAS_SCLRB = "0" *) (* C_HAS_SCLRC = "0" *) 
(* C_HAS_SCLRCONCAT = "0" *) (* C_HAS_SCLRD = "0" *) (* C_HAS_SCLRM = "0" *) 
(* C_HAS_SCLRP = "0" *) (* C_HAS_SCLRSEL = "0" *) (* C_LATENCY = "-1" *) 
(* C_MODEL_TYPE = "0" *) (* C_OPMODES = "000100100000010100000000" *) (* C_P_LSB = "0" *) 
(* C_P_MSB = "31" *) (* C_REG_CONFIG = "00000000000011000011000001000100" *) (* C_SEL_WIDTH = "0" *) 
(* C_TEST_CORE = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "zynq" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module design_1_xbip_dsp48_macro_0_1_xbip_dsp48_macro_v3_0_16
   (CLK,
    CE,
    SCLR,
    SEL,
    CARRYCASCIN,
    CARRYIN,
    PCIN,
    ACIN,
    BCIN,
    A,
    B,
    C,
    D,
    CONCAT,
    ACOUT,
    BCOUT,
    CARRYOUT,
    CARRYCASCOUT,
    PCOUT,
    P,
    CED,
    CED1,
    CED2,
    CED3,
    CEA,
    CEA1,
    CEA2,
    CEA3,
    CEA4,
    CEB,
    CEB1,
    CEB2,
    CEB3,
    CEB4,
    CECONCAT,
    CECONCAT3,
    CECONCAT4,
    CECONCAT5,
    CEC,
    CEC1,
    CEC2,
    CEC3,
    CEC4,
    CEC5,
    CEM,
    CEP,
    CESEL,
    CESEL1,
    CESEL2,
    CESEL3,
    CESEL4,
    CESEL5,
    SCLRD,
    SCLRA,
    SCLRB,
    SCLRCONCAT,
    SCLRC,
    SCLRM,
    SCLRP,
    SCLRSEL);
  input CLK;
  input CE;
  input SCLR;
  input [0:0]SEL;
  input CARRYCASCIN;
  input CARRYIN;
  input [47:0]PCIN;
  input [29:0]ACIN;
  input [17:0]BCIN;
  input [15:0]A;
  input [15:0]B;
  input [47:0]C;
  input [17:0]D;
  input [47:0]CONCAT;
  output [29:0]ACOUT;
  output [17:0]BCOUT;
  output CARRYOUT;
  output CARRYCASCOUT;
  output [47:0]PCOUT;
  output [31:0]P;
  input CED;
  input CED1;
  input CED2;
  input CED3;
  input CEA;
  input CEA1;
  input CEA2;
  input CEA3;
  input CEA4;
  input CEB;
  input CEB1;
  input CEB2;
  input CEB3;
  input CEB4;
  input CECONCAT;
  input CECONCAT3;
  input CECONCAT4;
  input CECONCAT5;
  input CEC;
  input CEC1;
  input CEC2;
  input CEC3;
  input CEC4;
  input CEC5;
  input CEM;
  input CEP;
  input CESEL;
  input CESEL1;
  input CESEL2;
  input CESEL3;
  input CESEL4;
  input CESEL5;
  input SCLRD;
  input SCLRA;
  input SCLRB;
  input SCLRCONCAT;
  input SCLRC;
  input SCLRM;
  input SCLRP;
  input SCLRSEL;

  wire [15:0]A;
  wire [29:0]ACIN;
  wire [29:0]ACOUT;
  wire [15:0]B;
  wire [17:0]BCIN;
  wire [17:0]BCOUT;
  wire CARRYCASCIN;
  wire CARRYCASCOUT;
  wire CARRYIN;
  wire CARRYOUT;
  wire CLK;
  wire [31:0]P;
  wire [47:0]PCIN;
  wire [47:0]PCOUT;

  (* C_A_WIDTH = "16" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "0" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000100100000010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "31" *) 
  (* C_REG_CONFIG = "00000000000011000011000001000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "zynq" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_xbip_dsp48_macro_0_1_xbip_dsp48_macro_v3_0_16_viv i_synth
       (.A(A),
        .ACIN(ACIN),
        .ACOUT(ACOUT),
        .B(B),
        .BCIN(BCIN),
        .BCOUT(BCOUT),
        .C({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CARRYCASCIN(CARRYCASCIN),
        .CARRYCASCOUT(CARRYCASCOUT),
        .CARRYIN(CARRYIN),
        .CARRYOUT(CARRYOUT),
        .CE(1'b0),
        .CEA(1'b0),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEA3(1'b0),
        .CEA4(1'b0),
        .CEB(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEB3(1'b0),
        .CEB4(1'b0),
        .CEC(1'b0),
        .CEC1(1'b0),
        .CEC2(1'b0),
        .CEC3(1'b0),
        .CEC4(1'b0),
        .CEC5(1'b0),
        .CECONCAT(1'b0),
        .CECONCAT3(1'b0),
        .CECONCAT4(1'b0),
        .CECONCAT5(1'b0),
        .CED(1'b0),
        .CED1(1'b0),
        .CED2(1'b0),
        .CED3(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CESEL(1'b0),
        .CESEL1(1'b0),
        .CESEL2(1'b0),
        .CESEL3(1'b0),
        .CESEL4(1'b0),
        .CESEL5(1'b0),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN(PCIN),
        .PCOUT(PCOUT),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nT1iDpedwZFVkRSZDJusiwI7kFIMBvviCRm9M+pZKTgQdGFO5jX8oqNrtlexCu/uDfp0YQ+QGyHf
W9HJmnELyQ==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
LSiX96nVtTeT6QH6SYBUiN1RW5Mga6q/2lxWqXdOG38n69A/VIFv4MZSHjz1gILFox9JEY7OFwGs
6ebz/mUxmwP3DNumoccQ6uOcSkKQV1eRSlyyHm4UhahbN/tD6kRdHgTGQgjiOPFINjK/bQof7LKF
xQMmQeb2+71XHcPjUHU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
T14r4uT0q5iPsUM9da3RnLjqN8Qn724f3Fcj5n9r1n/OCu7B1m+A10bBZuAn11d+eTpUOqwU/X/p
2zzSaUcTE8ijWpgSLXU8J/0wcBVyuWUHOoOpFIkqda/gzGVSmbiUUBGDhktV/P2ktOR9PeMW1pHu
QeJD3NMerGL8xO8RkFz8+37CXz+yNeWbl9EKsnw81po0312geoX3g2TFZsqRUaRMVN1P8+qQzlEb
OAUU+/BYNrtsGGxq57Lea7LASqCQSI6ZVYSocjpQzYz+zpK1Ifn6KpwvU5YLStgDnK95pF56yxWy
4DsarUkJGiFZnz4hzdYJeRLciFb00Y7Z7OHKXQ==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
JB9E+rFzptTgWubhsk/ytb/NrSJBaKLviXMn62i8KWfOUbd7Q37B9GOtkDXor5Q39oNYqlzgkXQQ
9g+vxtDNbMGPBkiP8HfN2tKmqAP3203t/R+B1D0CmN2mK9Bzwi5rAw0zNBanLu0Huhygqeuyv4SW
RjQSZSiUCtH8UQpPnwdKQSS3zlTnpPv4po2tgA8ZzjRNyXUAFGD15dFRCsv3KN9TGY3ySFrBZTpy
ddZI86gPVOR8QamQKAtVPZgLCYSIOtqQrQOt9c7yM0NqlnlC0kVD8X16GQ8LchOJaRRndKljCiJu
T7V6wUYHHVdREAeFxWPEgIwm8uncarb/xI/YFw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
hiRSLr4QLw5/mMP2Zn25/s5s8AF5rzEvu2TjIzKu71zUg0RQR79nm8y7jnlLFI54qMdeDd0ag1F9
TU+c3zvS4L4EyGAGLDGmOYcQ2klSCEkAp0eYHfZNyKQhLKpfpdEXhwpsfAMa8mfqBL6skxrp6C+l
wSbnOqvq502wmvReAdkBa7hQBquCP/Kxu+jlOzeR76T33fKFxe/GKjVFC7CzkdJFg59HGnCzg15A
KPrAj/GAtXhrFFCtzppSIgO8GnVXXMrxXlQOTW8Pa8dpXzVVlhWlbclRL5vPlMcPuo76TstX69zf
yyp3rGNQXyTGQn2cIxCTDQ183lOjoKza3cx3JQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
YGcCI/CcJmhsdgWdOuARrKP5BvDGllkS2MoY0dfL6ioXfX2lO7pKY3qpVerntGDre0ZdXSkxLBW4
1veoXYSLGmDdonWSixQKLqlzm2MuxscRuCLcic/Y885s9obEV+bR2Ys2BljpSBpVcE/Ur6Ywxmzk
LxfHQW2SwTpLvo2b2fY=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Qfahy1mSmZHw7posW16zQRrSI47b5EnD2EOzgkKc27KVqFCtYxFhu2K8HcIi4g9qHxVkiuCMS2xv
+leE7EvRlzy778OaDw5sNTj6pKXuDNf0TM9Z5qWIQfZXHe1pN3vk5+JwIPlnKOQNdR/ZvyF/MGlN
OiLTikOABwXxl8J3xz7JkKAD22NG7mPIcFEx4r+67vvFAsaNrRdR1eeZqoEWtdnoXxed7RU4EF+M
gRoH6yIiT9Y1/s6OYskQ6JtiRhnYtAuCfzREnZAh899nzaIcLd7LEVfL5Iz9Ugu5o0kDqSWTin3h
e8cg4A7UdkCUVgAKEJvninJ2KykH8gXo3fcIvw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
iH0JES/NvEFuTjt4he9VbmbaRMMC3Az+avxSvav5homUT4AN4kL0Mr4DduP6qq2S7yA9vmLjymM3
vUy0LCnW4MRJjyOhZ5+lKiHHna+B7i67+QBNgfHJZBo1o/MQKyhRY1NEPO2OcbO0Fa7vWxMb0fxD
tLYj3lDptr2t6lE5QyvfRkm0+K1QUewQYhPoottRx/YRIB1HGmuGxAGvtktBkl8YKImYUAhUsulz
EIDhzDvY2mjO2Xcjs4bARB39XehWet0GgDIyU7JIse7GFBpk0KwuFUmt8GyECacKOV3tNHp0JNby
fTW1A4Q/icYz09CCUtWXTEHhWrJdwGru1IPXUw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
EJbk8tLLjlKBWWzy9ed/VxLmGUjR/VyjMLsCneAE+j84oRp+KvuysqptdWBdiS7vf4A7tBZ/bLRt
i2XAajV7I+0PfuDBovQu1eym/cWgnSSLDzT/4VP1p2nj5nrtcAn9tYUy9j2vRJKggOQ7q40vZBV1
634FZbpF3fZnOeqhBPy4C+83B6JqHPofffC+hmvoz91FfJThjOv67xFe2cyVlop55qBbOiy+dgF/
gFoSvZBYPkujshJPw4v0bGdz+YfFgPqDQjQUHNRLlqRKte12d/jEOfxr3FTz8UVNsigk6EEgaP78
IiEcx2HkSGbTNf5Gv3xxX0JktYxSp2ciWXghxA==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 30336)
`pragma protect data_block
sCfzYP8HtVycSISpYIw0BROypWRZNs7tThpNZwwXsmwPnq7KylWXh6nGq2ee/acTrDvy2AwLZBx4
E3LnNn0Ed9kImb6bxNSkSoJWAd3AC4Bb39WgqRdBhBz4SdOhVoPgYFoTkypHOzD0fvo+Fbp0au9P
51pBROytIed7o3CW8aYb5ogNdRuubNC2k7Lb/hJ0jwBo/Hyy/CuYDiamNSbA+zEFeteep7n4/zpR
0Gq5qGTARf4BOznpU4LYiYFJli8BtGiyROStSIPUIdfXJ7nI/YSJxJzDMkd385nFKb+pqTON4x5A
h0EVUFo6QATzG5TmD8w8pv7SjC7Ywq8ia2eiF6or/inEaa5krg/8MWkXj45k8Z97MeaN77gxB33P
YYp0NHJE/6howv7a3o9/ah0rm+kD63IUWGEy/TfbpaiVGFNjGkpQigVfXxd9Rdy6NTYK5hT16YeD
cg3TqWQvPl+H4drFt8FPjJRCu0P4/wGrLKuPIlr4gpl2MVASpN2Rt0Rb4HvpetbQ+BVi9boPMKCp
PNBEjzIa0G3/RSYaK3CpCIsCVCA6grPj2bZxUbdOvfXzZ8ySo0zsBTWpFq8RI7hKffGBWKnqGuY9
RpNutC4HyRlx4RqwXLNkwMc4sSU3Z/lZZTpar3ZkBDjrFBdDfuQS0XNvbv+qW+zT5Pvh+RiBxdT3
yOMZOpuR6N6DTe9+rsGW9mM1pAPbtiVApew7H3PzAQdUGACCTpIWnIT6aqbOf1UCM4ojHeDig15W
rvdte385N4/jLeAcMO9rMgH5pR5oYcobuHrIX6sKJeRO9tXVPkw/UJMFezVN3eH7+3zxRbDIrNps
DF9Q3Ic4IF01dTD28h6es2lZsOdqUd1fSeJQPAnSpnPxQYNFx2qtPQpEpSI07ms/Bxh/zhv4ISk1
I9g3ndhEZ+6j5P9X1XYDOyQ3toU9ZQfyGHFiaqo9NRAf1h1XPZ9umvmUGVHncz3TidYZOtpw0I3x
7miDBdFSuVa2T2RBtsG0q1kOiEV6ESvX9JCy9n26cD/FjFuxZCKRAL1u7GMihd2JIf1eaF28vTRb
GhbFKEEO+w9mEyVh4m40nVHQKf6FOlJHTkiqjPUxQlquBneALI5GrrgbBK+YtPxSrudrm+aANumB
MZ9kFKTxP/snGG49+kH53no4InDiQ2XjOoEIhLEOKOriyhogwCx+81fyDjO2dhcb1kEWlvx1t9YQ
R7/4DEe4Qu2ZhZDTVqSGKGhmCntQbHGrnwIJCFrz2NTy7OhPUeUNjyO4kztEHm6e3fHW5Tr984eL
EiDsCuj3t/pHjp7oq2mfcRENrrPgXJipD77NKiTBDUDEv83QxAH49UBAwh2nGWodw/+DQEQ6Arzx
Jb+kQTUuks0y+7nI67YRcjrLbyH7nwE4Tjc9OJ7otYAYx1iQHpF8ynUO+Nn+n2Z3JenHnIV+gShp
v7scivekkRKHuFjW3s+k9wdZKymKfB2QTEz1084RwhISVXdDrUdxrhwJ7RP/HHx/VjR1GDaRfoL1
4pRurEpjJuhAn6ZRr0PbB6x36HpdMTbMJ8TFGCl/2WZKTMI2d/Ci4u/t0LZBczvWB5ecbq/JRnKy
zVgYHuWSG6zcxx2LzIRaHKXuOscAn6itmNh8O685uC8+t7E3kY6cnEXcKc9rSRuVJhag/oTV/PIP
ddcR0mzaLwBO4uNiUDPaQanHDzL1+CCHZ2j5yagOQnLU9bQGouDszVXz6oY2rjtOd7UrPx+xWEOw
E1AUt2mCA9p9+LsJQEdLrx+nPoZ+v5YWICVkYFLHRA7EUH3+AutE90CHRVPTpdyRUKaA7voOc/JL
LFuP2ZAH3ssAAxL2KE6VBWGeS2kDxDA5ev5gYWQVKqSQyrOuqIx+13I/cZaYXoENCCnwQYxGm5Zp
90oybzOP2rT/LT7qgX2Eix/OL0AqLkDe388ucBI0Msc7iO6gBNle5jzPO+L2V8Q92G81GDx9+wj0
VbQiHLsAwq7A/T/PfrQwEtl8EflVkcWZ25xcZQAPHF2ofaeIOuXtd/0FkgLMnD8rHuGV1HYMp9z/
mabmJUZIyNo9aJt8si7ajpNKyG50pNtLCvp+oe/3pIvUaIzmP2jkjYWPJ531EbYtkR/K5LDJMDt0
DsgWVsLTvZUBzKXvye//LHlLDMzxRCuFBdMUssJuPAMdtRtIZty2rRpO1Vhd3DwxXL93c8gSL6LT
E94s/rxtQSfB7K4Oma9eRQR0Wm2ApLk19dtvNaogOmp/6OSgkpBymiHmcOrfnbPUvaiv4JfKg3jH
ac4L50I4qaCpm9ZucTNnCCmF2X7miiIcrnedKKk4aXGVQRV3ftc98UxWOunRVrEo2PtzOUhrc6sX
D09K3bDDaTUxAi0G1zmqVjbrd6DHGP5/fYgZjnwS17dtTtFWq+vSED3q4en/kwN6WnjQiwxMq1Bi
HuGYP+1VzoSUHjRkBd+Elj8UT6OM8KGZwU47xKFh0Tl1Bfpi67zzKVurECQ15H/x3o/YmF/mX+4B
jDKFjitNMIIh8HL4kI3BPV5zmVhf+umzdByzj8rQ/u9Ro+OZAyNK98rWm7d9l/fwZoocgNTYwJ6n
ccKkqhrf0CfdhYwIhCyBcqatz6MPt9Fjrc7CMvXM1IliP7MbiDr8s26v3WGp4Q7lFf6vHQUpP+EJ
G3X/+5SMuhbaqOeehd7vJdefSOZB9drabrnOnKCZOSJD6/DrZr/HzX0Z7LIyI7zJhbzTP7FTz9oL
XBO6xF/laGthIquxxUlJxGbcKVjO9dZSOK1kZizK+Y5kEyF0rvXp/r8ZZa0gPYiAetefgr+GuKuh
fU/5pkRPCO/W52HMVVP+KpA0vyVvMeNMZpet161ynVA8r27Qxg31wqQxzOJq9xvQfwNSLwI8b7si
wHtay8u2w5mjt8CCUKrnhcEIr3YjbGjPhd2S3jsfsXvOo8TsJdQLIscbQC+pXIrjRK+A4QQMVx9q
GHIbLREMzd6AnuxjrBd/8sJAaCDEeDvYsUeuIaX2ic6juXFt3kUGM14AqE+aRtviQtfqnTJCXFKC
xv170XIXxhhSxHMDmNt2LWAWaMhI5K9QD/J6y4rc/qTxPk167xlza/u7e/I+N0G8t2F1/lBOA8Cg
bY9WFrhVH7m4wcwN+J92jb9DgHOH0jyRfW+dlDdIS27oELLDvgTELsUeSsUImARsVwqFLW6EK32Z
S4D5kre4ldpkym3ne8XgDUyrWioiplacjTax21jv24mPsnWe54USFWNQpLfNx25k8CBWADQf6rZY
lwebkPmQ5EqfB/IcnHER6C6k4HRBMHew7QWPjn/C3vb+VshWmDSWhgj3VDaIYkdQmMNosS9cK4q4
UXuUHk3tIwVrOIIuW25LU6XvWGJdANYn6ctjoMHYIKRD6OeDTAX3qi8WRC2gTuz02b9itTCRJ/+A
/ThaCEfyr4aDcsVbWcK+TJcioPmSRTDfFd7KpbDatwDSmKnopbkcy2f28qxgq0NRRNjVlC1tqZLF
g6pIpJG2Fok+/h6ClWfUOY/5lEOrOFc8QMBoA9hfgGC83qkQcmc8s0mJgVtrrwciA2MotqP4rSqq
NhPZTKS1EuduOX2I9WHQBfs4eUOLqindTjt9fumtN6qzfrWDlAOp/Fvro47EIprAN41oWWHpgSI8
Pf/31EfbKT6kARRpxgCaQlmoDD43MTvye92dUPEbA6g0BWZDpUX1xszhoN0azYNI0epm6w/bOc2I
gUWlsN3IQO6kaNf82BY20qAoYWvd2GMrCwCIxwXwrKM5b2YajDhlxulL/JRuqOYKyvf/FoQOa8Kt
uqdfSW8OvBb+7Gt0q01IJchELr452ZrPBO+hWPULOF5dozgxyuFbjXCYG7e9lhYNjKlUtxlUerQN
A0GZTe37zADGOnx3cpKttcb1piFDiWr1r/pPJAEiVtBsiyYklSALtjB+iCpKPIX2iuvdXth+TXac
dV7uZgwGpr0aqITbV8WPoDePffM/oSqT4Me6STK7COYoSOAAD9i2zmOlyjfnva4NNTpBpd+yfi3O
b2rOCEhIRjlx748hz1n6UeNKV2kyR5ceMHFMJ3nXyC9NjJsU/8fLAixnlamloj6C5PRTj5KZ+Dvp
0Z4Sr6OEerWlWh7P/WZ6Hz9hMIEyV4TyqjA4BYBsTdB7rdlIuXP54Iw+FPfHL/4ujgfuZ9VSswYa
TXk80He2xyus4SLXRayvfJk4w2GsFsvYszxdaNkKZTsBZgCzV/QUGRwqdu8ifZLVcz+I1AzHKH8G
QxRCtg8+Q+emGqNH7P76m+cq8oNjhwDR9ZvOXgBByIWjjA+fGD26CjNaesbSyUWBJZHGRBvxGhN8
UlKtUz+NsQL7GWHUtWbcp4QPUE/+ZBYGgd80qvJ4eKNmRWOmBkECDLKxYsovykolhhnBfAMRs9Aw
fAdmyLScpP8Nzq5helK8Nmx+H2AEFfeNgTv7xKYQUvz8AhSfwk1aRd0aVuR9Acgds7w7zYPI0n20
mioSCeIVYWuqmZ48KOljHiNRm/V1M8TXRV/+7oauXZ5cvYnpvEUclYDY3+BI8n3/WymV396syQtT
rWSYlELimlOSl59znNWYCgxhrsiQngF4I7FPz/t1EzFUtRa26sNw779dxZ1WuyWgQ4SUQPOzluFA
3AMXWiciM6v7v6vZ056xQ55ZlUlMt3urD/6RoRIarwmSFCBZAbjTT6VupG+o28fb3DAFFYlN60Xo
m4DBgxGniBF4BRYHGLjHyuDLpZd53kpZhyXLsLmnq/SSstQYyr6lGie4vi7tCdxMUZ5xM8zNEqsX
ASYvbox5i+nOtQ7pg65nQ1GJnk1rdalcTQgm5eNMQJLYGhByzwtM/Y4JBjySMMPMB1HxCw17jZQl
sHVtmLTNgKSmdiFLtj9UICOEyF0yBhZJo+CCPoykVgDwgbTGj4vIzQHYbIT6REuraVUfsiTVtGNL
cY7ySC4Y3pqY77H5tjwEl/rKz6WbocULZowfwQDVwkU6Oy8S6oorf8yn1yLUQv8DVpeqARh/+BDS
13wClsXqrlT+lsL4DEJLeybLCGsrj+fSOtmOswz5Hagf5eJ7OI01PU6JdB9SdnfnEN662Gkm+lC3
b+Vn9mujaJ0uIefypUf8qI+5glLTL0gjnGsOHYstPAVDnFbuthzuQTh8iMA9kZd6opiSCxVLfk8r
P5duOv74I3TdaBZfvxT/Co7uQv+AWXre7uLezY1ZGL5sF0cAH9xu9vcrmNtWvFjjuzl4wTwq3YM0
F4Mu7N8uhrMTug2qryPIrzuN1rTtFGGYGtuC4ZJcWM0cJcTTXrtlc1pjccbTlvImyl+ctlltTShj
F/PpDuJWK34LAb42elgY1FbvrS8d8dwdZVzwl7uztwAGbPZHODSoMppklhkAHv/x9QXd1/kzcAn3
yfEYPTpsnMuXz73KZCWMkYVUCqSosXCWmkLHC8OYX9hTcZCX2X5M4aaADT21KeXwRnG0w05ooo2r
7BGc4C4b0Eqvlf9xm8Uzsf8A3IIofoVjg3899oXP9xF3r7Hsx3axAa1Zs1GluAxQoj37eY72Tzd+
ChF/lvhY2429Evz6dnXlxZWmYNJ9GYxI69aD/5qUoV12jtZXVSjUc7FiSgz0OVXJPa32fE8P286u
hmIBvk10cXzHVyGtczJk2yxOgoaWejCkxOzHA6FQSPAL7hSQXtNsAKp9fHoaOhK9IwPanKRa75wh
PM24Y5aZ3xP8JbaWMeVHo/iDPce7HqgKqxgs1zFyn2FtgrmTVeHR9domA8Kr6MMYkJBQbPl9oLgp
b/r+iNvF3Si2PeOEKPYhJwWstHx83ZA+Tx5yGU2DRkGsC4yh1fm1bdszMTskhf93GPVP+ExT//Eh
t/j2YNrFavVIWgvYob9Z+CPgnWrnccJWJJC4zFexEBfAmr/RyVmjFGp29JqHJtPkSa0qtLnVZofZ
gxz7MbWec6gYg91lTZoleEb4qHkRCYodC4eD/38WFBDv2GvE299YbvcnPYa5nh1YGGgEzDnkNC7I
rCe9bs812/j8QQtfAVJrb1F64X2VuUelbJ9Y+dpQkNzGcxZboLJyQWMcXOkwUyDGZHtDZywsAx7P
BoflcFSz/H/6kmAfIyGx685CH4zzyQ2cskKmEULkQsRRv9GM0uwcnZ382c2yl2K9Z01CI106yRXR
dIeeGKZs2oAvbau4MwMnAUszHz6PQRORwnqEEGTVpb/vMEVWF0MVOyOpnCzLGGiKFWP/g7Kq28C4
v44y/fleNKYZB2rehJOwrHyD5f55nehqMVBHrsjZGXzDzgYJ2uBTWlmI3F5QIRYZnsg7CFcYx5td
f0pWP45HRLohBMJ6W2jimVIagPtUzDqsX/QQPzvieFUJAtyA75S3C51twIJUilnjxylCRyUAhRQl
eXYZenLVfTTfQbG4ttHLvnM7LuPQRIuKxiQiZfJH7qhOtwXuB3WXxauxffWceQHX3Nnuwodb6TyY
Q8OMiov4Ta8oFBivRwBJl3Xzrx2zvUHf0BytzN+ini17duow3CxdcXqy/lKh2S1DrmGYOQY4Z+gv
b3kITccb5MdaMJKyZ5SawzSu6kE4DgHOhSSg/ihe6tf4QtIv+4DJI3J5eDpp7ImgCo1fE07fxzR9
LjHTfVE4UNgGXW00wH+BioT8PbCEQ18xTOFRlpCbBRGrbntOc0gh6XZWvRCYERypORKo5zvRJrlt
DLhBciK/OYDyatpSlIUcHTpWP9sIW7bWN8nGYKhuJy1YabIr06elLZ5pWk1juZ4Mi6AqA4mj9mGg
KCK0yCYC01DA9jOD4W/FIMFw9vBIpymF5u3p9cS8kyBGbzPdXy2of+L7hH2QigbOjw/qaeC0dkHD
woEVqhkilKTfp7zF0p+BSJ6sGE2ocah90ErLcx271ZIzreiOsyxik4OCxBpA1+dTThX2eP9gDJhs
YicYgbt6jqkp/VnvWepVy7qDcCGN0+yPE4K3MXvS2DP6oOVQDy8wEXoXx9flDctyzVQxOKzTtmU6
KPjuNjtBnpWmcZYN+xFz2yUGna1D0KKVcRx7JLY3s0GcfiMgRHy5Zkr0LCjQD7diMkdGutKEAeQC
+tWTWZay1PLbvrR6Fi5Pq6Ba0I4RY0D3TbThVT0505Fg1Q6x/uyCDQnFg5CoR8JLNHHe6ETkcs2Z
gkqO9yb6bmGjR/tqRLpNdNPWSGHKkZ7rYZuP6X/zT/2NLltttbw/9y9mi4JnSu8C48Kta3HE138c
PSgoVbPMxAXRCzNsykuFqwdjGOCL57UbLHAhUs0CDiLyAAaUCHc95fntqG3uJ3dqKzocPSmV0swx
ZbdolmhvcEz4LL5Dni2/uL66dyenWCQWobz7xeqMQSLNQ6daXu5A/nE8GW/DVexDKs4Y5d5CLF+m
zr1/iOJM/e4SJDtyKlLTRw9Wl0gSLiUMBBMC8OknmMXIn7pSMnqMsvStB+FsvnBsZibZqJhowx1f
8HFZPRcU0N6d3myz4fZKZYto8bTj51a1dT8XDUoH8JbNIkvMQku7d1WQeNxkkhljx7jdgTs7usai
h/I1xVNpexQLjwH/p3SkhzU3NivZQSGCpEWrX0GFgJJN/CCo+uIxJUFiG7JNI9snHHYRzXiAdvAV
Kfj1tn5Ks1Qx8hTa337M6IKzpdRblHJfRg9q9EwydTbpdG6K/t1wo/FJ09xX8ZJ9TBvsNHhbBDAl
NrpDrSYhasQEYMim/i/gUrEeT01aCPL0m2WqcCp3sKT2x79OPBZPA2ft1p83Mny6hkLY+hsICcL+
Ao/rB2vBNvhEWsqkD42dSE28k2Xl7BKgBHbXayWnIpWlqhUZ/UJKcb48458W4xW3cCa2FAbATvu9
9OwpfUlMfH/+c0Ki3Tswjy4XGAyfOYvlelk4PxvxA3hWPwy7N/8RXRi0NdtArVq6CocYceKbNf/3
yZplo8hAC5tM5KfCidThQtS1aGlzrlCQmGxnusuHPSybA3oB3EbmqFAa6OJyS2Hrlh+sWxD/R3su
o5AqygnTgupT22tUnaee2vU/EmCscatEweeMNV2/D5XPA5br8vpLs+KY+N5gzct3KNPgiQfZZMG0
MOFmxQbrSsn3CE7MCOHSxZQMdZCE0cZY6u+XBwLMrT2EADpsZYRBA/rBvK5B4u/Ct1GndS5BpHB6
uYoSBZ+9HJGHGKkjQAS4n/oPXGUVdA5NUgiqJXQfJGuRMXfl4mJS+p1QIu/5PSNqjazAuOgq8tfY
a4Lcy8Xs3RXPrUvbjSXQa11hzLE8xJYvHWYfn6mSxHrIdEVWvRe8qRnZKgKw23IWXtSpP8F3Gh0l
PfbYscn7WIZcAksk5y+SriBiiD4zcHLVGTCNnbeFIhEaQ792V8IMSdNexmXR5eM/4pcMkb0ux6Um
uIJnwpho6VO8BimtG8P048BLZq2ovCk3kIzNXdwMMNl8Tar8CHNUeYmMtr1G+JJKU2/b9co1N5Hm
TQ6JF24ri0qds48lYefl6UCN2dPTSdgZyvcZy6McopPJJi7Yjnzql0SfzWnW0p9jXJzlUpdGWV6o
arM3a6bRyT12aAvgREz0nMrEqByI0N3Vc2Bsc4jhc8I/Q6bvoweWJfO0d6rrSmnPORgjj9j3AyU6
88fZWJVOzGfcqTD8Th/yyDkJk/F+5BnlhJQ0Q3E1bamYhVEfIYj99Dyym5OmOwz2F6QLqwfi13HR
3LixoAKjN62ZKPJ71KNNFHW1O3YcOhWxsbWnOSbOTeNnJ1t2uPnDnSdYlBLAvyaJwF9UBUipaLhr
UMGE5sOseba5t8P1VmWXLImtdZeMiyiZM0eSEAGPfsFtomugvFiAxXkxhjHihTxagGs2yKdMiCfi
Z4DZrNCfNTYS5o4VcEbDyIIqZBGRmW15BXMBXxDRqWOAPmrn0ZqhNCTZ0ro1SOd68Q6D4Voft0bN
spfEo2RX1ZhBy1ERr3l7qFa1uEc9ZvK3y9D67DM6jSZgXr9O/X5wFJAUCYoxsloqTjdc83Dih7Jy
u9gPqGmQCGaWXDRm4QHNUkfjYhRCrpC7yjYaxh+8lNkVPuTb7hCfhdCQbnpgWlcCR5R8IjKKBZ75
/ka9CEtbYVztkcB8RY5pwK8HADrHpNn+tljBdSgIEvC8E5CAO7jmcjQ/8ly1cMNsCi8v4xL8JUl7
2QI/G4Y3QftXW4JOX51ZFrmV+DSA91WG/rV8zijnyzWqY3m1pYoBUFvnBcw6fwT0BS5aoacX61iZ
q3XMwzg83ORLXeRzQxICKr2iyQgjA5+frL8GC2JU/Wi3vN+3zPdys/4T41bdiaonXVkvmeRGcW8N
7upVU8NH7PwtaDd3vwkrlfgObxUw5uzZ6C385+HFeU4fah8EMCJzOyYP7bCL9Q1DUbNPVy94JJfL
2YtUcGMD8noZM4UF4Mdg1ZpVdLaCIYY5froTrY2ZU8/u2wMuE5gFnzlAkx31yOc+Y1crgVAdIu0f
Iknx55lCHueu/gEskPTuSGA/k+EuQTAIDE0VzPkriumsVPvyUyHVGEhbidjTFNyiKH0Z6j95ban/
dASjXaRy7+6rfpP/uxMSkBqdsfhXysHx92m5HycT0rRBdgPY4zgUgq2GLNw4CDiQX8f41mBF8fpV
7PoMCCo+9Zhh3/Bo8kMyROuGHjk/iF166dHCvAvbnq2wr/h9z5V4LV8sNWhIehVrGRGIVp6hPfj9
GX3ydTdZNYN3OphLJgI+8rWHveAkuHCWrm3XAL3YVFwGCX2rc1VKwlIkxvpOvqfOPYvydu2UuuMe
b8KAhhNZ/DgLx5mHrR8luVF0iUwpyKXoGZUTsmW+VS4p0r8TuGPV0Q0kIAlKy4nZkeoawOYLKeP1
YZgu1EioAs9tCprV8RdOhxmIjDhmQniYkzisMJL/276KOPCTYPVLfnxfC2zSlBIYh9Kt0CrXo4NP
jRUoz2qW5t9r89VT5Jg1X/HatMwQ2i+SKgONM9EcjpGMxJYB6gld/hVgoZe164d5VEP7djb9+Biz
E+JOmnW9X2PUDmze3nynhWkAEGvLZ8pmVK9m7tyXZS5CR0LivGzjFclM/RaJbC2pOzhTZbyTBTf7
E4go7t0fZd6o6QRtzXVQyjCivPXQ3Y2QKEN2PMUyHKVoL22VVIENmw9oa0hcQGw5aRglqpNtrBWF
85q5R/tYCp/kqqczDmCn0zlRDZ5sK6vtU1gibWZ55gTpwVqdIcQXFHPz7/g2enrDv4B7US5XTQvs
CnYalDx6RF44dP/voG/lnnDjfe8iDsEN20wn9vqrWPmHRHRgaqzWU3en4wutDVbbyWjr1NIDHJ00
1Y1Y3/bWQ3nxKMUkBJLlR7aKeiMNU0JWmfHZTaMXOM8bYzw5CbLDld5vU1742bcKQvAoOhIQj5Gq
wAszpZTCJI6jgaLlTzi1+VMk2jhbdJPgGjc0YoTnabCbBIoDeC5iCkzWNNUec82dUQphGYWJCJ3A
i71JaZW4piBT0HYmA2Zj0mNjA5g8TK7tvR8ca5wtD8YpE+7GMMrSrRpAROb435fSHsLByu5Y/jbs
PEDvGf8yBVOFEuUaCyKax9UGh73cLGU8Z1Waym4D+MXv9vJkT4BsM2VurrEjkRB2m9KyoiRpp1oA
vXsOB2Xg6Uqzb+r3l2dZ9IhNPKvoM42IjsSeVsPQgAFmWPJiuktMskOfKaHuhMEWlOdbRKZv0uQU
NtJB147TKlA5r4q93DGbB45FO1oaUi13SgVVXmXMR7wd2e1k4axx7MvDHDZxm5Szjsf2NxsCtbNc
enzMJ/UXQVQl+ugrqF4aMALfCdiW7HDRror3Vfyls/TpZj01N8GyWEIhISPMHzxETEmqP2SJTbjp
NaAIhR2tXpQem70cbKo7i1/JuwoiJ/KkHl95LBwl8KMhzJZm3Q0MdO1ZsZtnJlOtqNvHQ0WNnapN
tbdMIKL+2l6HgZNJoiwnUqcFmFZzau+WwkPyn08p+fCqLnEuxz6IBFu0TxLZ2fFypONUdC6UQSh5
DJnKXu9uDh/Kdd4cpKz9ApCAMggcGdlie1lYT1uVPj0f0toDO7Honcf8ylNngVlGULh442KuIRsO
Mc880xTFGnXRe7560Gt2ASp5MCWGFe3WpnHzehrhgSEg8i1xhme3epb6eOFfTJs9Fsx0RtSmDtdI
/rM7SSumU68d+K7HBXATxY2YyJFtl19439YSRo85OufjmylREdJpkEPT38WFDq4LXTz7Yr7OYLfx
Db3EP9/DHwwxytQJcl1Q6cMxKayy5PG3kCBYU1qbM3S3UY7dP/Sl3cbqG2yamuIW/pcE+T2koAYG
dtzFxKLga3TXJwLCM0JcennbSuNGojBVx7UoxPiblguawrHUly9LZhbxq22h24cjzhyyROLK4uF8
qXBizuxGWBXYOd1BxMRk5/Q143r1FZsPcWjt5wnUymkN0myCJ5VJuxPOrcl7rUS/XiGLIaN+vqGM
LPgs13xghYdKpeSRzzUe712oKDgi20JN9OU+2sg6C/WYaVdMUL8A8hDu+sNKt9tsRxTtYszYAL0S
0RUsDNh73WmPTAXU5X/5Gjb+e+/0hU1hcC3gaoucQMgopU1YVBas6NQnofvIsCukvKBu59+EzyN5
mjIv3j7tMYcv+hTvoWKvToZ6/OBcLie4poz6mGebZ0UJQygdQzPg2qElSPvugfMXX3Gqyvhjh16a
ZexRmco+YPOCOGgjHKYHLHPVYDmkdPfLm9YUPW5QE/63eG9LELnplwy4D5W2V4Vjla/2xnEctpeL
TEjvPZYSyvCXoOqDYQysqcTgXfHJogjzTptKn6d/89HIyFUIrQ8SITWp6akzydPOYgc4u9O1mOHe
B3B56aetQQM9zmg25XInnHEjExOw7u+LnycF+yGNhD+qzGUhukcK+dbqIBrs3eOBM52VqdSYlv2X
3jeg4FfeQgiTgPU18rAWDZf3epwymUDX4JTF20JH1Psy9yWnvrDOF2BitmDRifY+/Jv7ZnMhptqg
yZX1FYTExSzJlxFGGairkB+ojJsfznt8RujlmkrQYlou8cvaBnKlK/jC1BlItB2om8AYhpH9ZgbF
CdZ6asiFmW4orSZk5Fh+nwnm1mElcLkdYL4yuT2r6M9CVR1jxtOML4ppWmNXkNk9xf/hxTMv4Tlk
u9//vvTJEBK3l3dpE8ZiBsygu8KWnHgWGiaUfduhd3TLCb2J5B8QslVLOFf3KTXMkNSO11GdS2lj
nYJnNdISIxeZ4jwATdFdg3NUhK/Mx5pToVQc5tYnY5w2ofqE9pnx5nJfW0iPxLjgwcheQ+SQvTQO
c4SN2OGXNjgzfWZsD8RF5k2Y+WaxecxHA9EQiZrsh+LVutgKy0c2lU50rycRqrce7Xr6Nkmq5wBA
zb0dUBKziPEXNJFJos6M0FxrilenjxVPRd+cqa4Y6fuUDyfVDAg4Ew0PeQWOYhLsHqJQmeYUzz2x
v4jMXExhLJEO6qSs9ezNUkcl/brqYKRZlCb81r6e4Bqwgoe3yqpOl7xTzQD9r66WYbClXX6jya04
hF9flwJp7oId/eXA3fN8Q+f8BTolnXf/ObC+ryYyu0Up4ArucUiC+IoyR0JTREObQfBace8vS7OQ
d+YPA/U9MQh7EmO6+JmAzflufpYJiQtma+EnHqo1w41kgChKjtJ3ENhWEKaQ7gO7Ko989SgHr1VX
16oqY/a8sSkjutZhnI/nJwjwBoEuIfxu25C7vVHXWLamh+RDN3Jqp0LvaRZDw9ffjX0GIT7ckAcL
pIJG+UF/KzacnAuqx4eclYOzHrixx3bN7GnCFbudEbEkzBDjj9eiTYIuYJCwbQi50JS3u4MEwVWz
AuMVg1tivKOvfRs3bgqd2LRIx1qVFaz16322O5bVkwfXQXY30OLL0U+vX+uhiJ3/BtPg/Ymh4fTC
1MHY481SlWstPynVNpfn0Ba2y9nWDliLC5NdLHLuL8aYB1w/l7ScUmo7UqtNPHzyCPXIQmTpqsIr
cOe3u1sqCi0aWok+ZGjjLog64z+ZgizW0xti5/30L22rFLnfCcYI0QUPHJWr/dfLjfQH3DFc96i4
W1iOfFKaK1SZs4PFqRkKsWFhFbEQvH/8pmH4P9UbtbtuyKwzYFe4k5oeIsOnJVDtjODPen4A5YvI
xyj7KmrZZM4ygbTtm5POXtvO+JYSAjgCe3VywN+eKLaRUkzjQ2/WA/nq4Zw+qwpUEdoQof8V1SjB
Dl7dpQxxN2pzRUOPZ5ORgZNi2koVV8p4hXcuX5OFUlQEnkuBp0+vbjBQeR+xZxE3vUkXpRAGnyGh
2Svz52R4HccP20y7MqAfiPLUxfkSy+chJo9zPJWcS/Q7jzjSwiBDOiow1TrAOF/nZAyZSuZzZXo+
kIAYgy7aTqf7lHmU0KVbd6gEVo4jd6giedawGySEgEtt5ObWIJZVJYFF9S/EfKLMDry6BNkUVvIp
P9JKZgc9NM+gCPMgm6MXjPZOZhNsl7jE5TBoUyLd24Bbe759rYFluH1pYjpTi25aaRCNQkcH9YBD
OfTFZDidUzTZSopm743cHPukYkKjcpQH4ZvZo3eOSKrKoJFtcgN7fBZqw1jliA+p9rOSykXu+we7
gGY6gn7strCRBDBsLRwgRlPCQkYMgfzNwHVSOjvTNHCBBTpl2QlqGoQb3hsvQ5JBnWYJuKwbq4jM
EruFfjFN/VgcJplEKTQrYjFAXs110VwW77s1oNspbKGQdaPm/wPJ/ak4NZR5i1Bycdg5evZx4HSw
IY0EVKeGzROtrHkRwuFKfnDlDL4K/tYESBbPx9QdegdXOpwxYwVQ5sPCb9WC7PIaea+PYC3HUOcF
fhjc6iMQq/NC7VL1KlVQq+2KAvSErRDtilq47gxO4ie3EZfyYA+beIEuDOfbAV5nvVcjH94g4Zx6
eL/B1S0gGMwKFzbxnzHhaB3iBFUQ9jp7lrshLcPbhdqYUU1fu4Vlo04V1hOrhrm4qO+KzAeeKPal
srkEhBBUkffAP/zCxo/rmJdgTVyDHxDhPVOUem7bNGJHl763eh3Lj5deqLG+uKcsiME+GWhPjxlc
TfzofqiqYpOF2TR+WkRDO7VdhWTEu/J/ZCveqgoH3FI63g24tW2xNnVpB0e1UoQrWo6LuzmujjK+
0LI2dQy+w9uWm2xhFfzaLkUj027c+AyFoQfT0dgr0s/kPRqjZh42r9co4My8IRu3K8699+gR2Egu
OJIY8crstIe+tsLAiwee4LX4l3UX+XI1pPxJGWA83nWYt/G4T+kj/+tGBtwvHhyL9DI7j3Wvl79K
sz+QbunErituF4hWSAOWmwc0eb0ymyAxtkZaxEzlWuWK/kEFo7xUoh9+z93Sdpw6kN1NLD9yI2ac
3wiYqWKRzvBtfOuVGj3qnc7H7vCmNE9dmhQxT+0KRdHlj53NdE77vSuIk+uguWtsUdvXkcTsyDIZ
7MqD5UPTnc3/AKv6s8ns2uFKvDuhCLV8DDK3mo6nhqqbHzpd4DHP5jKXnF/UF6zHDYpoB5qR7qbI
zZ1vjzSIcoqJv5GIoaf4ipCtd/RPRvRCl8nNW4dfFxpIw7Qtr0O3yqb5/0wf/pI0m4Hk6JBs5SBE
nEapzLESYU7p0gRByTOclP88WKfHk/9QvsExtgz+7BWZcSxIbtbhCkIYWxentBVbau8cELQvgQ/q
V0PmKrDDXRKEtArz/NAH5/oHVLRljb38B/aLbR80vflgxY6FvR0b93q9jt/UxlfbsTIDVmGOXK5O
RjcOYNc3jQ+OMYWWeuoGBrYivCwZ92iceyuj2oDo9U+oxKk5mpcNAUxlQNEwlrtxlf3WkWA+mmvN
L9mDzLSsIRPF/LaTTx/sLYP0aoa6HcbxHAwk6tFsbFa7G5rjd4YI7QakR2xRacPnlt3GYxogO3jp
8SvNo4dXpuhS7ROosvLPZA696+rUQLYqTA5Y8ilw4Ra87kA7MMOkap2QpTK5trNmRrgmuFcSBL53
kW/TI+qXFeJqkwo5eoENjLCryd7O+bVrlewIovkF2/nrO1iss3whi6D4Vf9+X8Ht65+SaA1bqihv
e085eEQ4wpSMVTgoHzYDDEOlQzb3Ty+liCqsX2Ei8dqz4XYFIjfrJaTi2uw198H1uzAYRSe4butO
i/mfHFr6lfCAup9wcxSdwzVpnx7mFSv8qLreqMzWIVfJ79btAxHHfaDvRgzy1Fvj5IE/H6qDmq9l
TdxlEdTFJVYvHr7Amytp8ct262syVjVNwYl+Ohpx6zCA15VzSAKrYMS8XEXFo/70G5kPMtIeoka+
3DN5IaaSN1y82KCvjblLCOwCXCw/RZPYZKtwYrGXyL0DKXHYZp+IBTAY/WuuS1FAk0PTmH7Jd7lL
PXtTgc5viHqbn4NaBErgmFsQTFJzPges5VJvCriFIsZ3sp4J2vftoLdtLCH86i6QaAkvXqbhskvp
Evk4yt4xsPLRo2jC/6OxcqjuIiHDUBy+UlT9TYvIDkIb/bcpWETRllrxA5JTZ57tlfC9BXd976iG
uoQyJyTQ18vNhND5e8muJJsaIPQ+QAjukB8MQd1QO/nhIWcZoksWETnItd9kPKCE54BcOP6xDlE1
Lx7Ruq/tK7gGFZucHkOXHCfOmPGvf4+Cfd1DIQO1uYr8IUa3kvMIGcIhnYMaHD8Y02IK6R10gxqS
5MhIPAnbxlVdvL7TR5UeSITsPELjsyRwwhgbOuNYlA4ZjvM53gGSdunYdSHToKjUaBw+9KUNjxD9
KeNiQd7G5T2yAUV/cGaqR3FzIxDRnkYXnlQTLurOZwL4LwL8iPFyflz8nYkRT2b7M/TQXr2nPYjQ
zxfLalcNozFfunjeF1f3m4XqkDTk6QX0AFP4nmgQ8nZxTnC2sexi2VXt7+unuAG5dqLDl49w8PVW
L6eIENDUxK3X/elexReJ21UMAPI++wqUAKjIkoyIOTnLE1oZOPp8hIl8hGjHLN/GarGPEtKT4zVt
1W8DrMeCOt36k7ATGa/VkxguRN8jwOkUUALzBnDXETKXmoy5fxEhRxQxjPNjS3+v5jhp36tK4AAU
oS7rxJRjgdyufrKil7e6ziNFrSv3Sss+wsr3obG9uXeMUPBQGVmXP1fuHjhGKqB1aU7LHuaSCIg/
zG4g5erjGScl56y8fCRXdv01yx0x8sqPPHd40f/Rkxf5QvxWRf316gocLQ1Mxw9YA99gk9WiBxiz
aNsdcnnqLkHHu23sAqPKwhinLqjYPHIyeXjz0mBcl6Rfnd2cgbw1Y5bITxD6e/E+X1fMHeA655hy
36Qe+v2k/BckBFnRFC8Kt31EyKmqdpQPztGg30wx4SyWcDqIwt8DuttP01xOh3j4P5sHoyxh9pUx
eG+b5nsdxIdtkGHIGtBfwpV5v7KYadbl9Waallq8amewrAE7DBS8o0nIxtNsstNjfB/+OYJ64ZQt
EkIvjHmRTvyg+l9/0B6LNzHGAfk3Dq/DuO/aMp6R1ZHe1bP7Pq9YUgdQ+pseTCb5RT3otmKEhZAR
cXSuDBnGD5Hf2W4MFvb7UODcyvsWNaK1l9eazfehVzPnG08JWGYNLEahXJZj5vNYCzGSL0HhT40p
z9AEzL2g1Bf9AAqBji7NWuzKU1kXw/ppnJNctH1cFV71hieKbtCXREB3cZKcUG4HDZSgEFNsgl0Y
T4R2wOVSjv7/yE9gvDMAY+GK7lrp+xcGPsDVAwP8iloS2yDKiaf2nxkv09E3FgSroc9ClxG3FZwl
6BUos3mJP4abtU7EmV7K1XS88Tmj6dtetVoXEVScp/zzGdMQaMSat4lrUpvHmFpZTdS0cy8mvQRm
VVI380pCPDE1KfdPTcqEJfsvpg+EcTqrLi5n2kldgQKFKu52x5YL2GK5aLPazFVz4Z8zdoOlrD3G
PLdSYwcQXPk/8Byv3ECPvHTCvZBpB3b3IFLoexc8z2bQ5HT+o2VHBLtJ487GU0yjitlvoMrwa7LW
wKUIhNd9Pd6dgOWNoSviFK27pL2O0ANYJujrX8hOsuidMgiU7Z6B4yjiaXtsU4KBnL6GZ9rqeKb0
GVks3eNJ1L8gnGRlgxTm1qQi7/j01RwfrpadeKcI/xoughpWKlYnC19Wk6UoQABQVd85mJ9qwSG2
O6Om6Ob9oxvblDOxzfmbrKDMSoLhMn3raoEoMdc4iO3y+ZDjHwKA+GHZOhiH7hAmt1lyE+SC63GX
ZJfeUYRxaAZQv8S1Kzkqtnr8E4YhZWlECKt49keOu2G1OmQX/LvluM1fP6Yu8q88FZVJkjTwYhca
41KZtqLtn+EsKwpFp29SrnrsctCFve9IyxNQDMEQKmIL8N28I51B3l7IfG5mxsCw/EYyLjNrL4cA
av/7hxLsmFP8vPIXpqRb29E8YdxxgWjTkUk0AnPnP2BnJFSQa3mEaCivg3lOZA/mPFxI4uqWPCYe
aCisduk9Dq0rZeFmnkWLRJiCk+w22/TsDxRR3tQ8hLPirjPxPzQuOIVVhXsmVuEZEQ8wjfn4sw+a
B0EfM5imX80a97DWPq/oUS7N0MuRhzuqYQxzvjt7I2Q02daKZPk+dTLaqmOaNHgdPmQikvYTQ3SD
xzc84KHUNJnv0tfdL1lmaeXhW0USMLzQm/odXmUAz1IZ1WPBmIhoQ8r25chTQ/VqQrGiA9GvhAxi
QQlGcRxvrM0/EX4dQOnPc4ASaKxf6F/Drdq+H2lW268VpzsjQX6L5x1qTFC+tiqUJsEX4RwTunkg
a7kOLCwXI7QqbGBbNZLL9LPlGgOR4BoZtPeT3TRFiE3qvLdzQk3pmSgzTE/e2Gd7PfbLWn0bAQEG
fdb6EW+PUuI9vlzoDkChI7nevskJuDXdhogLDY0w6neozDVWzB9qqY6Q6y22pHDeEYawpqjKKnmy
yA5c3dhgYHl/6PxM5TEZqFJDURyyj0SpRCAD11G7Hgp9fNNFnb18iehJh4A/q5njjOkY7SmJ8lTL
sKAkLnVZeJSOMzuF/qwkenDB89JP4fLv2BGcLwrxbgtuwDd2V1ycXPSxJxArdY2cKHDtd/gL+zBY
JG90ncSfRfzZ/eKAXxikKVD56MrHggpsDUk1/nowr9KyCgZIB05cajU6QOl62+P/jGYuw4mBGlHR
OWMghUi2+OWbJMfV7lsdb9EPLDj8Agja0NnA3r/KY6UQLLr4wnnqqiGGmLtctupouaRopP4mGsMF
PpIasX2dDgsbglMuJ3yuqET3NnFF2aqfNBemFp2bEydLteIQAa9H7Xkd3Q1HG0tB3nHCq2SkBKRn
0n13VsWq1t/y02HHCj+lahtODPfxZi5qXhaETchFbuefmxXQaAQfMIT0fDUURFtm8W0KV7ErbL4Q
OdrcNSftgpSemRColyIdk2VeZmluvJLxNwQmiri/wYnH6078eiu9Atzki7M2/D3pi6ofk3azYM0Y
nPRepJcswe4JiV5jDjL25F/m2HaYLc5fK4qIgT3KMvFxYsV9+0lh7b/1B04NMBjxr8FcVztfTrHL
oxDcYcfRJK07ba7YxKdld91PVsCulnRrkrGRqDohj1Olvhk3/ymHyuaINUWi5Xkb5ImYU3x/7UyN
XNglVFkl4CzfqAFboXPiUASWa/ZUeLzUwJ9KaGtLl35mrl3OjDYWlDdVrQqfCCbP/qSEsJspgpq4
5yPQ7T5xK4NdD/yLLyBGE6HVCXzkOVigH8iM5/O32FH1Jrvyt8joRSirlhIIS40aVzhEz/J10gxl
roSZl5FhTMNHIrSor7nqVKSEN/Ql2AD6QJvYCUdzgHUZvDNUH0RayeRmwdTTmwk55s+41k1Jy+gQ
DPhTpJ7D0zJbWF0bqe73mkrySui5NtB0pu/MjYFuimlZR6KtVgbn91cPJWHmgTeze00Ua2dSnOoL
0kPdyXa3f4cvpos6c9noX73katSKnYc3o8RUXP1DOriVBXg6C1zBcwHmZNH34a7lbwiNEqE/wbKk
ZK7VDI7P4eswW1X3Z1ZoEJmGlVWTgIxuObW347k2Fa62gr7RVGtcMyo1T47wgTXyu/Em3wTIndPY
B1LFb8Grox6ZEkIG7Pda/1DKiFEcwfSmRcpr6KQ1FFEs8vc7ho4u3yAbtsgbjiVqip1FSMH6vKlE
g9GjNRXcFNOtS61anTMMhYN/dL53cpwjqOv/uoO2XzJNIF7CyweUhLlEJgMfX0wOxM7ZBaSSyuSn
aCjNcHdHk/NNZ5YNP7ItFRc8aFsJt5DP3DRBEUPZhxjZciCkS6S8yVoNI5ksy/4TkBUsDQLlyqzN
bWz9lkpMTUfdDUo1jXMORQYECPFzAe5g0jTc1U9aHytggyWssvDBpyAYxzOVn6+euEt4sB0iKKLV
P3W8TH6WATmXn7tQp1cKG//8IOuAk21gXWdKd6rkTESUvkr7Y7UBMaZl00tQm5cZNYcZwomgydhW
NYhQS64vx5A1whyQonprjbPPeX4Y9BgtLTSm/VEaetJn3oZ48OV+MJ7kk2RdxbbLf5vPFcrcJNpX
SaZ0NEbOGdAO2TBAmIBjQK4LGtnn1dQPmyqgzUXJRr1YS/IKWOfkOMkaflDj5/2mggHtl0kgYE6x
8Mkzu+oypydDe2CNU1gri7TufG4mIFpV7P11b/vLIW5yzdGAyyFhdO16+iJnetzXeeCyBtwmUcdG
F9An72y4mVsE4CBdb2CN8EFtc5qP3YKzSMSFdr23Gwftq0nsZLf/bRehP+qJJ+XfLrUk8pDbRQx7
kKV/SdFFogAvGjtP4WEwm2QSDh5902Ks7RFAr1OWalZacnNPbrWOpjpKKYPkxHWr3ZBB+XUbliJa
0YsvT1rhwb1G+xRXXJBgD0j2uSpjfpOUibjm9ssm0Mk/J4aLwQDmXNV/ED5sTpCRELE/CARElzhD
++cw5zhApNMolMdcWyfM/dCcppHNSqI7Q/cUyaCxh6igZ4+oK7l5BClksq4nm/mR/sdo+ew+fAI9
OSBj4fDWTUgmMqcPxuBQ4RWgLB223Cg0YaFIrOC1zuLa+mZ+fxZjsBkB1IxHtTdgZu6GrVqtxyki
LwYujRDY51in18xUlCHqB09qZV8Nb4RZ/uHWlgzv+0GnctSYBeaAN57Rhjre/MPSq2dXSS8mjbnj
6RwxNUJU95foVVWWYMmTNC4SSV3RSjdv3bALZIQWrs29ZRH3+af4MPBM/nmtJiveNM5ni0/xTeA1
0IKeLeZjLKTeEKWUuzBt51QUFytVxnSREGAYp6OlK/KJ/Zw04DxKwnDxXeq1bWlCUwErKYRnqgAc
J1Infk4FLVBQnTnK1hk7sm47ICswmItsif7+xmapUMoevMFCG7O1kyrFcMs4xCU2q0tDdRy+WAWF
vCsVAvpHJsAA0R8xDFD2szXYggbFzeFsbL9W3eudoegrTZN8HfjaBal0qDhdsAlo845tWWtFH47R
e97oQkhJhclZL+03MBiTnedgQ7R/o2UkJzZ5CTm8KgFIU/rHK85Fb4euduBt5ojj/vYC6DmlNx5y
W/YFbJIfqSjpiC3mepp2kjJeeLFDKm7UaIyvEKKblX5UeWXdrBjAIrE9ZejwyxuGwqbTHpyjS/sP
dNVbHQ819mkIuteiC/e6x56QZohCEvuiDZZQSUmikEgBCH1L2TQ+GcM8eK9Vrw1j1GRotwtZ7Q1U
LYyDWu0fiYhLdS+jHA75paGFCwZvLG1ewBlpN8viuOL3+myYSbabYfZFzKXtwrTq+UcKPyYnq46V
bWB0jdgo0kCmb1BCPhnH52NNqs4fRY3a28RwP5tWJAwlyv5GPgGuZURKp/TsQ63BYlwHFy2TIp3u
cNgZOiEsMlOMudBTtHyow3i10vEZuhMnVjZIP3ABTDlwoVWK+liBmJVwVkOV7xM7YqjlnclGTD0m
1iQjFCd8g5fSjJ1HIQWpCUEwKhraPe6uCdzm6zEb0OITSa1KNCbjchtFgaxm8J7VYISi3aqvFHxL
9AcgJkGiq2anZJeWtLy6qTWItiDZWC1Pv8bPOx927o1gbQxHyH3GHXS6NE2JjEi6+QyxFMcrwP0Y
x6AlYIynoM0SsQ89XObzOleSiSOVE+NzvFW/SiYGnROWzr+13QOvoJVF1i0lzJnd73r4B6Bbyf5A
br53yu8qhJhg2V0XHtm82F95NlKna+jHYrSwfiaVJR17iey4Nw1oABczLOIhfXKf+HOvFkYtTSY5
kuWzcUdCzljRPBHZLAl3SHmTvOrDh/nEFed/F4FbdMx5wN7+PeOtuh5ekH78CXRlrqbBMd9RIYXc
eyrzrNtBggf7KmCKWaoqu64m8zIrcCOEDdqg6cll003XxeJOIY7pjVEFkjT9OpqYABpN6Agc+4Nh
bEPNayyBK/ULNZ5/sMDKJivwkbzxc1KiUANN35lNW2PhwAZyur33gecDyKa5lflGQFuqM1KVGjOV
rmj7SGAba7+8ujKSwt1v4g8hUaUfeyhWXrr/TV1qTwzwe7HLwO3mLU8gejjev/SLGDwuA5ThTw9E
Rl/g8lg+VjgzIlV+IhiCDFvxXgTWTL6EwJvlGc6GVk9ToIFj2x6W9hCkiLFtCUrzqgS/M/nhOdNc
3+qA5SUvSP9gobR/BZeZDCphwcCoX0jbqi4LcXasQ6YiUxDfi90nSOrt57nMXvv5t410TSs2hDOX
bkpQ/5yHJCXmCUMtzhWQEVx463IgxChKkpqAIa1KienrV6RuLFJ+HkzrxhAbKqJ77XlHowqBfJVb
oVyUpqDbai4MDrN7uAZu/yCj2H88pVJpA+NT+EvUTZtvfRgq6nR0PYODnbFvAWtN5ax3U4pvCxki
CoGvEQZ+ryvxwDtvtB7udGhC9KyZvNrrNfuPFzGyb2a6GxQruzZ35ZwWZM9EdN2KXQ3RuRE60+nV
CLl4MO+wogDp8gifKHpQjHnnzo/YItSoSuI+LsnBMhqhllAkaV5+yN3eF9+/BsNVWDD6qMg9p5Ij
B65IUR0w5mKY/Xjtlf9rti02GzohliBpCn54j1Z0A/ntipcxM+04BtEZKNVLyrbb7rqWfkPo84rw
mhU3tBhFxCatg6ipVFQN57gTUUbrGhNP2RforLJOkt3zINIp+9IQXPz8xst6iiVxmEoA6lHhPeRc
ovQIKOJ6HJSBQltW809FdxTT/2kAnTqiWjMNiMQMSRv/Pt+fN8/zT67Vvv5sm/Y6tK4RMLkOl+Vp
g3RGeyPksZXCvrbAFvjnW5h5KejtwZygS+p4XV3YzU+G30QhneNMQS06EGufjNcK5NgvVVuNTcQr
rBJoaLHGfsGPXG6jE8jvv/UGI9SNRrIVuZeuo8O8yO/V6OqOhFkwi1jWnkcW1tuU2VJANjHQsyiM
X/EYkbF3ZWK3mNdvcJBvuSrnnbgHXXuexyhVlSYbb+V18qbY2ao3+nxc6SXwPEymeDRq2D5wxiaX
g8tqDfXW5xM9v/gUDnRHtoxbuVIx8HCZxeBpawGtEQWQvZ1lXaPx/F3hi+CmptxOQ35PdWES9Mvk
OMO21Cr2745+CdPU+xakajpfqLjD81MUBUs9UfTRQ8FH3C1+K7TWV4DOefcDp+UHWwQ60tGwvhQ2
ld9l2LrjyMxonXZP1eouHZasLaPAuv3o9n1LQzFRhcZZWdFjUtpiZXrUkr+g4sBZSGNpmmc8P5sr
E51N6TBp3eaZYhkr3R73hCtWkpyYG/i+/fr0x5AzkOsOJJBawjEP5fzhhmJ9LEPxZu6jkDOnTqko
WcOcAGdlHzb8QGavJ9IzVQizRoZnIshHZ0xJeWhDju68jZvAS8AclGNqsY1pUNWBvgEqdwSy1809
DtCQcHdb7IeLnavF/D+cORr4S/QPA5KwYq1Dill/MNKlo+hw/5++XQa05pUOBeGXrIPAJaHlz+OG
rNcCLxiO3QAuzKzP7n8CTvQYrT0PbLQf3JQg7w9gPJKHvu8Saa3pk+FbJz42EwisK219yPQ9A5hH
Oe6D0ABPynFW4VJ7seK1cyrmkgJ6RdGDmYhT/i77G2Gn4GhTvZC65bgP6lcLS1ax0hJk1EWl+oZy
h4ajFdGEAZxMoWAUZNKzydehmfqOtoARQUsrjiLLCgywZSl+nglMUd1s4D/4ewab7PAaxbuepW8L
iGZC7Q9yyLG5XwxVbcHAN83Bgv/J/nVcMrp9tqa07YC02zGjFsG/ncGL5e0Lseki9F8mgmecopcp
z7MeWaiMwiXzp/sqdU+ZqULE+K7kstoDtVjNMWonDGmelnI70Dt9DO4Q+IJIr8ozDn9CbPKn1mWf
VqVmngqu9sQsNHeEbCnDxEd4NHJ86wbpue9Z0dkbE+NFqxBF1TkNoCQONXO765PrOxMS5T49+MLh
uzIc6MCKmUX4cixv3oNlwVHnbgqF9/0KbU+h8KOAd5Mkw7U11fd3l/CSBSDqzUNb4+qRTt6yJeqT
4KZgzB2ITLfm9FTU+ZJIFLnLuJGaOLlimEH5ABA7p2xUAF1q8usdJwfxjmKhSgyrI5+YUSRf6WVJ
4uU9nmlKMWbSrdLu4HRS3Bn7C7Q6Nju2rjsbktDyaph398a75wOLnT7RAMuHMvft21A66TNdR3lv
Ajz60tpGqQvWFRE5UYSSI+ChuJKODyf7AVQSDWO3Nmgb2gI/dMMcYG/1s/I91yu9zFrAq4czGqS1
ScQU/fALp0U2jaHhan3klQU8/Ou+u+P0vj9NDkYhTMUumyuoXL5EtVmmXygH9Y8ccKFjaWZWC3Ao
dCMRWb1r0E32XPjMLeWkAGcPAawwYfGFtqBoMvtydprWYo/78TeTmxZCvCwN47ew8c80r+uZg8DS
qN8f+SQ9CUPUzidimKeypkzFtELnwFYX2YIS84bjDDErXwHNBegtzCYbXSCdTMVoU+j92cLd2zng
LcZqgXT9SuWhN52f4ZzfBfGRSe6NaLgbCoLKb4zxNerxInzrV3jpd8QvvLDXiqFxM9ukR7fM80QU
UMGdKMkVrQtTXQRRv7XQSGrc6hg+jzUx10OpSKkkV7lW1JgZw8tM+27pCnMp0Aamhf5ug4Z6CK2J
1zqR3VOPplFUCGijVliY44UWw/VqS33yR0O/hDVZ/6IzJUUevs7efQWtmdrfsdkX7h8KQscIHid+
kh/V/J96XPgW0P9oYPRSuMfzkWRLvQf9hYoj4Ph6oFqeaRJ1EclC3tdjxpBa7C3mQSUd5AodAco7
nTk/7lYpc2d7ZXwudRE2zAC36S/KdBX35qL+JlRH0Ud9PvthfBuSsJvuSc1Ci8fo/ryfLeRB8Wto
XGWzGtKcWEMIHb1hcRb5HhQmLdlJqC9KG2ZQBLA+BLFNqHcKb3GgFuNLXrsEh0rSffoqSbHT9PFl
kaFnHLCtHH1faqI0zvFyx2fbSy182NW3woQ2iquHmARHT8N7aqiEqnU2bKi8lZkW91vZ0528wCNG
pDgtESFgftGmnHsn3nPAFhla5UwH7niD7sVTIkyylZMXVwvUG58gm4IqVw2+jbcci64AgbxAAxV/
e3ab+CLCEqm47TcaN6fXPCiqFchy1vj5tWW777OSa0sHNIlPneDRtqeg0Z93VWk8VszRMzdAph91
AwvtN7W9DUJTGuHyqWnd03sdEuuJ9uKwhRYCaNpWw1KSjGq8OAvcdZA4CT5HXP0m4ol22GJzGHcu
VqPK7beVhsh1zDw6EtVmrY+DVOeSyWkw9ZgmRdusNO/EtL+7ICAoFpVYNSPbz/dAotCah3+6Nali
Tuz25pQDWPp8M7wnvd7y8ObsaGfyj9nIEJhCFpVyc1YN4Um9YraWX1fKbwYIul1mNajjsb/Ap+v7
VXgztYjWiBAfYeXvPbwA5MpvOoyLeW6+IJSwavbx1wSC/YOJzLJoGermIwQoJfHHBv9doWbsQi3u
eO+rS8PE1/CV1UezAbbmJjccnsTKjRseylgruoEBRh61KtgpYWevpsmEXGosGGftK/wWIbAwfWkJ
gykdhA3bxk7+Itl6NsD25+29e/f7Xynxnmnts7J6vBS5AIIy1+co4mc8BXz7WJW371zrs9Md2XQ1
T/ki/j3UCbRWK9C06L35o7xx4iJKzTsT2D14PmpKpg6mNYTQMGf05BldL/EXX+x749ziipujCDwk
xqxuHPGiZDMh1D3p0ztU18x/MdWWpjMJFw0kvENUBc5CJYIWVlnIWEL1UCubCYNic8K70Wk6sxp8
t0/j0E0zL+wIQclRu+FEfq82Kt/OAH1OTcXTpVEkyycVaNiOPuR5MAOeGppv47uFFd7D/SUDVyp6
GC6stBLb8jmkNFX9HVpYxpeEzCL6XCU5r0/xLms4DnIr80gwWGgfIFCMb6QJEWfmcwFpJDwUj1Jn
HBokd7ovehj4rw1xamo4hBmrXmJiCrDC/YprMpXHJxq9ALSYCj1fd4rvPlqymT12NOlI3y7x8cZI
lQTVcZsH2l0YVWVIjS/ZPvfwmxH82IPyKmG8yyOWvhBM/FL2YHcVx5e0ifEsfcFNxvkEshsAC0tu
qeMM55KLU3Ve3Vjm/40LnlHdRNJgNtMCGA40STRpKmtEJxJe+TWmV+91BandhxHL7RWTe0H6QkXt
OZ8JrO5B/uCZz3iRQ2EOc0YwxsObBSHOladecfGYAfYne7CAafIG55hX5kyxpRS7JZPD8E4vWz8y
85u+loEClJBWBNtk6AQKAq+w4Yftr6aSh40EKL2Q+H8efXyk+nvq9aXl3+qxw8nnXOH7hUt9v6E3
4viIPvAVlQ3BLuxrPLpdboZ3Tq8XPev4jg0yJVs3F/gR2TD8oJdrg0XR3dPVSNVgsdHs0BddF92p
vKJbrOsQvz0RpZKsWMnVv9LAf+X0mA58a5naV8M+Y8JrkSTt9uXsojXnQZxPzwXe8em8PNbzqGPs
oAh+6xffet6/hCIzZ08xsls7MCCnHgLRx+uJE1KkIDHLangGIh1OvNqWLhMs9vPPhsQfMZX5Rubo
Ii4EfAxrp5RiWGrneafBEETGbN6s0DIejXxQrP8KzovjZ+rvl7GGcZt17/oc6AFYaDBktLiYZAsH
8j7ZHx3hhFTdGXNcd3Pu8booT7Z8FlyvMsrdMh4RfGDjitL5bvWtz1kXRZJKGrR8BP3W6Pd+3le9
Py5aR64gVGCAtOmOLAWSw+kzrxZnsSooO6Y5LT4uLqK/AKp0X141b3hCrEJZpUDQ6U3ffawj86Ur
WGRIFPW6RYHeCMhvDa71vg/KGzieE3ofanWbo40HiGv4TTVV8/sDIPtIcx77m6CUvi0PKRbssKKQ
UizTfx/Xuhh/ru9CvaU00QR/bbcu/SUzSKnV1m3QRgzxE42X2SiH+4SXyIxWqTauHmwf2ImPAdrV
yNhus2ilmSF8sgPwCrN74Ra+Lkt8nK981kuyeNVJBRt4L7iuzchvPUuuo6vcsUO+0j+YioBjNeu0
5qaaODdyMVOPPMZxQWI/Wj8HMzbxb6VdX+lS4/lXS3YybvTcjbVeour28oowsVsJMcYgdmznIGsi
3BQUnRbEpD07q8soh0XL0GiO57eTy1kyZgyRAWZJ8JOVFrimWwsLiDtYPcihovKCRt1M1xIkUsbR
BE9f35UU2atFEOefHWKBAsfc6upnR/J/j1d2Zmu7RijZBY5gL155y1Ev8na98tC9kBI10ByQ4YQq
BsPEgVNhYuMLuiQFdJm8iDlVbP+m7Kz8l9rTuD4hf/ut2nzuhEBJEh7ZT3uQcmbNLqa6OHqO778G
lEx0rRI0ymyRAwVLtP43msa+FND2PQSvpRSJUiTvX0kuT3YjrwtdcqfkCbvdoJVC6w13E65pziEw
q2XDV+NIumYHbyFtNzM/maHkFQi13+k9hGcCLyVwDSM/IRFBMSh/oRFW8uAbkyQzaIUAXtnSuw0i
xnbA7Ky/cv9BO2tbyGR6DRFjYJDB1GnAXTTlr/Ntej62kG4Gu47/JXqnFMkfe0BsznfYdlfaUX4S
PYQ9cvPJhqRfR3WWjRmKFGuyyGKbH/5HMaG2vqmAnDJ+710m9zmyXO71AcnI1KxUCr6c/T6/8WeR
pR5L0+s3ks0/HVsQKQRgK3hHElNsMzmaUZEw6XFu7GhD7BiCPF8m7RaWpbAfWoVeNc9ISlqxj0Oq
AKx1LK/YaEDv6tMzBIpcoBysVnc6VuzZzLfXHgRPprjzX0DHnBUydR5GgigNmrZgN2Nf69RTR09d
55TOiwZT1j7+ZCN4gy8pQsgvTrnBAO3YbPJbgvyqkRD9kmmRupzT8vtnaCcGYkcDbNx4/qq/uto7
R2KjtVKRPTGBjbDmZlskrfFF7LbVcmuzIkE9NKmGRqsx/VvqkIvzAsR1UigOP+84lX+LhXPAI3A8
IMGz4G0c3lDCvQ498bgeAqrEoAJzhCtC/2ja/fhMotQF/8ATwi2IXm0ZBjlvgkJd+zoH6r9FYE3l
GtRuCexoCXd11pGhmQ9LOU6pP/X7nt+57M/j2FLBj1mluOinCqrRchlDFIRxJVqS+dNyMree+RkH
B3WLNsSDMytrkGlCPOkSP0w4LbmvZPUKQJ7UhwLoBMb9wvEdDwAo5vFgCIODIsRdsx7WybnwS728
w6Jr5tjfrdOwiXdKxL9KWzp18a9gXj2ZsWojiKxZ2rmOkYSBxFkaTMrXyDmNEK9sBbWrNiu1SEJS
FfqzXmUM1TrwQ+j+N8nyd22BudIK8YmMcaEYjUdTa77e2Cf7250VnMgE82RzA9tjP24+k5IL6phK
L76x5AQfIQNOLeto8NxK9slo3/6AkMFJ+9U+FygF1j10/fQNIuiINFS/edErKFT6H6a+mweHw0TC
a65Qgpl3teF/xer0zKnEsOP7FMC/gNhgESRXRF1nluW0/6RDg5B8Ca2qLZWxZtHCl3+rTyQTxjUp
nSnUMMuqXYMQQBlREYycJKIU7Rl8O6l6RLvyMg/IEkyFFB104ANFLZRyP9EpzS3Nnb+OkVzQWc3b
XVZJn+GKkmUHKed4y0dNMVHpYZ4nBWRj4bpyhBDTgV7tQYmPNu61n+XMHSW14XlFjthlWplRozjT
it/A0ugCW4LJpboNZRickajFAQOlvUl3wyx9jnHmn3tiKa/G5cyBUAl/i5wwtdfMeW1NgWM7RwMe
3l+Xau3GOcERQU1Lutg/dF88YLOXMozIV7o0uwNwAJT+aznA0kF7RnDHhGs86ChCSoVq4xocXDbA
l2L6PeTdyyWI2VeyaS0Dw6MCdoFtQYLtMxLrIKLHOkQWqxu3IzS81oxYiEj+FWYFw+xW+0vBF4OJ
ZNJVZo13jqPPfjF5wfg4AFHes+ZFzt68dmqP4MA7FHYBPht3UOcwjvY30hPxKrVMEd+uxha5TUha
PXpItsW2t/Nf4iuqKL/ZHSXhADkYVFcwL1Daf2RTv7QdydhFqzB3HJbdsIk53QEh9kaGLA1PaES0
3Hl1PaEs2OtctITNo2oyuhgCuCcHzjf6tDaQBYAUZpcoV9nyHEn7g0a9aZd6+6RnUUeYPfFR8RlA
g1EBSSCyO8HwpDIJBRyDeY3rVO1bNkSv06Pd40Tpv7i3OXcATXb6HTSi5kcgg8aSeGVO9H/J6LxL
6jGsXYG8OZ44sfMAW18XWkULpr06Dpqp5r3+cmdEnomoKdhPOIKIbKBv2QLrOY0HqBwqzQeN/B5p
CYwp1eFzbEc/ZjCsxuARgGS7/sK343jAxy1ZUaQHYtz6adUsIo+6ta2JSyCn5TvP15B+7ffYSzVL
Ackz5k6Ou3xJTz/7qA9i/n0ZPtqOWkvv5QSB5iK6TSurJWrsE37/My4kHnKzpCAwJxSZT31hJiag
AOFE2aqRir6ikbVuXysfD9uN42B+FDRUy80SbAFrKI/tIU4RkAwFXQ2TLoZnIXEGIVwhgEMzziD+
PNlfswVRrpYE30EwN2JvN6a0+ilef4kOom4Hw0iMiDMsaIEZC6fASVtucS28FdbSqbqM5kfodgvC
hj06OVuXM1+aFEwIzd7xQUSz8Hed/8q136uBnLm33fHY9aZays+LrCYHY3F0TZ5ibGi6U3C8uqSF
+nK2fAEHF3INNg/hedmqVe3T2P3GLvLIpJk+L7mZxnIpgCEdslWF6VTcHBez7eHeN8rPOMeC/0zF
AAOWppr1/iHkKGmJEw4syYB4oKsogll7NzOa7/dQQTj7Ez27HV6tZvkiqQqPeUzGVmDTzBM+l5V0
xjsA/D2UmCbz599UU5fSEGDFgaqE5o8pvqIxlzcu8wLsMuw0eFDVwc6HMYz8KaNqlV9iZuwRW1kc
T3bAn0wZbgUex1Twi7l5BLTlXcdWYLT2P2moE+aH8XvD784iKwGbdXS1id+4zD35JHvvzb0xUGul
wg/4MB5kCwaDOhKgI32iWw96GAkV2a/7hsm8T4R5/zSk3DefPOumhID0Z2o0OO3niKqu62+1TKuA
qnJXgAPKxagt5vfU12/id+1/osnpuWFgP/gUXuX4gHdGtUxQdBf1/GF+plCDODJf9gcohO8wYoYY
X+J60YI8U0shZYxkSEuP7fgjCYLhqU42/Tyt+oGBJsK68tp9nOorI9RUiPZOL5LNjIaBhLZVxMVG
HZAJTQ780rSZzJipY6c6tIAsXwcBIRaSzih8/MK5Fpmf6F+9vMKZsOTeXULf7ZBlpJDsAMHg8g+v
+C5IXncyYnm434j5j2kbhjL8uYomN5t0fDA8uZ3m6W5IIxU46LIirTiunKMvP1Zvl/rKPrOFJu69
9wjTRR6yfM0adCBHaS3fN4hQnx2S7eLY4FIYHSDzi90aOSqFKd5NQSfenYBjwExUDipOYlOWxJOe
YIENkt1vhAX2UY1EM0uj8ek+AG83tMokEn3ULJJYHFTGXDKWATBMh59Ff5OKxzIhLHYgxwr0b9+4
foQarwDZwucbi/vNT6/I0RhvvWy7Z2lU5lQBKcz7ZD4e/jOcPORwX3wjutC2DU2D9XxeknZ6F39u
P2dSWTJGZ2aKmtSCbBLGDITZ0V1K5e5CelTfrirZGnYEWQ0UBEyTUqBbpC0STPBVwyceKKhvTdRu
m3yostpyLRZkCzX9HFT0s+Y5O52reAX+QrzFPsPUQ/Jc3ssQGtKtKc4NWcDvCseWo41BTWx5VwKs
fC/D6O38ygf4m1aT/T7sZQF/t0Pt6exkFqV38QoPWEqTIuQeD469D4tA99gn1Fo7b1hSupEYvPnq
fMbR/Cll+XsFSTgJw3rXCrYzK04kdya8gXxIluv6Hqo17lhAeVH23BSvZHM3s+7l9+OQSgSak+Wr
QNoa+ndZmX+tVRtpiSTuRC0SXw/jWNFxUH+WNhC0s39wdAEzRohcFwOaQYM64orEOojKkyZ0ZoRm
VXsQaqAYmn6qO1ZQIqytFb1zuecw6KoJ4tFzi/GRDMNqvkIm5S6tZKuoAXAk6KWKJz0q1o/16inF
wsgivpRSRHd60SRP66kF03FsbxqlXAdvYQMs7gEtcT7gxcJBsPwuggXhiO7eHaXnx8aWuEdw6EPv
bmnJ9XZYpudgPIWl9t6KRZeKmNO6xGlkx4B2eF7C7WoGnSZdA0GNrJ64foVMYnwRJQkxLXZascq+
8r5XWgnmyUKT/5jJflBOHjUtsFe3RrPWrR/Ft+TNdc/eKy72F9agRnAeewXJXdGwG1rD+vkrKQC4
Somfp4HnpHz1H7gtWKDTKCiFFaP04o4s+XkWtD5txycnco/v/hB+vs1GYkHvDmIiHCWKZmlciHub
0Q9kiMYnbzwJJoTLQ6wzmMEx5Lgzuqc3l3jgt7SKj5bHq5O+txQUPlsIqoKqoO5W21TcQs1EdUJv
uEzgMERKp8GFGxuASFOiHeHD75IhHXrGyNyjwuCHRjgL+2hvcjhhupfxvmahNKwAdhVk+eebQQPN
eS7YD1d/EXp3k9VEOx48qJ4iZA5C6TeOLdc1+UcNZ3L/OlXk7N6OFDFdNooyQME2nwqzTa2jlLMX
2vTGCLoAqT5quYWDGQAakDIOJQonvZEvShUi92A05s134qtAF4KI+PtxGNIUAs4IjwmAtZlcn6IM
QnGZhy8hZic4Cw06EVc/NKrZWq40LZg0VqTJFsNm+DqtjqRLOT2BBV902RMYJehnxYiuZSPOs8TK
RGG9mW+BWeNk9/4hjYuz39lSmp5Y6p/rtegSDW0fDqA7Wb9wwRLTUE/KUBePCCnu0Wm73V9XDCS4
nTyXecr+YuWQs/iVe4e9lXOTXjQiq3wtrwGcg5PxO7SEWTwjsRVUuT+LCtli5vpRrCtIOTUN8MKP
PSfIK+3gAH7jxw/47qn9nCTBTc8n1vpTEH8DoX3pDrn7xNgvVBs5+QpL50x3wEh7xTecKzFFjYep
FILhjcneIm8NvDExSYzhYlA61iQSpPLy3WifH8sPSMi7DDRYUPNjaWMnIv0g7q9lGQtkOeVtNDwY
GpLHV0cg827dfItWqYVMxjsOWWqi6e+S5U2pSPb5utb/NZah77wJUFgJb0gxHdYQc8pIFUEW5bbE
Ohg7w0SLV8gxbpkfSTijJpBtBgTRu5zCIrGJ7BMdTpGAHr/+wJVbK8vUbkBrbw5ZIeb3u9WtIhax
3D+1MZi2KVduZDriCmzhhzd6DABRMKS795EBQuqWQokb6XwEIcGr41FEGCbhVOku2L2tcXDfQhMn
0wQVhKNWVSEHVOIrgus6ULDS1zIMajt0+myCgEpTO8IlK5vh+zo5dkc2gIKGFvtDTmwN80b7MxyJ
BTXr1oEYMjMpQ33C1wa7CKq/YtvTG1iH1ejqjwOjavFCXXU7JKFcSyAkyJ09WBFylug+yoNhVXz7
5U8aFrwjHQ1/C8Awx2KVrxLc20IQjjQCnWsdOU76EQNDAih0FYA+SrbgeAVayGaFPcYalyL6S8VP
iifeHunDMaNKowJKTgNr5ed0+GAm/J8ks2eQjhAeFBTtE0y3AyYpTe76IXmwvkg4WOe/Q29iLRR6
Q5LKhZPexIasJb9tJJSn03XvNz5T/GnzqgxCaZhkONQUakFbNCuUo/UT4PDenRTjMu2QuVR1E3Nf
uGMSaYSAKeSaNwTtOiR71QekfZYMceC1JlyN9QFqXkBqKua+En0sJDgJMBmtIz6g6qvSbgy6k/ro
STrLuY6jtXSuIzKoL/2WjOZO8oa6anmBCwn3H146uz/cdjlmifKLtHSC/PWWdZuc42vBRvXZoEAr
W0POQTl5fIYofxl4WYG+HMxa61il5tli0wz+CBSJnh5BrSh89HmDTTaDvTcrvtxr6yTsHv8VA7CJ
ohDsbe2LOeO82+bUfy7Jc3OymcXdJoI9t0GzQmsoibnF+DzCj/Z5JaGrhZV5fVa80D9Gk91Gy2qb
dGGofWkJN7QJ7QmRrgQyD53v6PG24M6svam79gqXgR19mcav8ZX2vRJDht8rrh7k8ypPGFIKSf3k
ZQP+uT128h/OA9o9QM1+HEKef7Irdw4qydmIARD0YFmnd/siaBsQ1hHZwb0SAgS0er6qECw3L3+2
5u0rs7iVROS2ww+kw5P+oVMtWFM9DDYvQCao5xWMApqFZOOHeXcnrYVfkGI33cwpyBODjAM+uEzI
rtIHnyyEhwQscKDWXttHCa2kqC/SBBT84sRWoytIHiRo3oGSJ/22d2wOvZvwA7VeadP03hLuNFfV
+CrbVi2fDXGxKROhzE8pdYgCi27bhq4ic/1o5WhkPGk0BqFMWGRqVlZTDZxp6JupIrVMQnEkbvVO
Fvyc43xbaGwB7nJMWkwQyQWOApvv/lFDOxx/bYCReb14Xp/T56lCVLx9WBWP0GxRczfa99zgY/y4
47qWU0VJ50NV9xKS7BIStPrfsG6HD/Rthm8179BWjPoK7Wnz6YgTlqnYv+x3Pw0n4mnU9DgcDRn7
NrXHsSqHs42oegH53MQMbj4Dlwz5FfGBj2J4evOISeE90zPDFCZmYQkssbFUIC6VsU0kGaGr7iWE
LUYC7bJGxy+8hQDYZU3+8m7xoXVT5UPgv2hy3ULXkEpjgc2nvbq/xtYxas76lUPiygQMSC60uCzG
pHoi7FQU2t19SirEY3oE7WO0wRRJxmOfxARuJvqnp4YihJka5RXg7xmUBbObY43E22Mk0Px5n/BY
k1q0onmJY52biiXhNGgOiXi/oCjYeaVjrDHdLgY8d0zgseoaQWcL5kQsNZTp7Q6snrshwCUOkAyt
pLYbG+xny1ACosKee/CLceNyB6cBuKmEtunDdHhCizH6nEueZMRAMM32xQUOiEqbVbkiJ7M/QAv6
HVGVBRIPsKhldnha0Od8D4xXAbADFXD9736So1hU/WfoSMQsurlESRILkqFY4GPmsFxRFvE4ENQv
tDRNuHXhowWvOd/wsKGrqWNzMRWGuE5AVRRZ/lWnpPPEG3wjABVLACZmn6on2YGIgmNHgUFu1kyF
Ntr9uvHOXIxT1FQwKusxxRUaTPDIjqzeST/ZQ1gkU142mslHBepq6GEwXFLgIHfKoXjdf42XIAFe
vdvJUqJCrL4Hul4CtwGeyqAXYKmQRhNGE8UkY1ban32khA7GIYU8/bsMYM7jtb659gByhGG8uI8K
qE2dp0vmAVlQJB1zrENuDyaVIukzbGVD2y3HxGZOBZJcUWBCAwBkn8Lg6FPo49rkEyTQhXgk3Qhr
UlTydXimi22/XYKxAUJHZ8O2fpTaDy9qa8AwxIiGi3b5QTbbux54lGZMm6sXBYEucSWusnuHL5EV
u2LIpZOECuGaHtu7rK18hZHx0yP2mujjZhAUZJE9hI+8EZar2qyG21QauL5OXnlZYm4QMHLGNwLf
37avW5hypVnL6LsBVjcORnfHPOx8Qr4pbUGk+hqiOkfIkegSl4DjrVR3rdm+A9OzRInCVLF83fOs
x9XCxoyW+5t0FZ0I2atxnruT52GuseJ+UX+8BzyJKDR8ItKdnehV522lv71jPV+T1VPw3Q+miunc
+V0OBGxgwXOezdiga9sglPY74y0Ze3/aO7YMOd3fMkOMkVNf+5oZVn/7EbE+JrIJOoX6YpRIy3nC
cDdAf7hG75wtiRAp8OI1HwA0fr+t9ApqHZWMS5fD/3hiQlAhxU65Wsb/thMBH5MaEzoeaiSX0BrS
0DPHLU4FnCXclwP1YOOquCZZL5SBwkb5dtNNAUdB5hZpEi2WB8BpGS0eTnXM7eNtuzgqnuGiOTfo
JJCCh3QGDZEs3qdJB6YZNPhJu9CIsAO8Jc43eG1RklAhdCQUvqTXn32tt8OxqtKVKYQtDhX8hZ81
h0BS0rjQOSJ0ZRNXcv+PO6BAyqrlRXP/7uACDViUdHZjFGIUSd2lVmp55bptdgakIvdLjQi24bws
nBmgfgpSAAIF+2e+8unGcw8LU7Ef0PwJiMNbXc5r2GAuquYZf3dMUNHHPb9yE7CR6HwAwaBcvF/U
1GYurf5XGRsZNsIDN9nQFndEL61Erjw7XnBPXwv5zZmKGfRes1Qv/3Xk+QxHXAuNhMa1usxK0lKd
h/EgLo2cbNaqqEtcH6lbyiEkXRnsxv/uV5pT6FRb+6KQmSYAMgf8HhE72aM5+nld2MSwbQPAW8Bw
hEbLmCq+KzEXdcm/qdyraPdwvaideDuJRRIhAJtfYw0vqHnNdds4/nvtzJVrmrAPwSEzMY0Gi7JY
2g+nt/RQFIC3tJmPBTh/gI588h37p90IhP4/96eLcpwqx49CKIngsZi6kbGuFr8rvSDe0o9GjaLm
CuhY8BATcf+ysGs3vvQwIw27gFLsQvpH6WuUa68pUaEVn0xmaTGvf1vF7SLiXO7g4j49FPmf4ECy
AzCCr0rkrE65jjKudtuuvh1HRsLXyyLTnreR5UiO/fvwrj8pVcJ6T0aU2QjhuVHSHI2k2MD3SdPn
Fdct2I596CbBJpj5tGWXcdzSzv2Pz77AbTLKmPDFy8RJVexSbWPY4iNmDpCILh//faR963GMkGPt
AW+p4hO4HR1pAi7VqF2SiYTrJFk8y7Jlv2XU+phSuYqOIR3MB+HudXLks/TLXP1XemoLq+Q6+W4t
P9OCmOngdWjz0qtlnVZMsX7WET5tWV6n9/jhmlfYKk+T/WsesM3tSu2ePT/vnMUR8DPG+eWob7Iy
ZBPp1VQg3om05WqcFeMjHd53YC88eb04zaPX3durVq7W0dAmdr6YwnNvJBJwytpc4prLxCX2jmJT
B/eD617Ipy26Akldu0lXD6CHAIW2BuMrqSW69iJWtl/cJ8IeWO6YBXIUv+ABmXrP+bok0Da0xIqm
ebEGDfKII6icc1X//GavFIVWAIhSdCyWn+A4vI1DhrfmbRZ2JgYsAGQcFJtfg/deKvXI0NI0vzn8
WOolwE4i3qXeoQTW18gRB4pYSCd7iZeD+f73hfnE0UUmjWBhWOGc0mtuvdej6QGLR77VPXjXC/yJ
BJFWVCzNOJwpuxEl6E0NkoQHOgmEikE9OtZNCSyc09JSYUBLzut1bEeM5RX4wEIp7pSnAdiULPc7
8uavKq2AbfuPgAzrojJuuPBlPj+vYPhrAh26H7SASuiAUSzkxHbtoMVEt6BRPoc174UEIM5OC7yi
Jh4t5CKFz6/5byhSHa5fCdTLrFAQ6B9ZgPGIaO3rShJAF+HIr5w+V9dxkV6buFuYx2PrEjK1RQRe
+qg3tlzNKo0hMjeorYYKDyh49//cK9M2hwQKmD3K40Ar+91rsTYcHre68GdHnnqvpulofZI6cyRS
cJFZ3IYNpBasaYBviSxZJ4Zcv9YByNh1OpfOcf4GevylLhbTN3yO+cvR9fCOuZ7HzwzGJE16lloo
hsz5OKVXWGFy0ODpYbykqetKAb1rrHMueabwikwINiaekBzzyXTBRnSxNCBuNvlqQAZ1bGHb+ZGo
Zb1gsKXgezXMlwmqzfuD72wp6W7Y029BWN5ztKc2p6X1eXPstuWquqoTXE/g5m2Sp0UNtgxW+1BJ
cW6WPvOrzjFSN42SRbbHAqTjU1g1RmncubyB+dMtl5tA/lYYHiW7DYWvoVaeczE07bKIBbYNUd//
ANanw2jFlwLX+AYjorA7lcYRyD+DAhf9aJNo8w7+CVMuOYzXPBMpBrR6c88nAvmQm6om+rW7B4DA
R+zHnMyGBodm9y4JyB2/vIAENHpioQcSQHWPIIxz03hesHtWty165Gtc0I2OccsQjKiMy820oU28
cTus0Eq5WkfNcBuEykp2tdiqxVt65HGOPaJ1M3P+z8MqY4SxxQ709q5VpR3NePw6Dd+SBERU18py
i5vcmjtl9xUHh/E7qIzyRN1Q0tqb73ji1zy5mz+M1vlf0Bi7AAKKemtwRvoM52Z+nIwjP9jqdg0y
2c7+O4gmMDAHPjgvZRaLo9POi9wkCxHLyQ5SekIMC+IXSxsHQ4IwelVs3KbGe8WkTOsx4VoM4pao
SfF+l5LJ5p1aKHJHBQLmOPv/qla2GcexOu2/knk+EZ6/PARFuNdxlGHIe1KZ0Vh1tqmLeFotmm/8
sk7hqZSit3bEamD00XOGqxWeBHlp2vm0xAP4eEG72lV5UkcObfuH+GMnsx69Y/OIUXGMXlX19TTI
kACgNPwZTAM75j+Gkrm4hegya61sicSDl0nDoSMmSt0nVxIOn1B8zs++Lx4U2B12ePMSjVo8wjxk
uQIozXMkbqMQ1IQrIVnbn1Cn6ECopldn/cunLFtNH0yKW3aab9E7XAJcm6WWP270HuO3kdZVPx6I
/VJG7L4XuSLEYfffZXA475Zltm7TmanQJ+N6jXtHwou01SlDJnZFz98PXANf+Y+ViStmft61/80w
t2hR5zowecfi2fnJIpIFwBfQCmz2u2IjOVTTJigeWxIjjJO3ZiVYd8TPd+nVhUlCZiqVaTEx7pj9
fcTlkuvSJNBdwsxptCERZvLF/7WG35cqMWfeVg3kdvg9XmHg7m7nZMX9L9WnvVQ493YMsA+pNrFe
OA0jbX921xGj2xyq+hiHi+AfnRXEhYkOytaYrFvgq/ZN4JqRrjnlP4BfJJghXN9CCHGi34Lc1xce
jZLly9HWGBKDjKiUl3ZNfu2ZhigAZqvnIFvpx+laFtVdMRxFjAwe82Ds7XsbQlXM/sqUK9AnlpmQ
9OwZullNeHKSIzE9w3siqUcTw3dMoxu6s7eCagf8/+T/kGpdePuj1g6dyRhRFYu0vrRzRMvCbVil
uFZ3lJn+6yOD2kpSH6AI2TjBEqniBc636tpQDWN5wtkoebiWH0ixxHCTdvTIVQNOAs4+w8Ks3XVr
uN7tiuUVKRbylifY5HTP6QnKO9ydsOxtVpmSYha+opfpYRU3KRgmcGE1U6ydd+BIUZjyuxkqJ3Nk
4cNPgresNzfDZyg4R6HRrmsGXt/zZOpJm2fGwA0BFuN8MfvgdsHgH9mkHWZav419wi1sl2vnDLKS
kw7OdC6H0GXbL7g1d3W1r0obTw9+kcikn8rKIB4hVpkP0U4LgdlIlLbTeVWuLQdLPSvZX1vPiH9L
ewFXhWbsvocSmbbNa13m0T/HuzM/dNLFaD7k3Xn9ntzkl81F4Sbjz3ehjWMg5M/O5mNvFe2vr9XA
WcgfDKT2auryNw9HINffSx3rFhFkwVfw7uB0F2E5+wu8S9ooniwORKyfVmp8pn3mNc9oSOhm9/HF
ui1a3p9VkaB8hjcjuNW8N+uefOJUxtz74hnaIsoyUQNZIL0TuV+lJLAY7MsPnfqb+M7RHbfOt4AV
NO97V//p2O4yV4snlGMztveIGUzn3SRy+yjLD9V3crTnIa7STDb9pDqp7DADPHAuTkbFCxqFNtDF
Htn9XCjZD3UEBZgXGIUFDb6OYFW/pvY+A3sAzeACZMQoDAWwoD0kQMYtuQvjtU0qntvkSIrZkT/V
9ry5WkXj2n4Eiqzs/YAiR4wm2PHLHDUjrNLsJDJE/r3XCxz5ShkbsT9KKGKd91D8X/kqP+U8+Mb2
B8MmyRm2vcL4JGRpcLXiPNFPuLy27PSguj/qJ+kInxNiUjHd5tIV0NzD5g5OjgVA7LS3GJKSGM2c
JaD8WfIgqfsH8CdyIYVyUJe+FP1bA9uG61GfPSTui/rep5X1Mqky61gsVY5iPwCJ0owFuC6DUf29
ZjO4ooRYxIiJZ09DpI38X1jqzBcDrnmRyTEx6DWnuUQoBgYPl1OH+2LnD/H01x+jbByNVUMgmYVF
fHeVz6/wc9b8msRUTZQUx2W2H2riQn+9iQovPrjFTqzWU8dmXRbXx0+efMIVwtcsVrvX1lblIvOw
Qc/+aL8xk7PstnzKS7ltRzxql4IvbsjWt3K/sIKf15U+1pLFwWjAp7Pp5U45vst1HL9bWuY+yTlQ
1vkBpttLXi1x7R5zY2yxbOoOCKdd6LYOZsxodf0dFnYSD3atxs7QdMWVzmWRqoVOdO+FLgifXx3s
rAXbrOOS1URfCguZLz8cTigRzXhVzIt3atjK+utW/1aERC2DK1pdsv+eZW0I+ev/+qg5LP0L1pHV
TmI7p1mpih+M3uxgKKBGpSukl003dhqYC8bxUt9al1kxD0YUg0jd1Jks+7L+fulDs2RP/NNqgcOK
KyP0HoST1f/7xpeM9+WNCFS3orsaEafT/9ZKVVlin4oj5brQjkbqTR4d6cIZ/2+uXSCU67WvwJwQ
4SsgbKYbL+SrAPRIb5MeFRFsRtkqJa7emR1MNlC+P5kqzXBKq0L9rsD/EnFKL3nS53hnYG/SWba+
g8vcZkqiEhfeUpKRHQQNtXYFPQr9dGdR/TR42UXYFuEM8PXVckc35tP3z6PBx6pXlVgPGXlYDAGZ
bS/vEdC6vOQJ/CSvlISrQd6AsIJQUHNcdzHroLglrcqYLTHhWz3sz8lataHnNiZhSLCuBx5jb5NN
iYwMtRgEp45KUuYLjaUEiq5OqyRpho0cMbRUXl+VsMdOVcoCbT8znLfH8PDwlKt7A3aRPo2lbyjS
S76eHgn+sFjUF2KHlwhhwzpdI/Ddv0H7oAy8uCgO2ZnUMJnpJ2W+AIV8N2riNE7ZoBWegS0jgAmA
stN4cZ1akfeDU+EN95C6p0y5DJEYt/UQ+sCf0uvPg8d2UcpfwaaPkzBsaWwzDHObYXRlKCPKXcQ+
PK8iUTThf/d9pM5KXL3x9c/c/qXhepYAsA68FetzwJRUTnHsFgIOhP8bOZQ7v8xPD64FmQCnq7cM
R8m6Hu7KZuHd1UnkEebNXxuyfPX2A0U0wIFS9hGn9MbbugHpdbcKDUfZkVrz1fKMJklKvsM/202a
pSKi7Qbbe6ym8jvNpz1FBYUWYtrIQuSwXHTXfjb3bOrOyX+ksSYj3Oijj6mMjonty3lvOUK8ugib
2AvJ9xBS+q8abOtZxW0qhL0AgrEIFouRMEYo99Fxi4ol8AxTyNkC14UC8Y82+1B15jeI6iVLHz8g
j9AIDlafkp8Vbl3z/kSs99x7oxUMVCuIdYURV3ymj32tDxQtF48oMKY4qhLW3oTRK+agwztszi4z
BUmTCPJKcm8YmLghyJQHipP0AuOmJnZAcg1Dvv+Q4ybnwW5c98/NvU8A8r6MGSmhDkqGr9VMgEM2
jBClbAJYR7PtoLusxJIDfPFgb/VLgOwpJT+71q2ewjM+zUflFvaD7j2FrT2nCZK+wy2iQMNapQW3
KtGokff/KM1ECBrNsBbZlqB+k4FrKBKEzavKsgVVjVGm9ebxvxgeiAqm/sJq545z6bRnowXQnnlj
+7vGeijaPx8OpG/eq6S9gnOsH5unf60ed9mHuPoDrqAnFYzuMxNAH4gMK+gznccS/qlpGM0xE8ol
vLgj308BgMuOMVh1wrcYfwqVJIpW/Wh7hb3LQAsv3uc8H8iO73F+NSAGbnW0LUuBratglRLJ75fR
PAMXOF8ByV07tFgBA2wAHdNphUodFfQ9RcBC7+ZsHqFymTFrdGwfCwDCl0mMBSZ0GzyRrbecLvif
Qlcz/nIUYTwibTQfVESgLm2WtRs4vwWnISmKRpq9rD/WARVLQVLVLWf3LR0NKl2cHgFyq9JPmZ8e
gbbTxjzeAgRp6fdkFaE339sjzAQ7fbb1j6zdbxO/ZP8V+yM2WieVb/COa4s53CFjfZidz0KfDTlw
Pe4vVBV7WLkdWpADClnY31nQC+J2YS5gBJfiLzIz2HoQo4AxsfiXsM7soxXfIlS5lnXuarLtNX5G
YhVZig+g2GLP3dekqJ+qVTo9HfbvPuOV/ZqYmfgdrnNlrIc/BwmzOi5pfbycTMWkYF9TFXhB9TyT
RYlVozLp7+CfenEEaXnvgdM1hAopjoTwCcABOxJI7wBGBCVWYHV4F6Loc2bjR6P6SENYoaQg4wUy
kxoqTpuTxDVw50estaEu+TQybsiEet2tX2HcoIX64rR/3qYDQWN5+v0xqwrSXhfyfiF7txnfEyDm
4Ma4I+k1fwqsH6P/HHtxSuDnG3G/KwQCvY9P4UrdTRtoDh6Z8VCTA8gsmBI8L4WLkPQ0WbemMUhc
dm9MzFHJVNIrURmVGu8V5Bg3k3cRimIJNjDQErDeAMdMJrEE8VXda2AUIsC5z01gQi6NEnwQ5ePi
G9nz2Ss0qw==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
